# Database Migration

This repository contains current and historical tooling to automate various PostgreSQL life cycle tasks.


## PostgreSQL Major Upgrades

### Zero Downtime PostgreSQL Upgrades

- [pg-upgrade-logical](pg-upgrade-logical/) hosts the automation to upgrade our PostgreSQL cluster with minimal downtime, which was developed in collaboration with [postgres.ai](https://postgres.ai/) Find more details in [pg-upgrade-logical/README.md](pg-upgrade-logical/README.md).
- [pg14_upgrade.md](.gitlab/issue_templates/pg14_upgrade.md) is the template to create change requests to orchestrate upgrades in our CR system.



## References

* Patroni in Omnibus - https://gitlab.com/gitlab-org/omnibus-gitlab/issues/3752
* Patroni / Console Setup - https://docs.google.com/document/d/1NTqnRzT-elPEmPyq_sNC80sULFsCUXX7F10EVOFyZK0/edit
