---
- name: "(TARGET) Start PostgreSQL on Standby Cluster Leader to reach recovery_target_lsn (duration: ~30-120 sec)"
  command: >-
    {{ pg_old_bindir }}/pg_ctl start -D {{ pg_old_datadir }} -w -t {{ pg_ctl_timeout }}
    -o '--config-file={{ pg_old_confdir }}/postgresql.conf'
    -o '-c archive_mode=off'
    -l /tmp/pg_recovery_target_lsn_{{ ansible_date_time.date }}.log
  async: "{{ pg_ctl_timeout }}"  # run the command asynchronously
  poll: 0
  register: pg_ctl_start_result
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

- name: Wait for the PostgreSQL start command to complete
  async_status:
    jid: "{{ pg_ctl_start_result.ansible_job_id }}"
  register: pg_ctl_start_job_result
  until: pg_ctl_start_job_result.finished
  retries: "{{ (pg_ctl_timeout | int) // 10 }}"
  delay: 10
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

- name: "(TARGET) Wait until the recovery is complete (duration: ~10-60 sec)"
  command: gitlab-psql -tAXc 'select pg_is_in_recovery()'
  register: pg_is_in_recovery
  until: pg_is_in_recovery.stdout == "f"
  retries: 360
  delay: 10
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

- name: (TARGET) Get the current PostgreSQL log file
  shell: |
    set -o pipefail;
    ls -t {{ pg_log_dir }} | grep .{{ pg_log_format }} | head -n 1
  args:
    executable: /bin/bash
  register: pg_log_filename
  changed_when: false
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

- name: (TARGET) Check the PostgreSQL log file
  shell: >
    set -o pipefail;
    grep -E "recovery stopping after WAL location (LSN)|{{
    (hostvars[groups['source_primary'][0]]['pg_slot_lsn']['stdout']
    if pg_publication_count | int == 1
    else
    hostvars[groups['source_primary'][0]]['multiple_pg_slot_lsn']['results'][-1]['stdout']) }}" \
    {{ pg_log_dir }}/{{ pg_log_filename.stdout }}
  args:
    executable: /bin/bash
  register: pg_log_result
  changed_when: false
  failed_when: false
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

# Stop, if target LSN not reached
- name: Result of checking the PostgreSQL log
  fail:
    msg: >-
      Recovery target (LSN "{{
      (hostvars[groups['source_primary'][0]]['pg_slot_lsn']['stdout']
      if pg_publication_count | int == 1
      else
      hostvars[groups['source_primary'][0]]['multiple_pg_slot_lsn']['results'][-1]['stdout']) }}")
      has not been reaching
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader
    - pg_log_result.stdout | length < 1

- name: Result of checking the PostgreSQL log
  debug:
    msg: '{{ pg_log_result.stdout }}'
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader
    - pg_log_result.stdout | length > 0

# Resume and check physical replication
- name: "(TARGET) Resume WAL replay (recovery) on Standby Cluster Replicas"
  command: gitlab-psql -tAXc "select pg_wal_replay_resume()"
  when:
    - inventory_hostname in groups['target_cluster']
    - not patroni_cluster_leader

- name: "(TARGET) Wait until physical replication becomes active"
  command: >-
    gitlab-psql -tAXc "select count(*)
    from pg_stat_wal_receiver where status = 'streaming'"
  register: pg_stat_wal_receiver_state
  until: pg_stat_wal_receiver_state.stdout|int > 0
  retries: 60  # max duration 2 minutes
  delay: 2
  changed_when: false
  ignore_errors: true
  when:
    - inventory_hostname in groups['target_cluster']
    - not patroni_cluster_leader

- name: "(TARGET) Wait until physical replication lag is 0 bytes"
  command: >-
    gitlab-psql -tAXc "select pg_wal_lsn_diff(pg_current_wal_lsn(),
    replay_lsn) pg_lag_bytes from pg_stat_replication
    order by pg_lag_bytes desc limit 1"
  register: pg_lag_bytes
  until: pg_lag_bytes.stdout|int == 0
  retries: 60  # max duration 2 minutes
  delay: 2
  changed_when: false
  ignore_errors: true
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

...
