---

- name: '[Pre-Check] (ALL) Make sure that the consul service files exist'
  become: true
  become_user: root
  stat:
    path: "{{ consul_cfg_path }}/{{ item }}"
  loop: "{{ pgbouncer_consul_service_file[cluster_environment][cluster_suffix] + patroni_consul_service_file[cluster_environment][cluster_suffix] }}"
  loop_control:
    label: "{{ consul_cfg_path }}/{{ item }}"
  register: consul_files_stat

# Stop, if the list of consul service files does not match
- name: "Pre-Check error. The list of consul service files does not match"
  debug:
    msg: "File {{ service_item.invocation.module_args.path }} does not exist on server {{ ansible_hostname }}"
  when: not service_item.stat.exists
  failed_when: not service_item.stat.exists
  loop: "{{ consul_files_stat.results }}"
  loop_control:
    loop_var: service_item
    label: "{{ service_item.stat.exists }}"

- name: "Pre-Check: (ALL) Make sure that the consul service names match with 'patroni_consul_service_name'"
  become: true
  become_user: root
  replace:
    path: "{{ consul_cfg_path }}/{{ item }}"
    regexp: '"name": ".*?"'
    replace: '"name": "{{ patroni_consul_service_name[cluster_environment][cluster_suffix] }}"'
  check_mode: true
  loop: "{{ patroni_consul_service_file[cluster_environment][cluster_suffix] }}"
  loop_control:
    label: "file: {{ item }}, name: {{ patroni_consul_service_name[cluster_environment][cluster_suffix] }}"
  register: patroni_consul_service_name_check

# Stop, if consul service names do not match with 'patroni_consul_service_name'
- name: "Pre-Check error. The consul service names do not match for 'patroni_consul_service_name'"
  debug:
    msg:
      - "Consul service name does not match the expected value '{{ patroni_consul_service_name[cluster_environment][cluster_suffix] }}'"
      - "in file {{ consul_cfg_path }}/{{ service_item.item }} on server {{ ansible_hostname }}"
  when: service_item.changed
  failed_when: service_item.changed
  loop: "{{ patroni_consul_service_name_check.results }}"
  loop_control:
    loop_var: service_item
    label: "{{ service_item.changed }}"

- name: "Pre-Check: (ALL) Make sure that the consul service names match with 'pgbouncer_consul_service_name'"
  become: true
  become_user: root
  replace:
    path: "{{ consul_cfg_path }}/{{ item }}"
    regexp: '"name": ".*?"'
    replace: '"name": "{{ pgbouncer_consul_service_name[cluster_environment][cluster_suffix] }}"'
  check_mode: true
  loop: "{{ pgbouncer_consul_service_file[cluster_environment][cluster_suffix] }}"
  loop_control:
    label: "file: {{ item }}, name: {{ pgbouncer_consul_service_name[cluster_environment][cluster_suffix] }}"
  register: pgbouncer_consul_service_name_check

# Stop, if consul service names do not match with 'pgbouncer_consul_service_name'
- name: "Pre-Check error. The consul service names do not match for 'pgbouncer_consul_service_name'"
  debug:
    msg:
      - "Consul service name does not match the expected value '{{ pgbouncer_consul_service_name[cluster_environment][cluster_suffix] }}'"
      - "in file {{ consul_cfg_path }}/{{ service_item.item }} on server {{ ansible_hostname }}"
  when: service_item.changed
  failed_when: service_item.changed
  loop: "{{ pgbouncer_consul_service_name_check.results }}"
  loop_control:
    loop_var: service_item
    label: "{{ service_item.changed }}"

- name: '[Pre-Check] (SOURCE) fetch source cluster info'
  uri:
    url: "http://{{ inventory_hostname }}:{{ patroni_restapi_port }}/cluster"
    method: GET
    return_content: true
    headers:
      Content-Type: "application/json"
  register: patroni_source_cluster_info
  changed_when: false
  when:
    - inventory_hostname == groups['source_cluster'][0]

- name: '[Pre-Check] (SOURCE) Extract hosts from source cluster info'
  set_fact:
    source_hostnames: "{{ patroni_source_cluster_info.json | json_query('members[*].name') }}"
  when:
    - inventory_hostname == groups['source_cluster'][0]

- name: '[Pre-Check] (SOURCE) check all cluster hosts are defined in the inventory'
  assert:
    that:
      - source_hostnames | difference(groups['source_cluster']) == []
      - groups['source_cluster'] | difference(source_hostnames) == []
    fail_msg:
      - "There are missing hosts in the inventory"
      - "Inventory hosts that are not in the cluster: {{ groups['source_cluster'] | difference(source_hostnames) }}"
      - "Cluster hosts that are not in the inventory: {{ source_hostnames | difference(groups['source_cluster']) }}"
    success_msg: "All source hosts are defined in the inventory"
  when:
    - inventory_hostname == groups['source_cluster'][0]

- name: '[Pre-Check] (TARGET) fetch target cluster info'
  uri:
    url: "http://{{ inventory_hostname }}:{{ patroni_restapi_port }}/cluster"
    method: GET
    return_content: true
    headers:
      Content-Type: "application/json"
  register: patroni_target_cluster_info
  changed_when: false
  when:
    - inventory_hostname == groups['target_cluster'][0]

- name: '[Pre-Check] (TARGET) Extract hosts from target cluster info'
  set_fact:
    target_hostnames: "{{ patroni_target_cluster_info.json | json_query('members[*].name') }}"
  when:
    - inventory_hostname == groups['target_cluster'][0]

- name: '[Pre-Check] (TARGET) check all cluster hosts are defined in the inventory'
  assert:
    that:
      - target_hostnames | difference(groups['target_cluster']) == []
      - groups['target_cluster'] | difference(target_hostnames) == []
    fail_msg:
      - "There are missing hosts in the inventory"
      - "Inventory hosts that are not in the cluster: {{ groups['target_cluster'] | difference(target_hostnames) }}"
      - "Cluster hosts that are not in the inventory: {{ target_hostnames | difference(groups['target_cluster']) }}"
    success_msg: "All target hosts are defined in the inventory"
  when:
    - inventory_hostname == groups['target_cluster'][0]

- name: '[Pre-Check] (TARGET) Make sure that is Standby leader'
  uri:
    url: http://{{ inventory_hostname }}:{{ patroni_restapi_port }}/standby-leader
    status_code: 200
  register: patroni_standby_leader_result
  changed_when: false
  failed_when: patroni_standby_leader_result.status != 200
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

- name: '[Pre-Check] (ALL) Test PostgreSQL DB Access'
  command: gitlab-psql -tAXc 'select 1'
  changed_when: false

- name: '[Pre-Check] (TARGET) Check the current version of PostgreSQL'
  command: gitlab-psql -tAXc "select setting::integer/10000 from pg_settings where name = 'server_version_num'"
  register: server_version_num
  changed_when: false
  when:
    - inventory_hostname in groups['target_cluster']

# Stop, if the current version does not match pg_old_version
- name: "Pre-Check error. An incorrect version of PostgreSQL may have been specified"
  fail:
    msg:
      - "The current version of PostgreSQL is {{ server_version_num.stdout }}"
      - "Make sure that you have specified the correct version in the pg_old_version variable."
  when:
    - inventory_hostname in groups['target_cluster']
    - server_version_num.stdout is defined
    - patroni_cluster_leader
    - server_version_num.stdout|int != pg_old_version|int

# Stop, if the current version greater than or equal to pg_new_version
- name: "Pre-Check error. An incorrect target version of PostgreSQL may have been specified"
  fail:
    msg:
      - "The current version of PostgreSQL is {{ server_version_num.stdout }}, no upgrade is needed."
      - "Or, make sure that you have specified the correct version in the pg_new_version variable."
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader
    - server_version_num.stdout is defined
    - server_version_num.stdout|int >= pg_new_version|int

# This check is necessary to avoid the risk of deleting the current data directory
# the current directory must not be equal to the path specified in the pg_new_datadir variable
# which will later be cleaned up before executing initdb for a new version of PostgreSQL
- name: '[Pre-Check] (TARGET) Ensure new data directory is different from the current one'
  command: gitlab-psql -tAXc "show data_directory"
  changed_when: false
  register: pg_current_datadir
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

# Stop, if the current data directory is the same as pg_new_datadir
- name: "Pre-Check error. The current data directory is the same as new data directory"
  fail:
    msg:
      - "The new data directory ({{ pg_new_datadir }}) must be different from the current one ({{ pg_current_datadir.stdout }})"
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader
    - pg_current_datadir.stdout == pg_new_datadir

- name: "[Pre-Check] (TARGET) None of target nodes have 'archive_mode=always'"
  command: gitlab-psql -tAXc "show archive_mode"
  changed_when: false
  register: pg_archive_mode_result
  when:
    - inventory_hostname in groups['target_cluster']

# Stop, if 'archive_mode' is 'always'
- name: "Pre-Check error. 'archive_mode = always' is present"
  fail:
    msg:
      - "At least one of target cluster's nodes has 'archive_mode = always'."
  when:
    - inventory_hostname in groups['target_cluster']
    - pg_archive_mode_result.stdout == "always"

- name: '[Pre-Check] (SOURCE) Check the current version of pg_repack (if installed)'
  command: >-
    gitlab-psql -d {{ pg_source_dbname }} -tAXc "select
    extversion from pg_catalog.pg_extension
    where extname = 'pg_repack'"
  register: pg_repack_version
  changed_when: false
  when:
    - inventory_hostname in groups['source_cluster']
    - patroni_cluster_leader

# Stop, if the old version of pg_repack is installed
- name: 'Pre-Check error. Old version of the pg_repack extension'
  fail:
    msg: "pg_repack version is {{ pg_repack_version.stdout }}. Please update the extension before proceeding with the upgrade."
  when:
    - inventory_hostname in groups['source_cluster']
    - patroni_cluster_leader
    - pg_repack_version.stdout | length > 0
    - pg_repack_version.stdout is version(min_pg_repack_version, '<')

- name: '[Pre-Check] (SOURCE) Drop table "test_replication" if exists'
  command: >-
    gitlab-psql -d {{ pg_source_dbname }} -tAXc
      "DROP TABLE IF EXISTS test_replication"
  register: pg_replica_identity_nothing
  changed_when: false
  when:
    - inventory_hostname in groups['source_cluster']
    - patroni_cluster_leader

- name: '[Pre-Check] (SOURCE) Make sure there are no tables with replica identity "nothing"'
  command: >-
    gitlab-psql -d {{ pg_source_dbname }} -tAXc "select
    format('%I.%I', n.nspname, c.relname)
    from pg_class c
    join pg_namespace n on (n.oid = c.relnamespace)
    where nspname not in ('pg_catalog', 'information_schema')
    and relkind='r' and relreplident='n'"
  register: pg_replica_identity_nothing
  changed_when: false
  when:
    - inventory_hostname in groups['source_cluster']
    - patroni_cluster_leader

- block:  # Stop, if replica identity "nothing" is present
    - name: 'Print tables with replica identity "nothing"'
      debug:
        msg: "{{ pg_replica_identity_nothing.stdout_lines }}"

    - name: "Pre-Check error. Print error message"
      fail:
        msg: Tables with replica identity "nothing" were found.
  when:
    - inventory_hostname in groups['source_cluster']
    - patroni_cluster_leader
    - pg_replica_identity_nothing.stdout | length > 0

- name: '[Pre-Check] (SOURCE) Make sure that tables with replica identity “default” have primary key'
  command: >-
    gitlab-psql -d {{ pg_source_dbname }} -tAXc "select
    format('%I.%I', n.nspname, c.relname)
    from pg_class c
    join pg_namespace n on (n.oid = c.relnamespace)
    where nspname not in ('pg_catalog', 'information_schema')
    and relkind='r' and relreplident='d'
    and c.oid not in (select c.oid from pg_class c
    join pg_index i on i.indrelid = c.oid
    where c.relkind='r' and i.indisprimary = true)"
  register: pg_replica_identity_default
  changed_when: false
  when:
    - inventory_hostname in groups['source_cluster']
    - patroni_cluster_leader

- block:  # Stop, if replica identity "default" without PK is present
    - name: 'Print tables without primary key'
      debug:
        msg: "{{ pg_replica_identity_default.stdout_lines }}"

    - name: "Pre-Check error. Print error message"
      fail:
        msg: Tables with replica identity "default" and without primary key were found.
  when:
    - inventory_hostname in groups['source_cluster']
    - patroni_cluster_leader
    - pg_replica_identity_default.stdout | length > 0

- name: '[Pre-Check] (ALL) Make sure that the password file "{{ ansible_user_dir }}/.pgpass" exists'
  stat:
    path: "{{ ansible_user_dir }}/.pgpass"
  register: pgpass_file

# Stop, if .pgpass is not exists
- name: 'Result of checking the password file'
  fail:
    msg: "Whoops! the password file \"{{ ansible_user_dir }}/.pgpass\" is not exists"
  when: not pgpass_file.stat.exists

- name: '[Pre-Check] (SOURCE) Make sure that the user "{{ pg_source_replica_user }}" is specified in the password file'
  command: grep -c ":{{ pg_source_replica_user }}:" "{{ ansible_user_dir }}/.pgpass"
  failed_when: false
  changed_when: false
  register: replica_user_presence
  when:
    - inventory_hostname in groups['source_cluster']
    - patroni_cluster_leader

# if 'enable_reverse_logical_replication' is 'true'
- name: '[Pre-Check] (TARGET) Make sure that the user "{{ pg_target_replica_user }}" is specified in the password file'
  command: grep -c ":{{ pg_target_replica_user }}:" "{{ ansible_user_dir }}/.pgpass"
  failed_when: false
  changed_when: false
  register: replica_user_presence
  when:
    - enable_reverse_logical_replication | bool
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

# Stop, if the replica user is not present
- name: 'Result of checking the user presence in the password file'
  fail:
    msg:
      "Whoops! the user {{ pg_source_replica_user }} is not present
       in {{ ansible_user_dir }}/.pgpass on the Source"
  when:
    - replica_user_presence.stdout is defined
    - replica_user_presence.stdout == "0"

- name: '[Pre-Check] (SOURCE) Get the password for the user "{{ pg_source_replica_user }}"'
  shell: |
    set -o pipefail;
    grep -m1 ":{{ pg_source_replica_user }}:" "{{ ansible_user_dir }}/.pgpass" | cut -d ":" -f5
  args:
    executable: /bin/bash
  changed_when: false
  register: replica_user_pass
  when:
    - inventory_hostname in groups['source_cluster']
    - patroni_cluster_leader

- name: '[Pre-Check] (TARGET) Configure the password file for the user "{{ pg_source_replica_user }}"'
  lineinfile:
    path: "{{ ansible_user_dir }}/.pgpass"
    regexp: "^[*]:5432:{{ pg_source_dbname }}:{{ pg_source_replica_user }}:{{ hostvars[groups['source_primary'][0]]['replica_user_pass']['stdout'] }}"
    line: "*:5432:{{ pg_source_dbname }}:{{ pg_source_replica_user }}:{{ hostvars[groups['source_primary'][0]]['replica_user_pass']['stdout'] }}"
  when:
    - inventory_hostname in groups['target_cluster']

- name: '[Pre-Check] (TARGET) Test the access for the user "{{ pg_source_replica_user }}" to the source database "{{ pg_source_dbname }}"'
  command: >-
    {{ pg_old_bindir }}/psql -w -h {{ hostvars[groups['source_primary'][0]]['inventory_hostname'] }}
    -p {{ pg_source_port }}
    -U {{ pg_source_replica_user }}
    -d {{ pg_source_dbname }}
    -tAXc 'select 1'
  changed_when: false
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

# if 'enable_reverse_logical_replication' is 'true'
- block:
    - name: "[Pre-Check] (SOURCE) Stop Chef client on the Source cluster to prevent unexpected changes"
      become: true
      become_user: root
      command: chef-client-disable "Upgrade PostgreSQL {{ pg_old_version }} to version {{ pg_new_version }}"
      when:
        - inventory_hostname in groups['source_cluster']

    - name: '[Pre-Check] (TARGET) Get the password for the user "{{ pg_target_replica_user }}"'
      shell: |
        set -o pipefail;
        grep -m1 ":{{ pg_target_replica_user }}:" "{{ ansible_user_dir }}/.pgpass" | cut -d ":" -f5
      args:
        executable: /bin/bash
      changed_when: false
      register: replica_user_pass
      when:
        - inventory_hostname in groups['target_cluster']
        - patroni_cluster_leader

    - name: '[Pre-Check] (SOURCE) Configure the password file for the user "{{ pg_target_replica_user }}"'
      lineinfile:
        path: "{{ ansible_user_dir }}/.pgpass"
        regexp: "^[*]:5432:{{ pg_target_dbname }}:{{ pg_target_replica_user }}:{{ hostvars[groups['target_primary'][0]]['replica_user_pass']['stdout'] }}"
        line: "*:5432:{{ pg_target_dbname }}:{{ pg_target_replica_user }}:{{ hostvars[groups['target_primary'][0]]['replica_user_pass']['stdout'] }}"
      when:
        - inventory_hostname in groups['source_cluster']

    - name: '[Pre-Check] (SOURCE) Test the access for the user "{{ pg_target_replica_user }}" to the target database "{{ pg_target_dbname }}"'
      command: >-
        {{ pg_old_bindir }}/psql -w -h {{ hostvars[groups['target_primary'][0]]['inventory_hostname'] }}
        -p {{ pg_target_port }}
        -U {{ pg_target_replica_user }}
        -d {{ pg_target_dbname }}
        -tAXc 'select 1'
      changed_when: false
      when:
        - inventory_hostname in groups['source_cluster']
        - patroni_cluster_leader
  when:
    - enable_reverse_logical_replication | bool

- name: '[Pre-Check] (SOURCE/TARGET) Make sure that physical replication is active'
  command: >-
    gitlab-psql -tAXc "select count(*) from pg_stat_replication
    where application_name != 'pg_basebackup'"
  register: pg_replication_state
  changed_when: false
  when:
    - patroni_cluster_leader

# Stop, if there are no active replicas
- name: "Pre-Check error. Print physical replication state"
  fail:
    msg: "There are no active replica servers (pg_stat_replication returned 0 entries)."
  when:
    - patroni_cluster_leader
    - pg_replication_state.stdout | int == 0

- name: '[Pre-Check] (SOURCE/TARGET) Make sure there is no high physical replication lag (more than {{ max_replication_lag_bytes | human_readable }})'
  command: >-
    gitlab-psql -tAXc "select pg_wal_lsn_diff(pg_current_wal_lsn(),
    replay_lsn) pg_lag_bytes from pg_stat_replication
    order by pg_lag_bytes desc limit 1"
  register: pg_lag_bytes
  changed_when: false
  failed_when: false
  until: pg_lag_bytes.stdout|int < max_replication_lag_bytes|int
  retries: 30
  delay: 5
  when:
    - patroni_cluster_leader

# Stop, if physical replication lag is high
- block:
    - name: "Print replication lag"
      debug:
        msg: "Current replication lag:
          {{ pg_lag_bytes.stdout | int | human_readable }}"

    - name: "Pre-Check error. Please try again later"
      fail:
        msg: High replication lag on the Target Cluster, please try again later.
  when:
    - patroni_cluster_leader
    - pg_lag_bytes.stdout is defined
    - pg_lag_bytes.stdout|int >= max_replication_lag_bytes|int

# # Rsync Checks
- name: '[Pre-Check] Make sure that the rsync package are installed'
  become: true
  become_user: root
  package:
    name: rsync
    state: present
  when: inventory_hostname in groups['target_cluster']

- name: '[Pre-Check] (TARGET) Rsync Checks: create testrsync file on Primary'
  become: false
  file:
    path: /tmp/testrsync
    state: touch
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

- name: '[Pre-Check] (TARGET) Rsync Checks: test rsync and ssh key access'
  become: false
  shell: >
    rsync -e "ssh -o StrictHostKeyChecking=no" --archive --delete --hard-links --size-only --no-inc-recursive --omit-dir-times
    /tmp/testrsync {{ item }}:/tmp/
  args:
    executable: /bin/bash
  loop: "{{ groups.target_secondary | list }}"
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

- name: "(TARGET) Cleanup testrsync file"
  become: false
  file:
    path: /tmp/testrsync
    state: absent
  when:
    - inventory_hostname in groups['target_cluster']

# Stop Chef client to prevent unexpected changes during the process
- name: "[Pre-Check] (TARGET) Make sure that the Chef client is stopped"
  become: true
  become_user: root
  command: chef-client-disable "Upgrade PostgreSQL {{ pg_old_version }} to version {{ pg_new_version }}"
  when:
    - inventory_hostname in groups['target_cluster']

# Check restore_command
- name: "[Pre-Check] (TARGET) Make sure that the 'restore_command' parameter is not specified"
  command: gitlab-psql -tAXc "show restore_command"
  changed_when: false
  register: pg_restore_command_result
  when:
    - inventory_hostname in groups['target_cluster']

# if 'restore_command' is specified
# The purpose of this task is to comment out the recovery_conf block in the patroni.yml configuration file
# in order to avoid conflicts with WAL files in the archive. Such conflicts may arise if WAL files generated by
# previous tests are present in the archive, causing restore_command to attempt to restore a WAL belonging to
# another cluster, leading to a recovery error. Additionally, if the archive directory is empty,
# restore_command will endlessly wait for the WAL file to appear in the repository.
# By commenting out the block, we can prevent these conflicts while still preserving the original configuration for future use.
# It is expected that when the Chef client is started after the upgrade, the parameters will be returned to their original state.
- block:
    - name: "(TARGET) patroni.yml | check if the 'recovery_conf' is specified"
      command: >-
        awk '/^[^#]*recovery_conf:/ {print; while (getline > 0 && /^[[:blank:]]/ || /^#[[:blank:]]*\S+/)
        {gsub(/#.*/, ""); if (length($0) > 0) print}}' "{{ patroni_config_file }}"
      register: recovery_conf_output
      changed_when: false

    - name: "(TARGET) patroni.yml | comment out 'recovery_conf' parameters"
      replace:
        path: "{{ patroni_config_file }}"
        regexp: '^( *)(recovery_conf:|{{ item }})'
        replace: '# \1\2'
      loop: "{{ recovery_conf_output.stdout_lines }}"
      when: recovery_conf_output.stdout_lines | length > 0

    - name: "Execute CHECKPOINT before restarting PostgreSQL"
      command: gitlab-psql -tAXc "CHECKPOINT"
      async: 3600  # run the command asynchronously
      poll: 0
      register: checkpoint_result

    - name: Wait for the CHECKPOINT to complete
      async_status:
        jid: "{{ checkpoint_result.ansible_job_id }}"
      register: checkpoint_job_result
      until: checkpoint_job_result.finished
      retries: 360
      delay: 10

    - name: "(TARGET) Stop Patroni service on Cluster Replicas"
      become: true
      become_user: root
      service:
        name: patroni
        state: stopped
      when: not patroni_cluster_leader

    - name: "(TARGET) Stop Patroni service on Cluster Leader"
      become: true
      become_user: root
      service:
        name: patroni
        state: stopped
      when: patroni_cluster_leader

    - name: "(TARGET) Start Patroni service"
      include_tasks: start_services.yml

    - name: "(TARGET) Make sure that 'restore_command' is disabled"
      command: gitlab-psql -tAXc "show restore_command"
      changed_when: false
      register: pg_restore_command_recheck

    # Stop, if 'restore_command' is defined
    - name: "Pre-Check error. 'restore_command' is present"
      fail:
        msg:
          - "Could not disable 'restore_command' parameter on the target cluster nodes."
          - "Please disable this option before proceeding."
      when: pg_restore_command_recheck.stdout | length > 0
  when:
    - inventory_hostname in groups['target_cluster']
    - pg_restore_command_result.stdout | length > 0

# WAL-G version (if min_walg_version is defined)
- block:
    - name: '[Pre-Check] Check the WAL-G version'
      ansible.builtin.shell: |
        set -o pipefail;
        /opt/wal-g/bin/wal-g --version | awk {'print $3'} | tr -d 'v'
      args:
        executable: /bin/bash
      changed_when: false
      register: wal_g_installed_version

    # Stop, if an incompatible version of wal-g is installed
    - name: 'Pre-Check error. Incompatible version of the WAL-G installed'
      fail:
        msg: "WAL-G version is {{ wal_g_installed_version.stdout }}. Please update to version {{ min_walg_version }} or later."
      when:
        - wal_g_installed_version.stdout | length > 0
        - wal_g_installed_version.stdout is version(min_walg_version, '<')
  when:
    - inventory_hostname in groups['target_cluster']
    - min_walg_version | default('') | length > 0

# WALG_GS_PREFIX / WAL-G backups
- block:
    - name: '[Pre-Check] (SOURCE/TARGET) Check the current value of WALG_GS_PREFIX'
      command: "cat /etc/wal-g.d/env/WALG_GS_PREFIX"
      register: walg_gs_prefix
      changed_when: false
      when: inventory_hostname in groups['source_cluster'] or inventory_hostname in groups['target_cluster']

    # Stop, if WALG_GS_PREFIX is the same
    - name: "Pre-Check error. The WALG_GS_PREFIX value is the same"
      fail:
        msg:
          - "WALG_GS_PREFIX value is the same on source and target clusters."
          - "Ensure that WALG_GS_PREFIX is different to prevent deleting backups of the source cluster."
      when:
        - inventory_hostname in groups['target_cluster']
        - walg_gs_prefix.stdout == hostvars[groups['source_primary'][0]]['walg_gs_prefix'].stdout

    # else, Clear WAL-G backups and WALs in the GCS bucket
    - name: "[Pre-Check] (TARGET) Make sure that there are no backups in the bucket"
      command: "{{ clear_backups_command }}"
      async: "{{ clear_backups_command_timeout }}"  # run the command asynchronously with a maximum duration 1 hour
      poll: 0
      register: clear_backups
      when:
        - inventory_hostname in groups['target_cluster']
        - clear_backups_before_upgrade | bool

    - name: "[Pre-Check](TARGET) Wait for the backup cleanup command to complete"
      async_status:
        jid: "{{ clear_backups.ansible_job_id }}"
      register: clear_backups_job_result
      until: clear_backups_job_result.finished
      retries: "{{ (clear_backups_command_timeout | int) // 10 }}"  # max wait time 1 hour
      delay: 10
      when:
        - inventory_hostname in groups['target_cluster']
        - clear_backups_before_upgrade | bool
  become: true
  become_user: "{{ os_pg_user }}"
  when:
    - patroni_cluster_leader

...
