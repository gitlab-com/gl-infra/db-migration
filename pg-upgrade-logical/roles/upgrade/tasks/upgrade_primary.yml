# Upgrade with pg_upgrade (hard-links)
---
# Add a temporary local access rule for pg_upgrade to allow the upgrade process to proceed without authentication issues.
# This rule will be added to both the old and new PostgreSQL configuration files (pg_hba.conf).
# The rule grants the specified PostgreSQL user ({{ pg_user }}) trust-based access to all databases locally.
# This is necessary to ensure a smooth upgrade process and will be removed after the upgrade is complete.
- name: Add temporary local access rule for pg_upgrade
  blockinfile:
    path: "{{ item }}"
    marker: "# {mark} ANSIBLE TEMPORARY pg_upgrade RULE"
    insertbefore: BOF
    content: "local all {{ pg_install_user.stdout }} trust"
  loop:
    - "{{ pg_old_confdir }}/pg_hba.conf"
    - "{{ pg_new_confdir }}/pg_hba.conf"
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

# Perform pg_upgrade
- name: "Upgrade the PostgreSQL to version {{ pg_new_version }} on the Primary using pg_upgrade --link"
  command: >
    {{ pg_new_bindir }}/pg_upgrade
    --username={{ pg_install_user.stdout }}
    --old-bindir {{ pg_old_bindir }}
    --new-bindir {{ pg_new_bindir }}
    --old-datadir {{ pg_old_datadir }}
    --new-datadir {{ pg_new_datadir }}
    --old-options "-c config_file={{ pg_old_confdir }}/postgresql.conf"
    --new-options "-c config_file={{ pg_new_confdir }}/postgresql.conf -c shared_preload_libraries='{{ pg_shared_preload_libraries_value }}'"
    --jobs={{ ansible_processor_vcpus }}
    --link
  register: pg_upgrade_result
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

# Return the pg_hba.conf file to its original state
- name: Remove temporary local access rule for pg_upgrade
  blockinfile:
    path: "{{ item }}"
    marker: "# {mark} ANSIBLE TEMPORARY pg_upgrade RULE"
    state: absent
  loop:
    - "{{ pg_old_confdir }}/pg_hba.conf"
    - "{{ pg_new_confdir }}/pg_hba.conf"
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

# If the length of the pg_upgrade_result.stdout_lines is greater than 100 lines,
# the upgrade_output variable will include the first 70 lines, an ellipsis (...),
# and the last 30 lines of the pg_upgrade_result.stdout_lines.
- name: Print the result of the pg_upgrade
  debug:
    msg:
      - "{{ pg_upgrade_result.stdout_lines[:70] }}"
      - "    ...    "
      - "{{ pg_upgrade_result.stdout_lines[-30:] }}"
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader
    - pg_upgrade_result.stdout_lines | length > 100

# Otherwise, it will include all lines of the pg_upgrade_result.stdout_lines.
- name: Print the result of the pg_upgrade
  debug:
    msg:
      - "{{ pg_upgrade_result.stdout_lines }}"
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader
    - pg_upgrade_result.stdout_lines | length <= 100

...
