# UPGRADE POSTGRESQL VERSION (logical replication mode)

This playbook creates logical replication using a Patroni Standby Cluster created using physical replication. \
This method will avoid using pg_dump and copying all data from the main cluster, instead, we can prepare a standby cluster from a backup (or using a disk snapshot - GCP), then synchronize the data (delta) using physical replication.

We perform pg_upgrade on the standby cluster and convert physical replication to a logical replication.

When the new cluster is fully ready (the standby cluster will receive all changes from the main cluster), we switch traffic to the new cluster without long downtime.

## List of playbooks:

- `upgrade.yml` - This playbook is designed to upgrade PostgreSQL to a new major version. It is possible to upgrade to any available new version (12->13, or 12->14, 14->16, etc).
- `switchover_replica.yml` - This playbook is designed to switchover the R/O traffic to a new Patroni Cluster.
- `switchover_leader.yml` - This playbook is designed to switchover the R/W traffic to a new Patroni Cluster.
- `switchover_rollback.yml` - This playbook is used for the rollback scenario. It will return traffic back to the old cluster.
- `stop_reverse_replication.yml` - This playbook is used to stop reverse logical replication (after a decision is made that a rollback to the old cluster is no longer necessary).

## Requirements

1. Prepare the Patroni Standby Cluster from a backup or disk snapshot (not part of this playbook).
2. Make sure your ssh-agent has your identity loaded (`ssh-add -L`), since we use agent forwarding by default (configured in `ansible.cfg`), to upgrade replicas using `rsync`.

## Before upgrade

1. Check the available variables in the `inventory/group_vars` directory. Ensure that the following files are present:

    -  inventory/group_vars/[all.yml](inventory/group_vars/all.yml)
    -  inventory/group_vars/[source_cluster.yml](inventory/group_vars/source_cluster.yml)
    -  inventory/group_vars/[target_cluster.yml](inventory/group_vars/target_cluster.yml)

2. Make sure that the variables in these files correspond to the cluster you are updating. For example, verify the name of the database, names of Consul services, paths to the configuration files, and other relevant details.

3. If necessary, modify the values of the variables to accurately reflect the cluster you are upgrading.


## How to run

Firstly you need a proper inventory file to run Ansible on the correct hosts.
To achieve this goal you must edit the `inventory/hosts.yml` file.
The actual file is composed in `yaml` format, you can see details on [Ansible docs](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

e.g: `gstg-main.yml`

<details><summary>Click here to expand...</summary><p>

```yaml
all:
  vars:
    cluster_environment: gstg
    cluster_suffix: main
  children:
    source_cluster:
      hosts:
        patroni-main-2004-01-db-gstg.c.gitlab-staging-1.internal:
        patroni-main-2004-02-db-gstg.c.gitlab-staging-1.internal:
        patroni-main-2004-03-db-gstg.c.gitlab-staging-1.internal:
        patroni-main-2004-06-db-gstg.c.gitlab-staging-1.internal:
        patroni-main-2004-07-db-gstg.c.gitlab-staging-1.internal:
    target_cluster:
      hosts:
        patroni-main-pg13-2004-01-db-gstg.c.gitlab-staging-1.internal:
        patroni-main-pg13-2004-02-db-gstg.c.gitlab-staging-1.internal:
        patroni-main-pg13-2004-03-db-gstg.c.gitlab-staging-1.internal:
        patroni-main-pg13-2004-06-db-gstg.c.gitlab-staging-1.internal:
        patroni-main-pg13-2004-07-db-gstg.c.gitlab-staging-1.internal:
    pgbouncer:
      hosts:
        pgbouncer-01-db-gstg.c.gitlab-staging-1.internal:
        pgbouncer-02-db-gstg.c.gitlab-staging-1.internal:
        pgbouncer-03-db-gstg.c.gitlab-staging-1.internal:
        pgbouncer-04-db-gstg.c.gitlab-staging-1.internal:
        pgbouncer-05-db-gstg.c.gitlab-staging-1.internal:
        pgbouncer-06-db-gstg.c.gitlab-staging-1.internal:
    pgbouncer_sidekiq:
      hosts:
        pgbouncer-sidekiq-01-db-gstg.c.gitlab-staging-1.internal:
        pgbouncer-sidekiq-02-db-gstg.c.gitlab-staging-1.internal:
        pgbouncer-sidekiq-03-db-gstg.c.gitlab-staging-1.internal:
```

In the example above, hosts in groups are indicated: 

- `source_cluster` - The Source Cluster hosts with an old version of PostgreSQL from which replication is performed.
- `target_cluster` - The Target Cluster hosts on which PostgreSQL will be updated to the new version.
- `pgbouncer`, `pgbouncer_sidekiq` - The dedicated PgBouncer hosts that proxy traffic to Primary (using the consul service name, e.q. `master.patroni.service.consul`). More details [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/pgbouncer/patroni-consul-postgres-pgbouncer-interactions.md#path-from-rails-app-to-primary-db).

</p></details>

### 1. Upgrade PostgreSQL:

**Multi-Slot Replication:** Use `-e "pg_publication_count=4"` to use Multi-Slot logical replication during the Upgrade; by default the Upgrade uses single-slot logical replication. 

**Multi-Slot Replication Notes:**
  * We recomend 4 replication slots `pg_publication_count=4`, but it can be any number `>1`;
  * If Multi-Slot replication was used **only** in the `upgrade`, then `eventual data consistency` will happen if we perform `switchover_replica` first before R/W (leader).
  * If Multi-Slot replication was used in **both** the `upgrade` and in the `switchover_leader`, then `eventual data consistency` will happen independent of the switchover order.

Main cluster:

```
ansible-playbook -i inventory/gstg-main.yml upgrade.yml -e "pg_old_version=14 pg_new_version=16"
```

CI cluster:


```
ansible-playbook -i inventory/gstg-ci.yml upgrade.yml -e "pg_old_version=14 pg_new_version=16"
```

Registry cluster:

```
ansible-playbook -i inventory/gstg-registry.yml upgrade.yml -e "pg_old_version=14 pg_new_version=16 pg_source_dbname=gitlabhq_registry"
```


#### 1.1. [If aborting the upgrade] Stop the logical replication after the Upgrade:

If for some reason you decide to not continue with the Upgrade and would like to destroy the TARGET cluster, you need to stop the logical replication using the following playbook

```
ansible-playbook -i inventory/<inventory-file>.yml stop_upgrade_replication.yml
```


### 2. Switch traffic to the new cluster:

**Multi-Slot Replication:** Use `-e "pg_publication_count=4"` to use Multi-Slot logical replication during the R/W Switchover (switchover_leader); by default the Switchover uses single-slot logical replication. 

**Multi-Slot Replication Notes:**
  * We recomend 4 replication slots `pg_publication_count=4`, but it can be any number `>1`;
  * If Multi-Slot replication was used **only** in the `switchover_leader`, then `eventual data consistency` will happen if we perform switchover_leader first before R/O (replica).
  * If Multi-Slot replication was used in **both** the `upgrade` and in the `switchover_leader`, then `eventual data consistency` will happen independent of the switchover order.

#### Switchover R/O traffic

```
ansible-playbook -i inventory/gstg-main.yml switchover_replica.yml
```

_Registry cluster:_

```
ansible-playbook -i inventory/gstg-main.yml switchover_replica.yml -e "pg_source_dbname=gitlabhq_registry"
```

#### Switchover R/W traffic

```
ansible-playbook -i inventory/gstg-main.yml switchover_leader.yml
```

_Registry cluster:_

```
ansible-playbook -i inventory/gstg-main.yml switchover_leader.yml -e "pg_source_dbname=gitlabhq_registry"
```

Note: To disable interactive mode, specify the variable "non_interactive=true"


### 3. Stop reverse replication

Execute this playbook after a decision is made that a rollback to the old cluster is no longer necessary.

Note: If reverse logical replication is enabled (enabled by default, `enable_reverse_logical_replication: true`)

```
ansible-playbook -i inventory/gstg-main.yml stop_reverse_replication.yml
```

## Rollback

If problems are detected, perform a rollback - switch the traffic back to the old cluster:

```
ansible-playbook -i inventory/gstg-main.yml switchover_rollback.yml
```


## force_mode

"Plan B": switch traffic immediately (optional, not used by default).

Here we switch from zero-downtime to micro-downtime approach (not more than 10s), which is more reliable but some clients will notice some errors (brief downtime). \
By default, we go with "Plan A" (default mode), but if we see we cannot afford it (lags > 10 MiB all the time, the pgbouncer pool is not paused due to high activity, etc.), we switch to the reliable "Plan B".


Switch traffic to the new cluster in the force_mode:

Note: In this example, we allow switching with a lag of up to 3 GiB

```
ansible-playbook -i inventory/gstg-main.yml switchover.yml -e "force_mode=true force_mode_max_replication_lag_bytes=3221225472"
```

Rollback in the force_mode:

```
ansible-playbook -i inventory/gstg-main.yml switchover_rollback.yml -e "force_mode=true"
```

Please see the detailed description of the plan below.

---

## Upgrade Plan:

#### Step 1: PRE-UPGRADE: Perform Pre-Checks
  - (ALL) Make sure that the consul service files exist
    - Stop, if the list of consul service files does not match
    - with the values of the variables `patroni_consul_service_file` or `pgbouncer_consul_service_file`
  - (ALL) Make sure that the consul service names match
    - Stop, if consul service names do not match
    - with the values of the variables `patroni_consul_service_name` or `pgbouncer_consul_service_name`
  - (TARGET) Make sure that is Standby leader
  - (ALL) Test PostgreSQL DB Access
  - (TARGET) Check the current version of PostgreSQL
    - Stop if, an incorrect version of PostgreSQL may have been specified
      - if the `pg_old_version` variable does not match the current version of PostgreSQL
    - Stop if, an incorrect target version of PostgreSQL may have been specified
      - if the `pg_new_version` variable is equal to or less than the current version of PostgreSQL (no update required) 
  - (TARGET) Ensure new data directory is different from the current one
    - Stop, if the current data directory is the same as `pg_new_datadir`
    - Note: This check is necessary to avoid the risk of deleting the current data directory. The current directory must not be equal to the path specified in the pg_new_datadir variable which will later be cleaned up before executing initdb for a new version of PostgreSQL
  - (TARGET) None of target nodes have 'archive_mode=always'
    - Stop if `archive_mode = always` at least on one of target nodes
  - (SOURCE) Check the current version of pg_repack (if installed)
    - Stop, if the old version of pg_repack is installed
    - Note: less than specified in the `min_pg_repack_version` variable (the minimum version of the extension is compatible with the new version of PostgreSQL).
  - (SOURCE) Drop table "test_replication" if exists
  - (SOURCE) Make sure there are no tables with replica identity "nothing"
    - Stop, if replica identity "nothing" is present
  - (SOURCE) Make sure that tables with replica identity “default” have primary key
    - Stop, if replica identity "default" without PK is present
  - (ALL) Make sure that the password file ".pgpass" exists
    - Stop, if .pgpass is not exists
  - (SOURCE) Make sure that the user is specified in the password file
    - Stop, if the user (`pg_source_replica_user` variable) is not present
  - (TARGET) Make sure that the user "{{ pg_target_replica_user }}" is specified in the password file
    - if `enable_reverse_logical_replication` is 'true'
    - Stop, if the user (`pg_target_replica_user` variable) is not present
  - (SOURCE) Get the password for the user from the password file
  - (TARGET) Configure the password file for the user
    - Note: Optional step. Nothing will be changed if the password file is already configured.
  - (TARGET) Test the access to the source database
    - Note: User from the `pg_source_replica_user` variable to database from the `pg_source_dbname` variable
  - (TARGET) Get the password for the user from the password file
    - if `enable_reverse_logical_replication` is 'true'
  - (SOURCE) Configure the password file for the user
    - if `enable_reverse_logical_replication` is 'true'
    - Note: Optional step. Nothing will be changed if the password file is already configured.
  - (SOURCE) Test the access to the source database
    - if `enable_reverse_logical_replication` is 'true'
    - Note: User from the `pg_target_replica_user` variable to database from the `pg_target_dbname` variable
  - (SOURCE/TARGET) Make sure that physical replication is active
    - Stop, if there are no active replicas
    - if pg_stat_replication returned 0 entries (except pg_basebackup)
  - (SOURCE/TARGET) Make sure there is no high physical replication lag
    - Note: No more than `max_replication_lag_bytes`
    - Stop, if physical replication lag is high
  - (TARGET) Rsync Checks
    - Create /tmp/testrsync file on Primary
    - Test rsync and ssh key access
    - Cleanup testrsync file
  - (TARGET) Make sure that the Chef client is stopped
    - Note: to prevent unexpected changes during the process
  - (TARGET) Make sure that the '`restore_command`' parameter is not specified
    - if 'restore_command' is specified:
        - check if the 'recovery_conf' is specified in patroni.yml
        - comment out 'recovery_conf' parameters in patroni.yml
        - execute CHECKPOINT before restarting PostgreSQL
        - stop Patroni service on Replicas and Leader
        - start Patroni service and check if Patroni cluster is healthy
        - make sure that 'restore_command' is disabled
        - stop, if 'restore_command' is defined
  - (TARGET) Check the WAL-G version
    - Stop, if an incompatible version is installed (less than specified in the `min_walg_version` variable)
  - (SOURCE/TARGET) Check the current value of WALG_GS_PREFIX
    - Stop, if WALG_GS_PREFIX value is the same on source and target clusters
  - (TARGET) Make sure that there are no backups in the bucket (if `clear_backups_before_upgrade` is 'true' (default 'true'))
    - Note: run the `wal-g delete everything` command (`clear_backups_command` variable) asynchronously
    - Wait for the backup cleanup command to complete (max wait time of 1 hour, `clear_backups_command_timeout` variable)
#### Step 2: PRE-UPGRADE: Install new PostgreSQL packages
  - Update apt cache
  - Make sure that the new PostgreSQL packages are installed (install if missing)
#### Step 3: PRE-UPGRADE: Initialize new db, schema compatibility check, and pg_upgrade --check
  - Make sure new PostgreSQL data directory exists
    - Note: create data directory
  - Make sure new PostgreSQL data directory is not initialized
  - Clear the new PostgreSQL data directory
    - Note: if already initialized
  - Get the current encodig and data_checksums settings
  - Get the current install user
  - Initialize a new PostgreSQL data directory on the Primary
    - with the same encoding
    - with enabled checksums (if they are enabled in the current cluster)
  - Get the current shared_preload_libraries settings
  - Start new PostgreSQL on port `{{ pg_target_port + 1 }}` to check the schema compatibility
  - Check the compatibility of the database schema with the new PostgreSQL
    - using pg_dumpall --schema-only
    - run the command asynchronously
  - Wait for the schema compatibility check to complete
  - Check the result of the schema compatibility
    - Stop, if the scheme is not compatible (there are errors)
  - Stop PostgreSQL to re-initdb
  - Clear a new PostgreSQL data directory and re-initdb (after checking the scheme)
  - Add temporary local access rule for pg_upgrade
    - Update the PostgreSQL configuration
  - Verify the two clusters are compatible
    - using `pg_upgrade --check` (check clusters only, don't change any data)
  - Remove temporary local access rule for pg_upgrade
    - Update the PostgreSQL configuration
#### Step 4: Create a publication not the Source and reach `recovery_target_lsn` on the Target leader
  - (SOURCE) Increase the number of WAL files to hold in the Source Cluster
    - Increase the `wal_keep_segments`/`wal_keep_size` parameter to `pg_source_wal_keep_gigabytes`  
      - Note: To guarantee that the necessary WAL files are available to reach `recovery_target_lsn` via streaming replication.
    - Update the PostgreSQL configuration on the Source Primary to apply changes
  - (TARGET) Pause Patroni on the Target Cluster
  - (TARGET) Stop Patroni service on the Cluster Replicas
  - (TARGET) Stop Patroni service on the Cluster Leader
  - (TARGET) Wait until the Patroni cluster is stopped
  - (TARGET) Pause WAL replay (recovery) on the Standby Cluster Replicas
  - (TARGET) Execute CHECKPOINT before stopping PostgreSQL
  - (TARGET) Stop PostgreSQL on the Standby Cluster Leader
  - (SOURCE) Execute checkpoint before creating a publication
  - (SOURCE) Create a publication for logical replication
    - Note: If the variable `pg_publication_count` is more than `1`, instead of creating one publication for all tables:
      - Get a list of tables distributed by groups
      - Start pg_terminator script: Monitor locks and terminate the 'create publication' blockers
        - terminate the backend blocking the create publication query for more than 15 seconds (if exists)
        - after creating publications, the pg_terminator script stops.
  - (SOURCE) Create a slot for logical replication, and save 'lsn'
    - Note: If the variable `pg_publication_count` is more than `1`
      - Create multiple publications and replication slots
      - Advance all created slots to the last LSN position
  - (TARGET) Specify recovery parameters on the Standby Cluster Leader
    - `recovery_target_lsn` = '{{ pg_slot_lsn }}'
    - `recovery_target_action` = 'promote'
    - `recovery_target_timeline` = 'latest'
    - temporarily comment out the `restore_command` parameter.
      - Note: For data consistency, we need to restore the standby cluster exactly to the LSN that we receive on the source cluster during the creation of the publication and the slot for logical replication. Using streaming replication for this purpose is a more reliable way, as tests have shown us.
      - Note: When starting the Patroni service, the parameters will be returned to their original state (as defined in DCS).
  - (TARGET) Start PostgreSQL on the Standby Cluster Leader to reach recovery_target_lsn
  - (TARGET) Wait until the recovery is complete
  - (TARGET) Get the current PostgreSQL log file
  - (TARGET) Check the PostgreSQL log file
    - Stop, if target LSN not reached
  - Print the result of checking the PostgreSQL log
  - (TARGET) Resume WAL replay (recovery) on Standby Cluster Replicas
  - (TARGET) Wait until physical replication becomes active
  - (TARGET) Wait until physical replication lag is 0 bytes
  - (SOURCE) Reset the `wal_keep_segments`/`wal_keep_size` parameter to original state on the Source Primary
    - Update the PostgreSQL configuration on the Source Primary
  - (TARGET) [Disable DB Cron Daemon] Stop cron service
  - (TARGET) Execute CHECKPOINT before stopping PostgreSQL
  - (TARGET) Stop PostgreSQL on the Cluster Leader
  - (TARGET) Stop PostgreSQL on the Cluster Replicas
  - (TARGET) Check if old PostgreSQL is stopped
  - (TARGET) Check if new PostgreSQL is stopped
  - (TARGET) Get 'Latest checkpoint location' on the Target Cluster Leader and Replicas
  - (TARGET) Print 'Latest checkpoint location'
  - (TARGET) Check if all 'Latest checkpoint location' values match
    - if 'Latest checkpoint location' doesn't match:
       - clean up logical replication objects on the Source Cluster
       - stop the upgrade procedure with an error
    - if 'Latest checkpoint location' values match on all cluster nodes
      - continue the upgrade procedure
#### Step 5: PRE-UPGRADE: Prepare the Patroni configuration
  - Edit patroni.yml | update parameters: data_dir, bin_dir, config_dir
  - Edit patroni.yml | prepare the parameters for PostgreSQL
     - Note: (multiple tasks) Check the removed or renamed parameters starting from version 13 to 16
  - Edit patroni.yml | remove parameters: standby_cluster (if defined)
  - Copy the pg_hba.conf file to a new PostgreSQL (to save pg_hba rules)
#### Step 6: UPGRADE: Upgrade PostgreSQL
  - Add temporary local access rule for pg_upgrade
    - Note: to allow the upgrade process to proceed without authentication issues.
  - Upgrade the PostgreSQL to new version on the Target Cluster Primary (using `pg_upgrade --link` (hard-links))
  - Remove temporary local access rule for pg_upgrade
  - Make sure that the new data directory (`pg_new_datadir`variable) are empty on the Replica
  - Upgrade the PostgreSQL on the Target Cluster Replica (using `rsync` (hard-links))
    - Note: before `rsync` is executed, the owner of the PostgreSQL data directories changes to the user from under whom the playbook is executed, to use ssh agent forwarding.
      - After rsync is executed, the owner of the directory returns to the user defined in the `os_pg_user` variable (gitlab-psql)
  - Remove existing cluster from DCS
  - Start Patroni service on the Cluster Leader
    - Wait for Patroni port to become open on the host
    - Check Patroni is healthy on the Leader
  - Start Patroni service on the Cluster Replica
    - Wait for Patroni port to become open on the host
    - Check Patroni is healthy on the Replica
#### Step 7: POST-UPGRADE: Create a subscription for logical replication
  - (TARGET) Create a subscription for logical replication using a previously created slot
    - Note: If the variable `pg_publication_count` is more than `1`
      - Create multiple subscriptions for each publication/slots.
  - (SOURCE) Make sure that logical replication is active
    - wait until the logical replication slot is active
    - Note: max wait time: 1 minute
  - (SOURCE) Check the logical replication lag
  - Print the result of setting up logical replication
    - example: "The logical replication setup is completed. Current replication lag: XX MB"
#### Step 8: POST-UPGRADE: Check Replication and Update extensions
  - Make sure that physical replication is active
    - Check pg_stat_replication (count records, except pg_basebackup)
  - Create a table "test_replication" with 10000 rows on the Primary
  - Wait until the PostgreSQL replica is synchronized
    - The PostgreSQL Replication is Ok. If the number of records in the test_replication table the same as the Primary.
    - Error, if the number of records does not match the Primary.
  - Drop a table "test_replication"
  - Update old extensions (if `update_extensions` is 'true' (default 'true'))
    - Get a list of old PostgreSQL extensions
    - Update all old PostgreSQL extensions
      - Note: excluding 'pg_repack', 'pg_stat_kcache', 'pg_stat_statements' as it requires re-creation to update
    - Recreate old PostgreSQL 'pg_repack', 'pg_stat_kcache', 'pg_stat_statements' extensions (if an update is required)
    - Note: If the pg_stat_cache extension is present, the pg_stat_statements extension will be removed in CASCADE mode,  because the pg_stat_kcache extension depends on it. This means that all dependent objects (views, functions) will also be deleted, and we need to take care to create them again. The pg_stat_statements and pg_stat_kcache extensions will then be re-created.
#### Step 9: POST-UPGRADE: Analyze a PostgreSQL database (update optimizer statistics)
  - Start 'pg_terminator' script to monitor locks and terminate the 'ANALYZE' blockers.
  - Run vacuumdb to analyze database (for a database defined in the `pg_target_dbname` variable)
    - Note: all CPU cores of the server are used (`vacuumdb_parallel_jobs` variable).
    - Note: run the command asynchronously with a max wait time of 1 hour (`vacuumdb_analyze_timeout` variable)
  - Wait for the analyze to complete.
    - Note: while the task is running, print the message "Collecting statistics in progress" every 30 seconds.
  - Stop 'pg_terminator' script after completing the statistics collection.
#### Step 10: Perform Post-Upgrade tasks
  - Update PostgreSQL data directory path in `/opt/wal-g/bin/backup.sh`
    - Note: if the script is specified in the `backup_command` variable
  - Run PostgreSQL backup command in the background (if `backup_after_upgrade` is 'true' (default 'true'))
    - Note: Ansible only initiates the backup script execution (see `backup_command` variable) and does not wait for it to finish (because it is a long-running job).
      - It is recommended to check the backup status after N hours (depending on the size of the database) using the command:
      `/usr/bin/envdir /etc/wal-g.d/env /opt/wal-g/bin/wal-g backup-list`
  - Run pg_amcheck command (in the background) to check indexes for corruption (if `pg_amcheck` is 'true' (default 'false'))
    - Note: Ansible only initiates the pg_amcheck execution and does not wait for it to finish (because it is a long-running job).
      - It is recommended to check the pg_amcheck result after N hours (depending on the size of the database) in the log file `/tmp/pg_amcheck.log` ('pg_amcheck_log_path' variable) on the Target cluster leader.
  - Delete the old PostgreSQL data directory
  - Remove old PostgreSQL packages
    - Note: if `pg_old_version_packages_remove` is 'true' (default 'true')
  - Check the current state of the Patroni cluster and the PostgreSQL version, then log the results in the Ansible output.

---

## Switchover Plan:

Note: At each step, we ask the human to confirm the execution (y/N). \
_If necessary, to disable interactive mode, specify "non_interactive=true" variable._

#### Step 0: Perform Preparation and Pre-Checks before switching traffic (`switchover_replica.yml`/`switchover_leader.yml`)

  - Prepare: Ensure the Chef client is stopped (to prevent unexpected changes during the switchover)
  - Pre-Check: Make sure that logical replication is active
    - Stop, if there is no replication slot with the name specified in the `pg_source_logical_slot_name` variable
    - Stop, if the replication slot `pg_source_logical_slot_name` is not active
  - Pre-Check: Make sure there is no high logical replication lag
    - Note: no more than `max_replication_lag_bytes` (if force_mode: false) or `force_mode_max_replication_lag_bytes` (if force_mode: true)
    - Stop, if high logical replication lag
      - Note: it will be suggested to try later. Example: "High logical replication lag, please try again later."
  - Pre-Check: Make sure there are no long-running transactions
    - Note: no more than `max_transaction_sec` (skipped if the `force_mode: true`)
    - Stop, if long-running transactions detected

#### Step 1: Start read-only traffic to replicas of the new cluster (`switchover_replica.yml`)

  - TARGET: Start read-only traffic to replicas of the new cluster
    - Edit the Consul Service config file and reload consul.service
    - Note: Here we adjust consul records (DNS) so replicas on the new cluster are added to the list, and application nodes see them, sending traffic.
    - Note: Make sure that there are no problems when working with replicas on the new version of PostgreSQL (check the status of the application and database metrics).
      - If problems are detected, perform a rollback - switch the read-only traffic back to the old cluster (playbook: switchover_rollback.yml).
      - If no problems are detected, perform the following step.

#### Step 2: Stop read-only traffic to replicas of the old cluster (`switchover_replica.yml`)

  - SOURCE: Stop read-only traffic to replicas of the old cluster: rename the Consul service
    - Edit the Consul Service config file and reload consul.service
    - Note: Now we stop sending read-only traffic to replicas of the old cluster. Similarly, adjusting Consul (DNS) records (adding a suffix, ex. "db-replica-old").
    - Note: Make sure that there are no problems with read-only traffic on the new version of PostgreSQL (check the status of the application and database metrics).
      - If problems are detected, perform a rollback - switch the read-only traffic back to the old cluster (playbook: switchover_rollback.yml).
      - If the application is stable with the new version of the database, after a while (think about the allowed observation time) - switch traffic to the new cluster leader.

#### Step 3: Switch write traffic to the new Patroni cluster leader (`switchover_leader.yml`)

  - (SOURCE) Get a list of tables distributed by groups (for reverse logical replication)
    - Note: if '`enable_reverse_logical_replication`' is 'true' and '`pg_publication_count_reverse`' > 1
  - Sequences: Increase the value of sequences on the new cluster
    - (SOURCE) Get the list and value for the sequences on the Source Cluster leader.
    - (TARGET) Generate the file /tmp/pg_sequences_increase.sql
    - (TARGET) Increase the value of sequences. Add + `pg_sequences_increase_value` to the current value of the sequences of the old cluster
      - Note: by default, the `pg_sequences_increase_value` variable is `1000000`
  - (SOURCE) Attempt to wait for the window without logical replication lag before switchover
    - Note: max wait time: 5 minutes (`before_switchover_no_lag_window_timeout` variable)
    - Note: if the lag is more than 0 bytes, but less than the allowed one, continue switchover. Else, stop if the lag is high (more than `max_replication_lag_bytes` (if force_mode: false) or `force_mode_max_replication_lag_bytes` (if force_mode: true))
  - `PAUSE` PgBouncer pools
    - Note: if `pgbouncer_pool_pause: true` (skipped if the `force_mode: true`)
  - SOURCE: Enable read-only mode for the old cluster leader (skipped if the `enable_reverse_logical_replication: true`)
    - Note: We prevent accidental writes to the old cluster database, to make sure that the writes go only to the database of the new cluster.
      - Used: `alter system set default_transaction_read_only = 'on'`
  - (SOURCE) Terminate active sessions on the source cluster leader (if `force_mode` and `force_mode_pg_terminate` is 'true')
    - Note: To ensure that there are no writing transactions that can make changes to the database
      - this task is not necessary (skipped) if the pool is paused
  - (SOURCE) Wait until the logical replication lag is 0 bytes (skipped if the `force_mode: true`)
    - Note: To prevent a risk of data loss after switching to a new cluster due to replication lag.
      - If, after reaching the timeout (`pgbouncer_pool_pause_zero_lag_check_timeout` variable, default '60' seconds) the logical replication lag is higher than 0 bytes, RESUME will be executed and the playbook will stop with an error and a request to try again later.
  - (TARGET) Stop logical replication
    - Note: Drop Subscription on the target cluster.
  - (SOURCE/TARGET): Stop traffic to the old cluster leader and start traffic to the new cluster leader
    - Note: Here we perform the renaming of еthe consul services and the reload of the consul.service
    - Note: The DNS record of the old leader will be deleted and the DNS record of the new cluster leader will be added.
  - (TARGET) Create publication and slot for reverse logical replication
    - if `enable_reverse_logical_replication` is 'true' (default 'true')
    - Note: By default, multiple publications/slots are used for reverse logical replication (variable `pg_publication_count_reverse`, default '4')
  - (SOURCE) Restart pgbouncer service (if `force_mode: true`)
    - Note: To ensure that new connections will be routed to the new address after changing the DNS record for the consul service
      - this task is not necessary (skipped) if the pool is paused
  - `RESUME` PgBouncer pools
    - if `pgbouncer_pool_pause: true` (skipped if the `force_mode: true`)
    - Note: PgBouncer "PAUSE" tries to disconnect from all servers, first waiting for all queries to complete. The command will not return before all queries are finished. New client connections to a paused database will wait until "RESUME" is called. We will execute the RESUME command immediately after the DNS record of the Consul service `master.patroni.service.consul` resolves the host of the leader of the new cluster. Clients that successfully waited for "RESUME" operation will get results with respective latency (according to the time in the pause).
    - Note: In the best case, this will allow switching without errors (without downtime), in the worst case (when the pause is more than N seconds), this will reduce the number of errors on the application (minimum downtime).
  - [Post-switchover] Clean up logical replication objects
    - Drop Publication and replication slot on the Source cluster
    - Drop Publication (replicated from the source cluster) on Target cluster
  - (SOURCE) Start reverse logical replication  (if '`enable_reverse_logical_replication`' is 'true')
    - Create a subscription for reverse logical replication
    - Make sure that reverse logical replication is active
  - (SOURCE) Pause Patroni on the Source Cluster (old database) after switchover
    - disable auto-failover (if `enable_reverse_logical_replication` is 'true')
  - (SOURCE) Deny access to the old database to avoid the risks of making unwanted changes to the old database after the switchover
    - Backup original hba file
    - Replace hba file
      - Notes: allow only local connections (as superuser only), and replication connections to keep physical and logical replication in active state.
    - Reload Postgres to apply changes

Note: After a full switchover to a new cluster (step 1, 2 ,3), check the status of the application and database metrics again. If problems are detected (the probability is low, after observation with a read-only traffic), and if the problem cannot be fixed promptly, perform a rollback to the old cluster (playbook: switchover_rollback.yml).
Note: The rollback process prevents the risk of losing data changes when reverse logical replication is enabled (controlled by the `enable_reverse_logical_replication` variable, which is enabled by default).

---
