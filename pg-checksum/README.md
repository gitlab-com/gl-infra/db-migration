# Implement data checksums in Postgres clusters

This playbooks will help in implementing data checksums in Postgres clusters.

[Data checksums](https://www.postgresql.org/docs/current/checksums.html) is not enabled for our Registry, CI, and Main Patroni Postgres clusters which means that corruption at the level of page content can go unnoticed. WAL writes are covered as checksums in WALs are always on.

### High-level procedure to implement checksums in a Postgres cluster
- Implement checksums on all the patroni replicas.
- Perform Patroni Switchover to one of the data checksum enabled patroni replica.

The playbooks will help implement `pg_checksum` on a patroni replica and also perform Patroni Switchover. The switchover playbook will switchover site traffic to a new patroni leader with zero-downtime using PgBouncer's PAUSE/RESUME option.

## List of playbooks:

- `checksum_replica.yml` - This playbook is designed to implement `pg_checksum` on a patroni replica.
- `switchover_patroni_leader.yml` - This playbook is designed to perform Patroni Leader Switchover. It is designed to switchover site traffic to a new patroni leader with zero-downtime using PgBouncer's PAUSE/RESUME option.


## Requirements

1. Make sure your ssh-agent has your identity loaded (`ssh-add -L`), since we use agent forwarding by default (configured in `ansible.cfg`).

## Before implementing checksum 
- Create inventory file for your environment

## How to run

### 1. Implement checksum (`pg_checksum`) on a patroni replica:
- Add inventory file for your environment

e.g: `./db-migration/pg-checksum/inventory/gprd-ci.yml`

```
patroni-main-v14-10-db-gprd.c.gitlab-production.internal
```

- Run `checksum_replica.yml` 

`gprd-ci` cluster:


```
cd ./db-migration/pg-checksum
ansible-playbook -i inventory/gprd-ci.yml   checksum_replica.yml  2>&1 | ts | tee -a ansible_checksum_gprd-ci_$(date +%Y%m%d).log
```


### 2. Perform Patroni Switchover (Switchover traffic from the current Patroni Leader to a Patroni Replica):
- Add inventory file for your environment - e.g. `db-benchmarking-mock.yml`

e.g: `./db-migration/dbre-toolkit/db-benchmarking-mock.yml`

<details><summary>Click here to expand...</summary><p>

```yaml
all:
  vars:
    cluster_environment: db-benchmarking
    cluster_suffix: main
  children:
    patroni_cluster:
      hosts:
        patroni-mock-v14-01-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        patroni-mock-v14-02-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        patroni-mock-v14-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        patroni-mock-v14-04-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
    pgbouncer:
      hosts:
        pgbouncer-main-01-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        pgbouncer-main-02-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        pgbouncer-main-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
    pgbouncer_sidekiq:
      hosts:
```

In the example above, hosts in groups are indicated: 

- `patroni_cluster` - Patroni Cluster hosts. It is a list of hosts of the patroni cluster. The Switchover playbook will Switchover Patroni Leader to one of the patroni replicas.
- `pgbouncer`, `pgbouncer_sidekiq` - The dedicated PgBouncer hosts that proxy traffic to Primary (using the consul service name, e.q. `master.patroni.service.consul`). More details [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/pgbouncer/patroni-consul-postgres-pgbouncer-interactions.md#path-from-rails-app-to-primary-db).

</p></details>


- Run `switchover_patroni_leader.yml`  Ansible playbook

`db-benchmarking-mock` cluster:


```
cd ./db-migration/dbre-toolkit
ansible-playbook -i inventory/db-benchmarking-mock.yml   switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_db-benchmarking-mock_$(date +%Y%m%d).log
```
PS: For now, I've provided instruction to perform Patroni Switchover in this README with reference to `./db-migration/dbre-toolkit`. DBRE ToolKit also has a README. I'll iterate on it and cleanup the README.