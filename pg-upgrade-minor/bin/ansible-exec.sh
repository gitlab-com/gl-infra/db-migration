#!/bin/bash

# Echo usage if something isn't right.
usage() { 
    echo "Usage: $0 [-e ENVIRONMENT] [-p PLAYBOOK (DEFAULT:main)] [-h HOSTNAME (OPTIONAL)] [-t TAGS1,TAGS2 (default: all)] [-k TAGS1,TAGS2] [-x EXTRA_VARIABLE1,EXTRA_VARIABLE2] [-s (Step)] [-c (Check Mode)] [-v]" 1>&2; exit 1;
}

# Default variables
PLAYBOOK=main

while getopts e:p:h:t:k:x:scv flag
do
    case "${flag}" in
        e) GCP_ENV=${OPTARG};;
        p) PLAYBOOK=${OPTARG};;
        h) GCP_HOST=${OPTARG};;
        t) TAGS=${OPTARG};;
        k) SKIP_TAGS=${OPTARG};;
        x) EXTRA_VARS=${OPTARG};;
        s) STEP='true';;
        c) CHECKMODE='true';;
        v) VERBOSE='true';;
        :)  
            echo "ERROR: Option -$OPTARG requires an argument"
            usage
            ;;
        \?)
            echo "ERROR: Invalid option -$OPTARG"
            usage
            ;;
    esac
done

# Check required switches exist
if [ -z "${GCP_ENV}" ]; then
    echo "-e Env is mandatory"
    usage

fi

# Ansible playbook and environment
ansible_extra_opts=("playbooks/${PLAYBOOK}.yml" "--inventory=inventory/${GCP_ENV}.yml")

# Ansible extra options
if [[ "$CHECKMODE" == "true" ]]; then
  ansible_extra_opts+=("--check")
fi

if [[ "$STEP" == "true" ]]; then
  ansible_extra_opts+=("--step")
fi

if [[ "$VERBOSE" == "true" ]]; then
  ansible_extra_opts+=("-vvvvv")
fi

# Limit to Host if defined
if [ -n "$GCP_HOST" ]; then
  ansible_extra_opts+=("--limit ${GCP_HOST}")
fi

# Add tags if defined
if [ -n "$TAGS" ]; then
  ansible_extra_opts+=("--tags ${TAGS}")
fi

# Skip tags if defined
if [ -n "$SKIP_TAGS" ]; then
  ansible_extra_opts+=("--skip-tags ${SKIP_TAGS}")
fi

# Add variables if defined
if [ -n "$EXTRA_VARS" ]; then
  ansible_extra_opts+=("--extra-vars ${EXTRA_VARS}")
fi

ansible-playbook ${ansible_extra_opts[@]}
