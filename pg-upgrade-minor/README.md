# PostgreSQL Minor Upgrade Playbook

1. We have a wrapper script for executing the ansible playbooks which can be used as follows:
   
```Usage: ./bin/ansible-exec.sh [-e ENVIRONMENT] [-p PLAYBOOK (default:main)] [-h HOSTNAME (OPTIONAL)] [-t TAGS1,TAGS2 (default: all)] [-s (Step)] [-c (Check Mode)]```
