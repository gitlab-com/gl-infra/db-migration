#!/usr/bin/env ruby

require 'optparse'
require 'httparty'

def main
  variables = [
    'GITLAB_QA_USER_AGENT',
    'GITLAB_QA_ACCESS_TOKEN',
    'GITLAB_QA_DEV_ACCESS_TOKEN',
    'GITLAB_QA_PASSWORD',
    'GITLAB_QA_USERNAME_1',
    'GITLAB_QA_PASSWORD_1',
  ]

  variable_mapping = {
    'GITLAB_QA_PASSWORD' => 'GITLAB_PASSWORD'
  }

  envs = {
    'GITLAB_USERNAME' => 'gitlab-qa',
    'QA_CAN_TEST_GIT_PROTOCOL_V2' => 'false',
    'QA_CAN_TEST_ADMIN_FEATURES' => 'false',
    'QA_CAN_TEST_PRAEFECT' => 'false'
  }

  config = {
    :quality_group_id => 354,
    :gstg => {
      :project_id => 263,
      :variables => variables + ['GITLAB_ADMIN_USERNAME', 'GITLAB_ADMIN_PASSWORD', 'GITLAB_QA_ADMIN_ACCESS_TOKEN'],
      :envs => envs.merge({ 'QA_PRAEFECT_REPOSITORY_STORAGE' => 'nfs-file22', 'QA_CAN_TEST_ADMIN_FEATURES' => 'true' }),
    },
    :gprd => {
      :project_id => 275,
      :variables => variables,
      :envs => envs,
    }
  }

  ARGV << '-h' if ARGV.empty?

  options = {
    base_url: 'https://ops.gitlab.net'
  }

  OptionParser.new do |opts|
    opts.banner = "Usage: #{__FILE__} [options] <gstg|gprd> <API key>"
    opts.on("-b", "--base-url URL", String, "GitLab URL (default: https://ops.gitlab.net)") do |url|
      options[:base_url] = url
   end
  end.parse!

  env = ARGV.shift
  raise "ERROR: you need to specify either 'gstg' or 'gprd'" unless ['gstg', 'gprd'].include?(env)
  env_config = config[env.to_sym]

  # Use API key from ARGV otherwise fallback to env variable
  api_key = ARGV.empty? ? ENV['QA_ENVS_API_KEY'] : ARGV.shift

  raise "ERROR: you need to specify an API key for '#{options[:base_url]}' either as an ARGV argument or env variable 'QA_ENVS_API_KEY'" if api_key.nil?

  headers = { 'PRIVATE-TOKEN' => api_key }

  project_variables = fetch_variables("#{options[:base_url]}/api/v4/projects/#{env_config[:project_id]}/variables?per_page=100", headers)
  group_variables = fetch_variables("#{options[:base_url]}/api/v4/groups/#{config[:quality_group_id]}/variables?per_page=100", headers)

  all_variables = project_variables + group_variables
  all_variables_h = all_variables.each_with_object({}) { |i, memo| memo[i['key']] = i['value'] }

  skipped_variables = []

  env_config[:variables].each { |var|
    skipped_variables.append(var) && next unless all_variables_h.key?(var)

    key = variable_mapping.key?(var) ? variable_mapping[var] : var
    puts export(key, all_variables_h[var])
  }

  env_config[:envs].each { |key, val|
    puts export(key, val)
  }

  unless skipped_variables.empty?
    puts "\n[SKIPPED VARIABLES]\n" << skipped_variables.join("\n")
  end
end

def export(key, val)
  return "export #{key}=\"#{val}\""
end

def fetch_variables(url, headers)
  response = HTTParty.get(url, :headers => headers)
  parsed_response = response.parsed_response

  raise "ERROR: unable to fetch variables from #{url}: #{parsed_response}" if response.code != 200

  return parsed_response
end

main
