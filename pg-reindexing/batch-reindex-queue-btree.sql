-- This query return/insert a batch of indexes limited to a batch size considering ondisk_size_bytes
-- The indexes returned should not have been reindexed after a date nor already enqueued
-- This window query returns sub-optimal fill result set, for optimal fill please use the procedure at batch-reindex-queue-btree-optimal.sql

-- INSERT into postgres_reindex_queued_actions(index_identifier, state, created_at) select identifier, 0, date_trunc('minutes',now() + make_interval(mins => row_number() over()::integer)) as created_at
SELECT identifier, ondisk_size_bytes/1000 ondisk_size_kbytes, batch_size_so_far/1000/1000/1000 as batch_size_so_far_gbytes, date_trunc('minutes',now() + make_interval(mins => row_number() over()::integer)) as created_at
FROM (
  select identifier, ondisk_size_bytes,
    sum(ondisk_size_bytes) over (order by ondisk_size_bytes desc) as batch_size_so_far
  from postgres_indexes indx
  where
    -- Skip indexes not yet reindexed nor enqueued (state != 0) after date ('09-09-2023 14:00' = latest pg upgrade date)
    not exists (
      select 1
      from postgres_reindex_queued_actions reind_q 
      where reind_q.index_identifier = indx.identifier
        and (reind_q.state = 0
        or reind_q.updated_at > TO_TIMESTAMP('09-09-2023 14:00', 'DD-MM-YYYY HH24:MI')) 
      )
    -- These conditions match our "reindexing support" conditions, so we only reindex indexes
    -- that would be hypothetically reindexed by the automated process.
    -- https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/database/postgres_index.rb#L25-25
    and partitioned is false
    and exclusion is false
    and expression is false
    and type = 'btree'
    and schema != 'gitlab_partitions_dynamic'
    and name !~ '\_ccnew[0-9]*'
    and ondisk_size_bytes > 16384) t
WHERE batch_size_so_far <= (1.5 * 1000 * 1000 * 1000 * 1000); -- batch limit of 1.5 TB