-- This procedure return/insert a batch of indexes, with optimal fill to the maximum batch size considering ondisk_size_bytes
-- The indexes returned should not have been reindexed after a date nor already enqueued

do $$
DECLARE
  upgrade_date CONSTANT VARCHAR DEFAULT '09-09-2023 14:00';
  max_batch_size CONSTANT BIGINT := (1.5 * 1000 * 1000 * 1000 * 1000); -- 1.5 TBytes
   
  v_sum_batch_size BIGINT;
  r_index_record RECORD;
  v_counter INT DEFAULT 0;
BEGIN

  v_sum_batch_size := 0;

  FOR r_index_record IN
    SELECT identifier, ondisk_size_bytes
      from postgres_indexes indx
      where
        -- Skip indexes not yet reindexed nor enqueued (state != 0) after the upgrade
        not exists (
          select 1
          from postgres_reindex_queued_actions reind_q 
          where reind_q.index_identifier = indx.identifier
            and (reind_q.state = 0
            or reind_q.updated_at > TO_TIMESTAMP(upgrade_date, 'DD-MM-YYYY HH24:MI'))
          )
        -- These conditions match our "reindexing support" conditions, so we only reindex indexes
        -- that would be hypothetically reindexed by the automated process.
        -- https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/database/postgres_index.rb#L25-25
        and partitioned is false
        and exclusion is false
        and expression is false
        and type = 'btree'
        and schema != 'gitlab_partitions_dynamic'
        and name !~ '\_ccnew[0-9]*'
        and ondisk_size_bytes > 16384
      order by ondisk_size_bytes desc
  LOOP
    IF (v_sum_batch_size + r_index_record.ondisk_size_bytes) <= max_batch_size
    THEN
      -- INSERT into postgres_reindex_queued_actions(index_identifier, state, created_at) values (r_index_record.identifier, 0, date_trunc('minutes',now() + make_interval(mins => v_counter)));
      RAISE NOTICE '---> QUEUE INDEX: % , KB index SIZE: %, CREATED AT: %', r_index_record.identifier, r_index_record.ondisk_size_bytes/1000, date_trunc('minutes',now() + make_interval(mins => v_counter));
      v_sum_batch_size := v_sum_batch_size + r_index_record.ondisk_size_bytes;
      v_counter := v_counter + 1;
    END IF;
  END LOOP;

  RAISE NOTICE '-----------------------------------------------------------------';
  RAISE NOTICE 'BATCH COUNT: %, GB BATCH SIZE: %', v_counter, v_sum_batch_size/1000/1000/1000;

END; $$;