#!/bin/bash

##########################################################################################################################
## Execute parallel reindexing jobs taken from the postgres_reindex_queued_actions table
##
## REQUIRE: postgres_reindex_queued_actions table populated in the database with batch-reindex-queue-btree-optimal.sql
## REQUIRE: sudo apt-get install parallel
##########################################################################################################################

PARALLEL_LEVEL="4"

REINDEX_JOB_FILE="/tmp/postgres_reindex_queued_actions.out"
JOB_NUM_FILE="/tmp/postgres_reindex_num_jobs"
REINDEX_LOG_FILE="/tmp/postgres_reindex_jobs.log"

### Set the number of parallel processes in the file /tmp/postgres_reindex_num_jobs; this value can be changed during runtime to inform /usr/bin/parallel to use more or less jobs
echo ${PARALLEL_LEVEL} > ${JOB_NUM_FILE}

export DB_PARALLEL_MAINTENANCE_WORKERS="0"
export DB_MAINTENANCE_WORK_MEM_GB="10"

PSQL="/usr/local/bin/gitlab-psql \
  --set ON_ERROR_STOP=on \
  --no-align \
  --quiet \
  --tuples-only \
  --pset footer=off"

$PSQL -c "SELECT index_identifier FROM postgres_reindex_queued_actions WHERE state = 0 ORDER BY MD5(index_identifier)" --output=${REINDEX_JOB_FILE}

/usr/bin/parallel --record-env

run_psql_reindex(){
  /usr/local/bin/gitlab-psql --set ON_ERROR_STOP=on -e -c "set max_parallel_maintenance_workers=${DB_PARALLEL_MAINTENANCE_WORKERS}; set maintenance_work_mem to '${DB_MAINTENANCE_WORK_MEM_GB} GB';" -c "REINDEX INDEX CONCURRENTLY ${1};"
  RETVAL=$?
  if [ $RETVAL -eq 0 ]
  then
    /usr/local/bin/gitlab-psql -c "UPDATE postgres_reindex_queued_actions SET state = 1 WHERE index_identifier = '${1}';"
  fi
  return $RETVAL
}
export -f run_psql_reindex

time /usr/bin/parallel --noswap --env _ --jobs ${JOB_NUM_FILE} -a ${REINDEX_JOB_FILE} --joblog ${REINDEX_LOG_FILE}  run_psql_reindex {}