# UPGRADE POSTGRESQL VERSION

Placeholder for working on playbooks that could be useful for administering the current patroni cluster.

## Setup environment

To run this playbook you need Ansible installed.

Besides, you need ssh access across the server fleet to run replicas upgrade with the `rsync` command tool.
Make sure your ssh-agent has your identity loaded (`ssh-add -L`), since we use
agent forwarding by default (configured in `ansible.cfg`).

The below requirements are needed on the host that executes the consul module.
(If you prefer consul-kv module to delete actual cluster info instead of new cluster creation)

   - python-consul

     `pip2 install python-consul`

  - Change the ssh key directory
    > For GL connection we use a file to connect through a jumper server

    `ansible_ssh_common_args: '-F /home/user/.ssh/gitlab_config'`

## How to run

Firstly you need a proper inventory file to run Ansible on the correct hosts.
To achieve this goal you must edit the `inventory/hosts.yml` file.
The actual file is composed in `yaml` format, you can see details on [Ansible's doc](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)
The hosts are separated into three groups:

- Primary: The group contains only one hostname, the leader instance.
- Secondary: The group contains all the replicas to upgrade.
- excluded_secondary: The group must contain the instances that won't be upgraded, reserved for rollback purposes.

e.g:

```yaml
children:
  gitlab:
    children:
      primary:
        hosts:
          patroni-04-db-gstg.c.gitlab-staging-1.internal:
            port: 5432
      secondary:
        hosts:
          patroni-05-db-gstg.c.gitlab-staging-1.internal:
            port: 5432
          patroni-06-db-gstg.c.gitlab-staging-1.internal:
            port: 5432
      excluded_secondary:
        hosts:
          patroni-01-db-gstg.c.gitlab-staging-1.internal:
            port: 5432
          patroni-02-db-gstg.c.gitlab-staging-1.internal:
            port: 5432
          patroni-03-db-gstg.c.gitlab-staging-1.internal:
            port: 5432
```

In the example above the groups `primary`, `secondary`, and `excluded_secondary` belong to gitlab group.

To run the upgrade you only will need run `upgrade.yml` playbook:

```
ANSIBLE_LIBRARY=$(pwd)/modules UPGRADE_ENV=test CI_API_V4_URL=https://ops.gitlab.net/api/v4 ../bin/ansible-wrapper.sh playbooks/upgrade.yml --ask-vault-pass -e "user=gitlab-psql database=gitlabhq_production port=5432 pg_old_version=9.6 pg_new_version=11" --step -vvv
```
For custom ansible settings

```
ANSIBLE_LIBRARY=$(pwd)/modules ANSIBLE_CONFIG="my_ansible.cfg" ansible-playbook playbooks/upgrade.yml --ask-vault-pass -i inventory/hosts.yml \
     -e "user=gitlab-psql database=gitlabhq_production port=5432  \
     pg_old_version=9.6 pg_new_version=11" [-v[vvvv]] --step [-C]
```
> Note: The step command argument is necessary to run the migration as required.

