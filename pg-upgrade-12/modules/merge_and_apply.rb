#!/usr/bin/env ruby
# From: https://gitlab.com/gitlab-com/gl-infra/ansible-migrations
require 'json'
require 'net/http'

DOCUMENTATION = <<-DOC
module: merge_and_apply
short_description: Merges a chef-repo merge request and applies it to Chef
options:
  mr_iid:
    description: Merge request number
    required: yes
    type: int
  api_token:
    description: ops.gitlab.net API token that can do merges and run pipelines
    required: yes
    type: str
  is_production:
    description: Controls whether to run `apply_to_prod` task on the merge pipeline
    required: no
    type: bool
  apply_timeout:
    description: How many seconds to wait for both the merge and the apply operations before failing
    required: no
    type: int
    default: 300
DOC

class Api
  OPS_API_BASE_URL = 'https://ops.gitlab.net/api/v4'

  def initialize(api_token)
    @api_token = api_token
  end

  def get(path)
    do_request(path, :get)
  end

  def post(path)
    do_request(path, :post)
  end

  def put(path)
    do_request(path, :put)
  end

  private

  def do_request(path, method)
    uri = URI("#{OPS_API_BASE_URL}#{path}")
    req = Net::HTTP.const_get(method.capitalize).new(uri)

    req['Private-Token'] = @api_token

    http = Net::HTTP.start(uri.host, uri.port, use_ssl: true)
    resp = http.request(req)

    raise "Unsuccessful request. Response: #{resp.inspect}" unless resp.code =~ /20./

    JSON.parse(resp.body)
  end
end

class MergeRequest
  attr_reader :attributes

  def initialize(project, iid, api)
    @api              = api
    @project          = project
    @project_api_path = "/projects/#{project}"
    @mr_api_path      = "#{@project_api_path}/merge_requests/#{iid}"
    @attributes       = {}
  end

  def mergeable?
    refresh! if attributes.empty?

    attributes['merge_status'] == 'can_be_merged'
  end

  def merged?
    refresh! if attributes.empty?

    attributes['state'] == 'merged'
  end

  def merge!
    return if merged?

    @attributes = @api.put("#{@mr_api_path}/merge")
  end

  def wait_for_apply_job(is_production, timeout)
    job_name = is_production ? 'apply_to_prod' : 'apply_to_staging'

    start_time = Time.now

    while (Time.now - start_time) < timeout
      begin
        job = merge_commit_pipeline.find_job_with_name(job_name)

        job.refresh!
        job.trigger! if is_production && job.manual?

        if job.finished?
          raise 'Apply job finished unsuccessfully' unless job.success?

          return
        end
      rescue Pipeline::PipelineNotFound
        # Merge pipeline may not be available immediately after merge
      end

      sleep 10
    end

    raise "Apply job didn't finish in time"
  end

  private

  def refresh!
    @attributes = @api.get(@mr_api_path)
  end

  def merge_commit_pipeline
    Pipeline.new(@project, attributes['merge_commit_sha'], @api)
  end
end

class Pipeline
  PipelineNotFound = Class.new(StandardError)

  def initialize(project, sha, api)
    @api              = api
    @project          = project
    @sha              = sha
    @project_api_path = "/projects/#{project}"
    @attributes       = {}
  end

  def id
    refresh! if @attributes.empty?

    @attributes['id']
  end

  def find_job_with_name(job_name)
    jobs = @api.get("#{@project_api_path}/pipelines/#{id}/jobs")

    job = jobs.find { |job| job['name'] == job_name }
    raise "Job '#{job_name}' was not found for pipeline ##{id}" unless job

    PipelineJob.new(@project, job['id'], @api)
  end

  private

  def refresh!
    pipeline = @api.get("#{@project_api_path}/pipelines?sha=#{@sha}").first
    raise(PipelineNotFound, "Pipeline for SHA #{@sha} was not found") unless pipeline

    @attributes = pipeline
  end
end

class PipelineJob
  def initialize(project, id, api)
    @api              = api
    @id               = id
    @project_api_path = "/projects/#{project}"
    @attributes       = {}
  end

  def trigger!
    @api.post("#{@project_api_path}/jobs/#{@id}/play")
  end

  def finished?
    !@attributes['finished_at'].nil?
  end

  def manual?
    @attributes['status'] == 'manual'
  end

  def success?
    @attributes['status'] == 'success'
  end

  def refresh!
    @attributes = @api.get("#{@project_api_path}/jobs/#{@id}")
  end
end

def main
  json_args = <<-STR
<<INCLUDE_ANSIBLE_MODULE_JSON_ARGS>>
  STR

  data = JSON.parse(json_args)

  mr_iid        = data['mr_iid']
  api_token     = data['api_token']
  apply_timeout = data['apply_timeout'] || 5 * 60
  is_production = !!data['is_production']

  api = Api.new(api_token)
  mr  = MergeRequest.new('gitlab-cookbooks%2Fchef-repo', mr_iid, api)

  raise 'Unmeregable merge request' unless mr.mergeable?

  unless data['_ansible_check_mode']
    mr.merge!
    mr.wait_for_apply_job(is_production, apply_timeout)
  end

  print JSON.dump({merge_request: mr.attributes})
rescue => e
  print JSON.dump({
    failed: true,
    message: e.message,
    stack_trace: e.backtrace,
    data: data
  })

  exit 1
end

main
