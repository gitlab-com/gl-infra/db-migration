# Patroni Cluster OS Upgrade Playbook

## How to use the Ansible Playbook to perform an upgrade or rollback

Add this to your ssh config (the cut command from your onboarding in your existing command doesn't work well for the db-onboarding bastion):
```# db-benchmarking boxes
Host *.gitlab-db-benchmarking.internal
        PreferredAuthentications        publickey
        ProxyCommand                    ssh lb-bastion.db-benchmarking.gitlab.com -W %h:%p
	ForwardAgent yes
```

We have a wrapper script for executing the ansible playbooks which can be used as follows:

```
Usage: ./bin/ansible-exec.sh [-e ENVIRONMENT] [-C CLUSTER(main/ci)] [-p PLAYBOOK (default:os_upgrade)] [-h HOSTNAME (OPTIONAL)] [-t TAGS1,TAGS2 (default: all)] [-s (Step)] [-c (Check Mode)] [-v (Verbose)]
```

For example: trigger the `os_upgrade` playbook for `main` cluster on the `db-benchmarking` environment without filtering for any particular host or tag

```
bin/ansible-exec.sh -e db-benchmarking -C main
```

You can execute the playbook on the `CI` cluster by setting the flag -C ci

```
bin/ansible-exec.sh -e db-benchmarking -C ci
```

### Inventory Setup for Upgrade/Rollback 
The default `os_upgrade` playbook referenced in the help usage can be used to perform an upgrade (ie. from version X to Y) and a rollback (from version Y to version X). The logic for an upgrade & rollback is exactly the same so the one playbook is used for both operations. The only difference lies in the inventory and what hosts belong to the source & target groups. For example: in db-benchmarking, for the `main` cluster if we're upgrading from 16.04 to 20.04, our inventory looks like this:

```
---
all:
  vars:
    patroni_cluster: main
  children:
    patroni-source:
      vars:
        os_version: "1604"
      hosts:
        patroni-main-pg12-1604-0[1:2]-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
    patroni-target:
      vars:
        os_version: "2004"
      hosts:
        patroni-main-pg12-2004-0[1:2]-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
```

If our intention is to rollback, you would update the inventory to look like this:

```
all:
  vars:
    patroni_cluster: main
  children:
    patroni-source:
      vars:
        os_version: "2004"
      hosts:
        patroni-main-pg12-2004-0[1:2]-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
    patroni-target:
      vars:
        os_version: "1604"
      hosts:
        patroni-main-pg12-1604-0[1:2]-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
```

As you can see above, the only change required is to ensure that all the source hosts become target hosts and vice-versa.

- You can test the inventory by making sure that Ansible can talk to hosts:
      - CI Cluster
      ```sh
      ENV=db-benchmarking # gstg or gprd
      CLUSTER=ci
      cd ~/src/db-migration/pg-os-upgrade
      ansible -i inventory/${ENV}-${CLUSTER}.yml all -m ping
      ```
      - Main Cluster
      ```sh
      ENV=db-benchmarking # gstg or gprd
      CLUSTER=main
      cd ~/src/db-migration/pg-os-upgrade
      ansible -i inventory/${ENV}-${CLUSTER}.yml all -m ping
      ```
      You shouldn't see any failed hosts!

## Setup environment in db-benchmarking

When the Patroni instances are created by Terraform, they will have the data snapshot mounted and they will run chef client, which installs the right software/users/etc on them. They will not have the Patroni clusters configured as per the pre-upgrade state so you would not be able to run the main playbook on them just yet.

Once the VMs have finished running `chef-client`, you can run the Ansible playbook called `setup_environment` to configure the Patroni clusters in the pre-upgrade state so you can then run the `main` playbook:

```
export GCP_ENV=db-benchmarking 
ansible-playbook -i inventory/db-benchmarking-main.yml playbooks/setup_environment.yml
```

You should check the 4x clusters to ensure they are configured correctly (ie. every cluster should have one replica, the 16.04 main cluster should be the leader, and the other 3 clusters should have a standby leader).

You are now ready to run the `main` playbook.

## Graphical representation of Ansible playbooks

Since this playbook includes many tasks/roles, it can be useful to create a graphical representation of the playbook. You can install [`ansible-playbook-grapher`](https://github.com/haidaraM/ansible-playbook-grapher) for this purpose to generate a `.svg` file and it even has clickable links:

For example:

```
ansible-playbook-grapher -i inventory/db-benchmarking.yml --include-role-tasks --open-protocol-handler vscode -o /tmp/main playbooks/main.yml
```
