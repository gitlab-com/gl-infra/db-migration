# Ansible 2.9
# Control SQL Traffic to ensure Replica writings are up to date.
---
- name: Create backup folder if it doesn't exist
  file:
    path: "{{ backup_path }}"
    state: directory
    mode: '0755'
  tags:
    - close-access
    - backup-path

- name: Backup original hba file
  copy:
    src: "{{ pg_datadir }}/pg_hba.conf"
    dest: "{{ backup_path }}/pg_hba.conf-{{ inventory_hostname }}"
    remote_src: true
    mode: preserve
  tags:
    - close-access
    - backup-hba

- name: Closing SQL traffic with new hba file
  template:
    src: pg_hba.conf.closed.j2
    dest: "{{ pg_datadir }}/pg_hba.conf"
    owner: "{{ os_pg_user }}"
    group: "{{ os_pg_user }}"
    mode: '0600'
    backup: true
  register: close_hba
  tags:
    - close-access
    - replace-hba

- name: "Reload PostgreSQL after changing hba file"
  shell: "gitlab-patronictl reload {{ patroni_cluster_name_fact }} {{ inventory_hostname }} --force"
  when: close_hba.changed
  tags:
    - patroni-reload
    - close-access

- name: waiting for CHECKPOINT to complete before stopping postgresql
  become: true
  become_user: root
  command: "gitlab-psql -c 'CHECKPOINT'"
  register: checkpoint_result
  until: checkpoint_result.rc == 0
  retries: 60
  delay: 10
  tags:
    - close-access
    - db-checkpoint

- name: Get database name
  command:
    "gitlab-psql -tAXc
    \"
      SELECT current_database();
    \""
  register: db_name
  tags:
    - close-access
    - get-db-name

- debug:
    var: db_name.stdout

- name: Terminate active connections
  command:
    "gitlab-psql -d postgres
     -tAXc
      \"
        SELECT pg_terminate_backend(pid)
        FROM pg_stat_activity WHERE datname = '{{ db_name.stdout }}';
      \""
  tags:
    - close-access
    - check-active-conn

- name: Loop until there are no active connections
  command:
    "gitlab-psql -d postgres
     -tAXc
      \"
        select count(*) from pg_stat_activity
        where backend_type = 'client backend'
        and state <> 'idle' and pid <> pg_backend_pid()
        and datname = '{{ db_name.stdout }}';
      \""
  register: activity
  until: activity.stdout == "0"
  retries: 60
  delay: 2
  tags:
    - close-access
    - check-active-conn
