#!/bin/bash
# Run amcheck (function bt_index_parent_check) on btree indexes
# the number of parallel workers is defined using JOBS_TOTAL variable
# Each worker process runs in the background (for gitlab-psql user) using nohup option
# Each worker process will log its status in /var/tmp/amcheck-worker-<job_number>.log file

OFFSET="${OFFSET:-}"
if test -n "${OFFSET}"; then
  OFFSET=" OFFSET ${OFFSET}"
fi

LIMIT="${LIMIT:-}"
if test -n "${LIMIT}"; then
  LIMIT=" LIMIT ${LIMIT}"
fi

JOBS_TOTAL=${JOBS_TOTAL:-96}  # number of parallel workers
job_number=1
# echo $JOBS_TOTAL
sudo -u gitlab-psql  gitlab-psql -XAtc "create extension if not exists amcheck"
if [[ 't' == $(sudo -u gitlab-psql gitlab-psql -XAtc "select installed_version is not null from pg_available_extensions where name = 'amcheck'") ]]; then
  echo "Extension created: amcheck"
  sudo -u gitlab-psql  gitlab-psql -XAtc "select * from pg_available_extensions where name ~ 'amcheck'"
else
  echo "Could not create extension amcheck"
  exit 12
fi

for (( job_number=1; job_number<=$JOBS_TOTAL; job_number++ ))
do
	# echo $job_number

sql_to_run="
SET statement_timeout = 0;
do \$\$
declare
  r record;
  sql text;
  ts_pre timestamptz;
  e_message text;
  e_detail text;
  e_context text;
  e_hint text;
  errcount int := 0;
begin
  raise info 'begin!...';

  for r in
with recs as (
  select
      row_number() over(order by tc.reltuples) as i,
      count(*) over() as cnt,
      c.oid,
      i.indisunique,
      c.relname,
      c.relpages::int8,
      tc.reltuples::int8 as tuples,
      tc.relnamespace,
      tc.relname tc_relname
    from pg_index i
    join pg_opclass op on i.indclass[0] = op.oid
    join pg_am am on op.opcmethod = am.oid
    join pg_class c on i.indexrelid = c.oid
    join pg_class tc on i.indrelid = tc.oid
    join pg_namespace n on c.relnamespace = n.oid
    where
      am.amname = 'btree'
      --and n.nspname = 'public'
      and c.relpersistence <> 't' -- don't check temp tables
      and c.relkind = 'i'
      and i.indisready
      and i.indisvalid
      -- and tc.relname = 'projects' -- comment this out to check the whole DB
      -- and ('x' || lpad(md5(tc.relnamespace::text || c.relname::text), 8, '0'))::bit(32)::int8 % ${JOBS_TOTAL} = ${job_number} - 1
      -- and tc.reltuples::int8 > 1667681920 -- filter by table size
      -- order by tc.reltuples OFFSET XXXX LIMIT YYYY -- filter by OFFSET and/or LIMIT
     order by tc.reltuples${OFFSET}${LIMIT}
    )
    SELECT
      i,
      count(*) over() as cnt,
      oid,
      indisunique,
      relname,
      relpages,
      tuples,
      relnamespace,
      tc_relname
from recs
where
      -- define the scope of work for worker/thread number $job_number; we are splitting  work at an index level
	 ('x' || lpad(md5(relnamespace::text || relname::text), 8, '0'))::bit(32)::int8 % ${JOBS_TOTAL} = ${job_number} - 1
      -- (you could split it at table level using tc_relname to have all the indexes belonging to one table are processed by the same worker)
  loop
    ts_pre := clock_timestamp();
    raise info '[%] Processing %/%: index: % (index relpages: %; heap tuples: ~%)...',
      ts_pre::timestamptz(3), r.i, r.cnt, r.relname, r.relpages, r.tuples;

    begin
      perform bt_index_parent_check(index => r.oid, heapallindexed => true);
       -- you could create amcheck_parallel_status table and insert amcheck parallel worker status
       -- insert into amcheck_parallel_status   values (${job_number}, r.i,r.cnt,r.oid,r.indisunique,r.relname,r.relpages,r.tuples,r.relnamespace,r.tc_relname, now());
      raise info '[%] SUCCESS %/% – index: %. Time taken: %',
        clock_timestamp()::timestamptz(3), r.i, r.cnt, r.relname, (clock_timestamp() - ts_pre);
    exception when others then
      get stacked diagnostics
        e_message = message_text,
        e_detail = pg_exception_detail,
        e_context = pg_exception_context,
        e_hint = pg_exception_hint;

      errcount := errcount + 1;

      raise warning \$err\$[%] FAILED %/% – index: %.
ERROR: %
CONTEXT: %
DETAIL: %
HINT: %
\$err\$,
        clock_timestamp()::timestamptz(3), r.i, r.cnt, r.relname, e_message, e_detail, e_context, e_hint;
    end;
  end loop;

  if errcount = 0 then
    raise info 'Scanning successfully finished. 0 errors.';
  else
    raise exception 'Index corruption detected, % errors, see details in the log.', errcount;
  end if;
end \$\$;

    "
  nohup sudo -u gitlab-psql gitlab-psql -XAtc "${sql_to_run}" >> "/var/tmp/amcheck-worker-${job_number}.log" 2>&1 &
done
