----------------------------------------------------------------------------------------
-- Reindex
--
----------------------------------------------------------------------------------------

-- check current parameter values
set statement_timeout to 0;
show maintenance_work_mem;
show max_parallel_maintenance_workers;
show max_parallel_workers;
-- set different parameter values 
set maintenance_work_mem to '30 GB';
set max_parallel_maintenance_workers to 32; 
set max_parallel_workers to 32;

-- check changed  parameter values
show maintenance_work_mem;
show max_parallel_maintenance_workers;
show max_parallel_workers;

do $$
declare
  r record;
  sql text;
  ts_pre timestamptz;
  e_message text;
  e_detail text;
  e_context text;
  e_hint text;
  errcount int := 0;
begin
  raise info 'begin!...';

  for r in
    -- Complete list of all the invalid btree indexes. 
    -- select unnest(array[ 'index_topics_on_lower_name','index_topics_on_name', 'index_snippets_on_project_id_and_title', 'index_tags_on_name', 'index_wiki_page_slugs_on_slug_and_wiki_page_meta_id', 'index_milestones_on_title', 'index_merge_request_diff_commit_users_on_name_and_email', 'index_emails_on_email', 'index_users_on_name', 'index_on_users_name_lower', 'index_on_namespaces_lower_name', 'index_namespaces_name_parent_id_type', 'index_namespaces_public_groups_name_id', 'index_projects_api_name_id_desc', 'index_projects_on_name_and_id', 'index_projects_on_lower_name', 'index_merge_requests_on_source_branch', 'unique_projects_on_name_namespace_id']) as i

    -- Reindex the following indexes
    select unnest(array[ 'index_merge_requests_on_source_branch']) as i
  loop
    ts_pre := clock_timestamp();
    raise info '[%] Processing : index: % ...',
      ts_pre::timestamptz(3), r.i;

    begin
 	EXECUTE format('REINDEX index %I', r.i);
      raise info '[%] SUCCESS  – index: %. Time taken: %',
        clock_timestamp()::timestamptz(3),  r.i, (clock_timestamp() - ts_pre);
    exception when others then
      get stacked diagnostics
        e_message = message_text,
        e_detail = pg_exception_detail,
        e_context = pg_exception_context,
        e_hint = pg_exception_hint;

      errcount := errcount + 1;

      raise warning $err$[%] FAILED  – index: %.
ERROR: %
CONTEXT: %
DETAIL: %
HINT: %
$err$,
        clock_timestamp()::timestamptz(3),  r.i, e_message, e_detail, e_context, e_hint;
    end;
  end loop;

  if errcount = 0 then
    raise info 'Reindex successfully finished. 0 errors.';
  else
    raise exception 'Reindex failed, % errors, see details in the log.', errcount;
  end if;
end $$;

