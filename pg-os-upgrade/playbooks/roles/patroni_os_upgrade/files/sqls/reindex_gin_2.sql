set statement_timeout to 0;
set maintenance_work_mem to '30 GB';
set max_parallel_maintenance_workers to 20;
set max_parallel_workers to 20;

show maintenance_work_mem;
show max_parallel_maintenance_workers;
show max_parallel_workers;
\set ECHO queries

\timing
select now()  ;

REINDEX INDEX  public.index_namespaces_on_traversal_ids;

select now()  ;
