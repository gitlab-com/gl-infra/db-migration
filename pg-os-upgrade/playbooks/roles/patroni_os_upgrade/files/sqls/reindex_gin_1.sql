set statement_timeout to 0;
set maintenance_work_mem to '30 GB';
set max_parallel_maintenance_workers to 20;
set max_parallel_workers to 20;

show maintenance_work_mem;
show max_parallel_maintenance_workers;
show max_parallel_workers;
\set ECHO queries

\timing
select now()  ;

REINDEX INDEX  public.index_gin_ci_namespace_mirrors_on_traversal_ids;
REINDEX INDEX  public.index_gin_ci_pending_builds_on_namespace_traversal_ids;
REINDEX INDEX  public.index_namespaces_on_traversal_ids_for_groups;
REINDEX INDEX  public.index_vulnerability_occurrences_on_location_k8s_agent_id;
REINDEX INDEX  public.index_vulnerability_occurrences_on_location_k8s_cluster_id;

select now()  ;
