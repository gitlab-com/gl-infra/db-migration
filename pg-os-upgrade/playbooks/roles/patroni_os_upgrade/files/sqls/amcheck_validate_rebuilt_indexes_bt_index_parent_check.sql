----------------------------------------------------------------------------------------
-- Check btree indexes (amcheck)
--
-- WARNING: bt_index_parent_check(..) used here will block INSERT/UPDATE/DELETE!
----------------------------------------------------------------------------------------

-- check current parameter values
set statement_timeout to 0;

do $$
declare
  r record;
  sql text;
  ts_pre timestamptz;
  e_message text;
  e_detail text;
  e_context text;
  e_hint text;
  errcount int := 0;
begin
  raise info 'begin!...';

  for r in
    -- The following indexes were rebuilt. Run amcheck validation against these indexes 
     select unnest(array['index_topics_on_lower_name', 'index_topics_on_name', 'index_snippets_on_project_id_and_title', 'index_tags_on_name', 'index_wiki_page_slugs_on_slug_and_wiki_page_meta_id', 'index_milestones_on_title', 'index_merge_request_diff_commit_users_on_name_and_email', 'index_emails_on_email', 'index_users_on_name', 'index_on_users_name_lower', 'index_on_namespaces_lower_name', 'index_namespaces_name_parent_id_type', 'index_namespaces_public_groups_name_id', 'index_projects_api_name_id_desc', 'index_projects_on_name_and_id', 'index_projects_on_lower_name', 'index_merge_requests_on_source_branch', 'unique_projects_on_name_namespace_id']) as i

  loop
    ts_pre := clock_timestamp();
    raise info '[%] Processing : index: % ...',
      ts_pre::timestamptz(3), r.i;

    begin
 	perform bt_index_parent_check(index => r.i, heapallindexed => true);
      raise info '[%] SUCCESS  – index: %. Time taken: %',
        clock_timestamp()::timestamptz(3),  r.i, (clock_timestamp() - ts_pre);
    exception when others then
      get stacked diagnostics
        e_message = message_text,
        e_detail = pg_exception_detail,
        e_context = pg_exception_context,
        e_hint = pg_exception_hint;

      errcount := errcount + 1;

      raise warning $err$[%] FAILED  – index: %.
ERROR: %
CONTEXT: %
DETAIL: %
HINT: %
$err$,
        clock_timestamp()::timestamptz(3),  r.i, e_message, e_detail, e_context, e_hint;
    end;
  end loop;

  if errcount = 0 then
    raise info 'amcheck successfully finished. 0 errors.';
  else
    raise exception 'amcheck failed, % errors, see details in the log.', errcount;
  end if;
end $$;

