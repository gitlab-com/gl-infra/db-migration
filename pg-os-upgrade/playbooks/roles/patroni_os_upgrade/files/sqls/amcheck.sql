----------------------------------------------------------------------------------------
-- Check btree indexes (amcheck)
--
-- WARNING: bt_index_parent_check(..) used here will block INSERT/UPDATE/DELETE!
----------------------------------------------------------------------------------------

create extension if not exists amcheck;
set statement_timeout to 0;

do $$
declare
  r record;
  sql text;
  ts_pre timestamptz;
  e_message text;
  e_detail text;
  e_context text;
  e_hint text;
  errcount int := 0;
begin
  raise info 'begin!...';

  for r in
    select
      row_number() over(order by tc.reltuples) as i,
      count(*) over() as cnt,
      c.oid,
      i.indisunique,
      c.relname,
      c.relpages::int8,
      tc.reltuples::int8 as tuples
    from pg_index i
    join pg_opclass op on i.indclass[0] = op.oid
    join pg_am am on op.opcmethod = am.oid
    join pg_class c on i.indexrelid = c.oid
    join pg_class tc on i.indrelid = tc.oid
    join pg_namespace n on c.relnamespace = n.oid
    where
      am.amname = 'btree'
      --and n.nspname = 'public'
      and c.relpersistence <> 't' -- don't check temp tables
      and c.relkind = 'i'
      and i.indisready
      and i.indisvalid
      --and tc.relname = 'projects' -- comment this out to check the whole DB
    order by tc.reltuples
  loop
    ts_pre := clock_timestamp();
    raise info '[%] Processing %/%: index: % (index relpages: %; heap tuples: ~%)...',
      ts_pre::timestamptz(3), r.i, r.cnt, r.relname, r.relpages, r.tuples;

    begin
      perform bt_index_parent_check(index => r.oid, heapallindexed => true);
      raise info '[%] SUCCESS %/% – index: %. Time taken: %',
        clock_timestamp()::timestamptz(3), r.i, r.cnt, r.relname, (clock_timestamp() - ts_pre);
    exception when others then
      get stacked diagnostics
        e_message = message_text,
        e_detail = pg_exception_detail,
        e_context = pg_exception_context,
        e_hint = pg_exception_hint;

      errcount := errcount + 1;

      raise warning $err$[%] FAILED %/% – index: %.
ERROR: %
CONTEXT: %
DETAIL: %
HINT: %
$err$,
        clock_timestamp()::timestamptz(3), r.i, r.cnt, r.relname, e_message, e_detail, e_context, e_hint;
    end;
  end loop;

  if errcount = 0 then
    raise info 'Scanning successfully finished. 0 errors.';
  else
    raise exception 'Index corruption detected, % errors, see details in the log.', errcount;
  end if;
end $$;

