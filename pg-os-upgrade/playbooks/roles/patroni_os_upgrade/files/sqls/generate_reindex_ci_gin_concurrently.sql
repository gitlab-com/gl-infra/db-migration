-- Generate a SQL script to reindex GIN indexes using the CONCURRENTLY option. 
-- The generated SQL script will not include the GIN indexes that were reindexed during scheduled downtime for the 20.04 upgrade.
   select 'select now() as "Reindex Start Time" ; ' ;
   select
	'REINDEX INDEX CONCURRENTLY '  || n.nspname || '.' || c.relname  || ';'
    from pg_index i
    join pg_opclass op on i.indclass[0] = op.oid
    join pg_am am on op.opcmethod = am.oid
    join pg_class c on i.indexrelid = c.oid
    join pg_class tc on i.indrelid = tc.oid
    join pg_namespace n on c.relnamespace = n.oid
    where
          am.amname = 'gin'
      and c.relpersistence <> 't' -- don't check temp tables
      and c.relkind = 'i'
      and i.indisready
      and i.indisvalid
      and c.relname  NOT IN (
		'index_gin_ci_namespace_mirrors_on_traversal_ids',
		'index_gin_ci_pending_builds_on_namespace_traversal_ids',
		'index_namespaces_on_traversal_ids_for_groups',
		'index_vulnerability_occurrences_on_location_k8s_agent_id',
		'index_vulnerability_occurrences_on_location_k8s_cluster_id',
		'index_namespaces_on_traversal_ids'
			)
      and tc.relname ~ '(ci_.*|external_pull_requests|taggings|tags)'
    order by tc.reltuples  
;
   select 'select now() as "Reindex End Time" ; ' ;
