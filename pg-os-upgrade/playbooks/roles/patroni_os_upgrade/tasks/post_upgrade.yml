---
- name: "[Post-Upgrade] Validate operation is set to a valid value"
  assert:
    that:
      - operation is defined
      - operation in ('promote', 'demote')
    fail_msg: "'operation' must be one of: promote, demote"
    success_msg: "operation is {{ operation|default() }}"
  tags:
    - post-upgrade

- name: "[Post-Upgrade] Print patroni node role"
  debug:
    var: patroni_node_role
  tags:
    - post-upgrade

- name: "include_role : patroni_os_upgrade :: upgrade_consul_configs"
  vars:
    pgbouncer_consul_sn: "{{ pgbouncer_consul_service_name[ gcp_env_fact ][ patroni_cluster ] }}"
    patroni_consul_sn: "{{ patroni_consul_service_name[ gcp_env_fact ][ patroni_cluster ] }}"
    source_suffix: "{{ '-' + os_version if operation == 'promote' else '' }}"
    target_suffix: "{{ '-' + os_version if operation == 'demote' else '' }}"
    pgb_consul_source_path: "{{ consul_cfg_path }}{{ pgbouncer_consul_sn }}{{ source_suffix }}"
    pgb_consul_target_path: "{{ consul_cfg_path }}{{ pgbouncer_consul_sn }}{{ target_suffix }}"
  include_role:
    name: patroni_os_upgrade
    tasks_from: upgrade_consul_configs
  tags:
    - consul-setup
    - post-upgrade

- name: "[Post-Upgrade] Test PostgreSQL DB Access"
  command: 'gitlab-psql -tAXc "SELECT pg_is_in_recovery();"'
  register: recovery
  tags:
    - test-db-access
    - post-upgrade
    - primary-check

- name: "[Post-Upgrade] Get PostgreSQL Version"
  command: 'gitlab-psql -tAXc "SELECT version();"'
  register: version
  tags:
    - test-db-access
    - post-upgrade

- name: "[Post-Upgrade] Print PostgreSQL installed version"
  debug:
    msg: PostgreSQL version installed - {{ version.stdout }}
  tags:
    - post-upgrade
    - postgres-version

- name: "[Post-Upgrade] Check local connections creating a table"
  command: 'gitlab-psql -tAXc
    "
    DROP TABLE IF EXISTS test_replication;
    CREATE TABLE test_replication AS SELECT generate_series(1, 100*100);
    "'
  register: connections
  when:
    - patroni_node_role == "master"
  tags:
    - create-test-table
    - post-upgrade

- name: "[Post-Upgrade] Ensure replicas are in sync"
  when: patroni_node_role == "replica"
  tags:
    - check-replication
    - post-upgrade
  block:
    - name: "[Post-Upgrade] Wait until the PostgreSQL replicas are synchronized"
      command: 'gitlab-psql -tAXc
        "
        SELECT
        pg_wal_lsn_diff(pg_current_wal_lsn(), replay_lsn) max_lag_bytes
        FROM
        pg_stat_replication
        ORDER BY
        max_lag_bytes DESC
        LIMIT 1;
        "'
      register: max_lag_bytes
      until: max_lag_bytes.stdout|int < 5242880
      retries: 300
      delay: 2
      changed_when: false

    - name: "[Post-Upgrade] Print max_lag_bytes"
      debug:
        var: max_lag_bytes

    - name: "[Post-Upgrade] Ensure the right amount of records in the test_replication table"
      command: 'gitlab-psql -tAXc
        "
        SELECT COUNT(*) FROM test_replication;
        "'
      register: count_test
      until: count_test.stdout == '10000'
      delay: 2
      retries: 60
