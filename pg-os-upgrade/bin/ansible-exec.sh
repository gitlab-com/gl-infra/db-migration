#!/bin/bash

# Echo usage if something isn't right.
usage() {
    echo "Usage: $0 [-e ENVIRONMENT] [-C CLUSTER] [-p PLAYBOOK (DEFAULT:os_upgrade)] [-h HOSTNAME (OPTIONAL)] [-t TAGS1,TAGS2 (default: all)] [-k TAGS1,TAGS2] [-a (START_AT_TASK)] [-s (Step)] [-c (Check Mode)] [-v]" 1>&2; exit 1;
}

# Default variables
PLAYBOOK=os_upgrade
EXEC=ansible-playbook

while getopts e:C:p:h:t:k:a:scvz flag
do
    case "${flag}" in
        e) export GCP_ENV=${OPTARG};;
        C) CLUSTER=${OPTARG};;
        p) PLAYBOOK=${OPTARG};;
        h) GCP_HOST=${OPTARG};;
        t) TAGS=${OPTARG};;
        k) SKIP_TAGS=${OPTARG};;
        a) START_AT_TASK=${OPTARG};;
        s) STEP='true';;
        c) CHECKMODE='true';;
        v) VERBOSE='true';;
        z) EXEC="echo ansible-playbook";;
        :)
            echo "ERROR: Option -$OPTARG requires an argument"
            usage
            ;;
        \?)
            echo "ERROR: Invalid option -$OPTARG"
            usage
            ;;
    esac
done
shift "$((OPTIND - 1))"

# Check required switches exist
if [ -z "${GCP_ENV}" ]; then
    echo "-e Env is mandatory"
    usage
fi
if [ -z "${CLUSTER}" ]; then
    echo "-e Cluster is mandatory"
    echo "Options: main/ci"
    usage
fi

# Ansible playbook and environment
ansible_extra_opts=("playbooks/${PLAYBOOK}.yml" "--inventory=inventory/${GCP_ENV}-${CLUSTER}.yml")

# Ansible extra options
if [[ "$CHECKMODE" == "true" ]]; then
  ansible_extra_opts+=("--check")
fi

if [[ "$STEP" == "true" ]]; then
  ansible_extra_opts+=("--step")
fi

if [[ "$VERBOSE" == "true" ]]; then
  ansible_extra_opts+=("-vvvvv")
fi

# Limit to Host if defined
if [ -n "$GCP_HOST" ]; then
  ansible_extra_opts+=("--limit ${GCP_HOST}")
fi

# Add tags if defined
if [ -n "$TAGS" ]; then
  ansible_extra_opts+=("--tags ${TAGS}")
fi

# Skip tags if defined
if [ -n "$SKIP_TAGS" ]; then
  ansible_extra_opts+=("--skip-tags ${SKIP_TAGS}")
fi

# Start at Task
if [ -n "$START_AT_TASK" ]; then
  ansible_extra_opts+=("--start-at-task ${START_AT_TASK}")
fi

export ANSIBLE_LOG_PATH="$HOME/ansible-${CLUSTER}-$(date +%Y%m%d%H%M%S).log"
${EXEC} ${ansible_extra_opts[@]} $@
