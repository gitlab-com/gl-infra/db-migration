CREATE OR REPLACE FUNCTION postgres_exporter.f_select_pg_stat_replication()
  RETURNS SETOF pg_catalog.pg_stat_replication                                         
  LANGUAGE sql                                                              
  SECURITY DEFINER                                                          
 AS $BODY$                                                             
   SELECT * from pg_catalog.pg_stat_replication;                            
 $BODY$;

CREATE VIEW "postgres_exporter"."pg_stat_replication" AS
 SELECT *
   FROM "postgres_exporter"."f_select_pg_stat_replication"() ;
   
 CREATE OR REPLACE FUNCTION public.f_pg_stat_wal_receiver()          
  RETURNS SETOF pg_stat_wal_receiver                                 
  LANGUAGE sql                                                       
  SECURITY DEFINER                                                   
 AS $BODY$
 select * from pg_catalog.pg_stat_wal_receiver
 $BODY$;

CREATE VIEW "postgres_exporter"."pg_stat_wal_receiver" AS
 SELECT *
   FROM "public"."f_pg_stat_wal_receiver"() ;
  
GRANT ALL PRIVILEGES ON postgres_exporter.pg_stat_replication TO postgres_exporter;
GRANT ALL PRIVILEGES ON postgres_exporter.pg_stat_wal_receiver TO postgres_exporter;

CREATE OR REPLACE FUNCTION public.pg_last_xlog_replay_location()
 RETURNS pg_lsn AS
 $BODY$
 SELECT pg_last_wal_replay_lsn();
 $BODY$
 LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION public.pg_current_xlog_insert_location()
 RETURNS pg_lsn AS
 $BODY$
 SELECT  pg_current_wal_insert_lsn();
 $BODY$
 LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION public.pg_current_xlog_location() 
 RETURNS pg_lsn AS $$
 SELECT pg_current_wal_lsn();
 $$ LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION public.pg_xlogfile_name(pg_lsn) 
 RETURNS text AS $$
 SELECT pg_walfile_name($1);
 $$ LANGUAGE SQL STABLE;

