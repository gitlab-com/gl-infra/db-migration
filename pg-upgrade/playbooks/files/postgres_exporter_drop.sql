DROP VIEW IF EXISTS postgres_exporter.pg_stat_wal_receiver;
DROP VIEW IF EXISTS postgres_exporter.pg_stat_replication;
DROP FUNCTION IF EXISTS public.f_pg_stat_wal_receiver();
DROP FUNCTION IF EXISTS postgres_exporter.f_select_pg_stat_replication();