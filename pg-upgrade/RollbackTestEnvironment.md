## Rollback test environment before start postgres services. (ONLY ONGRES TEST ENVIRONMENT - patroni-migrate 01 and 02)

- Check patroni stopped and stop it as normal user (All servers)

  `sudo gitlab-patronictl list;`
  `sudo systemctl status patroni`

  `sudo systemctl stop patroni`

- Asume as `gitlab-psql` user (all servers)

  `sudo -u gitlab-psql -i /bin/bash`

- bkp hba file from new data (optional)

  `cp /var/opt/gitlab/postgresql/data11/pg_hba.conf /var/opt/gitlab/postgresql/pg_hba.conf_ori`

- Delete data11 and useless files (all servers)

  ```sh
  rm -rf /var/opt/gitlab/postgresql/data11/
  rm /var/opt/gitlab/postgresql/analyze_new_cluster.sh
  rm /var/opt/gitlab/postgresql/delete_old_cluster.sh
  rm /var/opt/gitlab/postgresql/files_ownership.out
  rm /var/opt/gitlab/postgresql/pg_upgrade_*log
  ```

- Clean old data

  `rm -rf /var/opt/gitlab/postgresql/data/* && ls -lha /var/opt/gitlab/postgresql/data/`

- restore postgresql.conf path

  ```sh
  sed -i 's/data11/data/g' /var/opt/gitlab/postgresql/postgresql.conf
  sed -i '/REMOVETHISLINETOSTART/d' /var/opt/gitlab/postgresql/postgresql.conf
  sed -i '/# BEGIN ANSIBLE MANAGED BLOCK/d' /var/opt/gitlab/postgresql/postgresql.conf
  sed -i '/# END ANSIBLE MANAGED BLOCK/d' /var/opt/gitlab/postgresql/postgresql.conf
  sed -i 's/pg11\-bench\-cluster/pg\-bench\-cluster/g' /var/opt/gitlab/postgresql/postgresql.conf
  ```

- Restore patroni.yml

  ```sh
  sed -i 's/data11/data/g' /var/opt/gitlab/patroni/patroni.yml
  sed -i 's/\/11\//\/9\.6\//g' /var/opt/gitlab/patroni/patroni.yml
  sed -i '/tags\:/d' /var/opt/gitlab/patroni/patroni.yml
  sed -i '/nofailover\:/d' /var/opt/gitlab/patroni/patroni.yml
  sed -i '/noloadbalance\:/d' /var/opt/gitlab/patroni/patroni.yml
  sed -i '/BEGIN\ ANSIBLE/d' /var/opt/gitlab/patroni/patroni.yml
  sed -i '/END\ ANSIBLE/d' /var/opt/gitlab/patroni/patroni.yml
  sed -i 's/pg11\-bench\-cluster/pg\-bench\-cluster/g' /var/opt/gitlab/patroni/patroni.yml
  sed -i '/recovery_conf/d' /var/opt/gitlab/patroni/patroni.yml
  sed -i '/restore_command/d' /var/opt/gitlab/patroni/patroni.yml
  sed -i '/%f %p/d' /var/opt/gitlab/patroni/patroni.yml
  sed -i 's/\/dev\/null/\/var\/opt\/gitlab\/postgresql\/data/g' /var/opt/gitlab/patroni/patroni.yml
  ```

- Initdb with old version (only Primary)

  `/usr/lib/postgresql/9.6/bin/initdb -D /var/opt/gitlab/postgresql/data --locale=C.UTF-8 --encoding=UTF8`

- Restore hba file in new data (only Primary)

  `cp /var/opt/gitlab/postgresql/pg_hba.conf_custom /var/opt/gitlab/postgresql/data/pg_hba.conf`

  > Note: If the hba file doesn't exist, here the copy:

    ```ini
    local   all                 all                             trust
    host    all                 gitlab-psql		10.0.0.0/8  trust
    host    all                 gitlab-superuser    10.0.0.0/8  md5
    host    replication         gitlab-replicator   10.0.0.0/8  md5
    host    all                 all                 0.0.0.0/0   reject
    ```

- Start service manually (only Primary)

  `/usr/lib/postgresql/9.6/bin/postgres -D /var/opt/gitlab/postgresql/data --config-file=/var/opt/gitlab/postgresql/postgresql.conf &`

- Connect to postgres and import roles and objects. (only Primary)

  `psql -h 10.224.42.101 -d postgres`

  ```sql
  # create database gitlabhq_production;
  # \c gitlabhq_production
  # \i /var/tmp/roles_old.sql
  # \i /var/tmp/create_objects_old.sql
  ```

- Stop service manually (only Primary)

  `/usr/lib/postgresql/9.6/bin/pg_ctl stop -m fast -D /var/opt/gitlab/postgresql/data`

- Flush consult kv db (only Primary)

  `consul kv delete --recurse service/pg-bench-cluster/`

  `consul kv delete --recurse service/pg11-bench-cluster/`

- Check consul kv is empty (only Primary)

  `consul kv get --recurse service/pg-bench-cluster`

  `consul kv get --recurse service/pg11-bench-cluster`

- Restore original hba file (only Primary)

  `cp /var/opt/gitlab/postgresql/pg_hba.conf_ori /var/opt/gitlab/postgresql/data/pg_hba.conf`

- Restore pgpass file

  `cp /var/opt/gitlab/postgresql/pgpass /var/opt/gitlab/postgresql/.pgpass`

- AS NORMAL USER Start patroni service (only Primary)

  `sudo systemctl start patroni; sudo tail -f /var/log/gitlab/patroni/patroni.log`

- Join the Replica only need start the service, patroni will run pg_basebackup on master a create replica, not useful for big testing environments.

  `sudo systemctl start patroni; sudo tail -f /var/log/gitlab/patroni/patroni.log`

- check patroni cluster

  `sudo gitlab-patronictl list`


### Only if new service has not started and you need back.

- rename pg_control file

  `sudo stat /var/opt/gitlab/postgresql/data/global/pg_control.old`

  `sudo mv /var/opt/gitlab/postgresql/data/global/pg_control.old /var/opt/gitlab/postgresql/data/global/pg_control`

- restore hba ori

  `sudo stat /var/opt/gitlab/postgresql/data/pg_hba.conf.*`

  `sudo mv /var/opt/gitlab/postgresql/data/pg_hba.conf.* /var/opt/gitlab/postgresql/data/pg_hba.conf`
