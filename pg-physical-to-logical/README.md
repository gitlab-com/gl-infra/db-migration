# Convert PostgreSQL Streaming (physical) Replication to Logical Replication.

This role is designed to transform physical replication into logical replication in the Patroni Standby Cluster.

By default, logical replication is configured for all tables, it is also possible to set up partial replication only for the selected tables (for example, for decomposition) specified in the format "schemaname.relname" in the `logical_replication_tables` variable.

## List of playbooks:

- `physical_to_logical.yml` - This playbook is designed to convert PostgreSQL streaming (physical) replication to logical replication.
- `stop_logical_replication.yml` - This playbook is designed to stop logical replication by removing subscription on the Target cluster and publication and logical replication slot on the Source cluster.

## Requirements

1. Prepare the Patroni Standby Cluster from a backup or disk snapshot (not part of this playbook).
2. Prepare the inventory file. The actual file is composed in `yaml` format, you can see details on [Ansible docs](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

e.g: `gprd.yml`

<details><summary>Click here to expand...</summary><p>

```yaml
all:
  children:
    source_cluster:
      hosts:
        patroni-main-v16-[101:109]-db-gprd.c.gitlab-production.internal:
    target_cluster:
      hosts:
        patroni-sec-v16-[01:06]-db-gprd.c.gitlab-production.internal:
```

In the example above, hosts in groups are indicated: 

- `source_cluster` - The Source Cluster hosts of PostgreSQL from which replication is performed.
- `target_cluster` - The Target Cluster hosts on which the conversion of physical replication to logical replication is performed.

</p></details>

## How to run:

Example:

```
cd db-migration/pg-physical-to-logical
ansible all -i inventory/gprd.yml -m ping

ansible-playbook -i inventory/gprd.yml physical_to_logical.yml # break physical replication and enable logical replication

# if required (e.g. security decomposition)
ansible-playbook -i inventory/gprd-pgbouncers.yml pause_pgbouncer.yml # pause writer pgbouncers
ansible-playbook -i inventory/gprd.yml stop_logical_replication.yml # stop forward logical replication once replication lag hits zero
ansible-playbook -i inventory/gprd.yml configure_reverse_replication.yml # enable reverse logical replication
ansible-playbook -i inventory/gprd-pgbouncers.yml unpause_pgbouncer.yml # unpause writer pgbouncers
ansible-playbook -i inventory/gprd.yml stop_reverse_logical_replication.yml # TODO
```


## Plan:

#### Step 1: Perform Pre-Checks
  - (TARGET) Make sure that is Standby leader
  - (ALL) Test PostgreSQL DB Access
  - (SOURCE/TARGET) Make sure that physical replication is active
    - Stop, if there are no active replicas
    - if pg_stat_replication returned 0 entries (except pg_basebackup)
  - (SOURCE/TARGET) Make sure there is no high physical replication lag
    - Note: No more than `max_replication_lag_bytes`
    - Stop, if physical replication lag is high
  - (SOURCE) Make sure there are no tables with replica identity "nothing"
    - Stop, if replica identity "nothing" is present
  - (SOURCE) Make sure that tables with replica identity “default” have primary key
    - Stop, if replica identity "default" without PK is present
  - (SOURCE) Make sure all tables specified in `logical_replication_tables` exist in the database
    - Note: if 'logical_replication_tables' is defined (by default, they are not specified)
    - Stop, if any table specified in logical_replication_tables does not exist
  - (TARGET) Make sure that the Chef client is stopped
  - (ALL) Make sure that the password file ".pgpass" exists
    - Stop, if .pgpass is not exists
  - (SOURCE) Make sure that the user is specified in the password file
    - Stop, if the user (`pg_source_replica_user` variable) is not present
  - (SOURCE) Get the password for the replica user
  - (TARGET) Configure the password file for the replica user
  - (TARGET) Test the access for the replica user to the source database (`pg_source_dbname` variable)
  - (TARGET) Make sure that the '`restore_command`' parameter is not specified
    - if 'restore_command' is specified:
        - check if the 'recovery_conf' is specified in patroni.yml
        - comment out 'recovery_conf' parameters in patroni.yml
        - execute CHECKPOINT before restarting PostgreSQL
        - stop Patroni service on Replicas and Leader
        - start Patroni service and check if Patroni cluster is healthy
        - make sure that 'restore_command' is disabled
        - stop, if 'restore_command' is defined
#### Step 2: Create a publication not the Source and reach `recovery_target_lsn` on the Target leader
  - (SOURCE) Increase the number of WAL files to hold in the Source Cluster
    - Increase the `wal_keep_size` parameter to `pg_source_wal_keep_gigabytes` (default 100GB)
      - Note: To guarantee that the necessary WAL files are available to reach `recovery_target_lsn` via streaming replication.
  - (TARGET) Pause Patroni on the Target Cluster
  - (TARGET) Stop Patroni service on the Cluster Replicas
  - (TARGET) Stop Patroni service on the Cluster Leader
  - (TARGET) Wait until the Patroni cluster is stopped
  - (TARGET) Pause WAL replay (recovery) on the Standby Cluster Replicas
  - (TARGET) Execute CHECKPOINT before stopping PostgreSQL
  - (TARGET) Stop PostgreSQL on the Standby Cluster Leader
  - (SOURCE) Execute checkpoint before creating a publication
  - (SOURCE) Create a publication for logical replication
    - Note: If the variable `pg_publication_count` is more than `1`, instead of creating one publication for all tables:
      - Get a list of tables distributed by groups
      - Start pg_terminator script: Monitor locks and terminate the 'create publication' blockers
        - terminate the backend blocking the create publication query for more than 15 seconds (if exists)
        - after creating publications, the pg_terminator script stops.
  - (SOURCE) Create a slot for logical replication, and save 'lsn'
    - Note: If the variable `pg_publication_count` is more than `1`
      - Create multiple publications and replication slots
      - Advance all created slots to the last LSN position
  - (TARGET) Specify recovery parameters on the Standby Cluster Leader
    - `recovery_target_lsn` = '{{ pg_slot_lsn }}'
    - `recovery_target_action` = 'promote'
    - `recovery_target_timeline` = 'latest'
    - temporarily comment out the `restore_command` parameter.
      - Note: For data consistency, we need to restore the standby cluster exactly to the LSN that we receive on the source cluster during the creation of the publication and the slot for logical replication. Using streaming replication for this purpose is a more reliable way, as tests have shown us.
      - Note: When starting the Patroni service, the parameters will be returned to their original state (as defined in DCS).
  - (TARGET) Start PostgreSQL on the Standby Cluster Leader to reach recovery_target_lsn
  - (TARGET) Wait until the recovery is complete
  - (TARGET) Get the current PostgreSQL log file
  - (TARGET) Check the PostgreSQL log file
    - Stop, if target LSN not reached
  - Print the result of checking the PostgreSQL log
  - (TARGET) Resume WAL replay (recovery) on Standby Cluster Replicas
  - (TARGET) Wait until physical replication becomes active
  - (TARGET) Wait until physical replication lag is 0 bytes
  - (SOURCE) Reset the `wal_keep_size` parameter to original state on the Source Primary
  - (TARGET) Execute CHECKPOINT before stopping PostgreSQL
  - (TARGET) Stop PostgreSQL on the Cluster Leader
  - (TARGET) Stop PostgreSQL on the Cluster Replicas
  - (TARGET) Check if PostgreSQL is stopped
  - (TARGET) Get 'Latest checkpoint location' on the Target Cluster Leader and Replicas
  - (TARGET) Print 'Latest checkpoint location'
  - (TARGET) Check if all 'Latest checkpoint location' values match
    - if 'Latest checkpoint location' doesn't match:
       - clean up logical replication objects on the Source Cluster
       - stop the playbook execution with an error
    - if 'Latest checkpoint location' values match on all cluster nodes
      - continue the playbook execution
  - (TARGET) Remove 'standby_cluster' parameter from DCS and patroni.yml
  - (TARGET) Start Patroni service on the Cluster Leader
  - (TARGET) Wait for Patroni port to become open on the host
  - (TARGET) Resume Patroni on the Target Cluster
  - (TARGET) Check Patroni is healthy on the Leader
  - (TARGET) Start Patroni service on the Cluster Replica
  - (TARGET) Wait for Patroni port to become open on the host
  - (TARGET) Check Patroni is healthy on the Replica
#### Step 3: Create a subscription for logical replication
  - (TARGET) Create a subscription for logical replication using a previously created slot
    - Note: If the variable `pg_publication_count` is more than `1`
      - Create multiple subscriptions for each publication/slots.
  - (SOURCE) Make sure that logical replication is active
    - wait until the logical replication slot is active
    - Note: max wait time: 1 minute
  - (SOURCE) Check the logical replication lag
  - Print the result of setting up logical replication
    - example: "The logical replication setup is completed. Current replication lag: XX MB"

---
