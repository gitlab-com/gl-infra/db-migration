---
# This playbook performs the following tasks:
# 1. Retrieves the "Latest checkpoint location" value from the cluster leader and its replicas.
# 2. Displays debug information about the "Latest checkpoint location" on the leader and replicas.
# 3. Sets the fact 'pg_checkpoint_location_match' indicating whether the "Latest checkpoint location" matches on the leader and replica.
# 4. If the "Latest checkpoint location" matches on all cluster nodes, displays a success message.
# 5. Otherwise, executes a block of tasks for cleanup of the logical replication and throws an error if the "Latest checkpoint location" doesn't match. (Note, that in this case, the target cluster is left in stopped state. A possible optimization for the future: make an attempt "atomic", reverting the target cluster back to working on physical replication.)

- name: Get 'Latest checkpoint location' on the Target Cluster Leader
  shell: |
    set -o pipefail;
    {{ pg_bindir }}/pg_controldata {{ pg_datadir }} | grep 'Latest checkpoint location' | awk '{print $4}'
  args:
    executable: /bin/bash
  changed_when: false
  register: pg_checkpoint_location_leader
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

- name: Get 'Latest checkpoint location' on the Target Cluster Replicas
  shell: |
    set -o pipefail;
    {{ pg_bindir }}/pg_controldata {{ pg_datadir }} | grep 'Latest checkpoint location' | awk '{print $4}'
  args:
    executable: /bin/bash
  changed_when: false
  register: pg_checkpoint_location_replica
  when:
    - inventory_hostname in groups['target_cluster']
    - not patroni_cluster_leader

- name: Print 'Latest checkpoint location'
  debug:
    msg: "Leader's latest checkpoint location: {{ pg_checkpoint_location_leader.stdout }}"
  when:
    - inventory_hostname in groups['target_cluster']
    - patroni_cluster_leader

- name: Print 'Latest checkpoint location'
  debug:
    msg: "Replica: {{ inventory_hostname }}, latest checkpoint location: {{ pg_checkpoint_location_replica.stdout }}"
  when:
    - inventory_hostname in groups['target_cluster']
    - not patroni_cluster_leader

- name: Check if all 'Latest checkpoint location' values match
  set_fact:
    pg_checkpoint_location_match: "{{ pg_checkpoint_location_replica.stdout == hostvars[groups['target_primary'][0]]['pg_checkpoint_location_leader']['stdout'] }}"
  when:
    - inventory_hostname in groups['target_cluster']
    - not patroni_cluster_leader

- name: "SUCCESS: 'Latest checkpoint location' values match on all cluster nodes"
  debug:
    msg: "'Latest checkpoint location' is the same on the leader and its standbys"
  run_once: true
  when:
    # This condition retrieves the 'pg_checkpoint_location_match' value for each node in the 'target_secondary' group.
    # The 'select' filter selects all nodes whose 'pg_checkpoint_location_match' is 'False'.
    # If no such nodes exist (i.e., the length of the resulting list is less than 1), it means that the 'pg_checkpoint_location_match' is 'True' for all nodes.
    - groups['target_secondary'] | map('extract', hostvars, 'pg_checkpoint_location_match') | select('equalto', False) | list | length < 1

# Stop, if 'Latest checkpoint location' doesn't match
- block:
    - name: Clean up logical replication objects on the Source Cluster
      include_tasks: drop_publication.yml
      when:
        - inventory_hostname in groups['source_cluster']
        - patroni_cluster_leader

    - name: "ERROR: 'Latest checkpoint location' doesn't match"
      fail:
        msg: "'Latest checkpoint location' doesn't match on leader and its standbys"
      run_once: true
  when:
    # This condition retrieves the 'pg_checkpoint_location_match' value for each node in the 'target_secondary' group.
    # The 'select' filter selects all nodes whose 'pg_checkpoint_location_match' is 'False'.
    # If there is at least one such node (i.e., the length of the resulting list is greater than 0),
    # it means that the 'pg_checkpoint_location_match' is not 'True' for all nodes,
    # and the block of tasks is executed, including cleanup and throwing an error.
    - groups['target_secondary'] | map('extract', hostvars, 'pg_checkpoint_location_match') | select('equalto', False) | list | length > 0

...
