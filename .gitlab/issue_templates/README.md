# Maintenance CR templates

To generate a CR for your specific maintenance requirement, you will need to determine what type the template is.  For example, pg_upgrade.md contains erb templates and is intended to be generated and copied (to your clipboard) rather than generated directly to an issue.  For these erb templates, you'll need to set variables and copy the erb output like so:

```
  erb \
    coordinator=$coordinator_username \
    playbook_runner=$playbook_runner_username \
    comms_handler=$comms_handler_username \
    dbre=$dbre_username \
    sre=$sre_username \
    quality=$comma_delimited_list_quality_usernames \
    imoc=$imoc_username \
    cmoc=$cmoc_username \
    eoc=$eoc_username \
    video_call=https://gitlab.zoom.us/j/97279198952?pwd=eStENnFtK3UxRFFoNU5wT0xFR2JHdz09 \
    gitlab_com_cr="https://gitlab.com/gitlab-com/gl-infra/production/-/issues/15945" \
    pcl_start_time="2025-04-16 23:00" \
    decomp_start_time="2025-04-17 06:00" \
    switchover_start_time="2025-04-18 06:00" \
    pcl_end_time="2025-04-19 11:00" \
    production=0 \
    src_cluster=main \
    dst_cluster=sec \
    cluster_ver=v16 \
    -T- decomposition.md 2>&1 | pbcopy
```

(This example from [pg12 to pg14 upgrade](https://ops.gitlab.net/gitlab-com/gl-infra/db-migration/-/issues/62#note_165125)).

The non-erb templates can be used as any other Gitlab issue template.