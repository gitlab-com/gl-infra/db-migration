<%- if false %>
<!--
This template has lots of ERB tags so it's not meant to be used verbatim to create an issue. To create
the markdown to copy into an issue, you run it like this (times are in UTC):

  erb \
    cr_start_time="2024-10-25 23:00" \
    cr_end_time="2024-10-29 11:00" \
    cluster=ci \
    change_tech="$changeusername" \
    review_tech="$reviewusername" \
    hosts="patroni-ci-v16-06-db-gprd.c.gitlab-production.internal,patroni-ci-v16-03-db-gprd.c.gitlab-production.internal" \
    -T- proactive_gcp_migration.md | pbcopy

NOTE:
  1. You'll need to have the activesupport gem installed: `gem install activesupport`
  2. To save the contents in your clipboard, you can pipe the output to `pbcopy` on OS/X or
     `xclip -selection clipboard` on Linux (assuming you have it installed).
  3. We assume production here, because there is no customer impact for gstg hosts being live migrated.

Then simply paste the results into a new issue in dbo-issue-tracker (or other appropriate location)
-->
<%- end %>
<%
  unless defined?(cluster)
    raise "you must specify cluster name (e.g., 'cluster=main' or 'cluster=registry')"
  end

  env = 'gprd'
  gcp_project = 'gitlab-production'
  require 'time'
  require 'active_support'
  require 'active_support/time'
  
  cr_start_ptime = Time.strptime(cr_start_time + ' +0000', "%Y-%m-%d %H:%M %z")
  cr_end_ptime = Time.strptime(cr_end_time + ' +0000', "%Y-%m-%d %H:%M %z")

  cr_start_time_str = cr_start_time + ' UTC'
  cr_end_time_str = cr_end_time + ' UTC'
  
  window_in_hours = ((cr_end_ptime - cr_start_ptime) / 3600).to_i
%>

# Production Change

/title GPRD - Force manual VM migrations during off-peak period
/assign @<%= change_tech %>
/assign_reviewer @<%= review_tech %>
/label ~"change::unscheduled"
/label ~"C2"
/label ~"change::C2"

### Change Details

<%- if cluster == 'main' -%>
1. **Services Impacted** - ~"Service::Patroni"
<%- else -%>
1. **Services Impacted** - ~"Service::Patroni<%= cluster.upcase %>"
<%- end -%>
1. **Change Technician** - @<%= change_tech %>
1. **Change Reviewer** - @<%= review_tech %>
1. **Scheduled Date and Time (UTC in format YYYY-MM-DD HH:MM)** - <%= cr_start_time_str %>
1. **Time tracking** - <%= window_in_hours %> hour<% if window_in_hours > 1 %>s<% end %>
1. **Downtime Component** - {+TODO: Downtime possible if host(s) are active primaries and failover is required+}

### Set Maintenance Mode in GitLab

If your change involves scheduled maintenance, add a step to set and [unset maintenance mode](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/set_maintenance_window.md) per our runbooks. This will make sure [SLA calculations adjust](https://gitlab.com/gitlab-com/runbooks/-/merge_requests/5887) for the maintenance period.

---

# Change Detailed Steps

<details>
  <summary>

  - [ ] __(PREPARE) Verify if any host is active primary__
  
  </summary>

  1. [ ] 🐺 {+ Change Technician +}:
     <%- hosts.split(',').each do |hostname| -%>
      - [ ] <%= hostname %>
          - [ ] `ssh <%= hostname %> "sudo gitlab-patronictl list | awk '/<%= hostname %>/ {print \$6}'"` # should output `Replica`
     <%- end -%>
  
<details>
  <summary>

  - [ ] __(PREPARE) ONLY if one of the hosts is Leader__

  </summary>

  1. [ ] If any host is an active Leader, there will be additional steps involved, and possible ApDex impact.
      - [ ] ssh to console and prepare code
        - [ ] `ssh -A console-01-sv-gprd.c.gitlab-production.internal`
        - [ ] Start or resume a tmux session for this CR `tmux a -t pg_gcp_migration || tmux new -s pg_gcp_migration`
        - [ ] Configure ssh key for dbupgrade
            - [ ] Disable screen sharing to reduce risk of exposing private key
            - [ ] `sudo su - dbupgrade`
            - [ ] Copy dbupgrade user's private key from 1Password to `~/.ssh/id_dbupgrade` # as required
            - [ ] `chmod 600 ~/.ssh/id_dbupgrade`
            - [ ] Use key as default `ln -s ~/.ssh/id_dbupgrade ~/.ssh/id_rsa`
            - [ ] Enable re-screen sharing if beneficial
        - [ ] Ensure all necessary binaries are in place {+(one method, or the other)+}
            - [ ] ansible (local directory, specific version via venv/pip)
            ```
            python3 -m venv ~/ansible;
            cd ~/ansible; . bin/activate;
            # "pip3 index versions ansible" for a list of available versions
            pip3 install ansible==$VERSION
            ```
            - [ ] ansible (system-wide, whatever is available via apt) `sudo apt install ansible`
        - [ ] `[ -d "~/src" ] && rm -rf "~/src"; mkdir -p "~/src"; cd "~/src"`
        - [ ] Checkout relevant ansible playbooks repositories
            - [ ] `git clone https://gitlab.com/gitlab-com/gl-infra/db-migration.git`
            - [ ] `cd db-migration/dbre-toolkit`
            - [ ] Verify all hosts in the inventory are reachable `ansible -e "ansible_ssh_private_key_file=~/.ssh/id_dbupgrade" -i inventory/gprd-<%= cluster %>.yml all -m ping`
            - [ ] Document output in CR
  
  </details>

</details>

<details>
  <summary>

  - [ ] __Notify EOC__

  </summary>

  1. [ ] 🐺 {+ Change Technician +}:
      - [ ] Check for Active Sev1 or Sev2 incidents: [Active Sev1/2 Issues](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=Incident%3A%3AActive&or%5Blabel_name%5D%5B%5D=severity%3A%3A1&or%5Blabel_name%5D%5B%5D=severity%3A%3A2&first_page_size=100)
          - [ ] {+If any incidents show in the above link, discuss potential impact when you reach out to EOC+}
  1. [ ] 🐺 {+ Change Technician +}: Get a green light from the EOC and (if required) Release Manager/Incident Manager
      - [ ] ping `@sre-oncall` and (if required) `@release-managers`/`@imoc` in [#production](https://gitlab.enterprise.slack.com/archives/C101F3796) in slack
          - [ ] If any host is an active leader, let EOC/IMOC know.  Downtime isn't likely, but there could be ApDex impact and customers may experience increased error rates.
      - [ ] Set label ~"change::in-progress" `/label ~change::in-progress`
</details>



<details>
  <summary>

  - [ ] __Perform migrations__

  </summary>

  1. [ ] 🐺 {+ Change Technician +}:
<details>
  <summary>

  - [ ] __ONLY If one of the hosts is currently Leader__

  </summary>

  - [ ] Run the playbook with output to log
    `ansible-playbook -e "ansible_ssh_private_key_file=~/.ssh/id_dbupgrade" -i inventory/gprd-<%= cluster %>.yml switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_leader_switchover_<% cluster %>_$(date +%Y%m%d).log`
  - [ ] Document relevant output in CR

</details>

  - [ ] Verify the cluster is healthy
      - [ ] `ssh <%= hosts.split(',')[0] %> "sudo gitlab-patronictl list"`
          - [ ] Abort if there are issues
  <%- hosts.split(',').each do |hostname| -%>
  <%- short_hostname = hostname.split('.')[0] -%>
  - [ ] Tag <%= short_hostname %> with `nofailover`/`noloadbalance`
      - [ ] `knife node run_list add <%= short_hostname %> 'role[gprd-base-db-patroni-maintenance]'`
      - [ ] `knife ssh -N "<%= short_hostname %>" "sudo chef-client"`
  <%- end -%>
  - [ ] Wait for traffic to drain from the hosts
  <%- if cluster == 'main' -%>
      - [ ] [Dashboard](https://dashboards.gitlab.net/d/patroni-main/patroni3a-overview?orgId=1&from=now-6h%2Fm&to=now%2Fm&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gprd&var-environment=gprd)
  <%- else -%>
      - [ ] [Dashboard](https://dashboards.gitlab.net/d/patroni-<%= cluster %>-main/patroni-<%= cluster %>3a-overview?orgId=1&from=now-6h%2Fm&to=now%2Fm&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gprd&var-environment=gprd)
  <%- end -%>
  - [ ] Schedule GCP migration to happen ASAP
  <%- hosts.split(',').each do |hostname| -%>
  <%- short_hostname = hostname.split('.')[0] -%>
      - [ ] `gcloud compute instances perform-maintenance <%= short_hostname %> --project=gitlab-production`
  <%- end -%>
  - [ ] Wait for migrations to complete
      - [ ] [GCP logs](https://log.gprd.gitlab.net/app/discover#/view/5d6907fb-9ec2-4ed8-b3a3-66be508fe271?_g=())
  - [ ] Verify hosts are back in sync with cluster
      - [ ] `knife ssh -N <%= hosts.split(',')[0] %> "sudo gitlab-patronictl list"` # repeat until all hosts are back in sync
  <%- hosts.split(',').each do |hostname| -%>
  <%- short_hostname = hostname.split('.')[0] -%>
  - [ ] Remove `nofailover`/`noloadbalance` tags for <%= short_hostname %>
      - [ ] `knife node run_list remove <%= hostname %> 'role[gprd-base-db-patroni-maintenance]'`
      - [ ] `knife ssh -N "<%= hostname %>" "sudo chef-client"`
  <%- end -%>
  - [ ] Verify hosts are taking traffic again
  <%- if cluster == 'main' -%>
      - [ ] [Dashboard](https://dashboards.gitlab.net/d/patroni-main/patroni3a-overview?orgId=1&from=now-6h%2Fm&to=now%2Fm&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gprd&var-environment=gprd)
  <%- else -%>
      - [ ] [Dashboard](https://dashboards.gitlab.net/d/patroni-<%= cluster %>-main/patroni-<%= cluster %>3a-overview?orgId=1&from=now-6h%2Fm&to=now%2Fm&timezone=utc&var-PROMETHEUS_DS=mimir-gitlab-gprd&var-environment=gprd)
  <%- end -%>

</details>


<details>
  <summary>

  - [ ] __Wrapping up__

  </summary>

  1. [ ] 🐺 {+ Change Technician +}:
      - [ ] ping `@sre-oncall` and (if required) `@release-managers`/`@imoc` in [#production](https://gitlab.enterprise.slack.com/archives/C101F3796) in slack
      - [ ] Set label ~"change::complete" `/label ~change::complete`

</details>
