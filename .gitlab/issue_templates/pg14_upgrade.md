<% if false %>
<!--
This template has lots of ERB tags so it's not meant to be used verbatim to create an issue. To create
the markdown to copy into an issue, you run it like this (times are in UTC):

# imoc = https://gitlab.pagerduty.com/schedules-new - "Infra Escalation"
# cmoc = https://gitlab.pagerduty.com/schedules-new - "Incident Management - CMOC"
# eoc = https://gitlab.pagerduty.com/schedules-new - "SRE 8-hour"
# quality = https://gitlab.com/gitlab-org/quality/pipeline-triage#dri-weekly-rotation-schedule

  erb \
    maintenance_start_time="YYYY-MM-DD HH:mm" \
    maintenance_end_time="YYYY-MM-DD HH:mm" \
    production=0 \
    cluster=ci \
    -T- <this file>

For examples:

  erb \
    maintenance_start_time="2022-08-02 15:00" \
    maintenance_end_time="2022-08-02 17:00" \
    production=1 \
    -T- patroni_os_upgrade.md

For examples:

  erb \
    maintenance_start_time="2022-08-02 15:00" \
    maintenance_end_time="2022-08-02 17:00" \
    production=1 \
    -T- patroni_os_upgrade.md

You can also specify the usernames of the various roles. For example:

  erb \
    maintenance_start_time="2023-05-10 14:00" \
    maintenance_end_time="2023-05-10 19:00" \
    production=0 \
    cluster=main \
    coordinator=rhenchen \
    playbook_runner=bshah \
    comms_handler=kwanyangu \
    dbre=alexander-sosna \
    sre=anganga \
    quality=niskhakova,ddavison \
    imoc=kwanyangu,amoter \
    cmoc=_ONLY_PRODUCTION_ \
    eoc=knottos \
    video_call="VIDEO_CALL_LINK" \
    -T- pg14_upgrade.md | pbcopy

  erb \
      maintenance_start_time="2023-04-26 14:30" \
      maintenance_end_time="2023-04-26 19:30" \
      production=0 \
      cluster=main \
      coordinator=rhenchen \
      playbook_runner=bshah \
      comms_handler=dawsmith \
      dbre=alexander-sosna \
      sre=anganga \
      quality=willmeek,trea \
      imoc=afappiano \
      cmoc=_ONLY_PRODUCTION_ \
      eoc=rehab,stejacks-gitlab \
      -T- pg14_upgrade.md | pbcopy

  erb \
      maintenance_start_time="2023-04-24 14:00" \
      maintenance_end_time="2023-04-24 19:00" \
      production=0 \
      cluster=main \
      coordinator=rhenchen \
      playbook_runner=bshah \
      comms_handler=kwanyangu \
      dbre=alexander-sosna \
      sre=anganga \
      quality=_TBD_ \
      imoc=_TBD_ \
      cmoc=_TBD_ \
      eoc=_TBD_ \
      -T- pg14_upgrade.md | pbcopy

  erb \
      maintenance_start_time="2023-04-24 14:00" \
      maintenance_end_time="2023-04-24 19:00" \
      production=0 \
      cluster=main \
      coordinator=rhenchen \
      playbook_runner=bshah \
      comms_handler=kwanyangu \
      dbre=alexander-sosna \
      sre=anganga \
      quality=_TBD_ \
      imoc=_TBD_ \
      cmoc=_TBD_ \
      eoc=_TBD_ \
      -T- pg14_upgrade.md | pbcopy

NOTE:
  1. You'll need to have the activesupport gem installed: `gem install activesupport`
  2. To save the contents in your clipboard, you can pipe the output to `pbcopy` on OS/X or
     `xclip -selection clipboard` on Linux (assuming you have it installed).
-->
<% end -%>
<%
  unless defined?(production)
    raise "you must specify either 'production=0' (i.e., staging) or 'production=1'"
  end

  unless defined?(cluster)
    raise "you must specify cluster name (e.g., 'cluster=main' or 'cluster=registry')"
  end

  unless ["0", "1"].include?(production)
    raise "production must be either 0 or 1"
  end

  unless defined?(post_upgrade_mr)
    post_upgrade_mr = '{+ TODO +}'
  end

  unless defined?(post_switchover_mr)
    post_switchover_mr = '{+ TODO +}'
  end

  unless defined?(rollback_mr)
    rollback_mr = '{+ TODO +}'
  end

  unless defined?(teleport_db_endpoint_mr)
    teleport_db_endpoint_mr = '{+ TODO +}'
  end

  unless defined?(console_config_endpoint_mr)
    console_config_endpoint_mr = '{+ TODO +}'
  end

  unless defined?(snapshot_image_mr)
    snapshot_image_mr = '{+ TODO +}'
  end

  unless defined?(gitlab_com_cr)
    gitlab_com_cr = '{+ TODO +}'
  end
  production = production.to_i == 0 ? false : true

  if production
    env = 'gprd'
    gcp_project = 'gitlab-production'
    case cluster
     when "main"
      src_cluster_prefix = 'patroni-main-2004'
      dst_cluster_prefix = 'patroni-main-v14'
      src_cluster_host_prefix = 'patroni-main-2004-1'
      dst_cluster_host_prefix = 'patroni-main-v14-1'
      dst_gcs_backup_location = 'https://console.cloud.google.com/storage/browser/gitlab-gprd-postgres-backup/pitr-walg-main-v14'
      dst_backup_host = 'patroni-main-v14-102'
    when "ci"
      src_cluster_prefix = 'patroni-ci-2004'
      dst_cluster_prefix = 'patroni-ci-v14'
      src_cluster_host_prefix = 'patroni-ci-2004-1'
      dst_cluster_host_prefix = 'patroni-ci-v14-1'
      dst_gcs_backup_location = 'https://console.cloud.google.com/storage/browser/gitlab-gprd-postgres-backup/pitr-walg-ci-v14'
      dst_backup_host = 'patroni-ci-v14-102'
    when "registry"
      src_cluster_prefix = 'patroni-v12-registry'
      dst_cluster_prefix = 'patroni-registry-v14'
      src_cluster_host_prefix = 'patroni-v12-registry-'
      dst_cluster_host_prefix = 'patroni-registry-v14-'
      dst_gcs_backup_location = 'https://console.cloud.google.com/storage/browser/gitlab-gprd-postgres-backup/pitr-walg-registry-v14'
      dst_backup_host = 'patroni-registry-v14-03'
    end
  else
    env = 'gstg'
    gcp_project = 'gitlab-staging-1'
    case cluster
    when "main"
      src_cluster_prefix = 'patroni-main-2004'
      dst_cluster_prefix = 'patroni-main-v14'
      src_cluster_host_prefix = 'patroni-main-2004-'
      dst_cluster_host_prefix = 'patroni-main-v14-1'
      dst_gcs_backup_location = 'https://console.cloud.google.com/storage/browser/gitlab-gstg-postgres-backup/pitr-walg-main-v14'
      dst_backup_host = 'patroni-main-v14-102'
    when "ci"
      src_cluster_prefix = 'patroni-ci-2004'
      dst_cluster_prefix = 'patroni-ci-v14'
      src_cluster_host_prefix = 'patroni-ci-2004-'
      dst_cluster_host_prefix = 'patroni-ci-v14-1'
      dst_gcs_backup_location = 'https://console.cloud.google.com/storage/browser/gitlab-gstg-postgres-backup/pitr-walg-ci-v14'
      dst_backup_host = 'patroni-ci-v14-102'
    when "registry"
      src_cluster_prefix = 'patroni-v12-registry'
      dst_cluster_prefix = 'patroni-registry-v14'
      src_cluster_host_prefix = 'patroni-v12-registry-'
      dst_cluster_host_prefix = 'patroni-registry-v14-'
      dst_gcs_backup_location = 'https://console.cloud.google.com/storage/browser/gitlab-gstg-postgres-backup/pitr-walg-registry-v14'
      dst_backup_host = 'patroni-registry-v14-03'
    end
  end

  case cluster
  when "main"
    prometheus_type = 'patroni'
    consul_service_name = 'patroni'
    consul_replica_endpoint = 'db-replica'
  when "ci"
    prometheus_type = 'patroni-ci'
    consul_service_name = 'patroni-ci'
    consul_replica_endpoint = 'ci-db-replica'
  when "registry"
    prometheus_type = 'patroni-registry'
    consul_service_name = 'patroni-registry'
    consul_replica_endpoint = 'registry-db-replica'
  end

  require 'time'
  require 'active_support/time'
  maint_start_time = Time.strptime(maintenance_start_time + ' +0000', "%Y-%m-%d %H:%M %z")
  maint_end_time = Time.strptime(maintenance_end_time + ' +0000', "%Y-%m-%d %H:%M %z")
  maint_start_time_str = maintenance_start_time + ' UTC'
  maint_end_time_str = maintenance_end_time + ' UTC'
  window_in_hours = ((maint_end_time - maint_start_time) / 3600).to_i


  TBD_placeholder ||= "__[TBD - It is a placeholder for now. We will add step(s)/link(s)/description here if necessary]__"

  # This is the IP of the VM used to run "gitlab-qa" against staging/production
  qa_vm_ip = '104.196.188.184' #  Nik: do we really needed this?

  # Using define_method instead of def so we can access the variables in the outer scope
  define_method("role") do |name|
    unless binding.local_variable_defined?(name)
      return ""
    end

    binding.local_variable_get(name).split(",").map do |name|
      "@" + name
    end.join(" ")
  end

  # Similarly we use define_method here (instead of "def") so we can access the variables in the outer scope
  define_method("partial") do |file, vars={}, indent_spaces=0|
    erbvar = '_erbout' + rand(1000).to_s

    b = binding
    vars.each do |k, v|
      b.local_variable_set k.to_sym, v
    end

    ' ' * indent_spaces + ERB.new(File.read(file), nil, '-', erbvar).result(b).split("\n").join("\n" + ' ' * indent_spaces)
  end
-%>

# Postgres Upgrade Rollout Team

| Role                | Assigned To |
| ------------------- | ----------- |
| 🐺 Coordinator     | <%= role('coordinator') %> |
| 🔪 Playbook-Runner | <%= role('playbook_runner') %> |
| ☎ Comms-Handler    | <%= role('comms_handler') %> |
| 🐘 DBRE            | <%= role('dbre') %> |
| 🐬 SRE             | <%= role('sre') %> |
| 🏆 Quality         | <%= role('quality') %> |
| 🎩 IMOC            | <%= role('imoc') %> |
| 📣 CMOC            | <%= role('cmoc') %> |
| 🚑 EOC             | <%= role('eoc') %> |
| 💾 Head Honcho     | <%= role('head_honcho') %> |

Link to gitlab.com CR: <<%= gitlab_com_cr %>>

## Collaboration

During the change window, the rollout team will collaborate using the following communications channels:

| App | Direct Link |
|---|---|
| Slack | [#g_infra_database_reliability](https://gitlab.slack.com/archives/C02K0JTKAHJ) |
| Video Call | <%= video_call ||= "<LINK_TO_VIDEO_CALL>" %> |

# Immediately

Perform these steps when the issue is created.

- [ ] 🐺 {+ Coordinator +}: Fill out the names of the rollout team in the table above.

# Support Options

| Provider | Plan | Details | Create Ticket |
|----------|------|---------|---------------|
| __Google Cloud Platform__ | [Gold Support](https://cloud.google.com/support/?options=premium-support#options) | 24x7, email & phone, 1hr response on critical issues | [__Create GCP Support Ticket__](https://enterprise.google.com/supportcenter/managecases) |

# Entry points

| Entry point                | Before                                                               | Blocking mechanism                                             | Allowlist | QA needs                                                                    | Notes                                                                                               |
| -------------------------- | -------------------------------------------------------------------- | -------------------------------------------------------------- | --------- | --------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------- |
| Pages                      | Available via *.gitlab.io, and various custom domains                | Unavailable if GitLab.com goes down for a brief time. There is a cache but it will expire in `gitlab_cache_expiry` minutes  | N/A       | N/A                                                                         |
|                                                                                                     |

# Database hosts

- [Staging replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gstg&var-prometheus=Global&var-prometheus_app=Global)
- [Production replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gprd&var-prometheus=Global&var-prometheus_app=Global)

# Accessing the rails and database consoles

<%- if production %>
## Production
- rails: `ssh $USER-rails@console-01-sv-gprd.c.gitlab-production.internal`
- main db replica: `ssh $USER-db@console-01-sv-gprd.c.gitlab-production.internal`
- main db primary: `ssh $USER-db-primary@console-01-sv-gprd.c.gitlab-production.internal`
- ci db replica: `ssh $USER-db-ci@console-01-sv-gprd.c.gitlab-production.internal`
- ci db primary: `ssh $USER-db-ci-primary@console-01-sv-gprd.c.gitlab-production.internal`
- main db psql: `ssh -t patroni-main-2004-04-db-gprd.c.gitlab-production.internal sudo gitlab-psql`
- ci db psql: `ssh -t patroni-ci-2004-05-db-gprd.c.gitlab-production.internal sudo gitlab-psql`
- registry db psql: `ssh -t patroni-v12-registry-01-db-gprd.c.gitlab-production.internal sudo gitlab-psql`
<%- else %>
## Staging
- rails: `ssh $USER-rails@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db replica: `ssh $USER-db@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db primary: `ssh $USER-db-primary@console-01-sv-gstg.c.gitlab-staging-1.internal`
- ci db replica: `ssh $USER-db-ci@console-01-sv-gstg.c.gitlab-staging-1.internal`
- ci db primary: `ssh $USER-db-ci-primary@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db psql: `ssh -t patroni-main-2004-02-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`
- ci db psql: `ssh -t patroni-ci-2004-01-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`
- registry db psql: `ssh -t patroni-v12-registry-01-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`
<%- end %>

# Dashboards and debugging
These dashboards might be useful during the rollout:

<%- if production %>
## Production
- [PostgreSQL replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1)
- [Triage overview](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s&var-env=gprd&var-environment=gprd&var-prometheus=prometheus-01-inf-gprd&var-prometheus_app=prometheus-app-01-inf-gprd&var-backend=All&var-type=All&var-stage=main)
- [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-24h&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main)
- [Sentry - includes application errors](https://sentry.gitlab.net/gitlab/gitlabcom/)
  - Workhorse: <https://sentry.gitlab.net/gitlab/gitlab-workhorse-gitlabcom/>
  - Rails (backend): <https://sentry.gitlab.net/gitlab/gitlabcom/>
  - Rails (frontend): <https://sentry.gitlab.net/gitlab/gitlabcom-clientside/>
  - Gitaly (golang): <https://sentry.gitlab.net/gitlab/gitaly-production/>
  - Gitaly (ruby): <https://sentry.gitlab.net/gitlab/gitlabcom-gitaly-ruby/>
- [Logs (Kibana)](https://log.gprd.gitlab.net/app/kibana)
<%- else %>
## Staging
- [PostgreSQL replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gstg&var-prometheus=Global&var-prometheus_app=Global)
- [Triage overview](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s&var-env=gstg&var-environment=gstg&var-prometheus=prometheus-01-inf-gstg&var-prometheus_app=prometheus-app-01-inf-gstg&var-backend=All&var-type=All&var-stage=main)
- [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-24h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main)
- [Sentry - includes application errors](https://sentry.gitlab.net/gitlab/staginggitlabcom/)
- [Logs (Kibana)](https://nonprod-log.gitlab.net/app/kibana)
<%- end %>

# Repos used during the rollout
The following Ansible playbooks are referenced throughout this issue:
- Postgres Upgrade, Switchover & Rollback: <https://gitlab.com/gitlab-com/gl-infra/db-migration/-/tree/master/pg-upgrade-logical>

---

# High level overview

This gives an high level overview on the procedure.

<details><summary>Upgrade Flowchart</summary>

```mermaid
flowchart TB
    subgraph Prepare new enviroment
    A[Create new cluster $TARGET as a carbon copy of the one to upgrade, $SOURCE] --> B
    B[Attach $TARGET as a standby-only-cluster to $SOURCE via physical replication] --> C
    end
    C[Make sure both clusters are in sync] --> D1
    subgraph Upgrade: ansible-playbook upgrade.yml
    D1[Disable Chef] --> D2
    D2[Perform clean shutdown of $TARGET] --> D3
    D3[On $SOURCE, create a replication slot and publication FOR ALL TABLES; remember its LSN] --> D4
    D4[Configure recovery_target_lsn on $TARGET] --> D5
    D5[Start $TARGET] --> D6
    D6[Let $TARGET reach the slot's LSN, still using physical replication] --> D7
    D7[Once slot's LSN is reached, promote $TARGET leader] --> D8
    D8[Upgrade $TARGET to new version using pg_upgrade and rsync] --> D9
    D9[Create logical subscription with copy_data=false] --> D10
    D10[Let $TARGET catch up using logical replication] --> H
    end
    subgraph Prepare switchover
    H[Ensure that all replication lags are close to zero] --> I
    I[Merge Chef MRs so $TARGET uses roles for new PostgreSQL version] --> K
    K[Enable Chef, run Chef-Client] --> L
    L[Make sure Chef finished sucessfully and cluster is still operational] --> M
    M[Disable Chef again] --> N
    end
    N[Metrics and confirmation checks are as expected] --> O
    subgraph Switchover: ansible-playbook switchover.yml
    O[Redirect RO traffic to $TARGET standbys in addition to $SOURCE] --> P
    P[Check if cluster is operational and metrics are normal] --"Normal"--> Q
    P --"Abnormal"-->GR
    Q[Redirect RO only to $TARGET] --> R
    R[Check if cluster is operational and metrics are normal] --"Normal"--> S
    R --"Abnormal"--> GR
    S[DBRE verify E2E tests run as expected with Quality help] --"Normal"--> T
    S --"Abnormal"-->GR
    end
    T[Switchover: Redirect RW traffict to $TARGET] --> U1
    subgraph Post Switchover Verification
    U1[Check if cluster is operational and metrics are normal]--"Normal"--> U2
    U1 --"Abnormal"--> LR
    U2[Enable Chef, run Chef-Client] --"Normal"--> U3
    U2 --"Abnormal"--> LR
    U3[Check if cluster is operational and metrics are normal] --"Normal"--> Success
    U3 --"Abnormal"--> LR
    Success[Success!]
    end
    subgraph GR[Gracefull Rollback - no dataloss]
    GR1[Start gracefull rollback]
    end
    subgraph LR[Fix forward]
    LR1[Fix all issues] -->LR2
    LR2[Return to last failed step]
    end
```

</details>

Sketches of the upgrade.yml actions can be found here: [upgrade.yml.pdf](https://gitlab.com/gitlab-com/gl-infra/db-migration/uploads/5d99fd4e9887e69180ed433c2f091cc3/upgrade.yml.pdf)

# Prep Tasks

<details>
  <summary>

  - [ ] __T minus 1 week (<%= 1.weeks.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__
  
  </summary>

  1. [ ] ☎ {+ Comms-Handler +}: Discuss scheduling of this CR and assess impact on deployments and releases with the Release Managers (`@release-managers` in Slack). Ask them to comment with approval on this issue. 
  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on @gitlabstatus. Workflow: https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events
     
      - Message:
        ```text
        Next week, we will be undergoing scheduled maintenance to our <%= cluster %> database layer. The maintenance will take up to 5 hours starting from <%= maint_start_time.strftime("%H:%M") %> UTC to <%= maint_end_time.strftime("%H:%M") %> UTC. GitLab.com will be available but users may experience degraded performance during the maintenance window. We apologize in advance for any inconvenience this may cause. See <<%= gitlab_com_cr %>>
        ```
</details>

<details>
  <summary>

  - [ ] __T minus 3 days (<%= 3.days.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` to make sure deployments have been paused until the maintenance window ends. Kindly ask to comment with approval on this issue. `Hi @release-managers :waves:, we would like to make sure that deployments have been stopped for the affected environments until <%= maint_end_time_str %>. Be aware that we are deactivating certain feature flags during this time. All details can be found in the CR. Please be so kind and comment with approval on <%= gitlab_com_cr %>. Be aware that on the first working day after the upgrade performance is closely monitored and performance tuning might be required. Therefore please halt database migrations further, until the following Tuesday. :bow:`

  <%- if production %>
  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on @gitlabstatus. Workflow: https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events
     
      - Message:
        ```text
        In 3 days, we will be undergoing scheduled maintenance to our <%= cluster %> database layer. The maintenance will take up to 5 hours starting from <%= maint_start_time.strftime("%H:%M") %> UTC to <%= maint_end_time.strftime("%H:%M") %> UTC. GitLab.com will be available but users may experience degraded performance during the maintenance window. We apologize in advance for any inconvenience this may cause. See <<%= gitlab_com_cr %>>
        ```
  <%- end %>

  <%- if production %>
  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send the tweet link from @gitlabstatus to #social_media_action channel on Slack:

      - Message:
        ```text
        Hi team, please retweet and pin this from our status page on GitLab Twitter about the upcoming production change where GitLab.com will undergo a scheduled maintenance to our database layer for up to 5 hours: {TWEET_LINK}
        ```

      - [ ] @gitlab retweeted from @gitlabstatus

      - [ ] Tweet pinned on @gitlab
  <%- end %>

  <%- if production %>
  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send on Slack `#whats-happening-at-gitlab`:

      - Message:

        ```text
        :loudspeaker: *Postgres upgrade for our <%= cluster %> database clusters is scheduled for <%= maint_start_time.strftime("%Y-%m-%d") %> between <%= maint_start_time.strftime("%H:%M") %> UTC and <%= maint_end_time.strftime("%H:%M") %> UTC* :rocket:
        Taking place in 3 days time! See <<%= gitlab_com_cr %>>

        :hammer_and_wrench: *What to expect?*
        GitLab.com will be available but users may experience degraded performance during the maintenance window. If you experience any issues likely realted to the upgrade in the next few days after the upgrade, please open an issue and reach the upgrade team at slack channel `#pg_upgrade`
        ```
  <%- end %>

  <%- if production %>
  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Share message from `#whats-happening-at-gitlab` to the following channels:

      - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
      - [ ] `#g_delivery` (cc `@release-managers`)
      - [ ] `#community-relations` (Inform Marketing team)
      - [ ] `#support_gitlab-com` (Inform Support SaaS team)
          - [ ] Share with team a link to the change request regarding the maintenance
  <%- end %>

  1. [ ] 🐘 {+ DBRE +}: [Create a C1 change request in the production repo](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/new?issuable_template=change_management) and link to this issue. Example: <https://gitlab.com/gitlab-com/gl-infra/production/-/issues/8448>
      - [ ] Ensure the CR is reviewed by the 🚑 {+ EOC +}

  1. [ ] 🐘 {+ DBRE +}: Ensure this issue has been created on <https://ops.gitlab.net/gitlab-com/gl-infra/db-migration>, since `gitlab.com` could potentially be unavailable during the rollout!!!

  1. [ ] 🐬 {+ SRE +}: Create a merge request that may be needed in case of rollback and link it in the below rollback section

  1. [ ] 🐘 {+ DBRE +}: Check that you have `Maintainer` or `Owner` permission in <https://ops.gitlab.net/gitlab-org/quality> to be able to trigger Smoke QA pipeline in schedules ([Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules), [Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules)). Reach out to Test Platform to get access if you don't have permission to trigger scheduled pipelines in the linked projects.

  1. [ ] 🐘 {+ DBRE +}: Communicate to [Test Platform On-call DRIs](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/oncall-rotation/#schedule) about upcoming Postgres upgrade. Template: `Hello @<on-call DRIs>, we plan to perform Postgres Upgrade at <time window> against <environment>. If E2E tests fail, we will ask you to join the call for debugging and deciding whether to proceed with upgrade or not.`.

</details>

<details>
  <summary>

  - [ ] __T minus 2 days (<%= 2.days.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  1. [ ] 🐺 {+ DBRE +}: Disable the DDL-related feature flags:
    1. [ ] Disable feature flags by typing the following into `#production`:
        <%- if production %>
        - PRODUCTION:
            1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags true`
        <%- else %>
        - STAGING:
            1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags true --staging`
        <%- end %>

  1. [ ] 🐘 {+ DBRE +}: Inform the database team that DDL feature flags have been disabled until the CR is complete. Post the following comment on the gitlab.com CR (<<%= gitlab_com_cr %>>):
    <%- if production %>
       ```text
      Hi @gl-database,

      Please note that `execute_batched_migrations_on_schedule` and `execute_background_migrations`, reindexing, async_foreign_key, async_index features and partition_manager_sync_partitions tasks will be disabled in the `PRODUCTION` environment, as we are carrying out Postgres upgrades to the database layer between `<%= maint_start_time_str %>` and `<%= maint_end_time_str %>`.

      We will re-enable the feature flag once work is complete.

      Thanks!
      ```
    <%- else %>
      ```text
      Hi @gl-database,

      Please note that the `execute_batched_migrations_on_schedule` and `execute_background_migrations`, reindexing, async_foreign_key, async_index features and partition_manager_sync_partitions feature flags will be disabled in the `STAGING` environment, as we are carrying out Postgres upgrade to the database layer between `<%= maint_start_time_str %>` and `<%= maint_end_time_str %>`.

      We will re-enable the feature flags once the CR is complete.

      Thanks!
      ```
    <%- end %>

</details>

<details>
  <summary>

  - [ ] __T minus 1 day (<%= 1.days.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  <%- if production %>
  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>

      - Message:
        ```text
        Reminder: Tomorrow, we will be undergoing scheduled maintenance to our <%= cluster %> database layer. The maintenance will take up to 5 hours starting from <%= maint_start_time.strftime("%H:%M") %> UTC to <%= maint_end_time.strftime("%H:%M") %> UTC. GitLab.com will be available but users may experience degraded performance during the maintenance window. We apologize in advance for any inconvenience this may cause. See <<%= gitlab_com_cr %>>
        ```
  
  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send message to #social_media_action to retweet from @gitlabstatus Twitter.

      - Message:
        ```text
        Hi team, please retweet this from our status page to GitLab Twitter about the scheduled maintenance that is taking place tomorrow: {TWEET_LINK}
        ```

      - [ ] @gitlab retweeted from @gitlabstatus

  1. [ ] 🐘 {+ DBRE +}: Confirm that smoke QA tests are passing on the current cluster by checking latest status for `Smoke` Type tests in <%- if production %>[Production](https://gitlab-qa-allure-reports.s3.amazonaws.com/production-sanity/master/index.html) and [Canary](https://gitlab-qa-allure-reports.s3.amazonaws.com/canary-sanity/master/index.html) <%- else %>[Staging](https://gitlab-qa-allure-reports.s3.amazonaws.com/staging-sanity/master/index.html) and [Staging Canary](https://gitlab-qa-allure-reports.s3.amazonaws.com/staging-canary-sanity/master/index.html) <%- end %> Allure reports listed in [QA pipelines](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/debugging-qa-test-failures/#qa-test-pipelines). Reach out to [Test Platform On-call DRI](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/oncall-rotation/#schedule) if there are questions.
  <% end %>

</details>

<details>
  <summary>

  - [ ] __T minus 14 hours (<%= 14.hours.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

## Prepare the environment
 
  1. [ ] 🔪 {+ Playbook-Runner +}: Get the console VM ready for action
      - [ ] SSH to the console VM in `<%= env %>`
      - [ ] Configure dbupgrade user
         - [ ] Disable screen sharing to reduce risk of exposing private key
         - [ ] Change to user dbupgrade `sudo su - dbupgrade`
         - [ ] Copy dbupgrade user's private key from 1Password to `~/.ssh/id_dbupgrade`
         - [ ] `chmod 600 ~/.ssh/id_dbupgrade`
         - [ ] Use key as default `ln -s /home/dbupgrade/.ssh/id_dbupgrade /home/dbupgrade/.ssh/id_rsa`
         - [ ] Repeat the same steps steps on target leader (it also has to have the private key)
         - [ ] Enable re-screen sharing if benficial
      - [ ] Start a / resume the tmux session `tmux a -t pg14 || tmux new -s pg14`
      - [ ] Create an [access_token](https://gitlab.com/-/profile/personal_access_tokens) with at least `read_repository` for the next step
      - [ ] Clone repos:

        ```sh
        rm -rf ~/src \
          && mkdir ~/src \
          && cd ~/src \
          && git clone https://gitlab.com/gitlab-com/gl-infra/db-migration.git \
          && cd db-migration \
          && git checkout latest_stable
        ```

      - [ ] Ensure you have the pre-requisites installed:

        ```sh
        sudo apt install ansible
        ```

      - [ ] Ensure that Ansible can talk to all the hosts in <%= env %>-<%= cluster %>
          ```sh
          cd ~/src/db-migration/pg-upgrade-logical
          ansible -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" -i inventory/<%= env %>-<%= cluster %>.yml all -m ping
          ```

      - [ ] In advance, run pre-checks, and upgrade-check, pre-install packages to ensure that everything is ready for future upgrade:
          ```sh
          cd ~/src/db-migration/pg-upgrade-logical
          ansible-playbook \
            -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" \
            -i inventory/<%= env %>-<%= cluster %>.yml \
            upgrade.yml -e "pg_old_version=12 pg_new_version=14<%- if cluster == 'registry' %> pg_source_dbname=gitlabhq_registry<%- end %>" \
            --tags "pre-checks, packages, upgrade-check" 2>&1 \
          | ts | tee -a ansible_upgrade_pre_checks_<%= env %>_<%= cluster %>_$(date +%Y%m%d).log
          ```

      - [ ] Refresh tmux command and shortcut knowledge, https://tmuxcheatsheet.com/. To leave tmux without stopping it, use sequence `Ctl-b, Ctrl-z` 

        You shouldn't see any failed hosts!

  ## Postgres Upgrade rollout

  ### Pre Postgres upgrade checks

  1. [ ] 🐺 {+ Coordinator +}: Check if disallow_database_ddl_feature_flags is ENABLED:
  <%- if production %>
      - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags`
  <%- else %>
      - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags --staging`
  <%- end %>
  
  1. [ ] 🐺 {+ Coordinator +}: Check if the underlying DDL migrations, patritioning and reindex features were disabled by disallow_database_ddl_feature_flags: 
      - [ ] Open a new rails console
          <%- if production %>
          - __PRODUCTION__: URL [production.teleport.gitlab.net](https://production.teleport.gitlab.net) or tsh:
              ```shell
              tsh login --proxy=production.teleport.gitlab.net --request-roles=rails-ro --request-reason="Validate if Database Migration/Reindex Workers are disabled during PG14 upgrade: <%= gitlab_com_cr %>"
              tsh ssh rails-ro@console-ro-01-sv-gprd
              ```
          <%- else %>
          - __STAGING__: URL [staging.teleport.gitlab.net](https://staging.teleport.gitlab.net) or tsh:
              ```shell
              tsh login --proxy=staging.teleport.gitlab.net --request-roles=rails-ro 
              tsh ssh rails-ro@console-ro-01-sv-gstg
              ```
          <%- end %>
      - [ ] Paste the script in the console
          ```ruby
          def output(name, value)
            color = value ? '31' : '32'
            result = value ? 'enabled' : 'disabled'

            puts "\e[33m#{name} is\e[0m \e[#{color}m#{result}.\e[0m"
          end

          def check
            ActiveRecord::Base.logger = nil
            output('Database::BatchedBackgroundMigration::MainExecutionWorker', Database::BatchedBackgroundMigration::MainExecutionWorker.new.send(:enabled?))
            output('Database::BatchedBackgroundMigration::CiExecutionWorker', Database::BatchedBackgroundMigration::CiExecutionWorker.new.send(:enabled?))
            output('Database::BatchedBackgroundMigration::CiDatabaseWorker', Database::BatchedBackgroundMigration::CiDatabaseWorker.enabled?)
            output('Database::BatchedBackgroundMigrationWorker', Database::BatchedBackgroundMigrationWorker.enabled?)
            output('Gitlab::Database::Reindexing', Gitlab::Database::Reindexing.enabled?)

            is_execute_background_migrations_enabled = !(Feature.enabled?(:disallow_database_ddl_feature_flags, type: :ops) || Feature.disabled?(:execute_background_migrations, type: :ops))
            output('BackgroundMigration::CiDatabaseWorker', is_execute_background_migrations_enabled)
            output('BackgroundMigrationWorker', is_execute_background_migrations_enabled)

            is_database_async_index_operations_enabled = !(Feature.enabled?(:disallow_database_ddl_feature_flags, type: :ops) || Feature.disabled?(:database_async_index_operations, type: :ops))
            output('rake gitlab:db:execute_async_index_operations', is_database_async_index_operations_enabled)

            is_database_async_foreign_key_validation_enabled = Feature.disabled?(:disallow_database_ddl_feature_flags, type: :ops) && Feature.enabled?(:database_async_foreign_key_validation, type: :ops)
            output('rake gitlab:db:validate_async_constraints', is_database_async_foreign_key_validation_enabled)
            output('Gitlab::Database::AsyncConstraints', is_database_async_foreign_key_validation_enabled)

            is_database_async_index_creation_enabled = Feature.disabled?(:disallow_database_ddl_feature_flags, type: :ops) && Feature.enabled?(:database_async_index_creation, type: :ops)
            output('Gitlab::Database::AsyncIndexes', is_database_async_index_creation_enabled)

            is_partition_manager_sync_partitions_enabled = !(Feature.enabled?(:disallow_database_ddl_feature_flags, type: :ops) || Feature.disabled?(:partition_manager_sync_partitions, type: :ops)) 
            output('Gitlab::Database::Partitioning#sync_partitions', is_partition_manager_sync_partitions_enabled)
            output('Gitlab::Database::Partitioning#drop_detached_partitions', is_partition_manager_sync_partitions_enabled)
          end

          check
          ```
      - [ ] Check the output - All workers/tasks should be `disabled`, like for example:
          ```shell
          Database::BatchedBackgroundMigration::MainExecutionWorker is disabled.
          Database::BatchedBackgroundMigration::CiExecutionWorker is disabled.
          Database::BatchedBackgroundMigration::CiDatabaseWorker is disabled.
          Database::BatchedBackgroundMigrationWorker is disabled.
          Gitlab::Database::Reindexing is disabled.
          BackgroundMigration::CiDatabaseWorker is disabled.
          BackgroundMigrationWorker is disabled.
          rake gitlab:db:execute_async_index_operations is disabled.
          rake gitlab:db:validate_async_constraints is disabled.
          Gitlab::Database::AsyncConstraints is disabled.
          Gitlab::Database::AsyncIndexes is disabled.
          Gitlab::Database::Partitioning#sync_partitions is disabled.
          Gitlab::Database::Partitioning#drop_detached_partitions is disabled.
          ```

<%- if production %>
  1. [ ] 🐘 {+ DBRE +} ADD the following silences at <https://alerts.gitlab.net> to silence `WALGBaseBackup` alerts in <%= src_cluster_prefix %> until the end of the maintenance:
      - Start time: `<%= Time.now.strftime('%FT%T.000Z') %>`
          - Duration: `<%= window_in_hours + 14 + 6 %>h`
        - [ ] `env="gprd"`
        - [ ] `type="<%= env %>-<%= src_cluster_prefix %>"`
        - [ ] `alertname=~"WALGBaseBackupFailed|walgBaseBackupDelayed"`

  1. [ ] 🐘 {+ DBRE +} ADD the following silences at <https://alerts.gitlab.net> to silence `WALGBaseBackup` alerts in <%= dst_cluster_prefix %> until the end of the maintenance:
      - Start time: `<%= Time.now.strftime('%FT%T.000Z') %>`
          - Duration: `<%= window_in_hours + 14 + 6 %>h`
        - [ ] `env="gprd"`
        - [ ] `type="<%= env %>-<%= dst_cluster_prefix %>"`
        - [ ] `alertname=~"WALGBaseBackupFailed|walgBaseBackupDelayed"`
<%- end %>

  1. [ ] 🐘 {+ DBRE +}: Monitor what pgbouncer pool has connections: [Thanos](https://thanos.gitlab.net/graph?g0.expr=max(pgbouncer_pools_client_active_connections%7Benv%3D%22<%= env %>%22%2C%20user%3D%22gitlab%22%2C%20type%3D~%22<%= prometheus_type %>%7C<%= prometheus_type %>-v14%22%7D)%20by%20(fqdn%2Cdatabase)&g0.tab=1)

  1. [ ] 🐘 {+ DBRE +}: Check if anyone except application is connected to source primary and interrupt them:
      1. [ ] Login to source primary
          ```sh
          ssh <%= src_cluster_host_prefix %>01-db-<%= env %>.c.<%= gcp_project %>.internal
          ```
      1. [ ] Check all connections that are not `gitlab`:
          ```sh
          gitlab-psql -c "
            select
              pid, client_addr, usename, application_name, backend_type,
              clock_timestamp() - backend_start as connected_ago,
              state,
              left(query, 200) as query
            from pg_stat_activity
            where
              pid <> pg_backend_pid()
              and usename <> 'gitlab'
              and not backend_type ~ '(walsender|logical replication|pg_wait_sampling)'
              and usename not in ('pgbouncer', 'postgres_exporter', 'gitlab-consul')
              and application_name <> 'Patroni'
            "
          ```
      1. [ ] If there are sessions that potentially can perform any writes, spend up to 10 minutes to make an attempt to find the actors and ask them to stop.
      1. [ ] Finally, terminate all the remaining sessions that are not coming from application/infra components and potentially can cause writes:
          ```sh
          gitlab-psql -c "
            select pg_terminate_backend(pid)
            from pg_stat_activity
            where
              pid <> pg_backend_pid()
              and usename <> 'gitlab'
              and not backend_type ~ '(walsender|logical replication|pg_wait_sampling)'
              and usename not in ('pgbouncer', 'postgres_exporter', 'gitlab-consul')
              and application_name <> 'Patroni'
            "
          ```

  1. [ ] 🐘 {+ DBRE +}: Monitor the Primary Leader and Standby Leader PostgreSQL Log Files: 
      1. [ ] Login to node 01:
          ```sh
          ssh <%= src_cluster_host_prefix %>01-db-<%= env %>.c.<%= gcp_project %>.internal
          ssh <%= dst_cluster_host_prefix %>01-db-<%= env %>.c.<%= gcp_project %>.internal
          ```
      1. [ ] Get leader for each cluster:
          ```sh
          sudo gitlab-patronictl list
          ```

      1. [ ] Connect via SSH to the previously identified leaders and tail Postgres logs:
          ```sh
          ssh ... # the leader host here
          sudo tail -f /var/log/gitlab/postgresql/postgresql.csv
          ```

      1. [ ] On the v14 leader, start loop to terminate autovacuum workers, to unblock concurrent vacuumdb workers attempting to ANALYZE tables after pg_upgrade:
          ```shell
          while sleep 10; do
            gitlab-psql -XAtc "
                  select query, pid, pg_terminate_backend(pid)
                  from pg_stat_activity 
                  where query like 'autovacuum: VACUUM % (to prevent wraparound)'" 2>&1 \
              | ts | sudo tee -a /var/opt/gitlab/autovacuum_terminator_$(date +%Y%m%d).log
          done
          ```

  ### Postgres Upgrade
  Playbook source: <https://gitlab.com/gitlab-com/gl-infra/db-migration/-/tree/master/pg-upgrade-logical>

  For this part, since each cluster takes 30-40mins, we will trigger the upgrades in parallel to save time.


  #### UPGRADE – execute!
  1. [ ] 🔪 {+ Playbook-Runner +}: Press `Ctrl-b` then `up` to go to the first terminal.

  1. [ ] 🔪 {+ Playbook-Runner +}: Run Ansible playbook for Upgrading the <%= env %>-<%= cluster %> cluster:  

      ```sh
      cd ~/src/db-migration/pg-upgrade-logical
      ansible-playbook \
        -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" \
        -i inventory/<%= env %>-<%= cluster %>.yml \
        upgrade.yml -e "pg_old_version=12 pg_new_version=14<%- if cluster == 'registry' %> pg_source_dbname=gitlabhq_registry<%- end %>" 2>&1 \
      | ts | tee -a ansible_upgrade_<%= env %>_<%= cluster %>_$(date +%Y%m%d).log
      ```

  ### Post Postgres upgrades verification
  You can execute the following steps as soon as their respective upgrades in the previous step have finished executing.

  1. [ ] 🐘 {+ DBRE +}: Check logical replication lag, and wait to get in sync: [PG14 Upgrade Dashboard](https://dashboards.gitlab.net/d/pg14-upgrade-main-pg14-upgrade/pg14-upgrade-postgres-upgrade-using-logical?orgId=1&var-PROMETHEUS_DS=Global&var-environment=<%= env %>&var-cluster=<%= cluster %>&from=now-1h&to=now&refresh=5m)

  1. [ ] 🐘 {+ DBRE +}: Ensure <%= env %>-<%= cluster %> cluster is in the desired state.
      1. [ ] Login to node 01:
          ```sh
          ssh <%= dst_cluster_host_prefix %>01-db-<%= env %>.c.<%= gcp_project %>.internal
          ```
      1. [ ] Get leader for each cluster:
          ```sh
          sudo gitlab-patronictl list
          ```

      1. [ ] Connect via SSH to the previously identified leaders and tail Postgres logs:
          ```sh
          ssh ... # the leader host here
          sudo tail -f /var/log/gitlab/postgresql/postgresql.csv
          ```

      1. [ ] 🐘 {+ DBRE +}: On the v14 leader, stop the monitoring-terminate of autovacuum workers –  in psql, press Ctrl-C

  1. [ ] 🐘 {+ DBRE +}: Cleanup the Destination GCS  Backup location to avoid conflicts in `wal-g` (**IMPORTANT: perform this action pairing with another DBRE/SRE** to make sure that the you are deleting the right location)

      - GCS Backup Location: <%= dst_gcs_backup_location %>

  1. [ ] 🐘 {+ DBRE +}: Initiate a full backup (using `wal-g`) on the new V14 Patroni **<%= cluster %>** cluster:
      - [ ] SSH to `<%= "#{dst_backup_host}-db-#{env}.c.#{gcp_project}.internal" %>`
        - [ ] Run a [Wal-G backup](https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/patroni/postgresql-backups-wale-walg.md#daily-basebackup):

            ```sh
            sudo su - gitlab-psql
            tmux new -s PGBasebackup
            nohup /opt/wal-g/bin/backup.sh >> /var/log/wal-g/wal-g_backup_push.log 2>&1 &
            ```

  1. [ ] 🐘 {+ DBRE +}: Trigger GCS snapshot on the new v14 Patroni **<%= cluster %>** cluster:
      - [ ] SSH to `<%= "#{dst_backup_host}-db-#{env}.c.#{gcp_project}.internal" %>`
        - [ ] Run a [manual GCS Snapshot](https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/patroni-ci/rebuild_ci_cluster_from_prod.md#take-a-snapshot-from-the-writer-node)
            ```sh
            sudo su - gitlab-psql
            tmux new -s GCSSnapshot
            /usr/local/bin/gcs-snapshot.sh
            ```

  1. [ ] 🐬 {+ SRE +}: Merge the MR that update PostgreSQL dirs and binaries references in Chef for <%= dst_cluster_prefix %>. First confirm there are no errors in merge pipeline. If the MR was merged, then revert it, and get it merged.
      1. [ ] MR for <%= dst_cluster_prefix %>: <%= post_upgrade_mr %>

  1. [ ] 🐬 {+ SRE +}: Ensure that the changes merged from the previous step have been deployed to the Chef server before re-enabling Chef by confirming the linked `master` pipeline for `ops.gitlab.net` completed successfully.

  1. [ ] 🐬 {+ SRE +}: Re-enable Chef in all <%= env %>-<%= cluster %> nodes:
        ```sh
        knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo chef-client-enable"
        ```

  1. [ ] 🐬 {+ SRE +}: Confirm chef-client is enabled in all nodes [thanos link](https://thanos.gitlab.net/graph?g0.expr=chef_client_enabled%7Benv%3D%22<%= env %>%22%2Ctype%3D~%22<%= prometheus_type %>-v14%22%7D&g0.tab=1&g0.stacked=0&g0.range_input=30m&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

  1. [ ] 🐬 {+ SRE +}: Run chef-client on Patroni Nodes:
        ```sh
        knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo chef-client"
        ```
        Confirm that chef-client ran on all nodes [thanos link](https://thanos.gitlab.net/graph?g0.expr=(time()-chef_client_last_run_timestamp_seconds%7Benv%3D%22<%= env %>%22%2Ctype%3D~%22<%= prometheus_type %>-v14%22%7D)%2F60&g0.tab=0&g0.stacked=0&g0.range_input=30m&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - [ ] 🐬 {+ SRE +}: Confirm:
        - [ ] No errors while running chef-client [thanos link](https://thanos.gitlab.net/graph?g0.expr=chef_client_error%7Benv%3D%22<%= env %>%22%2Ctype%3D%22<%= prometheus_type %>%22%7D%20%3E%200&g0.tab=0&g0.stacked=0&g0.range_input=30m&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) and we still have v14 binary.
            ```sh
            knife ssh "roles:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo /usr/lib/postgresql/14/bin/postgres --version"
            ```
            - Output should show `postgres (PostgreSQL) 14.x (Ubuntu 14.x-x.pgdg20.04+1)` for all nodes

        - [ ] Consul service endpoint `<%= consul_replica_endpoint %>-v14.service.consul.` points to v14 nodes, and consul service endpoint `<%= consul_replica_endpoint %>.service.consul.` is pointing to v12 replica nodes.
            ```sh
            dig @127.0.0.1 -p 8600 <%= consul_replica_endpoint %>.service.consul. SRV +short
            dig @127.0.0.1 -p 8600 <%= consul_replica_endpoint %>-v14.service.consul. SRV +short

            dig @127.0.0.1 -p 8600 master.<%= consul_service_name %>.service.consul. SRV +short
            dig @127.0.0.1 -p 8600 master.<%= consul_service_name %>-v14.service.consul. SRV +short
            ```

  1. [ ] 🐬 {+ SRE +}: Stop chef on both old and new clusters, on all nodes, before we execute switchover
      ```sh
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %> OR role:<%= env %>-base-db-<%= src_cluster_prefix %>" "sudo chef-client-disable <%= gitlab_com_cr %>"
      ```

  1. [ ] 🐬 {+ SRE +}: Confirm chef-client is disabled [thanos link](https://thanos.gitlab.net/graph?g0.expr=chef_client_enabled%7Benv%3D%22<%= env %>%22%2Ctype%3D%22<%= prometheus_type %>-v14%22%7D%20%3D%3D%200&g0.tab=1&g0.stacked=0&g0.range_input=30m&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

  1. [ ] 🐘 {+ DBRE +}: A KNOWN ISSUE (__TODO__ to improve) – at this point, it is very likely that logical replication is broken because `.pgpass` on target leader has only 1 line again (chef removed the 2nd line, that is needed to connect to the source leader). Get it back manually – copying the existing line, replacing `localhost` in the beginning with `*`.

  1. [ ] 🐘 {+ DBRE +}: Restart target patroni cluster nodes if `gitlab-patronictl list` command show `Pending restart` is required. A successful cluster restart will display `Success: restart on member` for each cluster memebers and a subsequent `gitlab-patronictl list` command will not show `Pending restart` required
        <%- if production %>
        ```sh
        sudo gitlab-patronictl list
        sudo gitlab-patronictl restart gprd-patroni-<%= cluster %>-v14 --force
        sudo gitlab-patronictl list
        ```
        <%- else %>
        ```sh
        sudo gitlab-patronictl list
        sudo gitlab-patronictl restart gstg-patroni-<%= cluster %>-v14 --force
        sudo gitlab-patronictl list
        ```    
        <%- end %>

  1. [ ] 🐘 {+ DBRE +}: Check logical replication lag, and wait to get in sync: [PG14 Upgrade Dashboard](https://dashboards.gitlab.net/d/pg14-upgrade-main-pg14-upgrade/pg14-upgrade-postgres-upgrade-using-logical?orgId=1&var-PROMETHEUS_DS=Global&var-environment=<%= env %>&var-cluster=<%= cluster %>&from=now-1h&to=now&refresh=5m)
   
  1. [ ] 🐘 {+ DBRE +}: Remove old data_dir to free up disk on all PG14 nodes, `knife ssh "role:<%= env %> -base-db-<%= dst_cluster_prefix %>" "sudo rm -rf /var/opt/gitlab/postgresql/data12"`

  ## Start data corruption check - pg_amcheck

  1. [ ] 🐘 {+ DBRE +}: On the v14 **Replica nodes only**, run `pg_amcheck`. Run tmux and as a nohup command):
      - [ ] On each Replica: `sudo su - gitlab-psql` and start a / resume the tmux session `tmux a -t pg_amcheck || tmux new -s pg_amcheck`
          ```shell
          export PGOPTIONS="-c statement_timeout=30min"
          cd /tmp; nohup time /usr/lib/postgresql/14/bin/pg_amcheck -p 5432 -h localhost -U gitlab-superuser -d gitlabhq_production -j 96  --verbose -P   --heapallindexed 2>&1 | tee -a /var/tmp/pg_amcheck.$(date "+%F-%H-%M").log &
          tail -f /var/tmp/pg_amcheck.$(date "+%F-%H-%M").log
          ```
          - [ ] Monitor logical replication lagging, if it seems that the logical replication is throttling, kill pg_amcheck and start it again with a smaller value for `-j`;

          - [ ] **IMPORTANT: make sure you are not running pg_amcheck in the v14 Writer/Primary node**, as this will cause logical replication lag in the target and spikes of rollbacks and errors

</details>

<details>
  <summary>

  - [ ] __T minus 3 hours (<%= 3.hours.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  <%- if production %>
  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
      - Message:
        ```text
            We will be undergoing scheduled maintenance to our <%= cluster %> database layer in 3 hours. The maintenance will take up to 5 hours starting from <%= maint_start_time.strftime("%H:%M") %> UTC to <%= maint_end_time.strftime("%H:%M") %> UTC. GitLab.com will be available but users may experience degraded performance during the maintenance window. We apologize in advance for any inconvenience this may cause. See <<%= gitlab_com_cr %>>
        ``` 
  <%- end %>

  1. [ ] ☎ {+ Comms-Handler +}:  Send on Slack `#whats-happening-at-gitlab`
    - Message:
      <%- if production %>
      ```text
        :loudspeaker: *Postgres upgrade for our database clusters is scheduled for <%= maint_start_time.strftime("%Y-%m-%d") %> between <%= maint_start_time.strftime("%H:%M") %> UTC and <%= maint_end_time.strftime("%H:%M") %> UTC :rocket:
        This is taking place in 3 hours :hourglass_flowing_sand:

        :hammer_and_wrench: *What to expect?*
        GitLab.com will be available but users may experience degraded performance during the maintenance window. If you experience any issues likely related to the upgrade in the next few days after the upgrade, please open an issue and reach the upgrade team at slack channel - #pg_upgrade.

        You can follow our issue link on ops.gitlab.net for the upgrade.
      ```
      <%- else %>
        ```text
        :loudspeaker: *Postgres upgrade for our database clusters is scheduled for <%= maint_start_time.strftime("%Y-%m-%d") %> between <%= maint_start_time.strftime("%H:%M") %> UTC and <%= maint_end_time.strftime("%H:%M") %> UTC :rocket:
        This is taking place in 3 hours :hourglass_flowing_sand:

        :hammer_and_wrench: *What to expect?*
        https://<%= 'staging.' unless production %>gitlab.com will continue to be available during the maintenence window. If you experience any issues likely related to the upgrade in the next few days after the upgrade, please open an issue and reach the upgrade team at slack channel - #pg_upgrade.

        You can follow our issue link on ops.gitlab.net for Staging upgrade.
        Registry Cluster - https://ops.gitlab.net/gitlab-com/gl-infra/db-migration/-/issues/46

        CI Cluster - https://ops.gitlab.net/gitlab-com/gl-infra/db-migration/-/issues/47

        Main Cluster - https://ops.gitlab.net/gitlab-com/gl-infra/db-migration/-/issues/48
        ``` 
      <%- end %>

  1. [ ] ☎ {+ Comms-Handler +}: Share message from `#whats-happening-at-gitlab` to the following channels:
      - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
      - [ ] `#g_delivery` (cc `@release-managers`)

  1. [ ] 🐘 {+ DBRE +}: Monitor logical replication lagging, if it seems that the logical replication is throttling, kill pg_amcheck

</details>

<details>
  <summary>

  - [ ] __T minus 1 hour (<%= 1.hours.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  1. [ ] 🔪 {+ Playbook-Runner +}: Add the following silences at <https://alerts.gitlab.net> to silence alerts in V14 nodes for the duration of the change + 1 hour:
      - Start time: `<%= maint_start_time.strftime('%FT%T.000Z') %>`
      - Duration: `<%= window_in_hours + 1 %>h`
      - Matchers
        <%- if production %>
        - __PRODUCTION__
          - [ ] `env="gprd"`
          - [ ] `fqdn=~"<%= dst_cluster_prefix %>.*"`
        <%- else %>
        - __STAGING__
          - [ ] `env="gstg"`
          - [ ] `fqdn=~"<%= dst_cluster_prefix %>.*"`
        <%- end %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Add the following silences at <https://alerts.gitlab.net> to silence alerts in V12 nodes for 2 weeks:
      - Start time: `<%= maint_start_time.strftime('%FT%T.000Z') %>`
      - Duration: `<%= window_in_hours + 336 %>h`
      - Matchers
        <%- if production %>
        - __PRODUCTION__
          - [ ] `env="gprd"`
          - [ ] `fqdn=~"<%= src_cluster_prefix %>.*"`
        <%- else %>
        - __STAGING__
          - [ ] `env="gstg"`
          - [ ] `fqdn=~"<%= src_cluster_prefix %>.*"`
        <%- end %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Add the following silences at <https://alerts.gitlab.net> to silence sidekiq alerts for the duration of the change + 1 hour:
      - Start time: `<%= maint_start_time.strftime('%FT%T.000Z') %>`
      - Duration: `<%= window_in_hours + 1 %>h`
      - Matchers
          - [ ] `env="<%= env %>"`
          - [ ] `alertname="SidekiqServiceSidekiqExecutionErrorSLOViolationSingleShard"`
          - [ ] `component="sidekiq_execution"`

  <%- if production %>
  1. [ ] 🔪 {+ Playbook-Runner +}: Schedule a job to enable `gitlab_maintenance_mode` into a node exporter, during the upgrade window:
      - [ ] SSH to a console VM in `<%= env %>` (eg. `ssh console-01-sv-<%= env %>.c.<%= gcp_project %>.internal` )
        - [ ] Schedule jobs:
          ```sh
          sudo su -
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 1\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom | at -t <%= maint_start_time.strftime('%Y%m%d%H%M') %>
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 0\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom | at -t <%= maint_end_time.strftime('%Y%m%d%H%M') %>
          cat /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          atq
          ```
  <%- end %>

  <%- if production %>
  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}:  Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
      - Message:
        ```text
        We will be undergoing scheduled maintenance to our <%= cluster %> database layer in 1 hour. The maintenance will take up to 5 hours starting from <%= maint_start_time.strftime("%H:%M") %> UTC to <%= maint_end_time.strftime("%H:%M") %> UTC. GitLab.com will be available but users may experience degraded performance during the maintenance window. We apologize in advance for any inconvenience this may cause. See <<%= gitlab_com_cr %>>
        ```
  <%- end %>

  <%- if production %>
  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post to `#announcements` on Slack:
      - Message:

        ```text
        Scheduled maintenance to our <%= cluster %> database layer starts in an hour, lasting up to 5 hours. From <%= maint_start_time.strftime("%H:%M") %> UTC to <%= maint_end_time.strftime("%H:%M") %> UTC
        ```
  <%- end %>

  <%- if production %>
  1. [ ] __PRODUCTION ONLY__ ☁ 🔪 {+ Playbook-Runner +}: Create a maintenance window in [PagerDuty](https://gitlab.pagerduty.com/service-directory/maintenance-windows) with the following:

      - Which services are affected?
        - [GitLab Production](https://gitlab.pagerduty.com/service-directory/PATDFCE)
        - [SLO alerts gprd cny stage](https://gitlab.pagerduty.com/service-directory/P3DG414)
        - [SLO Alerts gprd main stage](https://gitlab.pagerduty.com/service-directory/P7Q44DU)
        - [Dean Man's snitch](https://gitlab.pagerduty.com/service-directory/PFQXAWL)

      - Why is this maintenance happening?

        ```text
        Performing Postgres cluster upgrades so silencing the pager.
        ```

      - Select `Start at a scheduled time`:

        - Timezone: `(UTC+00:00) UTC`
        - Start: `<%= maint_start_time.strftime('%m/%d/%Y | %I:%M %p') %>`
        - End: `<%= maint_end_time.strftime('%m/%d/%Y | %I:%M %p') %>`
  <%- end %>

  1. [ ] 🐬 {+ SRE +}: Check that all needed Chef MRs are rebased and contain the proper changes.
      1. [ ] Post-Upgrade MR, to change cluster to use PG14 roles:
          - MR for <%= dst_cluster_prefix %>: <%= post_upgrade_mr %>
      1. [ ] Post-Switchover MR to configure Consul and Prometheus
          - MR for <%= env %>-patroni-<%= cluster %>:  <%= post_switchover_mr %>

  1. [ ] 🐘 {+ DBRE +}: Ensure that we have a successful full WAL-G backup that has taken place in the last 24 hours for each cluster: [Thanos Graph](https://thanos.gitlab.net/graph?g0.expr=gitlab_job_success_timestamp_seconds%7Bresource%3D%22walg-basebackup%22%2C%20env%3D%22gprd%22%2C%20type%3D~%22.*patroni-(main%7Cci%7Cregistry).*%22%7D%20%3E%20gitlab_job_start_timestamp_seconds%7Bresource%3D%22walg-basebackup%22%2C%20env%3D%22gprd%22%2C%20type%3D~%22.*patroni-(main%7Cci%7Cregistry).*%22%7D%20and%20on%20(resource%2C%20env%2C%20type)%20gitlab_job_failed%20%3D%3D%200&g0.tab=0&g0.stacked=0&g0.range_input=2d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

      - If you see 2 rows (one for each cluster: MAIN + CI + REGISTRY), then a gap, then another 2 rows, then __you have a recent (< 24 hours) successful backup__. The gap is the period of time when the backups were executing. You can use a [timestamp converter](https://www.epochconverter.com/) to convert the timestamps into human-readable date/time if you want to check when the backup finished.

      - If you see that currently there are no lines (empty result), then it's possible that the backups are still running __OR__ they have failed, so check the following:

        - Check this [Thanos graph](https://thanos.gitlab.net/graph?g0.expr=gitlab_job_start_timestamp_seconds%7Bresource%3D%22walg-basebackup%22%2C%20env%3D%22<%= env %>%22%2C%20type%3D~%22.*patroni-(main%7Cci%7Cregistry).*%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=1w&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) to see the start time of the backup job - you should be able to see an increase of the value every time the backup starts (around midnight). If the last increase was more than 24 hours ago then it means that the last backup hasn't started as it should have, and you'll need to investigate why the job failed to start.

        - If the backup job should have finished by now, then you should check this [Thanos graph](https://thanos.gitlab.net/graph?g0.expr=gitlab_job_failed%7Bresource%3D%22walg-basebackup%22%2C%20env%3D%22<%= env %>%22%2C%20type%3D~%22.*patroni-(main%7Cci%7Cregistry).*%22%7D%20%3E%200&g0.tab=0&g0.stacked=0&g0.range_input=1d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) to see the job failed value for the last time the backup job ran. If the value is `> 0` for any time in the past 24 hours, then you'll need to investigate why the job failed.

        The backup job is triggered by `crond` (user: `gitlab-psql`) and any replica is eligible to run the job but it only runs on the one that acquires the consul lock.

  1. [ ] 🐘 {+ DBRE +}: On the v14 Replica nodes, review the pg_amcheck log files created in the previous steps to find out any data corruption errors and check amcheck progress:
      ```shell
      egrep 'ERROR:|DETAIL:|LOCATION:' /var/tmp/pg_amcheck.*.log
      cat /var/tmp/pg_amcheck.*.log | grep relations | tail -1
      ```

  1. [ ] 🐘 {+ DBRE +}: Monitor logical replication lagging, if it seems that the logical replication is throttling, kill pg_amcheck

</details>

## Postgres Upgrade Call
These steps will be run in a video call. The 🐺 {+ Coordinator +} runs the call,
asking other roles to perform each step on the checklist at the appropriate
time.

Changes are made one at a time, and verified before moving onto the next step.
All the steps will be executed from a console VM and we should keep the session
shared (tmux, screen...).

Whoever is performing a change should share their screen and explain their
actions as they work through them. Everyone else should watch closely for
mistakes or errors! A few things to keep an especially sharp eye out for:

- Exposed credentials (except short-lived items like 2FA codes)
- Running commands against the wrong hosts
- Navigating to the wrong pages in web browsers (gstg vs. gprd, etc)

Remember that the call will be recorded. We will consider making it public after we confirmed that no [SAFE data](https://about.gitlab.com/handbook/legal/safe-framework/) was leaked.
If you see something happening that shouldn't be public, mention it.

### Roll call
- [ ] 🐺 {+ Coordinator +}: Mark the change request as `/label ~"change::in-progress"`
- [ ] 🐺 {+ Coordinator +}: Ensure everyone mentioned above is on the call
- [ ] 🐺 {+ Coordinator +}: Ensure the video call room host is on the call

### Data Corruption Checks

1. [ ] 🐘 {+ DBRE +}: On the **v14 Replica Nodes**, review the pg_amcheck log files created in the previous steps to find out any data corruption errors and to get the last status of the progress:
    ```shell
    egrep 'ERROR:|DETAIL:|LOCATION:' /var/tmp/pg_amcheck.*.log
    cat /var/tmp/pg_amcheck.*.log | grep relations | tail -1
    ```

1. [ ] 🐺 {+ Coordinator +}: If there are **any errors that indicate possible data corruption, then abort the Maintenance** and proceed with the partial rollback of the steps already performed;

1. [ ] 🐘 {+ DBRE +}: On the **v14 Replica Nodes**, kill `pg_amcheck` processes:
    ```shell
    sudo killall pg_amcheck
    ps -ef | grep pg_amcheck
    ```

1. [ ] 🐘 {+ DBRE +}: On the **v14 Replica Nodes**, terminate any existing backend processes:
    ```
    sudo gitlab-psql -c "
    select pg_terminate_backend(pid)
    from pg_stat_activity
    where
      pid <> pg_backend_pid()
      and usename <> 'gitlab'
      and not backend_type ~ '(walsender|logical replication|pg_wait_sampling)'
      and usename not in ('pgbouncer', 'postgres_exporter', 'gitlab-consul')
      and application_name <> 'Patroni'
    "
    ```

1. [ ] 🐺 {+ Coordinator +}: [optional] Double check that no `pg_amcheck` processes nor queries are running on the **v14 Replica Nodes**.
    ```shell
    ps -ef | grep pg_amcheck

    sudo gitlab-psql -c "
    select pid, usename, application_name, client_addr, substr(query,1,120) as "query"
    from pg_stat_activity
    where
      usename <> 'gitlab'
      and not backend_type ~ '(walsender|logical replication|pg_wait_sampling)'
      and usename not in ('pgbouncer', 'postgres_exporter', 'gitlab-consul')
      and application_name <> 'Patroni'
    "
    ```

### Pre-maintenance Health Checks

<%- if production %>
1. [ ] 🐺 {+ Coordinator +}: Check if [gitlab_maintenance_mode is enabled for <%= env %> (Thanos link)](https://thanos.gitlab.net/graph?g0.expr=max(gitlab_maintenance_mode%7Benv%3D%22<%= env %>%22%7D)&g0.tab=0&g0.stacked=0&g0.range_input=1h)
    - If is not enabled ask 🔪 {+ Playbook-Runner +} to manually enable it by:
      - SSH to a console VM in `<%= env %>` (eg. `ssh console-01-sv-<%= env %>.c.<%= gcp_project %>.internal` )
        - Set `gitlab_maintenance_mode=1` on node exporter :
          ```sh
          sudo su -
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 1\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          cat /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          atq
          ```
<%- end %>

1. [ ] 🐺 {+ Coordinator +}: Check if disallow_database_ddl_feature_flags is ENABLED:
  <%- if production %>
    - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags`
  <%- else %>
    - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags --staging`
  <%- end %>
  
1. [ ] 🐺 {+ Coordinator +}: Check if the underlying DDL migrations, patritioning and reindex features were disabled by disallow_database_ddl_feature_flags: 
    - [ ] Open a new rails console
        <%- if production %>
        - __PRODUCTION__: URL [production.teleport.gitlab.net](https://production.teleport.gitlab.net) or tsh:
            ```shell
            tsh login --proxy=production.teleport.gitlab.net --request-roles=rails-ro --request-reason="Validate if Database Migration/Reindex Workers are disabled during PG14 upgrade: <%= gitlab_com_cr %>"
            tsh ssh rails-ro@console-ro-01-sv-gprd
            ```
        <%- else %>
        - __STAGING__: URL [staging.teleport.gitlab.net](https://staging.teleport.gitlab.net) or tsh:
            ```shell
            tsh login --proxy=staging.teleport.gitlab.net --request-roles=rails-ro 
            tsh ssh rails-ro@console-ro-01-sv-gstg
            ```
        <%- end %>
    - [ ] Paste the script in the console
        ```ruby
        def output(name, value)
          color = value ? '31' : '32'
          result = value ? 'enabled' : 'disabled'
          puts "\e[33m#{name} is\e[0m \e[#{color}m#{result}.\e[0m"
        end
        def check
          ActiveRecord::Base.logger = nil
          output('Database::BatchedBackgroundMigration::MainExecutionWorker', Database::BatchedBackgroundMigration::MainExecutionWorker.new.send(:enabled?))
          output('Database::BatchedBackgroundMigration::CiExecutionWorker', Database::BatchedBackgroundMigration::CiExecutionWorker.new.send(:enabled?))
          output('Database::BatchedBackgroundMigration::CiDatabaseWorker', Database::BatchedBackgroundMigration::CiDatabaseWorker.enabled?)
          output('Database::BatchedBackgroundMigrationWorker', Database::BatchedBackgroundMigrationWorker.enabled?)
          output('Gitlab::Database::Reindexing', Gitlab::Database::Reindexing.enabled?)

          is_execute_background_migrations_enabled = !(Feature.enabled?(:disallow_database_ddl_feature_flags, type: :ops) || Feature.disabled?(:execute_background_migrations, type: :ops))
          output('BackgroundMigration::CiDatabaseWorker', is_execute_background_migrations_enabled)
          output('BackgroundMigrationWorker', is_execute_background_migrations_enabled)

          is_database_async_index_operations_enabled = !(Feature.enabled?(:disallow_database_ddl_feature_flags, type: :ops) || Feature.disabled?(:database_async_index_operations, type: :ops))
          output('rake gitlab:db:execute_async_index_operations', is_database_async_index_operations_enabled)

          is_database_async_foreign_key_validation_enabled = Feature.disabled?(:disallow_database_ddl_feature_flags, type: :ops) && Feature.enabled?(:database_async_foreign_key_validation, type: :ops)
          output('rake gitlab:db:validate_async_constraints', is_database_async_foreign_key_validation_enabled)
          output('Gitlab::Database::AsyncConstraints', is_database_async_foreign_key_validation_enabled)

          is_database_async_index_creation_enabled = Feature.disabled?(:disallow_database_ddl_feature_flags, type: :ops) && Feature.enabled?(:database_async_index_creation, type: :ops)
          output('Gitlab::Database::AsyncIndexes', is_database_async_index_creation_enabled)
          is_partition_manager_sync_partitions_enabled = !(Feature.enabled?(:disallow_database_ddl_feature_flags, type: :ops) || Feature.disabled?(:partition_manager_sync_partitions, type: :ops)) 
          output('Gitlab::Database::Partitioning#sync_partitions', is_partition_manager_sync_partitions_enabled)
          output('Gitlab::Database::Partitioning#drop_detached_partitions', is_partition_manager_sync_partitions_enabled)
        end

        check
        ```
    - [ ] Check the output - All workers/tasks should be `disabled`, like for example:
        ```shell
        Database::BatchedBackgroundMigration::MainExecutionWorker is disabled.
        Database::BatchedBackgroundMigration::CiExecutionWorker is disabled.
        Database::BatchedBackgroundMigration::CiDatabaseWorker is disabled.
        Database::BatchedBackgroundMigrationWorker is disabled.
        Gitlab::Database::Reindexing is disabled.
        BackgroundMigration::CiDatabaseWorker is disabled.
        BackgroundMigrationWorker is disabled.
        rake gitlab:db:execute_async_index_operations is disabled.
        rake gitlab:db:validate_async_constraints is disabled.
        Gitlab::Database::AsyncConstraints is disabled.
        Gitlab::Database::AsyncIndexes is disabled.
        Gitlab::Database::Partitioning#sync_partitions is disabled.
        Gitlab::Database::Partitioning#drop_detached_partitions is disabled.
        ```

1. [ ] 🐺 {+ Coordinator +}: Ensure that there are no active critical alerts (s1) or open incidents:
    <%- if production %>
    - PRODUCTION:
        - <https://dashboards.gitlab.net/d/SOn6MeNmk/alerts?orgId=1&var-interval=1m&var-environment=gprd&var-alertname=All&var-alertstate=All&var-prometheus=prometheus-01-inf-gprd&var-prometheus_app=prometheus-app-01-inf-gprd>
        - <https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=incident>
    <%- else %>
    - STAGING:
        - <https://dashboards.gitlab.net/d/SOn6MeNmk/alerts?orgId=1&var-interval=1m&var-environment=gstg&var-alertname=All&var-alertstate=All&var-prometheus=prometheus-01-inf-gstg&var-prometheus_app=prometheus-app-01-inf-gstg>
    <%- end %>

1. [ ] 🐺 {+ Coordinator +}: Check Sentry if there are errors that might indicate database problems: <%- if production %>[Production Sentry](https://new-sentry.gitlab.net/organizations/gitlab/performance/)<%- else %>[Staging Sentry](https://new-sentry.gitlab.net/organizations/gitlab/performance/)<%- end %>

1. [ ] 🐘 {+ DBRE +}: Ensure writes are happening on Postgres/Patroni nodes in <%= env %>-<%= cluster %>: [Thanos](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22<%= env %>%22%2Cfqdn%3D~"<%= src_cluster_prefix %>.*"%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22<%= env %>%22%2Cfqdn%3D~"<%= src_cluster_prefix %>.*"%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22<%= env %>%22%2Cfqdn%3D~"<%= src_cluster_prefix %>.*"%7D%5B1m%5D))%20by%20(instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

1. [ ] 🐘 {+ DBRE +} Check prometheus sanity check metrics for reads all going to the correct hosts
    1. [Index reads](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22<%= env %>%22%2Cinstance%3D~%22(<%= src_cluster_host_prefix %>.*)%22%7D%5B1m%5D))%20by%20(instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
        - [ ] Expected result: all queries going to the `<%= src_cluster_prefix %>` cluster.
    1. [Sequential scans](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22<%= env %>%22%2Cinstance%3D~%22(<%= src_cluster_host_prefix %>.*)%22%7D%5B1m%5D))%20by%20(instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
        - [ ] Expected result: all queries going to the `<%= src_cluster_prefix %>` cluster.



#### Terminals
You should already be in a `tmux` session, but only if you are planning to upgrade two clusters at the same time, open a second pane so that we have one terminal for each cluster.
  - Press `Ctrl-b` then `"` your existing terminal in `tmux` to open a new pane, split horizontally.
  - You can move between the panes by pressing `Ctrl-b` then `up` or `down` arrows.

1. [ ] 🔪 {+ Playbook-Runner +}: Verify that the ansible inventory is up to date and reflects the real state from the cluster.

      - [ ] Once again, ensure that Ansible can talk to all the hosts in <%= env %>-<%= cluster %>:
          ```sh
          cd ~/src/db-migration/pg-upgrade-logical
          ansible -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" -i inventory/<%= env %>-<%= cluster %>.yml all -m ping
          ```

        You shouldn't see any failed hosts!

# T minus zero (<%= maint_start_time.strftime('%Y-%m-%d %H:%M') %> UTC)

We expect the maintenance window to last for up to <%= window_in_hours %> hours, starting from now.

### Pre Switchover checks (T plus 0 min)
1. [ ] 🐘 {+ DBRE +}: Monitor what pgbouncer pool has connections: [Thanos](https://thanos.gitlab.net/graph?g0.expr=max(pgbouncer_pools_client_active_connections%7Benv%3D%22<%= env %>%22%2C%20user%3D%22gitlab%22%2C%20type%3D~%22<%= prometheus_type %>%7C<%= prometheus_type %>-v14%22%7D)%20by%20(fqdn%2Cdatabase)&g0.tab=1)

1. [ ] 🐘 {+ DBRE +}: Monitor the Primary Leader and Standby Leader PostgreSQL Log Files: 
    1. [ ] Login to node 01:
        ```sh
        ssh <%= src_cluster_host_prefix %>01-db-<%= env %>.c.<%= gcp_project %>.internal
        ssh <%= dst_cluster_host_prefix %>01-db-<%= env %>.c.<%= gcp_project %>.internal
        ```
    1. [ ] Get leader for each cluster:
        ```sh
        sudo gitlab-patronictl list
        ```

    1. [ ] Connect via SSH to the previously identified leaders and tail Postgres logs:
        ```sh
        ssh ... # the leader host here
        sudo tail -f /var/log/gitlab/postgresql/postgresql.csv
        ```

1. [ ] 🐬 {+ SRE +}: Confirm chef-client is disabled [thanos link](https://thanos.gitlab.net/graph?g0.expr=chef_client_enabled%7Benv%3D%22<%= env %>%22%2Ctype%3D%22<%= prometheus_type %>-v14%22%7D%20%3D%3D%200&g0.tab=1&g0.stacked=0&g0.range_input=30m&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

### Evaluation of QA/Validations results - Commitment
If QA/Validations has succeeded, then we can continue to "Complete the Upgrade and Switchover to v14". If some
QA/Validations has failed, the 🐺 {+ Coordinator +} must decide whether to continue with the
upgrade, or to [rollback](#rollback-if-required). A decision to continue
in these circumstances should be counter-signed by the 🎩 {+ Head Honcho +}.

The following are commitment criteria:

Goals:
1. The top priority is to maintain data integrity. Rolling back after the maintenance
   window has ended is very difficult, and will result in any changes made in the
   interim being lost.
1. Failures with an unknown cause should be investigated further. If we can't
   determine the root cause within the maintenance window, we should rollback.


### Postgres Upgrade - Switchover (T plus `~30` mins)

Playbook source: <https://gitlab.com/gitlab-com/gl-infra/db-migration/-/tree/master/pg-upgrade-logical>

#### SWITCHOVER – execute!

1. [ ] 🔪 {+ Playbook-Runner +}: Press `Ctrl-b` then `up` to go to the first terminal.
1. [ ] 🔪 {+ Playbook-Runner +}: Run Ansible playbook to Switchover the <%= env %>-<%= cluster %> cluster (it is interactive; reply "y" three times):
    ```sh
    cd ~/src/db-migration/pg-upgrade-logical
    ansible-playbook \
      -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" <%- if cluster == 'registry' %>-e "pg_source_dbname=gitlabhq_registry"<%- end %>\
      -i inventory/<%= env %>-<%= cluster %>.yml \
      switchover.yml 2>&1 \
    | ts | tee -a ansible_switchover_<%= env %>_<%= cluster %>_$(date +%Y%m%d).log
    ```

    - switchover.yml asks to confirm steps 3 times (type "y"). After each step, DBRE has to verify traffic to proper nodes, lack of errors, DB latencies and confirm decision to continue
      - [ ] 🔪 {+ Playbook-Runner +}: First "y": **start R/O traffic to new v14 replicas** 
      - [ ] 🐬 {+ SRE +}: After 1st YES, The sevice `<%= consul_replica_endpoint %>.service.consul.` should be pointing to both V12 and V14 replica nodes
          ```sh
          dig @127.0.0.1 -p 8600 <%= consul_replica_endpoint %>.service.consul. SRV +short

          dig @127.0.0.1 -p 8600 master.<%= consul_service_name %>.service.consul. SRV +short
          ```
      - [ ] 🐘 {+ DBRE +}: Check Read-Only activity metrics for 15 minutes [PG14 Upgrade Dashboard](https://dashboards.gitlab.net/d/pg14-upgrade-main-pg14-upgrade/pg14-upgrade-postgres-upgrade-using-logical?orgId=1&var-PROMETHEUS_DS=Global&var-environment=<%= env %>&var-cluster=<%= cluster %>&from=now-1h&to=now&refresh=5m)
        - [ ] Compare the volume of `standbys TPS (commits)` between Target vs Source (it should split workload 50/50 between old and new replicas)
        - [ ] Compare volume of `rollback TPS – ERRORS` 
      - [ ] 🐺 {+ Coordinator +}: Wait for the end of the hourly Write TPS spike to finish (around 18m past the hour)
      - [ ] 🔪 {+ Playbook-Runner +}: Second "y": **stop R/O traffic to old replicas**
      - [ ] 🐬 {+ SRE +}: After 2nd YES, The sevice `<%= consul_replica_endpoint %>.service.consul.` should be pointing only to the new V14 replica nodes
          ```sh
          dig @127.0.0.1 -p 8600 <%= consul_replica_endpoint %>.service.consul. SRV +short

          dig @127.0.0.1 -p 8600 master.<%= consul_service_name %>.service.consul. SRV +short
          ```
      - [ ] 🐘 {+ DBRE +}: Check the metrics for as long as we observe connections to the `SOURCE` standbys, minimum time 15 minutes.  (this is not blocking the E2E tests).
      - [ ] 🐘 {+ DBRE +}: Trigger Smoke E2E suite against the environment that was upgraded:<%- if production %>[Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules): `Four hourly smoke tests`<%- else %>[Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules): `Four hourly smoke tests`<%- end %>. This has an estimated duration of 15 minutes.
      - [ ] 🐘 {+ DBRE +}: If the smoke tests fail, DBRE should re-run the failed job to see if it is reproducible. In parallel DBRE should reach out to [on-call Test Platform DRI](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/oncall-rotation/#schedule) for the help with investigation. If there is no avialable on-call DRI, reach out to `#test-platform` and escalate with the [management team](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/#management-team).
      - [ ] 🏆 {+ Quality +}: If the smoke tests fail, Quality performs an initial triage of the failure. If Quality cannot determine failure is 'unrelated' within that period - stop and reschedule the whole procedure.
      - [ ] 🐺 {+ Coordinator +}: This is the **point of no return**! We will not execute a rollback after this point! Proceed wisely!
        - [ ] Get agreement of peers and concent of 🎩 {+ Head Honcho +} to proceed
      - [ ] 🐺 {+ Coordinator +}: Wait for the end of the hourly Write TPS spike to finish (around 18m past the hour)
        - [ ] Only proceed with R/W traffic primary switchover **when logical replication lag < 500 MiB**
      - [ ] 🔪 {+ Playbook-Runner +}: Third "y": R/W traffic, primary switchover. 
      - [ ] 🐬 {+ SRE +}: After 3rd YES, the master service for `master.<%= consul_service_name %>.service.consul.` should be pointing only to the new V14 Leader/Writer node.
          ```sh
          dig @127.0.0.1 -p 8600 master.<%= consul_service_name %>.service.consul. SRV +shorthort
          ```
    - [ ] 🔪 {+ Playbook-Runner +}: If the first "y" fails, repeat in the "forced" mode:
      ```sh
      cd ~/src/db-migration/pg-upgrade-logical
      ansible-playbook \
        -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" <%- if cluster == 'registry' %>-e "pg_source_dbname=gitlabhq_registry"<%- end %>\
        -i inventory/<%= env %>-<%= cluster %>.yml \
        switchover.yml -e "force_mode=true" 2>&1 \
      | ts | tee -a ansible_switchover_<%= env %>_<%= cluster %>_$(date +%Y%m%d)_FORCE_MODE.log
      ```

#### Post Postgres Switchover verification
You can execute the following steps as soon as their respective upgrades in the previous step have finished executing.
1. [ ] 🐘 {+ DBRE +}: Ensure <%= cluster %> cluster is in desired state.
    1. [ ] Login to node 01:
        ```sh
        ssh <%= dst_cluster_host_prefix %>01-db-<%= env %>.c.<%= gcp_project %>.internal
        ```
    1. [ ] Get leader for each cluster:
        ```sh
        sudo gitlab-patronictl list
        ```

    1. [ ] Connect via SSH to the previously identified leaders and tail Postgres logs:
        ```sh
        ssh ... # the leader host here
        sudo tail -f /var/log/gitlab/postgresql/postgresql.csv
        ```

    1. [ ] 🐘 {+ DBRE +}: On the v14 leader, stop the monitoring-terminate of autovacuum workers –  in psql, press Ctrl-C.

1. [ ] 🐘 {+ DBRE +}: Trigger Smoke E2E suite against the environment that was upgraded:<%- if production %>[Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules): `Four hourly smoke tests`<%- else %>[Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules): `Four hourly smoke tests`<%- end %>

#### Metrics sanity check after switchover to v14
<%- if production %>
1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message:
      ```text
      GitLab.com planned maintenance for the database layer is almost complete. We're continuing to verify that all systems are functioning correctly. Thank you for your patience.
      ```
<%- end %>

<%- if cluster == 'registry' %>
1. [ ] 🐬 {+ SRE +}: Merge the MR that updates registry teleport DB endpoint. First confirm there are no errors in merge pipeline. If the MR was merged, then revert it, and get it merged.
    1. [ ] MR for registry teleport DB endpoint: <%= teleport_db_endpoint_mr %>
<%- else %>
- [ ] 🐬 {+ SRE +}: Merge the MR that updates <%= cluster %> teleport DB endpoint, the MR that updates the console config endpoint and the MR that updates the source snapshot of the <%= cluster %>-data-analytics DB. First confirm there are no errors in merge pipeline. If the MR was merged, then revert it, and get it merged.
    1. [ ] MR for <%= cluster %> teleport DB endpoint: <%= teleport_db_endpoint_mr %>
    1. [ ] MR for <%= cluster %> console config endpoint: <%= console_config_endpoint_mr %>
    1. [ ] MR for <%= cluster %> snapshot image: <%= snapshot_image_mr %>
<%- end %>
1. [ ] 🐘 {+ DBRE +}: Ensure writes are happening on Postgres/Patroni nodes in <%= env %>-<%= cluster %>: [Thanos](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22<%= env %>%22%2Cfqdn%3D~"<%= dst_cluster_prefix %>-.*"%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22<%= env %>%22%2Cfqdn%3D~"<%= dst_cluster_prefix %>-.*"%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22<%= env %>%22%2Cfqdn%3D~"patroni-<%= cluster %>-.*"%7D%5B1m%5D))%20by%20(instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

1. [ ] 🐘 {+ DBRE +} Check prometheus sanity check metrics for reads all going to the correct hosts
    1. [Index reads](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22<%= env %>%22%2Cinstance%3D~%22(<%= dst_cluster_host_prefix %>.*)%22%7D%5B1m%5D))%20by%20(instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
        - [ ] Expected result: all queries going to the `<%= dst_cluster_prefix %>` cluster.
    1. [Sequential scans](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22<%= env %>%22%2Cinstance%3D~%22(<%= dst_cluster_host_prefix %>.*)%22%7D%5B1m%5D))%20by%20(instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - [ ] Expected result: all queries going to the `<%= dst_cluster_prefix %>` cluster.

1. [ ] 🐘 {+ DBRE +} Check Sentry if there are errors that might indicate database problems: <%- if production %>[Production Sentry](https://sentry.gitlab.net/gitlab/gitlabcom/)<%- else %>[Staging Sentry](https://sentry.gitlab.net/gitlab/staginggitlabcom/)<%- end %>

1. [ ] 🐬 {+ SRE +}: Merge the MR that update Consul and Prometheus. First confirm there are no errors in merge pipeline. If the MR was merged, then revert it, and get it merged.
    1. [ ] MR for <%= env %>-patroni-<%= cluster %>: <%= post_switchover_mr %>

1. [ ] 🐬 {+ SRE +}: Ensure that the changes merged from the previous step have been deployed to the Chef server before re-enabling Chef by confirming the linked `master` pipeline for `ops.gitlab.net` completed successfully.

1. [ ] 🐬 {+ SRE +}: Start cron.service on all <%= env %>-<%= cluster %> nodes:
      ```sh
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo systemctl is-active cron.service"
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo systemctl start cron.service"
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo systemctl is-active cron.service"
      ```

1. [ ] 🐬 {+ SRE +}: Re-enable Chef in all <%= env %>-<%= cluster %> nodes:
    ```sh
    knife ssh "role:<%= env %>-base-db-<%= src_cluster_prefix %> OR role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo chef-client-enable"
    ```

1. [ ] 🐬 {+ SRE +}: Confirm Chef is enabled in all nodes [thanos link](https://thanos.gitlab.net/graph?g0.expr=chef_client_enabled%7Benv%3D%22<%= env %>%22%2Ctype%3D%22<%= prometheus_type %>%22%7D&g0.tab=1&g0.stacked=0&g0.range_input=30m&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

1. [ ] 🐬 {+ SRE +}: Run chef-client on Patroni Nodes:
      ```sh
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %> OR role:<%= env %>-base-db-<%= src_cluster_prefix %>" "sudo chef-client"
      ```
    - [ ] 🐬 {+ SRE +}: Confirm:
      - [ ] No errors while running chef-client [thanos link](https://thanos.gitlab.net/graph?g0.expr=chef_client_error%7Benv%3D%22<%= env %>%22%2Ctype%3D%22<%= prometheus_type %>%22%7D%20%3E%200&g0.tab=0&g0.stacked=0&g0.range_input=30m&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) and we still have v14 binary.
          ```sh
          knife ssh "roles:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo /usr/lib/postgresql/14/bin/postgres --version"
          ```
      - [ ] The sevice `<%= consul_replica_endpoint %>.service.consul.` should be pointing only to the new V14 replica nodes, and the master service for `master.<%= consul_service_name %>.service.consul.` should be pointing only to the new V14 Leader/Writer node.
          ```sh
          dig @127.0.0.1 -p 8600 <%= consul_replica_endpoint %>.service.consul. SRV +short

          dig @127.0.0.1 -p 8600 master.<%= consul_service_name %>.service.consul. SRV +short
          ```

#### Communicate
1. [ ] 🐺 {+ Coordinator +}:  __TODO__ Remove the broadcast message (if it's after the initial window, it has probably expired automatically)

<%- if production %>
1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - [ ] Click "Finish Maintenance" and send the following:
       - Message:
          ```text
          GitLab.com planned maintenance for the database layer is complete. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.
          ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: In the same thread from the earlier post, post the following message and click on the checkbox "Also send to X channel" so the threaded message would be published to the channel:
   - Message:
      ```text
      :done: *GitLab.com database layer maintenance upgrade is complete now.* :celebrate:
      We’ll continue to monitor the platform to ensure all systems are functioning correctly.
      ```
      - [ ] `#whats-happening-at-gitlab`
      - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
      - [ ] `#g_delivery` (cc `@release-managers`)
1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send a message to `#social_media_action` to unpin maintenance tweet on `@gitlab` Twitter:
   - Message:

      ```text
      Hi team :wave:, the maintenance upgrade is complete now, you may unpin the maintenance tweet on GitLab Twitter.
      ```
<%- end %>

<details>
  <summary>

### Complete the Upgrade (T plus `~<%= window_in_hours * 60 - 10 %>` mins)

  </summary>

#### Verification

1. __Start Post Switchover to v14 QA__
    1. [ ] 🐘 {+ DBRE +}: Trigger Smoke and Full E2E suite against the environment that was upgraded:<%- if production %>[Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules): `Four hourly smoke tests`, and `Twice daily full run`<%- else %>[Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules): `Four hourly smoke tests`, and `Daily Full QA suite`<%- end %>

#### Wrapping up
<%- if production %>
1. [ ] __PRODUCTION ONLY__ 🔪 {+ Playbook-Runner +}: If the scheduled maintenance is still active in [PagerDuty](https://gitlab.pagerduty.com/service-directory/maintenance-windows), click on `Update` then `End Now`.
<%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: Remove silences of `fqdn=~"<%= dst_cluster_prefix %>.*"` we created during this process from <https://alerts.gitlab.net> (**Important: don't remove the silence of SOURCE nodes**)

<%- if production %>
1. [ ] 🔪 {+ Playbook-Runner +}: ADD the following silences at <https://alerts.gitlab.net> to silence `WALGBaseBackup` alerts in <%= src_cluster_prefix %> for 2 weeks (14 days = 336 hours)
    - Start time: `<%= Time.now.strftime('%FT%T.000Z') %>`
    - Duration: `336h`
      - [ ] `env="gprd"`
      - [ ] `type="<%= env %>-<%= src_cluster_prefix %>"`
      - [ ] `alertname=~"WALGBaseBackupFailed|walgBaseBackupDelayed"`
<%- end %>

1. [ ] 🐘 {+ DBRE +}: Initiate a full backup (using `wal-g`) and trigger GCS snapshot on the new v14 Patroni **<%= cluster %>** cluster:
    - [ ] SSH to `<%= "#{dst_backup_host}-db-#{env}.c.#{gcp_project}.internal" %>`
        - [ ] Run a [Wal-G backup](https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/patroni/postgresql-backups-wale-walg.md#daily-basebackup):
          ```sh
          sudo su - gitlab-psql
          tmux new -s PGBasebackup
          nohup /opt/wal-g/bin/backup.sh >> /var/log/wal-g/wal-g_backup_push.log 2>&1 &
          ```
    - [ ] Open another SSH session to `<%= "#{dst_backup_host}-db-#{env}.c.#{gcp_project}.internal" %>`
        - [ ] Run a [manual GCS Snapshot](https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/patroni-ci/rebuild_ci_cluster_from_prod.md#take-a-snapshot-from-the-writer-node)
          ```sh
          sudo su - gitlab-psql
          tmux new -s GCSSnapshot
          /usr/local/bin/gcs-snapshot.sh
          ```

1. [ ] 🐘 {+ DBRE +}: DBRE team (after an hour): Check that the Smoke, and Full E2E suite has passed. If there are failures, reach out to [on-call Test Platform DRI](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/oncall-rotation/#schedule) for the help with investigation. If there is no avialable on-call DRI, reach out to `#test-platform` and escalate with the [management team](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/#management-team).

1. [ ] 🏆 {+ Quality +}: If the Smoke or Full E2E tests fail, Quality performs an initial triage of the failure. If Quality cannot determine failure is 'unrelated', team decides on [declaring an incident](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/debugging-qa-test-failures/#failure-needs-escalation) and following the incident process.

1. [ ] 🐘 {+ DBRE +}: Trigger smoke tests one more time now that Chef would have had time to run:

<%- if production %>
1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Notify our customer over [Slack channel](https://gitlab.slack.com/archives/CDRLZUVHB) that Postgres Upgrade finished, and request to validate GitLab.com.

1. [ ] __PRODUCTION ONLY__ 🐘 {+ DBRE +}: Ping `@NikolayS` on the gitlab.com CR (<<%= gitlab_com_cr %>>) (and/or in [#database-lab](https://gitlab.slack.com/archives/CLJMDRD8C)) that the work is complete so he can update the DLE environment.

1. [ ] __PRODUCTION ONLY__ 🐬 {+ SRE +}: Confirm that Teleport access works properly and we're hitting the right clusters:
    ```bash
    # Login to Teleport
    tsh login --add-keys-to-agent=no --proxy=production.teleport.gitlab.net --request-roles=database-ro-gprd --request-reason="Testing DB connectivity after PG14 Upgrade - <%= gitlab_com_cr %> "
    query="select setting from pg_settings where name='cluster_name'"

    # Check MAIN
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=gitlabhq_production db-main-primary-gprd --add-keys-to-agent=no
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=gitlabhq_production db-main-replica-gprd --add-keys-to-agent=no

    # Check CI
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=gitlabhq_production db-ci-primary-gprd --add-keys-to-agent=no
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=gitlabhq_production db-ci-replica-gprd --add-keys-to-agent=no
    ```
    You should see `v14` as part of the cluster name.
<%- end %>

1. [ ] 🐘 {+ DBRE +}: Update the wal-g daily restore schedule for the **[<%= env %>] - [<%= cluster %>]** cluster at https://ops.gitlab.net/gitlab-com/gl-infra/gitlab-restore/postgres-gprd/-/pipeline_schedules
    1. [ ] Change the following variables:
        - `PSQL_VERSION = 14`
        - `BACKUP_PATH = ?` (? = use the "directory" from the new v14 GCS backup location at: <%= dst_gcs_backup_location %>)

1. [ ] 🐘 {+ DBRE +}: Enable feature flags by typing the following into `#production`:
  <%- if production %>
    - PRODUCTION:
      1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags false`
  <%- else %>
    - STAGING:
      1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags false --staging`
  <%- end %>
  
    1. [ ] 🐘 {+ DBRE +}: Inform the database team that the CR is completed and that the background migrations and reindexing feature flags have been re-enabled. Post the following comment on the gitlab.com CR (<<%= gitlab_com_cr %>>):
      <%- if production %>
        ```text
        Hi @gl-database,

        Please note that we have completed the work for this CR in the `<%= env %>` environment. Therefore we have re-enabled the `execute_batched_migrations_on_schedule`, `execute_background_migrations`, reindexing, async_foreign_key,  sync_index and partition_manager_sync_partitions features and tasks in `PRODUCTION`.

        Could you please confirm that they have been re-enabled correctly?

        Thanks!
        ```
      <%- else %>
        ```text
        Hi @gl-database,

        Please note that we have completed the work for this CR in the `<%= env %>` environment. Therefore we have re-enabled the `execute_batched_migrations_on_schedule`, `execute_background_migrations`, reindexing, async_foreign_key,  sync_index and partition_manager_sync_partitions features and tasks in `STAGING`.

        Could you please confirm that they have been re-enabled correctly?

        Thanks!
        ```
      <%- end %>

1. [ ] 🐬 {+ SRE +}: Check if the underlying DDL features were ENABLED back by disabling the disallow_database_ddl_feature_flags:
  <%- if production %>
    - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags` should return DISABLED
  <%- else %>
    - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags --staging` should return DISABLED
  <%- end %>
    - [ ] On Rails Console: 
        - [ ] Open a new rails console
            <%- if production %>
            - __PRODUCTION__: URL [production.teleport.gitlab.net](https://production.teleport.gitlab.net) or tsh:
                ```shell
                tsh login --proxy=production.teleport.gitlab.net --request-roles=rails-ro --request-reason="Validate if Database Migration/Reindex Workers are disabled during PG14 upgrade: <%= gitlab_com_cr %>"
                tsh ssh rails-ro@console-ro-01-sv-gprd
                ```
            <%- else %>
            - __STAGING__: URL [staging.teleport.gitlab.net](https://staging.teleport.gitlab.net) or tsh:
                ```shell
                tsh login --proxy=staging.teleport.gitlab.net --request-roles=rails-ro 
                tsh ssh rails-ro@console-ro-01-sv-gstg
                ```
            <%- end %>
        - [ ] Paste the script in the console
            ```ruby
            def output(name, value)
              color = value ? '31' : '32'
              result = value ? 'enabled' : 'disabled'

              puts "\e[33m#{name} is\e[0m \e[#{color}m#{result}.\e[0m"
            end

            def check
              ActiveRecord::Base.logger = nil
              output('Database::BatchedBackgroundMigration::MainExecutionWorker', Database::BatchedBackgroundMigration::MainExecutionWorker.new.send(:enabled?))
              output('Database::BatchedBackgroundMigration::CiExecutionWorker', Database::BatchedBackgroundMigration::CiExecutionWorker.new.send(:enabled?))
              output('Database::BatchedBackgroundMigration::CiDatabaseWorker', Database::BatchedBackgroundMigration::CiDatabaseWorker.enabled?)
              output('Database::BatchedBackgroundMigrationWorker', Database::BatchedBackgroundMigrationWorker.enabled?)
              output('Gitlab::Database::Reindexing', Gitlab::Database::Reindexing.enabled?)
              
              is_execute_background_migrations_enabled = !(Feature.enabled?(:disallow_database_ddl_feature_flags, type: :ops) || Feature.disabled?(:execute_background_migrations, type: :ops))
              output('BackgroundMigration::CiDatabaseWorker', is_execute_background_migrations_enabled)
              output('BackgroundMigrationWorker', is_execute_background_migrations_enabled)
              
              is_database_async_index_operations_enabled = !(Feature.enabled?(:disallow_database_ddl_feature_flags, type: :ops) || Feature.disabled?(:database_async_index_operations, type: :ops))
              output('rake gitlab:db:execute_async_index_operations', is_database_async_index_operations_enabled)
              
              is_database_async_foreign_key_validation_enabled = Feature.disabled?(:disallow_database_ddl_feature_flags, type: :ops) && Feature.enabled?(:database_async_foreign_key_validation, type: :ops)
              output('rake gitlab:db:validate_async_constraints', is_database_async_foreign_key_validation_enabled)
              output('Gitlab::Database::AsyncConstraints', is_database_async_foreign_key_validation_enabled)
              
              is_database_async_index_creation_enabled = Feature.disabled?(:disallow_database_ddl_feature_flags, type: :ops) && Feature.enabled?(:database_async_index_creation, type: :ops)
              output('Gitlab::Database::AsyncIndexes', is_database_async_index_creation_enabled)

              is_partition_manager_sync_partitions_enabled = !(Feature.enabled?(:disallow_database_ddl_feature_flags, type: :ops) || Feature.disabled?(:partition_manager_sync_partitions, type: :ops)) 
              output('Gitlab::Database::Partitioning#sync_partitions', is_partition_manager_sync_partitions_enabled)
              output('Gitlab::Database::Partitioning#drop_detached_partitions', is_partition_manager_sync_partitions_enabled)
            end

            check
            ```
        - [ ] Check the output - All workers/tasks should be `enabled`, like for example:
            ```shell
            Database::BatchedBackgroundMigration::MainExecutionWorker is enabled.
            Database::BatchedBackgroundMigration::CiExecutionWorker is enabled.
            Database::BatchedBackgroundMigration::CiDatabaseWorker is enabled.
            Database::BatchedBackgroundMigrationWorker is enabled.
            Gitlab::Database::Reindexing is enabled.
            BackgroundMigration::CiDatabaseWorker is enabled.
            BackgroundMigrationWorker is enabled.
            rake gitlab:db:execute_async_index_operations is enabled.
            rake gitlab:db:validate_async_constraints is enabled.
            Gitlab::Database::AsyncConstraints is enabled.
            Gitlab::Database::AsyncIndexes is enabled.
            Gitlab::Database::Partitioning#sync_partitions is enabled.
            Gitlab::Database::Partitioning#drop_detached_partitions is enabled.
            ```

1. [ ] 🐬 {+ SRE +}: We have a separate issue to rebuild each cluster's DR Archive and Delayed replicas. We will use the following issue link to track rebuild the <%= cluster %> cluster's DR Archive and Delayed replicas from the most recent v14 database backup of the <%= cluster %>. It will be completed in the next couple of working days. __TODO__ add links for gprd
    <%- if production %>
    - PRODUCTION:
        - Registry : <https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/23886> https://gitlab.com/gitlab-com/gl-infra/production/-/issues/15822
        - Main : <https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/19140>
        - CI : <https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/19141>
    <%- else %>
    - STAGING:
        - Registry : <https://gitlab.com/gitlab-com/gl-infra/db-migration/-/issues/11> https://gitlab.com/gitlab-com/gl-infra/production/-/issues/8758
        - Main : <https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16436> https://gitlab.com/gitlab-com/gl-infra/production/-/issues/8763
        - CI : <https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16435> https://gitlab.com/gitlab-com/gl-infra/production/-/issues/9096
    <%- end %>

1. [ ] 🐘 {+ DBRE +} Logical replication from `SOURCE to TARGET`` should have been destroyed by the switchover, check and destroy it if it was not:
    1. [ ] 🐘 {+ DBRE +} On the TARGET cluster <%= dst_cluster_prefix %> Leader/Writer, drop subscription (if still existing) for logical replication:
        - [ ] Check if the subscription still exist:
          ```sh
          gitlab-psql \
              -Xc "select subname, subenabled, subconninfo, subslotname, subpublications from pg_subscription" 
          ```
        - [ ] Drop the logical replication subscription:
          ```sh
          gitlab-psql \
              -Xc "alter subscription logical_subscription disable" \
              -Xc "alter subscription logical_subscription set (slot_name = none)" \
              -Xc "drop subscription logical_subscription"
          ```
    1. [ ] 🐘 {+ DBRE +} On the SOURCE cluster <%= src_cluster_prefix %>  Leader/Writer, drop publication and logical_replication_slot for reverse replication:
        - [ ] Check if the publication and replication slots still exist:
          ```sh
          gitlab-psql \
            -Xc "select pubname from pg_publication" \
            -Xc "select slot_name, plugin, slot_type, active from pg_replication_slots"
          ```
        - [ ] Check if the publication and replication slots still exist:
          ```sh
          gitlab-psql \
            -Xc "drop publication logical_replication" \
            -Xc "select pg_drop_replication_slot('logical_replication_slot') from pg_replication_slots where slot_name = 'logical_replication_slot'" \
            -Xc "drop table if exists test_publication" \
            -Xc "drop table if exists test_replication"
          ```

#### Reverse replication validation

1. [ ] 🐺 {+ Coordinator +}: Coordinate with 🐘 {+ DBRE +} to make sure we stay in the current status for an hour and continue to run reverse replication from `v14` to `v12` per https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/23986#note_1524934967

1. [ ] 🐘 {+ DBRE +} __TODO___ @NikolayS TO ADD steps to validate the reverse replication is working fine as expected. 

1. [ ] 🐘 {+ DBRE +} On the SOURCE cluster <%= src_cluster_prefix %> Leader/Writer, drop subscription (if still existing) for reverse replication:
    - [ ] Check if the subscription still exist:
      ```sh
      gitlab-psql \
          -Xc "select subname, subenabled, subconninfo, subslotname, subpublications from pg_subscription" 
      ```
    - [ ] Drop the logical replication subscription:
      ```sh
      gitlab-psql \
          -Xc "alter subscription reverse_logical_subscription disable" \
          -Xc "alter subscription reverse_logical_subscription set (slot_name = none)" \
          -Xc "drop subscription reverse_logical_subscription"
      ```

1. [ ] 🐘 {+ DBRE +} On the TARGET cluster <%= dst_cluster_prefix %>  Leader/Writer, drop publication and reverse_logical_replication_slot for reverse replication:
    - [ ] Check if the publication and replication slots still exist:
      ```sh
      gitlab-psql \
        -Xc "select pubname from pg_publication" \
        -Xc "select slot_name, plugin, slot_type, active from pg_replication_slots"
      ```
    - [ ] Check if the publication and replication slots still exist:
      ```sh
      gitlab-psql \
        -Xc "drop publication reverse_logical_replication" \
        -Xc "select pg_drop_replication_slot('reverse_logical_replication_slot') from pg_replication_slots where slot_name = 'reverse_logical_replication_slot'" \
        -Xc "drop table if exists test_publication" \
        -Xc "drop table if exists test_replication"
      ```

1. [ ] 🐘 {+ DBRE +} Shutdown the SOURCE <%= env %>-base-db-<%= src_cluster_prefix %> cluster to avoid any risk of splitbrain:
      ``` 
      knife ssh "role:<%= env %>-base-db-<%= src_cluster_prefix %>" "sudo systemctl stop patroni"
      ```

<%- if production %>
1. [ ] 🐺 {+ Coordinator +}: Check if [gitlab_maintenance_mode is DISABLED for <%= env %> (Thanos link)](https://thanos.gitlab.net/graph?g0.expr=max(gitlab_maintenance_mode%7Benv%3D%22<%= env %>%22%7D)&g0.tab=0&g0.stacked=0&g0.range_input=1h)
    - If is not disabled ask 🔪 {+ Playbook-Runner +} to manually disable it by:
      - SSH to a console VM in `<%= env %>` (eg. `ssh console-01-sv-<%= env %>.c.<%= gcp_project %>.internal` )
        - Set `gitlab_maintenance_mode=0` on node exporter :
          ```sh
          sudo su -
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 0\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          cat /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          atq
          ```
<%- end %>

1. [ ] 🐺 {+ Coordinator +}: Mark the change request as `/label ~"change::complete"`

</details>

<details>
  <summary>

### __Rollback__ (if required)

  </summary>

<%- if production %>
1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post an update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message:

        ```text
        Due to an issue during the planned maintenance for the database layer, we have initiated a rollback of the changes. We will provide update once the rollback process is completed.
        ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Ask the IMOC or the Head Honcho if this message should be sent to any slack rooms:
   - `#whats-happening-at-gitlab`
   - `#infrastructure-lounge` (cc `@sre-oncall`)
   - `#g_delivery` (cc `@release-managers`)
   - `#community-relations`
<%- end %>

#### Health check

1. [ ] 🐺 {+ Coordinator +}: Ensure that there are no active critical alerts or open incidents:
    <%- if production %>
    - PRODUCTION:
        - <https://dashboards.gitlab.net/d/SOn6MeNmk/alerts?orgId=1&var-interval=1m&var-environment=gprd&var-alertname=All&var-alertstate=All&var-prometheus=prometheus-01-inf-gprd&var-prometheus_app=prometheus-app-01-inf-gprd>
        - <https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=incident>
    <%- else %>
    - STAGING:
        - <https://dashboards.gitlab.net/d/SOn6MeNmk/alerts?orgId=1&var-interval=1m&var-environment=gstg&var-alertname=All&var-alertstate=All&var-prometheus=prometheus-01-inf-gstg&var-prometheus_app=prometheus-app-01-inf-gstg>
    <%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: Verify that the ansible inventory is up to date and reflects the real state from the cluster.


#### Rollback Postgres Upgrade 

- After the switchover there is **NO reverse replication, replicating data from PG14 to PG12**!
- After enabling site traffic on the new cluster, all new changes to the database will **only be on the new cluster**
- There will be **no rollback after the switchover**!


1. [ ] 🐘 {+ DBRE +}: Check if rollback is possible
   1. [ ] We have **not switched** over
1. [ ] 🐘 {+ DBRE +}: Monitor what pgbouncer pool has connections [Thanos](https://thanos.gitlab.net/graph?g0.expr=max(pgbouncer_pools_client_active_connections%7Benv%3D%22<%= env %>%22%2C%20user%3D%22gitlab%22%2C%20type%3D~%22<%= prometheus_type %>%7C<%= prometheus_type %>-v14%22%7D)%20by%20(fqdn%2Cdatabase)&g0.tab=1)


1. [ ] 🐘 {+ DBRE +} Start patroni in the source <%= env %>-base-db-<%= src_cluster_prefix %> cluster nodes:
      ``` 
      knife ssh "role:<%= env %>-base-db-<%= src_cluster_prefix %>" "sudo systemctl start patroni"
      ```
1. [ ] 🐘 {+ DBRE +}: __TODO__ @vitabaks @NikolayS Disable "read only" flag on v12 cluster:
1. [ ] 🐘 {+ DBRE +}: Monitor the v14 and v12 PostgreSQL Log Files:
    1. [ ] Login to node 01 of each cluster:
        - `ssh <%= src_cluster_host_prefix %>01-db-<%= env %>.c.<%= gcp_project %>.internal`
        - `ssh <%= dst_cluster_host_prefix %>01-db-<%=env %>.c.<%= gcp_project %>.internal`

    1. [ ] Get leader for each cluster:
        ```sh
        sudo gitlab-patronictl list
        ```
    1. [ ] Connect via SSH to the previously identified leaders.
    1. [ ] Tail Postgres Logs:
        ```sh
        sudo tail -f /var/log/gitlab/postgresql/postgresql.csv
        ```


##### ROLLBACK – execute!
Goal: __Set <%= env %>-<%= cluster %> *v12* cluster as Primary cluster__


1. [ ] 🔪 {+ Playbook-Runner +}: Execute `switchover_rollback.yml` playbook to rollback to v12 cluster:

      ```sh
      cd ~/src/db-migration/pg-upgrade-logical
      ansible-playbook \
        -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" \
        -i inventory/<%= env %>-<%= cluster %>.yml \
        switchover_rollback.yml -e "force_mode=true" 2>&1 \
      | ts | tee -a ansible_switchover_rollback_<%= env %>_<%= cluster %>_$(date +%Y%m%d).log
      ```

1. [ ] 🐘 {+ DBRE +}: Ensure writes are happening on Postgres/Patroni nodes in <%= src_cluster_prefix %>: [Thanos](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22<%= env %>%22%2Cfqdn%3D~"<%= src_cluster_prefix %>.*"%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22<%= env %>%22%2Cfqdn%3D~"<%= src_cluster_prefix %>.*"%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22<%= env %>%22%2Cfqdn%3D~"<%= src_cluster_prefix %>.*"%7D%5B1m%5D))%20by%20(instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

1. [ ] 🐘 {+ DBRE +} Check prometheus sanity check metrics for reads all going to the correct hosts
    1. [Index reads](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22<%= env %>%22%2Cinstance%3D~%22(<%= src_cluster_host_prefix %>.*)%22%7D%5B1m%5D))%20by%20(instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
        - [ ] Expected result: all queries going to the `<%= src_cluster_prefix %>` cluster.
    1. [Sequential scans](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22<%= env %>%22%2Cinstance%3D~%22(<%= src_cluster_host_prefix %>.*)%22%7D%5B1m%5D))%20by%20(instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
        - [ ] Expected result: all queries going to the `<%= src_cluster_prefix %>` cluster.

### Complete the rollback

1. [ ] 🐘 {+ DBRE +}: Confirm that our smoke tests are still passing (continue the rollback as this might take an hour...)

1. [ ] 🐬 {+ SRE +}: Revert the MR that change Consul and Prometheus, IF it was Merged.
    1. [ ] Revert MR: <%= rollback_mr %>

1. [ ] 🐬 {+ SRE +}: Re-enable Chef in all <%= env %>-<%= cluster %> nodes:
    ```sh
    knife ssh "role:<%= env %>-base-db-<%= src_cluster_prefix %> OR role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo chef-client-enable"

1. [ ] 🐬 {+ SRE +}: Confirm chef-client is enabled in all nodes [thanos link](https://thanos.gitlab.net/graph?g0.expr=chef_client_enabled%7Benv%3D%22<%= env %>%22%2Ctype%3D~%22<%= prometheus_type %>-v14%22%7D&g0.tab=1&g0.stacked=0&g0.range_input=30m&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

1. [ ] 🐬 {+ SRE +}: Run chef-client on Patroni Nodes:
      ```sh
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %> OR role:<%= env %>-base-db-<%= src_cluster_prefix %>" "sudo chef-client"
      ```

1. [ ] 🐬 {+ SRE +}: Confirm no errors while running chef-client [thanos link](https://thanos.gitlab.net/graph?g0.expr=chef_client_error%7Benv%3D%22<%= env %>%22%2Ctype%3D%22<%= prometheus_type %>%22%7D%20%3E%200&g0.tab=0&g0.stacked=0&g0.range_input=30m&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

<%- if production %>
1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - [ ] Click "Finish Maintenance" and send the following:
       - Message:

          ```text
          GitLab.com rollback for the database layer is complete, and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.
          ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send the following message to slack rooms:

   ```text
   GitLab.com rollback for the database layer is complete and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.
   ```
   - [ ] `#whats-happening-at-gitlab`
   - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
   - [ ] `#g_delivery` (cc `@release-managers`)
<%- end %>

1. [ ] 🐘 {+ DBRE +}: Enable feature flags by typing the following into `#production`:
  <%- if production %>
    - PRODUCTION:
      1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags false`
  <%- else %>
    - STAGING:
      1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags false --staging`
  <%- end %>
  
    1. [ ] 🐘 {+ DBRE +}: Inform the database team that the CR was rolled back and that the background migrations and reindexing feature flags have been re-enabled. Post the following comment on the gitlab.com CR (<<%= gitlab_com_cr %>>):
      <%- if production %>
        ```text
        Hi @gl-database,

        Please note that we have rolled back the work for this CR in the `<%= env %>` environment. Therefore we have re-enabled the `execute_batched_migrations_on_schedule`, `execute_background_migrations`, reindexing, async_foreign_key, sync_index and partition_manager_sync_partitions features and tasks in `PRODUCTION`.

        Could you please confirm that they have been re-enabled correctly?

        Thanks!
        ```
      <%- else %>
        ```text
        Hi @gl-database,

        Please note that we have rolled back the work for this CR in the `<%= env %>` environment. Therefore we have re-enabled the `execute_batched_migrations_on_schedule`, `execute_background_migrations`, reindexing, async_foreign_key, sync_index and partition_manager_sync_partitions features and tasks in `STAGING`.

        Could you please confirm that they have been re-enabled correctly?

        Thanks!
        ```
      <%- end %>

1. [ ] 🐬 {+ SRE +}: Check if the underlying DDL features were ENABLED back by disabling the disallow_database_ddl_feature_flags:
  <%- if production %>
    - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags` should return DISABLED
  <%- else %>
    - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags --staging` should return DISABLED
  <%- end %>
    - [ ] On Rails Console: 
        - [ ] Open a new rails console
            <%- if production %>
            - __PRODUCTION__: URL [production.teleport.gitlab.net](https://production.teleport.gitlab.net) or tsh:
                ```shell
                tsh login --proxy=production.teleport.gitlab.net --request-roles=rails-ro --request-reason="Validate if Database Migration/Reindex Workers are disabled during PG14 upgrade: <%= gitlab_com_cr %>"
                tsh ssh rails-ro@console-ro-01-sv-gprd
                ```
            <%- else %>
            - __STAGING__: URL [staging.teleport.gitlab.net](https://staging.teleport.gitlab.net) or tsh:
                ```shell
                tsh login --proxy=staging.teleport.gitlab.net --request-roles=rails-ro 
                tsh ssh rails-ro@console-ro-01-sv-gstg
                ```
            <%- end %>
        - [ ] Paste the script in the console
            ```ruby
            def output(name, value)
              color = value ? '31' : '32'
              result = value ? 'enabled' : 'disabled'

              puts "\e[33m#{name} is\e[0m \e[#{color}m#{result}.\e[0m"
            end

            def check
              ActiveRecord::Base.logger = nil
              output('Database::BatchedBackgroundMigration::MainExecutionWorker', Database::BatchedBackgroundMigration::MainExecutionWorker.new.send(:enabled?))
              output('Database::BatchedBackgroundMigration::CiExecutionWorker', Database::BatchedBackgroundMigration::CiExecutionWorker.new.send(:enabled?))
              output('Database::BatchedBackgroundMigration::CiDatabaseWorker', Database::BatchedBackgroundMigration::CiDatabaseWorker.enabled?)
              output('Database::BatchedBackgroundMigrationWorker', Database::BatchedBackgroundMigrationWorker.enabled?)
              output('Gitlab::Database::Reindexing', Gitlab::Database::Reindexing.enabled?)
              
              is_execute_background_migrations_enabled = !(Feature.enabled?(:disallow_database_ddl_feature_flags, type: :ops) || Feature.disabled?(:execute_background_migrations, type: :ops))
              output('BackgroundMigration::CiDatabaseWorker', is_execute_background_migrations_enabled)
              output('BackgroundMigrationWorker', is_execute_background_migrations_enabled)
              
              is_database_async_index_operations_enabled = !(Feature.enabled?(:disallow_database_ddl_feature_flags, type: :ops) || Feature.disabled?(:database_async_index_operations, type: :ops))
              output('rake gitlab:db:execute_async_index_operations', is_database_async_index_operations_enabled)
              
              is_database_async_foreign_key_validation_enabled = Feature.disabled?(:disallow_database_ddl_feature_flags, type: :ops) && Feature.enabled?(:database_async_foreign_key_validation, type: :ops)
              output('rake gitlab:db:validate_async_constraints', is_database_async_foreign_key_validation_enabled)
              output('Gitlab::Database::AsyncConstraints', is_database_async_foreign_key_validation_enabled)
              
              is_database_async_index_creation_enabled = Feature.disabled?(:disallow_database_ddl_feature_flags, type: :ops) && Feature.enabled?(:database_async_index_creation, type: :ops)
              output('Gitlab::Database::AsyncIndexes', is_database_async_index_creation_enabled)

              is_partition_manager_sync_partitions_enabled = !(Feature.enabled?(:disallow_database_ddl_feature_flags, type: :ops) || Feature.disabled?(:partition_manager_sync_partitions, type: :ops)) 
              output('Gitlab::Database::Partitioning#sync_partitions', is_partition_manager_sync_partitions_enabled)
              output('Gitlab::Database::Partitioning#drop_detached_partitions', is_partition_manager_sync_partitions_enabled)
            end

            check
            ```
        - [ ] Check the output - All workers/tasks should be `enabled`, like for example:
            ```shell
            Database::BatchedBackgroundMigration::MainExecutionWorker is enabled.
            Database::BatchedBackgroundMigration::CiExecutionWorker is enabled.
            Database::BatchedBackgroundMigration::CiDatabaseWorker is enabled.
            Database::BatchedBackgroundMigrationWorker is enabled.
            Gitlab::Database::Reindexing is enabled.
            BackgroundMigration::CiDatabaseWorker is enabled.
            BackgroundMigrationWorker is enabled.
            rake gitlab:db:execute_async_index_operations is enabled.
            rake gitlab:db:validate_async_constraints is enabled.
            Gitlab::Database::AsyncConstraints is enabled.
            Gitlab::Database::AsyncIndexes is enabled.
            Gitlab::Database::Partitioning#sync_partitions is enabled.
            Gitlab::Database::Partitioning#drop_detached_partitions is enabled.
            ```


1. [ ] 🔪 {+ Playbook-Runner +}: On two nodes, console and target leader, remove the private keys temporarily placed in `~dbupgrade/.ssh`:
    ```shell
    rm ~dbupgrade/.ssh/id_rsa
    rm ~dbupgrade/.ssh/id_dbupgrade
    ```

1. [ ] 🐘 {+ DBRE +} On the TARGET cluster <%= dst_cluster_prefix %> Leader/Writer, drop subscription (if still existing) for logical replication:
    - [ ] Check if the subscription still exist:
      ```sh
      gitlab-psql \
          -Xc "select subname, subenabled, subconninfo, subslotname, subpublications from pg_subscription" 
      ```
    - [ ] Drop the logical replication subscription:
      ```sh
      gitlab-psql \
          -Xc "alter subscription logical_subscription disable" \
          -Xc "alter subscription logical_subscription set (slot_name = none)" \
          -Xc "drop subscription logical_subscription"
      ```
1. [ ] 🐘 {+ DBRE +} On the SOURCE cluster <%= src_cluster_prefix %>  Leader/Writer, drop publication and logical_replication_slot for reverse replication:
    - [ ] Check if the publication and replication slots still exist:
      ```sh
      gitlab-psql \
        -Xc "select pubname from pg_publication" \
        -Xc "select slot_name, plugin, slot_type, active from pg_replication_slots"
      ```
    - [ ] Check if the publication and replication slots still exist:
      ```sh
      gitlab-psql \
        -Xc "drop publication logical_replication" \
        -Xc "select pg_drop_replication_slot('logical_replication_slot') from pg_replication_slots where slot_name = 'logical_replication_slot'" \
        -Xc "drop table if exists test_publication" \
        -Xc "drop table if exists test_replication"
      ```

<%- if production %>
1. [ ] 🐘 {+ DBRE +}: ADD the following silences at <https://alerts.gitlab.net> to silence `WALGBaseBackup` alerts in <%= dst_cluster_prefix %> for 2 weeks (14 days = 336 hours)
    - Start time: `<%= Time.now.strftime('%FT%T.000Z') %>`
    - Duration: `336h`
      - [ ] `env="gprd"`
      - [ ] `type="<%= env %>-<%= dst_cluster_prefix %>"`
      - [ ] `alertname=~"WALGBaseBackupFailed|walgBaseBackupDelayed"`
<%- end %>

1. [ ] 🐘 {+ DBRE +}: UPDATE the following silence at <https://alerts.gitlab.net> to silence alerts in V14 nodes for 2 weeks (14 days = 336 hours):
    - Start time: `<%= maint_start_time.strftime('%FT%T.000Z') %>`
    - Duration: `<%= window_in_hours + 336 %>h`
    - Matcher:
      <%- if production %>
      - __PRODUCTION__
        - [ ] `env="gprd"`
        - [ ] `fqdn=~"<%= dst_cluster_prefix %>.*"`
      <%- else %>
      - __STAGING__
        - [ ] `env="gstg"`
        - [ ] `fqdn=~"<%= dst_cluster_prefix %>.*"`
      <%- end %>

1. [ ] 🐘 {+ DBRE +}: DELETE the following silences at <https://alerts.gitlab.net> 
    - Matcher:
      <%- if production %>
      - __PRODUCTION__
        - [ ] `env="gprd"`
        - [ ] `fqdn=~"<%= src_cluster_prefix %>.*"`
      <%- else %>
      - __STAGING__
        - [ ] `env="gstg"`
        - [ ] `fqdn=~"<%= src_cluster_prefix %>.*"`
      <%- end %>

<%- if production %>
1. [ ] 🐺 {+ Coordinator +}: Check if [gitlab_maintenance_mode is DISABLED for <%= env %> (Thanos link)](https://thanos.gitlab.net/graph?g0.expr=max(gitlab_maintenance_mode%7Benv%3D%22<%= env %>%22%7D)&g0.tab=0&g0.stacked=0&g0.range_input=1h)
    - If is not disabled ask 🔪 {+ Playbook-Runner +} to manually disable it by:
      - SSH to a console VM in `<%= env %>` (eg. `ssh console-01-sv-<%= env %>.c.<%= gcp_project %>.internal` )
        - Set `gitlab_maintenance_mode=0` on node exporter :
          ```sh
          sudo su -
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 0\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          cat /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          atq
          ```
<%- end %>

1. [ ] 🐺 {+ Coordinator +}: Mark the <%= gitlab_com_cr %> change request as `/label ~"change::aborted"`


</details>

## Extra details
### In case the Playbook-Runner is disconnected
As most of the steps are executed in a tmux session owned by the
Playbook-Runner role we need a safety net in case this person loses their
internet connection or otherwise drops off half way through. Since other
SREs/DBREs also have root access on the console node where everything is
running they should be able to recover it in different ways. We tested the
following approach to recovering the tmux session, updating the ssh agent and
taking over as a new ansible user.

- `ssh host`
- Add your public SSH key to `/home/PREVIOUS_PLAYBOOK_USERNAME/.ssh/authorized_keys`
- `sudo chef-client-disable <%= gitlab_com_cr %>` so that we don't override the above
- `ssh -A PREVIOUS_PLAYBOOK_USERNAME@host`
- `echo $SSH_AUTH_SOCK`
- `tmux attach -t 0`
- `export SSH_AUTH_SOCK=<VALUE from previous SSH_AUTH_SOCK output>`
- `<ctrl-b> :`
- `set-environment -g 'SSH_AUTH_SOCK' <VALUE from previous SSH_AUTH_SOCK output>`
- `export ANSIBLE_REMOTE_USER=NEW_PLAYBOOK_USERNAME`
- `<ctrl-b> :`
- `set-environment -g 'ANSIBLE_REMOTE_USER' <your-user>`
