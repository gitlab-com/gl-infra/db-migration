<% if false %>
<!--
This template has lots of ERB tags so it's not meant to be used verbatim to create an issue. To create
the markdown to copy into an issue, you run it like this (times are in UTC):

  erb \
    maintenance_start_time="YYYY-MM-DD HH:mm" \
    maintenance_end_time="YYYY-MM-DD HH:mm" \
    production=1 \
    -T- <this file>

For example:

  erb \
    maintenance_start_time="2022-08-02 15:00" \
    maintenance_end_time="2022-08-02 17:00" \
    production=1 \
    -T- patroni_os_upgrade.md

You can also specify the usernames of the various roles. For example:

  erb \
    maintenance_start_time="2022-08-02 15:00" \
    maintenance_end_time="2022-08-02 17:00" \
    production=1 \
    coordinator=username1 \
    playbook_runner=username2 \
    -T- patroni_os_upgrade.md

NOTE:
  1. You'll need to have the activesupport gem installed: `gem install activesupport`
  2. To save the contents in your clipboard, you can pipe the output to `pbcopy` on OS/X or
     `xclip -selection clipboard` on Linux (assuming you have it installed).
-->
<% end -%>
<%
  unless defined?(production)
    raise "you must specify either 'production=0' (i.e., staging) or 'production=1'"
  end

  unless ["0", "1"].include?(production)
    raise "production must be either 0 or 1"
  end

  production = production.to_i == 0 ? false : true

  if production
    env = 'gprd'
    gcp_project = 'gitlab-production'
  else
    env = 'gstg'
    gcp_project = 'gitlab-staging-1'
  end

  require 'time'
  require 'active_support/time'
  maint_start_time = Time.strptime(maintenance_start_time + ' +0000', "%Y-%m-%d %H:%M %z")
  maint_end_time = Time.strptime(maintenance_end_time + ' +0000', "%Y-%m-%d %H:%M %z")
  maint_start_time_str = maintenance_start_time + ' UTC'
  maint_end_time_str = maintenance_end_time + ' UTC'
  window_in_hours = ((maint_end_time - maint_start_time) / 3600).to_i

  gitlab_com_cr ||= "https://gitlab.com/gitlab-com/gl-infra/production/-/issues/XXXXXX"

  # This is the IP of the VM used to run "gitlab-qa" against staging/production
  qa_vm_ip = '104.196.188.184'

  # Using define_method instead of def so we can access the variables in the outer scope
  define_method("role") do |name|
    unless binding.local_variable_defined?(name)
      return ""
    end

    binding.local_variable_get(name).split(",").map do |name|
      "@" + name
    end.join(" ")
  end

  # Similarly we use define_method here (instead of "def") so we can access the variables in the outer scope
  define_method("partial") do |file, vars={}, indent_spaces=0|
    erbvar = '_erbout' + rand(1000).to_s

    b = binding
    vars.each do |k, v|
      b.local_variable_set k.to_sym, v
    end

    ' ' * indent_spaces + ERB.new(File.read(file), nil, '-', erbvar).result(b).split("\n").join("\n" + ' ' * indent_spaces)
  end
-%>
<!--
This markdown was generated on <%= Time.now.utc %> using the following:

erb <%=
  args = []
  `ps axw`.split("\n").select { |ps| ps[ /\A#{ $$ }/ ] }.first.split(" ")[6..-3].each do |val|
    if val.include?("=")
      args.append(val)
    else
      args[-1] += " #{val}"
    end
  end

  args.map { |arg|
    k, v = arg.split("=")
    "#{k}=\"#{v}\""
  }.join(" ")
%> -T- patroni_os_upgrade.md
-->
# OS Upgrade Rollout Team

| Role                | Assigned To |
| ------------------- | ----------- |
| 🐺 Coordinator     | <%= role('coordinator') %> |
| 🔪 Playbook-Runner | <%= role('playbook_runner') %> |
| ☎ Comms-Handler    | <%= role('comms_handler') %> |
| 🐘 DBRE            | <%= role('dbre') %> |
| 🏆 Quality         | <%= role('quality') %> |
| 🎩 IMOC            | <%= role('imoc') %> |
| 📣 CMOC            | <%= role('cmoc') %> |
| 🚑 EOC             | <%= role('eoc') %> |
| 💾 Head Honcho     | <%= role('head_honcho') %> |

Link to gitlab.com CR: <<%= gitlab_com_cr %>>

## Collaboration

During the change window, the rollout team will collaborate using the following communications channels:

| App | Direct Link |
|---|---|
| Slack | [#db_squad](https://gitlab.slack.com/archives/C02K0JTKAHJ) |
| Zoom  | <%= zoom_link ||= "<ZOOM_INVITE_LINK>" %> |

# Immediately

Perform these steps when the issue is created.

- [ ] 🐺 {+ Coordinator +}: Fill out the names of the rollout team in the table above.

# Support Options

| Provider | Plan | Details | Create Ticket |
|----------|------|---------|---------------|
| __Google Cloud Platform__ | [Gold Support](https://cloud.google.com/support/?options=premium-support#options) | 24x7, email & phone, 1hr response on critical issues | [__Create GCP Support Ticket__](https://enterprise.google.com/supportcenter/managecases) |

# Entry points

| Entry point                | Before                                                               | Blocking mechanism                                             | Allowlist | QA needs                                                                    | Notes                                                                                               |
| -------------------------- | -------------------------------------------------------------------- | -------------------------------------------------------------- | --------- | --------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------- |
| Web traffic                | Available via gitlab.com (Cloudflare)                                | Cloudflare maintenance mode                                    | QA VM     | Required for QA                                                             |                                                                                                     |
| API traffic                | Available via gitlab.com (Cloudflare)                                | Cloudflare maintenance mode                                    | QA VM     | Required for QA. Also QA docker runners needs access to this                |                                                                                                     |
| Shared runners             | Available by connecting via internal LB, and gitlab.com (Cloudflare) | Cloudflare maintenance mode; gcloud FW rule managed by Ansible | None      | Not required for QA as QA sets up own Docker-based Runner on its VM         | If we require shared runners, then it is possible to only allow private shared runners              |
| Project, and group runners | Available by connecting via gitlab.com (Cloudflare)                  | Cloudflare maintenance mode                                    | None      | N/A                                                                         |                                                                                                     |
| KAS                        | Available via kas.gitlab.com                                         | gcloud Cloud Armor rule managed by Ansible                     | None      | Not required for QA                                                         |                                                                                                     |
| git-over-ssh               | Available via gitlab.com port 22, and altssh.gitlab.com              | gcloud FW rule managed by Ansible                              | None      | Not required for QA (2 git-over-ssh QA smoke tests will fail, but that's OK) | We have git-over-ssh, git-over-http QA tests. Also CI pipeline tests can be triggered by commit API |
| Container registry         | Available via registry.gitlab.com                                    | Cloudflare maintenance mode                                    | None      | Not required for QA                                                         |                                                                                                     |
| Pages                      | Available via *.gitlab.io, and various custom domains                | Unavailable because GitLab.com is down. There is a cache but it will expire in `gitlab_cache_expiry` minutes  | N/A       | N/A                                                                         |                                                                                                     |

# Database hosts

- [Staging replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gstg&var-prometheus=Global&var-prometheus_app=Global)
- [Production replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gprd&var-prometheus=Global&var-prometheus_app=Global)

# Accessing the rails and database consoles

## Staging

- rails: `ssh $USER-rails@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db replica: `ssh $USER-db@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db primary: `ssh $USER-db-primary@console-01-sv-gstg.c.gitlab-staging-1.internal`
- ci db replica: `ssh $USER-db-ci@console-01-sv-gstg.c.gitlab-staging-1.internal`
- ci db primary: `ssh $USER-db-ci-primary@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db psql (16.04): `ssh -t patroni-03-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`
- ci db psql (16.04): `ssh -t patroni-ci-03-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`
- main db psql (20.04): `ssh -t patroni-main-2004-03-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`
- ci db psql (20.04): `ssh -t patroni-ci-2004-03-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`

## Production

- rails: `ssh $USER-rails@console-01-sv-gprd.c.gitlab-production.internal`
- main db replica: `ssh $USER-db@console-01-sv-gprd.c.gitlab-production.internal`
- main db primary: `ssh $USER-db-primary@console-01-sv-gprd.c.gitlab-production.internal`
- ci db replica: `ssh $USER-db-ci@console-01-sv-gprd.c.gitlab-production.internal`
- ci db primary: `ssh $USER-db-ci-primary@console-01-sv-gprd.c.gitlab-production.internal`
- main db psql (16.04): `ssh -t patroni-v12-06-db-gprd.c.gitlab-production.internal sudo gitlab-psql`
- ci db psql (16.04): `ssh -t patroni-ci-06-db-gprd.c.gitlab-production.internal sudo gitlab-psql`
- main db psql (20.04): `ssh -t patroni-main-2004-06-db-gprd.c.gitlab-production.internal sudo gitlab-psql`
- ci db psql (20.04): `ssh -t patroni-ci-2004-06-db-gprd.c.gitlab-production.internal sudo gitlab-psql`

# Dashboards and debugging

These dashboards might be useful during the rollout:

## Staging

- [PostgreSQL replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gstg&var-prometheus=Global&var-prometheus_app=Global)
- [Triage overview](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s&var-env=gstg&var-environment=gstg&var-prometheus=prometheus-01-inf-gstg&var-prometheus_app=prometheus-app-01-inf-gstg&var-backend=All&var-type=All&var-stage=main)
- [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-24h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main)
- [Sentry - includes application errors](https://sentry.gitlab.net/gitlab/staginggitlabcom/)
- [Logs (Kibana)](https://nonprod-log.gitlab.net/app/kibana)

## Production

- [PostgreSQL replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1)
- [Triage overview](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s&var-env=gprd&var-environment=gprd&var-prometheus=prometheus-01-inf-gprd&var-prometheus_app=prometheus-app-01-inf-gprd&var-backend=All&var-type=All&var-stage=main)
- [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-24h&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main)
- [Sentry - includes application errors](https://sentry.gitlab.net/gitlab/gitlabcom/)
  - Workhorse: <https://sentry.gitlab.net/gitlab/gitlab-workhorse-gitlabcom/>
  - Rails (backend): <https://sentry.gitlab.net/gitlab/gitlabcom/>
  - Rails (frontend): <https://sentry.gitlab.net/gitlab/gitlabcom-clientside/>
  - Gitaly (golang): <https://sentry.gitlab.net/gitlab/gitaly-production/>
  - Gitaly (ruby): <https://sentry.gitlab.net/gitlab/gitlabcom-gitaly-ruby/>
- [Logs (Kibana)](https://log.gprd.gitlab.net/app/kibana)

# Repos used during the rollout

The following Ansible playbooks are referenced throughout this issue:

- Stopping and starting components, maintenance page and traffic management: <https://gitlab.com/gitlab-com/gl-infra/ansible-migrations/-/tree/master/maintenance-mode>
- OS Upgrade & Rollback: <https://gitlab.com/gitlab-com/gl-infra/db-migration/-/tree/master/pg-os-upgrade>

---

# Prep Tasks

<details>
  <summary>

  - [ ] __T minus 3 days (<%= 3.days.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Ensure that the maintenance window is scheduled on status.io.

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
      - Message:

        ```text
        In 3 days time, we will be undergoing some scheduled maintenance to our database layer so we expect the GitLab.com site to be unavailable for up to <%= window_in_hours %> hours starting from <%= maint_start_time_str %>. We apologize in advance for any inconvenience this may cause.
        ```

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send the tweet link from `@gitlabstatus` to `#social_media_action` channel on Slack:

      - Message:

        ```text
        Hi team, please retweet and pin this from our status page on GitLab Twitter about the upcoming production change where GitLab.com will go down for up to <%= window_in_hours %> hours: {TWEET_LINK}`
        ```

      - [ ] `@gitlab` retweeted from `@gitlabstatus`
      - [ ] Tweet pinned on `@gitlab`

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send on Slack `#whats-happening-at-gitlab`:

      - Message:

        ```text
        :loudspeaker: *OS upgrades to our database instances is scheduled for <%= maint_start_time.strftime("%Y-%m-%d") %> between <%= maint_start_time.strftime("%H:%M") %> UTC and <%= maint_end_time.strftime("%H:%M") %> UTC* :rocket:
        Taking place in 3 days time!

        :hammer_and_wrench: *What to expect?*
        GitLab.com will be unavailable for up to <%= window_in_hours %> hours starting from <%= maint_start_time_str %>.
        ```

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Share message from `#whats-happening-at-gitlab` to the following channels:

      - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
      - [ ] `#g_delivery` (cc `@release-managers`)
      - [ ] `#community-relations` (Inform Marketing team)
      - [ ] `#support_gitlab-com` (Inform Support SaaS team)
          - [ ] Share with team Google Doc link to template for incoming tickets regarding the maintenance: {+ TODO: add link to Google doc +}

  1. [ ] 🐘 {+ DBRE +}: [Create a C1 change request in the production repo](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/new?issuable_template=change_management) and link to this issue. Example: <https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7413>
      - [ ] Ensure the CR is reviewed by the 🚑 {+ EOC +}

  1. [ ] 🐘 {+ DBRE +}: Ensure this issue has been created on <https://ops.gitlab.net/gitlab-com/gl-infra/db-migration>, since `gitlab.com` will be unavailable during the rollout!!!

  1. [ ] 🐘 {+ DBRE +}: Create a merge request that may be needed in case of rollback and link it in the below rollback section

</details>

<details>
  <summary>

  - [ ] __T minus 2 days (<%= 2.days.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  1. [ ] 🔪 {+ Playbook-Runner +}: Add a GKE maintenance exclusion window for the duration of the downtime window to avoid any interference from automated GKE maintenance. The maintenance exclusion windows are Terraform controlled:

      - STAGING: <https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/environments/gstg/locals.tf>
      - PRODUCTION: <https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/environments/gprd/locals.tf>

      ```text
      start_time = "<%= 2.hours.ago(maint_start_time).strftime('%Y-%m-%dT%H:%M:00Z') %>"
      end_time   = "<%= 2.hours.after(maint_end_time).strftime('%Y-%m-%dT%H:%M:00Z') %>"
      name       = "<%= gitlab_com_cr %>"
      ```

      Ensure the plan is applied once your change is merged.

  1. [ ] __PRODUCTION ONLY__ 🔪 {+ Playbook-Runner +}: Disable the database reindexing job that runs hourly on weekends:

      1. [ ] Disable feature flag by typing the following in `#production`:

          ```text
          /chatops run feature set database_reindexing false
          ```

      1. [ ] Inform the database team that database reindexing has been disabled until the CR is complete. Post the following comment on the gitlab.com CR (<<%= gitlab_com_cr %>>):

          ```text
          Hi @gitlab-org/database-team,

          Please note that the `database_reindexing` feature flag has been disabled in the `PRODUCTION` environment, as we are carrying out OS upgrades to the database layer between `<%= maint_start_time_str %>` and `<%= maint_end_time_str %>`.

          We will re-enable the feature flag once work is complete.

          Thanks!
          ```

</details>

<details>
  <summary>

  - [ ] __T minus 1.5 days (<%= 36.hours.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  1. [ ] __PRODUCTION ONLY__ 🐺 {+ Coordinator +}: Confirm with `#g_delivery` (`@release-managers`) that deployments have been __stopped__. We need migrations to stop so we can run `amcheck` and be confident that no further index changes will take place.

<%= partial "patroni_os_upgrade/_amcheck_partial.md.erb", vars={ reindex_concurrently: false, amcheck_main_host: production ? "patroni-v12-10-db" : "patroni-06-db", amcheck_ci_host: "patroni-ci-03-db" }, 2 %>

  If time permits, run the `amcheck` once again for good measure.

</details>

<details>
  <summary>

  - [ ] __T minus 1 day (<%= 1.days.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>

      - Message:

        ```text
        Reminder: Tomorrow we will be undergoing scheduled maintenance to our database layer. We expect the GitLab.com site to be unavailable for up to <%= window_in_hours %> hours. Starting time: <%= maint_start_time_str %>
        ```

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send message to `#social_media_action` to retweet from `@gitlabstatus` Twitter.

      - Message:

        ```text
        Hi team, please retweet this from our status page to GitLab Twitter about the scheduled maintenance that is taking place tomorrow: {TWEET_LINK}
        ```

      - [ ] `@gitlab` retweeted from `@gitlabstatus`

  1. [ ] __PRODUCTION ONLY__ 🐘 {+ DBRE +}: Remind the Fulfillment team about the downtime so they can make the necessary changes to customers.gitlab.com
    1. Ping in `#s_fulfillment`:

        ```text
        Team, as a reminder we are going ahead with downtime in GitLab.com tomorrow per <LINK>. Please consider this as a reminder for you to set maintenance mode in customers.gitlab.com as appropriate (cc @Vitaly Slobodin)`.
        ```

  1. [ ] 🐘 {+ DBRE +}: Disable background migrations:

      1. [ ] Disable feature flags by typing the following into `#production`:

          - PRODUCTION:
              1. [ ] `/chatops run feature set execute_batched_migrations_on_schedule false`
              1. [ ] `/chatops run feature set execute_background_migrations false`

          - STAGING:
              1. [ ] `/chatops run feature set execute_batched_migrations_on_schedule false --staging`
              1. [ ] `/chatops run feature set execute_background_migrations false --staging`

      1. [ ] Inform the database team that background migrations have been disabled until the CR is complete. Post the following comment on the gitlab.com CR (<<%= gitlab_com_cr %>>):

          ```text
          Hi @gitlab-org/database-team,

          Please note that the `execute_batched_migrations_on_schedule` and `execute_background_migrations` feature flags were disabled in the `<%= env %>` environment, as we are carrying out OS upgrades to the database layer between `<%= maint_start_time_str %>` and `<%= maint_end_time_str %>`.

          We will re-enable the feature flags once the CR is complete.

          Thanks!
          ```

  1. [ ] 🏆 {+ Quality +}: Prepare the QA machine:

      1. [ ] Login

        ```sh
        ssh qa-tests-runner-01-sv-gstg.c.gitlab-staging-1.internal
        ```

        Even though it's in staging, the same VM is used for running tests against production.

      1. [ ] Create `~/gitlab-qa-<%= env %>.env` on the QA VM by running the following locally on your own machine and pasting the output into the file:

        ```sh
        export QA_ENVS_API_KEY= # Key with owner access to ops.gitlab.net/gitlab-org/gitlab-qa/quality
        ENV=<%= env %>
        cd db-migration
        cd pg-os-upgrade/bin
        ./get_qa_envs.rb $ENV
        ```

        The script above (`get_qa_envs.rb`) pulls down variables from the CI/CD Variables page on `ops.gitlab.net`:

        - [Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/settings/ci_cd)
        - [Production](https://ops.gitlab.net/gitlab-org/quality/production/-/settings/ci_cd)

      1. [ ] 🏆 {+ Quality +}: Confirm that our smoke tests are passing:

<%= partial "patroni_os_upgrade/_trigger_smoke_tests_partial.md.erb", {}, 10 %>

</details>

<details>
  <summary>

  - [ ] __T minus 3 hours (<%= 3.hours.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  1. [ ] ☎ {+ Comms-Handler +}: Create a broadcast message
      - STAGING: <https://staging.gitlab.com/admin/broadcast_messages>
      - PRODUCTION: <https://gitlab.com/admin/broadcast_messages>
      - Text:

          ```text
          We will soon be undergoing scheduled maintenance to our database layer. We expect https://<%= 'staging.' unless production %>gitlab.com to be unavailable for up to <%= window_in_hours %> hours starting from <%= maint_start_time_str %>. Please note that any CI jobs that start before the maintenance window but complete during the window period will fail and may need to be started again.
          ```

      - Starts at (UTC): `<%= 3.hours.ago(maint_start_time).strftime('%Y %B %d - %H:%M') %>`
      - End date at (UTC): `<%= 1.hours.since(maint_end_time).strftime('%Y %B %d - %H:%M') %>`

  1. [ ] ☎ {+ Comms-Handler +}: Send on Slack `#whats-happening-at-gitlab`:
    - Message:

        ```text
        :loudspeaker: *OS upgrades to our database instances are scheduled for <%= maint_start_time.strftime("%Y-%m-%d") %> between <%= maint_start_time.strftime("%H:%M") %> UTC and <%= maint_end_time.strftime("%H:%M") %> UTC :rocket:
        This is taking place in 3 hours :hourglass_flowing_sand:

        :hammer_and_wrench: *What to expect?*
        https://<%= 'staging.' unless production %>gitlab.com will be unavailable for up to <%= window_in_hours %> hours starting from `<%= maint_start_time_str %>`.

        You can follow our [issue](__ISSUE_LINK__)) on ops.gitlab.net.
        ```

  1. [ ] ☎ {+ Comms-Handler +}: Share message from `#whats-happening-at-gitlab` to the following channels:
      - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
      - [ ] `#g_delivery` (cc `@release-managers`)

  1. [ ] 🔪 {+ Playbook-Runner +}: Get the console VM ready for action

      - [ ] SSH to the console VM in `<%= env %>`
      - [ ] Start a tmux/screen session
      - [ ] Clone repos:

        ```sh
        rm -r src
        mkdir src
        cd src
        git clone git@gitlab.com:gitlab-com/gl-infra/ansible-migrations.git
        git clone git@gitlab.com:gitlab-com/gl-infra/db-migration.git
        ```

      - [ ] Ensure you have the pre-requisites installed:

        ```sh
        sudo apt install python3.8-venv
        python3 -m venv ~/venv
        source ~/venv/bin/activate
        pip install --upgrade pip
        cd ~/src/ansible-migrations/maintenance-mode
        pip install -r requirements.txt

        GCP_PROJECT=gitlab-<%= production ? "production" : "staging-1" %>
        ```

      - [ ] Ensure that Ansible can talk to hosts:
        - [ ] CI Cluster

            ```sh
            ENV=<%= env %>
            CLUSTER=ci
            cd ~/src/db-migration/pg-os-upgrade
            ansible -i inventory/${ENV}-${CLUSTER}.yml all -m ping
            ```

        - [ ] Main Cluster

            ```sh
            ENV=<%= env %>
            CLUSTER=main
            cd ~/src/db-migration/pg-os-upgrade
            ansible -i inventory/${ENV}-${CLUSTER}.yml all -m ping
            ```

        You shouldn't see any failed hosts!

      - [ ] Configure `gcloud`:

        ```sh
        gcloud auth login
        gcloud config set project ${GCP_PROJECT}
        gcloud container clusters list
        gcloud container clusters get-credentials ${ENV}-us-east1-b --region=us-east1-b
        gcloud container clusters get-credentials ${ENV}-us-east1-c --region=us-east1-c
        gcloud container clusters get-credentials ${ENV}-us-east1-d --region=us-east1-d
        gcloud container clusters get-credentials ${ENV}-gitlab-gke --region=us-east1

      - [ ] Test `kubectl` works:

        ```sh
        BYPASS_SAFETY_PROMPT="true" kubectl --context gke_${GCP_PROJECT}_us-east1_${ENV}-gitlab-gke -n gitlab get deployments
        ```

      - [ ] Create `~/maintenance-mode.env` with the following content

        ```sh
        export CLOUDFLARE_API_TOKEN=X # in 1Password
        export CLOUDFLARE_ZONE=<%= "staging." unless production %>gitlab.com
        export ENVIRONMENT=<%= env %>
        export GCP_PROJECT=gitlab-<%= production ? "production" : "staging-1" %>
        ```

  1. [ ] 🔪 {+ Playbook-Runner +}: On the console VM, do the following tasks to add an annotation to each node that disables scale down:

      - [ ] Add annotation:

        ```sh
        for c in $(BYPASS_SAFETY_PROMPT=true kubectl config get-contexts -o=name); do
          BYPASS_SAFETY_PROMPT=true kubectl --context $c annotate node --all cluster-autoscaler.kubernetes.io/scale-down-disabled=true
        done
        ```

      - [ ] Confirm annotation was added:

        ```sh
        for c in $(BYPASS_SAFETY_PROMPT=true kubectl config get-contexts -o=name); do
          BYPASS_SAFETY_PROMPT=true kubectl --context $c get nodes -o=jsonpath='{.items[?(@.metadata.annotations.cluster-autoscaler\.kubernetes\.io\/scale-down-disabled=="true")].metadata.name}' | tr ' ' '\n' | sort
        done
        ```

</details>

<details>
  <summary>

  - [ ] __T minus 1 hour (<%= 1.hours.ago(maint_start_time).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  1. [ ] 🔪 {+ Playbook-Runner +}: Add the following silences at <https://alerts.gitlab.net>:
      - Start time: `<%= maint_start_time.strftime('%FT%T.000Z') %>`
      - Duration: `<%= window_in_hours + 1 %>h`
      - Matchers (Create an individual alert silence for each matcher):
        - __STAGING__
          - [ ] `env="gstg"`
          - [ ] `instance=~"https://staging.gitlab.com.*"`
        - __PRODUCTION__
          - [ ] `env="gprd"`
          - [ ] `instance=~"https://gitlab.com.*"`

  1. [ ] 🔪 {+ Playbook-Runner +}: Run the following playbook to disable crons on the leader and standby leader:
      1. [ ] CI Cluster

          ```sh
          cd ~/src/db-migration/pg-os-upgrade
          ENV=<%= env %>
          CLUSTER=ci
          ./bin/ansible-exec.sh -e ${ENV} -C ${CLUSTER} -t disable-db-crons
          ```

      1. [ ] Main Cluster

          ```sh
          cd ~/src/db-migration/pg-os-upgrade
          ENV=<%= env %>
          CLUSTER=main
          ./bin/ansible-exec.sh -e ${ENV} -C ${CLUSTER} -t disable-db-crons
          ```

  1. [ ] 🔪 {+ Playbook-Runner +}: Execute Ansible playbooks on the console VM to save current replica counts and HPA min/max limits:

      ```sh
      . ~/maintenance-mode.env

      cd ~/src/ansible-migrations/maintenance-mode

      K8S_BACKUP_DIR="$HOME/k8s-deployments-snapshot-$(date +%Y%m%d)" ansible-playbook -e @variables.yml enable.yml -t save-config
      K8S_BACKUP_DIR="$HOME/k8s-deployments-snapshot-$(date +%Y%m%d)" ansible-playbook -e @variables.yml disable.yml -t save-config
      ```

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
      - Message:

        ```text
        GitLab.com will undergo maintenance in 1 hour. Please note that any CI jobs that start before the maintenance window but complete during the window period (up to <%= window_in_hours %> hours) will fail and may need to be started again.
        ```

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post to `#announcements` on Slack:
      - Message:

        ```text
        Maintenance on database layer starting in 1 hour (`<%= maint_start_time_str %>`). We expect https://gitlab.com to be unavailable for up to <%= window_in_hours %> hours.
        ```

  1. [ ] __PRODUCTION ONLY__ ☁ 🔪 {+ Playbook-Runner +}: Create a maintenance window in [PagerDuty](https://gitlab.pagerduty.com/service-directory/maintenance-windows) with the following:

      - Which services are affected?
        - [GitLab Production](https://gitlab.pagerduty.com/service-directory/PATDFCE)
        - [SLO alerts gprd cny stage](https://gitlab.pagerduty.com/service-directory/P3DG414)
        - [SLO Alerts gprd main stage](https://gitlab.pagerduty.com/service-directory/P7Q44DU)
        - [Dean Man's snitch](https://gitlab.pagerduty.com/service-directory/PFQXAWL)

      - Why is this maintenance happening?

        ```text
        Performing DB OS upgrades so silencing the pager.
        ```

      - Select `Start at a scheduled time`:

        - Timezone: `(UTC+00:00) UTC`
        - Start: `<%= maint_start_time.strftime('%m/%d/%Y | %I:%M %p') %>`
        - End: `<%= maint_end_time.strftime('%m/%d/%Y | %I:%M %p') %>`

  1. [ ] 🐘 {+ DBRE +}: Check that all chef MRs are rebased and contain the proper changes.

  1. [ ] __PRODUCTION ONLY__ 🐘 {+ DBRE +}: Ensure that we have a successful full WAL-G backup that has taken place in the last 24 hours for each cluster: [Thanos Graph](https://thanos.gitlab.net/graph?g0.expr=gitlab_job_success_timestamp_seconds%7Bresource%3D%22walg-basebackup%22%2C%20env%3D%22gprd%22%2C%20type%3D~%22patroni(-v12%7C-ci)%3F%7Cpatroni-(main%7Cci)-%5B0-9%5D%2B%22%7D%20%3E%20gitlab_job_start_timestamp_seconds%7Bresource%3D%22walg-basebackup%22%2C%20env%3D%22gprd%22%2C%20type%3D~%22patroni(-v12%7C-ci)%3F%7Cpatroni-(main%7Cci)-%5B0-9%5D%2B%22%7D%20and%20on%20(resource%2C%20env%2C%20type)%20gitlab_job_failed%20%3D%3D%200&g0.tab=0&g0.stacked=0&g0.range_input=2d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

      - If you see 4 rows (one for each 16.04/20.04 main + CI cluster), then a gap, then another 4 rows, then __you have a recent (< 24 hours) successful backup__. The gap is the period of time when the backups were executing. You can use a [timestamp converter](https://www.epochconverter.com/) to convert the timestamps into human-readable date/time if you want to check when the backup finished.

      - If you see that currently there are no lines (empty result), then it's possible that the backups are still running __OR__ they have failed, so check the following:

        - Check this [Thanos graph](https://thanos.gitlab.net/graph?g0.expr=gitlab_job_start_timestamp_seconds%7Bresource%3D%22walg-basebackup%22%2C%20env%3D%22gprd%22%2C%20type%3D~%22patroni(-v12%7C-ci)%3F%7Cpatroni-(main%7Cci)-%5B0-9%5D%2B%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=2d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) to see the start time of the backup job - you should be able to see an increase of the value every time the backup starts (around midnight). If the last increase was more than 24 hours ago then it means that the last backup hasn't started as it should have, and you'll need to investigate why the job failed to start.

        - If the backup job should have finished by now, then you should check this [Thanos graph](https://thanos.gitlab.net/graph?g0.expr=gitlab_job_failed%7Bresource%3D%22walg-basebackup%22%2C%20env%3D%22gprd%22%2C%20type%3D~%22patroni(-v12%7C-ci)%3F%7Cpatroni-(main%7Cci)-.*%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=1d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) to see the job failed value for the last time the backup job ran. If the value is `> 0` for any time in the past 24 hours, then you'll need to investigate why the job failed.

        The backup job is triggered by `crond` (user: `gitlab-psql`) and any replica is eligible to run the job but it only runs on the one that acquires the consul lock.

</details>

## Patroni OS Upgrade Call

These steps will be run in a Zoom call. The 🐺 {+ Coordinator +} runs the call,
asking other roles to perform each step on the checklist at the appropriate
time.

Changes are made one at a time, and verified before moving onto the next step.
All the steps will be executed from a console VM and we should keep the session
shared (tmux, screen...).

Whoever is performing a change should share their screen and explain their
actions as they work through them. Everyone else should watch closely for
mistakes or errors! A few things to keep an especially sharp eye out for:

- Exposed credentials (except short-lived items like 2FA codes)
- Running commands against the wrong hosts
- Navigating to the wrong pages in web browsers (gstg vs. gprd, etc)

Remember that the call will be recorded. We will consider making it public after we confirmed that no [SAFE data](https://about.gitlab.com/handbook/legal/safe-framework/) was leaked.
If you see something happening that shouldn't be public, mention it.

### Roll call

- [ ] 🐺 {+ Coordinator +}: Mark the change request as `/label ~"change::in-progress"`
- [ ] 🐺 {+ Coordinator +}: Ensure everyone mentioned above is on the call
- [ ] 🐺 {+ Coordinator +}: Ensure the Zoom room host is on the call

### Notify Users of Maintenance Window

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - [ ] Click "Start Maintenance" and send the following:
       - Message:

          ```text
          GitLab.com is now shutting down for planned maintenance to our database layer for the next <%= window_in_hours %> hours. See you on the other side!
          ```

### Health check

1. [ ] 🐺 {+ Coordinator +}: Ensure that there are no active critical alerts or open incidents:
    - STAGING:
        - <https://dashboards.gitlab.net/d/SOn6MeNmk/alerts?orgId=1&var-interval=1m&var-environment=gstg&var-alertname=All&var-alertstate=All&var-prometheus=prometheus-01-inf-gstg&var-prometheus_app=prometheus-app-01-inf-gstg>
    - PRODUCTION:
        - <https://dashboards.gitlab.net/d/SOn6MeNmk/alerts?orgId=1&var-interval=1m&var-environment=gprd&var-alertname=All&var-alertstate=All&var-prometheus=prometheus-01-inf-gprd&var-prometheus_app=prometheus-app-01-inf-gprd>
        - <https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=incident>

1. [ ] 🔪 {+ Playbook-Runner +}: Verify that the ansible inventory is up to date and reflects the real state from the cluster.

1. [ ] 🐘 {+ DBRE +} Check prometheus sanity check metrics for reads all going to the correct hosts
    - STAGING:
      1. [Index reads for MAIN tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22(patroni-0.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22(patroni-main-2004-.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries should be going to the `patroni` cluster.
      1. [Sequential scans of MAIN tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22patroni-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22patroni-main-2004-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries should be going to the `patroni` cluster.
      1. [Index reads for CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-2004.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: base load of 0.13/s for ci_pipelines, ci_build_needs, ci_pipeline_variables only due to prometheus exporter on `patroni-ci-2004` cluster.
      1. [Sequential scans of CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-2004-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries should be going to the `patroni-ci` cluster.

    - PRODUCTION:
      1. [Index reads for MAIN tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22(patroni-v12-.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22(patroni-main-2004-.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries should be going to the `patroni-v12` cluster.
      1. [Sequential scans of MAIN tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2C%20instance%3D~%22patroni-v12-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2C%20instance%3D~%22patroni-main-2004-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries should be going to the `patroni-v12` cluster.
      1. [Index reads for CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2C%20instance%3D~%22patroni-ci-%5B0-9%5D%2B-db-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-2004.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: base load of 0.13/s for ci_pipelines, ci_build_needs, ci_pipeline_variables only due to prometheus exporter on `patroni-ci-2004` cluster.
      1. [Sequential scans of CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-%5B0-9%5D%2B-db-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-2004-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries should be going to the `patroni-ci` cluster.

# T minus zero (<%= maint_start_time.strftime('%Y-%m-%d %H:%M') %> UTC)

We expect the maintenance window to last for up to <%= window_in_hours %> hours, starting from now.

## Patroni OS Upgrade rollout

### Bring it all down (T plus 0 min)

Playbook source: <https://gitlab.com/gitlab-com/gl-infra/ansible-migrations/-/tree/master/maintenance-mode>

1. [ ] 🔪 {+ Playbook-Runner +}: Execute Ansible playbook to set a maintenance page on Cloudflare and add GCP FW block rules:

   ```sh
   . ~/maintenance-mode.env

   cd ~/src/ansible-migrations/maintenance-mode

   # Step 1 == Put up maintenance page on Cloudflare + add GCP FW ingress blocking rules
   ansible-playbook -e @variables.yml enable.yml -t step-1 -e "allowed_ips=<%= qa_vm_ip %>"
   ```

1. [ ] 🔪 {+ Playbook-Runner +}: Validation:
    <details>
      <summary>

      __STAGING__

      </summary>

      - [ ] {+ BLOCKED +}: `ssh git@staging.gitlab.com`
      - [ ] {+ REDIRECT +}: In an incognito window, <https://staging.gitlab.com> should redirect to <https://about.staging.gitlab.com>
      - [ ] {+ 503 +}: In a window where you're logged in, <https://staging.gitlab.com> should redirect to a `503` maintenance page.
      - [ ] {+ 503 +}: <https://staging.gitlab.com/gitlab-com> should redirect to a `503` maintenance page.
      - [ ] {+ 200 +}: On `qa-tests-runner-01-sv-gstg.c.gitlab-staging-1.internal`, run:

        ```sh
        curl -o /dev/null -s https://staging.gitlab.com/gitlab-com -w '%{http_code}\n'
        ```

      - [ ] {+ 502 +}: On your machine, run:

          ```sh
          curl -o /dev/null -s --http1.1 https://kas.staging.gitlab.com/ -w '%{http_code}\n'
          ```
    </details>

    <details>
      <summary>

      __PRODUCTION__

      </summary>

      - [ ] {+ BLOCKED +}: `ssh git@gitlab.com`
      - [ ] {+ REDIRECT +}: In an incognito window, <https://gitlab.com> should redirect to <https://about.gitlab.com>
      - [ ] {+ 503 +}: In a window where you're logged in, <https://gitlab.com> should redirect to a `503` maintenance page.
      - [ ] {+ 503 +}: <https://gitlab.com/gitlab-com> should redirect to a `503` maintenance page.
      - [ ] {+ 200 +}: On `qa-tests-runner-01-sv-gstg.c.gitlab-staging-1.internal`, run:

          ```sh
          curl -o /dev/null -s https://gitlab.com/gitlab-com -w '%{http_code}\n'
          ```

      - [ ] {+ 502 +}: On your machine, run:

          ```sh
          curl -o /dev/null -s --http1.1 https://kas.gitlab.com/ -w '%{http_code}\n'
          ```

    </details>

1. [ ] 🔪 {+ Playbook-Runner +}: Execute Ansible playbook to stop services:

   ```sh
   # Step 2 == Stop zonal services (except sidekiq)
   # Step 3 == Stop regional/canary services
   ansible-playbook -e @variables.yml enable.yml -t step-2,step-3
   ```

1. [ ] 🐘 {+ DBRE +}: Observe sidekiq execution rates and queue lengths to ensure they are going down:

    1. [ ] STAGING: [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-1h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main)
    1. [ ] STAGING: [Kibana Sidekiq jobs without `json.meta.root_caller_id: Cronjob`](https://nonprod-log.gitlab.net/goto/fabd2290-d882-11ec-b771-57a829f2c394)
    1. [ ] PRODUCTION: [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-1h&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main)
    1. [ ] PRODUCTION: [Kibana Sidekiq jobs without `json.meta.root_caller_id: Cronjob`](https://log.gprd.gitlab.net/goto/149eb250-db3f-11ec-aade-19e9974a7229)

1. [ ] Wait 5 minutes to allow sidekiq queues to go down before continuing.

1. [ ] 🔪 {+ Playbook-Runner +}: Once we're ready to shutdown sidekiq, run the final step:

   ```sh
   # Step 4 == Stop sidekiq
   ansible-playbook -e @variables.yml enable.yml -t step-4
   ```

- [ ] 🐘 {+ DBRE +}: Monitor what pgbouncer pool has connections:

  - STAGING: [Thanos](https://thanos.gitlab.net/graph?g0.expr=pgbouncer_pools_client_active_connections%7Benv%3D%22gstg%22%2C%20user%3D%22gitlab%22%7D&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
  - PRODUCTION: [Thanos](https://thanos.gitlab.net/graph?g0.expr=pgbouncer_pools_client_active_connections%7Benv%3D%22gprd%22%2C%20user%3D%22gitlab%22%7D&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

- [ ] 🐘 {+ DBRE +}: Monitor the Primary Leader and Standby Leader PostgreSQL Log Files:
  - [ ] Login to node 01 of each cluster:
  <%- if production %>
    - `ssh patroni-v12-01-db-gprd.c.gitlab-production.internal`
    - `ssh patroni-ci-01-db-gprd.c.gitlab-production.internal`
    - `ssh patroni-main-2004-01-db-gprd.c.gitlab-production.internal`
    - `ssh patroni-ci-2004-01-db-gprd.c.gitlab-production.internal`
  <%- else %>
    - `ssh patroni-01-db-gstg.c.gitlab-staging-1.internal`
    - `ssh patroni-main-2004-01-db-gstg.c.gitlab-staging-1.internal`
    - `ssh patroni-ci-01-db-gstg.c.gitlab-staging-1.internal`
    - `ssh patroni-ci-2004-01-db-gstg.c.gitlab-staging-1.internal`
  <%- end %>

  - [ ] Get leader for each cluster:

      ```sh
      sudo gitlab-patronictl list
      ```

  - [ ] Connect via SSH to the previously identified leaders.
  - [ ] Tail Postgres Logs:

      ```sh
      sudo tail -f /var/log/gitlab/postgresql/postgresql.csv
      ```

1. [ ] 🐘 {+ DBRE +}: Execute a checkpoint on the Primary Leaders (MAIN AND CI) as identified earlier:

   ```sh
   sudo gitlab-psql -c "checkpoint;"
   ```

### OS Upgrades (T plus `~20` mins)

Playbook source: <https://gitlab.com/gitlab-com/gl-infra/db-migration/-/tree/master/pg-os-upgrade>

For this part, since each cluster takes 30-40mins, we will trigger the upgrades in parallel to save time.

#### Terminals

You should already be in a `tmux` session, but we want to open a second pane so that we have one terminal for each cluster.
- Press `Ctrl-b` then `"` your existing terminal in `tmux` to open a new pane, split horizontally.
- You can move between the panes by pressing `Ctrl-b` then `up` or `down` arrows.

#### MAIN

1. [ ] 🔪 {+ Playbook-Runner +}: Press `Ctrl-b` then `up` to go to the first terminal.
1. [ ] 🔪 {+ Playbook-Runner +}: Run Ansible playbook for Upgrading the MAIN cluster:

    ```sh
    cd ~/src/db-migration/pg-os-upgrade
    ENV=<%= env %>
    ./bin/ansible-exec.sh -e ${ENV} -C main
    ```

    This process will take a while. Continue with the CI cluster.

#### CI

1. [ ] 🔪 {+ Playbook-Runner +}: Press `Ctrl-b` then `down` to go to the second terminal.

1. [ ] 🔪 {+ Playbook-Runner +}: Run Ansible playbook for Upgrading the CI cluster:

    ```sh
    cd ~/src/db-migration/pg-os-upgrade
    ENV=<%= env %>
    ./bin/ansible-exec.sh -e ${ENV} -C ci
    ```

#### Post OS upgrades verification

You can execute the following steps as soon as their respective upgrades in the previous step have finished executing.

##### MAIN

- [ ] 🐘 {+ DBRE +}: Ensure Patroni-MAIN clusters are in desired state.
  - [ ] Login to a node on both Patroni-CI clusters:
    - STAGING:
      - patroni-main-2004-01-db-gstg.c.gitlab-staging-1.internal
      - patroni-01-db-gstg.c.gitlab-staging-1.internal
    - PRODUCTION:
      - patroni-v12-01-db-gprd.c.gitlab-production.internal
      - patroni-main-2004-01-db-gprd.c.gitlab-production.internal

  - [ ] Get the status of the Patroni clusters and post it on the maintenance issue:

      ```sh
      sudo gitlab-patronictl list
      ```

##### CI

- [ ] 🐘 {+ DBRE +}: Ensure Patroni-CI clusters are in desired state.
  - [ ] Login to a node on both Patroni-CI clusters:
    - STAGING:
      - patroni-ci-2004-01-db-gstg.c.gitlab-staging-1.internal
      - patroni-ci-01-db-gstg.c.gitlab-staging-1.internal
    - PRODUCTION:
      - patroni-ci-01-db-gprd.c.gitlab-production.internal
      - patroni-ci-2004-01-db-gprd.c.gitlab-production.internal

  - [ ] 🐘 {+ DBRE +}: Get the status of the Patroni clusters and post it on the maintenance issue:

      ```sh
      sudo gitlab-patronictl list
      ```

### Bring services back up (T plus `~60` mins)

- [ ] 🔪 {+ Playbook-Runner +}: Execute Ansible playbook to start all services:

    ```sh
    . ~/maintenance-mode.env

    cd ~/src/ansible-migrations/maintenance-mode
    ansible-playbook -e @variables.yml -t start-services disable.yml
    ```

### During-Blackout QA (T plus `~85` mins)

#### Trigger smoke tests

<%= partial "patroni_os_upgrade/_trigger_smoke_tests_partial.md.erb" %>

#### Evaluation of QA results - __Decision Point__

NOTE: We can ignore failures in QA test that relate to pushing over SSH as this is blocked.

#### Metrics sanity check from QA results

1. [ ] 🐘 {+ DBRE +}: Ensure writes are happening on new Patroni 20.04 clusters:

    - STAGING:
      - CI: [Thanos](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22gstg%22%2C%20type%3D%22patroni-ci%22%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22gstg%22%2C%20type%3D%22patroni-ci%22%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22gstg%22%2C%20type%3D%22patroni-ci%22%7D%5B1m%5D))%20by%20(instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - Main: [Thanos](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22gstg%22%2C%20type%3D%22patroni%22%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22gstg%22%2C%20type%3D%22patroni%22%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22gstg%22%2C%20type%3D%22patroni%22%7D%5B1m%5D))%20by%20(instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
    - PRODUCTION:
      - CI: [Thanos](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22gprd%22%2C%20type%3D%22patroni-ci%22%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22gprd%22%2C%20type%3D%22patroni-ci%22%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22gprd%22%2C%20type%3D%22patroni-ci%22%7D%5B1m%5D))%20by%20(instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - Main: [Thanos](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22gprd%22%2C%20type%3D%22patroni%22%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22gprd%22%2C%20type%3D%22patroni%22%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22gprd%22%2C%20type%3D%22patroni%22%7D%5B1m%5D))%20by%20(instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

1. [ ] 🐘 {+ DBRE +} Check prometheus sanity check metrics for reads all going to the correct hosts
    - STAGING:
      1. [Index reads for MAIN tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22(patroni-0.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22(patroni-main-2004-.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries going to the `patroni-main-2004` cluster.
      1. [Sequential scans of MAIN tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22patroni-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22patroni-main-2004-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: should be 0 for the `patroni` cluster
      1. [Index reads for CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-2004.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=1&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: base load of 0.13/s for ci_pipelines, ci_build_needs, ci_pipeline_variables only due to prometheus exporter on `patroni-ci` cluster.
      1. [Sequential scans of CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-2004-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: should be 0 for the `patroni-ci` cluster

    - PRODUCTION:
      1. [Index reads for MAIN tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22(patroni-v12-.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22(patroni-main-2004-.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries going to the `patroni-main-2004` cluster.
      1. [Sequential scans of MAIN tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers%7Cgroup_.*)%22%2Cinstance%3D~%22(patroni-v12-.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers%7Cgroup_.*)%22%2Cinstance%3D~%22(patroni-main-2004-.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries going to the `patroni-main-2004` cluster.
      1. [Index reads for CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-%5B0-9%5D%2B-db-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-2004.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries going to the `patroni-ci-2004` cluster.
      1. [Sequential scans of CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-%5B0-9%5D%2B-db-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-2004.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries going to the `patroni-ci-2004` cluster.

1. [ ] 🐘 {+ DBRE +} Check Sentry if there are errors that might indicate database problems
   1. [ ] PRODUCTION [Sentry](https://sentry.gitlab.net/gitlab/gitlabcom/)
   1. [ ] STAGING [Sentry](https://sentry.gitlab.net/gitlab/staginggitlabcom/)

#### Commitment

If QA has succeeded, then we can continue to "Complete the Upgrade". If some
QA has failed, the 🐺 {+ Coordinator +} must decide whether to continue with the
upgrade, or to [rollback](#rollback-if-required). A decision to continue
in these circumstances should be counter-signed by the 🎩 {+ Head Honcho +}.

The following are commitment criteria:

1. We expect two QA tests to fail related to SSH access (the SSH access is blocked).
1. If no errors were observed and we are beyond `T + 70 mins`: we continue moving forward without a rollback as the happy path is the fastest to execute.
1. If we observed errors and we are beyond `T + 70 mins`: we rollback as we anticipate the rollback to take at least 30 minutes
1. If we observed errors and we are before `T + 70 mins`: we investigate the failure for up to 10 minutes, and re-run QA.
1. If the re-run of QA failed: we rollback.

Goals:

1. The top priority is to maintain data integrity. Rolling back after the maintenance
   window has ended is very difficult, and will result in any changes made in the
   interim being lost.
1. Failures with an unknown cause should be investigated further. If we can't
   determine the root cause within the maintenance window, we should rollback.

<details>
  <summary>

### Complete the Upgrade (T plus `~<%= window_in_hours * 60 - 10 %>` mins)

  </summary>

1. [ ] 🔪 {+ Playbook-Runner +}: Execute Ansible playbook to remove maintenance page on Cloudflare and remove the temporary GCP FW block rules:

  ```sh
  . ~/maintenance-mode.env

  cd ~/src/ansible-migrations/maintenance-mode
  ansible-playbook -e @variables.yml -t open-access disable.yml
  ```

#### Communicate

1. [ ] 🐺 {+ Coordinator +}: Remove the broadcast message (if it's after the initial window, it has probably expired automatically)
1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message:

      ```text
      GitLab.com planned maintenance for the database layer is almost complete. The site is back up and running although we're continuing to verify that all systems are functioning correctly. Thank you for your patience.
      ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - [ ] Click "Finish Maintenance" and send the following:
       - Message:

          ```text
          GitLab.com's database layer maintenance upgrade is complete now, and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.
          ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: In the same thread from the earlier post, post the following message and click on the checkbox "Also send to X channel" so the threaded message would be published to the channel:
   - Message:

      ```text
      :done: *GitLab.com database layer maintenance upgrade is complete now.* :celebrate:
      We’ll continue to monitor the platform to ensure all systems are functioning correctly.
      ```

      - [ ] `#whats-happening-at-gitlab`
      - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
      - [ ] `#g_delivery` (cc `@release-managers`)

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send a message to `#social_media_action` to unpin maintenance tweet on `@gitlab` Twitter:
   - Message:

      ```text
      Hi team :wave:, the maintenance upgrade is complete now, you may unpin the maintenance tweet on GitLab Twitter.
      ```

#### Verification

1. __Start After-Blackout QA__ This is the second half of the test plan.
    1. [ ] 🏆 {+ Quality +}: Trigger Smoke and Full E2E suite against the environment that was upgraded:
        1. [ ] [Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules): `Four hourly smoke tests`, and `Daily Full QA suite`
        1. [ ] [Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules): `Four hourly smoke tests`, and `Twice daily full run`
    1. [ ] 🐘 {+ DBRE +}: Go to [www-gitlab-com pipelines](https://gitlab.com/gitlab-com/www-gitlab-com/-/pipelines) and `Run pipeline` on `master`. Wait for the pipeline to succeed.

#### Wrapping up

1. [ ] 🐘 {+ DBRE +}: Make changes permanent by merging the following MR:
    1. [ ] STAGING: <https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/2143>
    1. [ ] PRODUCTION: <https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/2214>

1. [ ] 🔪 {+ Playbook-Runner +}: Ensure that the changes merged from the previous step have been deployed to the Chef server before re-enabling Chef by confirming the linked `master` pipeline for `ops.gitlab.net` completed successfully.

1. [ ] 🔪 {+ Playbook-Runner +}: Re-enable Chef in all patroni MAIN AND CI instances:

    1. [ ] CI

        ```sh
        ENV=<%= env %>
        CLUSTER=ci
        cd ~/src/db-migration/pg-os-upgrade
        ./bin/ansible-exec.sh -e ${ENV} -C ${CLUSTER} -p enable_chef
        ```

    1. [ ] MAIN

        ```sh
        ENV=<%= env %>
        CLUSTER=main
        cd ~/src/db-migration/pg-os-upgrade
        ./bin/ansible-exec.sh -e ${ENV} -C ${CLUSTER} -p enable_chef
        ```

1. [ ] 🔪 {+ Playbook-Runner +}: Run chef-client on Patroni Nodes:

      ```sh
      ENV=<%= env %>
      knife ssh "role:${ENV}-base-db-patroni-ci-2004" "sudo chef-client"
      knife ssh "role:${ENV}-base-db-patroni-ci" "sudo chef-client"
      knife ssh "role:${ENV}-base-db-patroni-main-2004" "sudo chef-client"
      <%- if production -%>
      knife ssh "role:${ENV}-base-db-patroni-v12" "sudo chef-client"
      <%- else -%>
      knife ssh "role:${ENV}-base-db-patroni-main" "sudo chef-client"
      <%- end -%>
      ```

1. [ ] __PRODUCTION ONLY__ 🔪 {+ Playbook-Runner +}: If the scheduled maintenance is still active in [PagerDuty](https://gitlab.pagerduty.com/service-directory/maintenance-windows), click on `Update` then `End Now`.

1. [ ] 🔪 {+ Playbook-Runner +}: Remove silences added during this process from <https://alerts.gitlab.net>

1. [ ] 🐘 {+ DBRE +}: Check that DR main/CI archive & delayed 20.04 servers have followed the timeline switch and do not lag behind:

    1. [ ] Check that the timeline matches either main or CI:

        ```sh
        sudo -u gitlab-psql /opt/gitlab/embedded/bin/pg_controldata -D /var/opt/gitlab/postgresql/data | grep -i Timeline
        ```

    1. [ ] Check replication lag in Thanos:

        - __ARCHIVE__ - should have very little lag (less than 100):
          - [ ] STAGING: [Thanos](https://thanos.gitlab.net/graph?g0.expr=pg_replication_lag%7Benv%3D%22gstg%22%2C%20instance%3D~%22postgres-(ci-)%3Fdr-archive-2004-.*%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=6h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
          - [ ] PRODUCTION: [Thanos](https://thanos.gitlab.net/graph?g0.expr=pg_replication_lag%7Benv%3D%22gprd%22%2C%20instance%3D~%22postgres-(ci-)%3Fdr-.*archive-2004-.*%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=6h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

        - __DELAYED__ - should be around 28800 (8 hours):
          - [ ] STAGING: [Thanos](https://thanos.gitlab.net/graph?g0.expr=pg_replication_lag%7Benv%3D%22gstg%22%2C%20instance%3D~%22postgres-(ci-)%3Fdr-delayed-2004-.*%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=6h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
          - [ ] PRODUCTION: [Thanos](https://thanos.gitlab.net/graph?g0.expr=pg_replication_lag%7Benv%3D%22gprd%22%2C%20instance%3D~%22postgres-(ci-)%3Fdr-.*delayed-2004-.*%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=6h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

1. [ ] 🐘 {+ DBRE +}: Initiate a full backup (using `wal-g`) and trigger GCS snapshots on the Patroni main & CI clusters:

    <%- (production ? %w{patroni-main-2004-10 patroni-ci-2004-03} : %w{patroni-main-2004-06 patroni-ci-2004-03}).each { |host| -%>
    - [ ] SSH to `<%= "#{host}-db-#{env}.c.#{gcp_project}.internal" %>`
    - [ ] Run a [Wal-G backup](https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/patroni/postgresql-backups-wale-walg.md#daily-basebackup):

        ```sh
        sudo su - gitlab-psql
        tmux new -s PGBasebackup
        nohup /opt/wal-g/bin/backup.sh >> /var/log/wal-g/wal-g_backup_push.log 2>&1 &
        ```

    - [ ] Open another SSH session to `<%= "#{host}-db-#{env}.c.#{gcp_project}.internal" %>`
    - [ ] Run a [manual GCS Snapshot](https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/patroni-ci/rebuild_ci_cluster_from_prod.md#take-a-snapshot-from-the-writer-node)

        ```sh
        sudo su - gitlab-psql
        tmux new -s GCSSnapshot
        /usr/local/bin/gcs-snapshot.sh
        ```
    <%- } -%>

1. [ ] 🐺 {+ Coordinator +}: Check for any concerning alerts:
    - Staging: <https://alerts.gitlab.net> or #alerts-nonprod in Slack
    - Production: <https://alerts.gitlab.net> or #alerts in Slack

1. [ ] 🐺 {+ Coordinator +}: Mark the change request as `/label ~"change::complete"`

1. [ ] 🔪 {+ Playbook-Runner +}: `rm ~/maintenance-mode.env` on console machine

1. [ ] 🏆 {+ Quality +}: (1 hour after __Start After-Blackout QA__) Check that the Smoke, and Full E2E suite has passed.
1. [ ] 🏆 {+ Quality +}: Trigger smoke tests one more time now that Chef would have had time to run:

<%= partial "patroni_os_upgrade/_trigger_smoke_tests_partial.md.erb", {}, 4 %>

1. [ ] 🏆 {+ Quality +}: `rm ~/gitlab-qa-<%= env %>.env` on QA VM
1. [ ] 🔪 {+ Playbook-Runner +}: On the console VM, do the following tasks to remove the annotation on each node that disabled scale down:

    1. [ ] Remove annotation:

      ```sh
      for c in $(BYPASS_SAFETY_PROMPT=true kubectl config get-contexts -o=name); do
        BYPASS_SAFETY_PROMPT=true kubectl --context $c annotate node --all cluster-autoscaler.kubernetes.io/scale-down-disabled-
      done
      ```

    1. [ ] Confirm no nodes have the annotation set:

      ```sh
      for c in $(BYPASS_SAFETY_PROMPT=true kubectl config get-contexts -o=name); do
        BYPASS_SAFETY_PROMPT=true kubectl --context $c get nodes -o=jsonpath='{.items[?(@.metadata.annotations.cluster-autoscaler\.kubernetes\.io\/scale-down-disabled=="true")].metadata.name}'
      done
      ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Notify our customer over [Slack channel](https://gitlab.slack.com/archives/CDRLZUVHB) that Patroni OS Upgrade finished, and request to validate GitLab.com.

1. [ ] __PRODUCTION ONLY__ 🐘 {+ DBRE +}: Ping `@NikolayS` on the gitlab.com CR (<<%= gitlab_com_cr %>>) (and/or in [#database-lab](https://gitlab.slack.com/archives/CLJMDRD8C)) that the work is complete so he can update the DLE environment.

1. [ ] __PRODUCTION ONLY__ 🏆 {+ Quality +}: Confirm that Teleport access works properly and we're hitting the right clusters for CI and MAIN:

    ```bash
    # Login to Teleport
    tsh login --proxy=teleport.<%= env %>.gitlab.net --request-roles=database --request-reason="Testing DB connectivity"
    query="select setting from pg_settings where name='cluster_name'"

    # Check MAIN
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=gitlabhq_production db-primary
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=gitlabhq_production db-secondary

    # Check CI
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=gitlabhq_production db-primary-ci
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=gitlabhq_production db-secondary-ci
    ```

    You should see `2004` as part of the cluster name.

1. [ ]  🐘 {+ DBRE +}: Reindex GIN indexes using the CONCURRENTLY option for MAIN and CI clusters. This will take a very long time so we run it in a new tmux session.

    1. [ ] SSH to the console VM in `<%= env %>`

    1. [ ] Copy the generate GIN reindex SQL scripts to the Patroni leaders:

        ```sh
        cd ~/src

        rsync -av ./db-migration/pg-os-upgrade/playbooks/roles/patroni_os_upgrade/files/sqls/generate_reindex_*gin*.sql master.patroni.service.consul:/var/tmp/

        rsync -av ./db-migration/pg-os-upgrade/playbooks/roles/patroni_os_upgrade/files/sqls/generate_reindex_*gin*.sql master.patroni-ci.service.consul:/var/tmp/
        ```

    1. [ ] Start a new tmux session with two windows (main and CI):

        ```sh
        tmux new -s reindex_gin_concurrently \; rename-window 'main'\; new-window -n 'ci'\; previous-window\; attach
        ```

    1. [ ] You are now in terminal `0`. SSH to the Patroni leader in MAIN:

          ```sh
          ssh master.patroni.service.consul
          ```

    1. [ ] Switch to the second terminal (`Ctrl-b` then `1`) and SSH to the Patroni leader in CI:

          ```sh
          ssh master.patroni-ci.service.consul
          ```

    1. [ ] Generate SQL script to reindex GIN indexes using the CONCURRENTLY option. The generated SQL script will not include the GIN indexes that were reindexed during scheduled downtime for the upgrade.

        1. [ ] Press `Ctrl-b` then `0` to go to the first terminal.

        1. [ ] Run generate script in MAIN:

            ```sh
            sudo gitlab-psql -tAXf /var/tmp/generate_reindex_gin_concurrently.sql -o /var/tmp/reindex_gin_concurrently.sql
            ls -l /var/tmp/reindex_gin_concurrently.sql
            ```

        1. [ ] Press `Ctrl-b` then `1` to go to the second terminal.

        1. [ ] Run generate script in CI:

            ```sh
            sudo gitlab-psql -tAXf /var/tmp/generate_reindex_ci_gin_concurrently.sql -o /var/tmp/reindex_ci_gin_concurrently.sql
            ls -l /var/tmp/reindex_ci_gin_concurrently.sql
            ```

    1. [ ] Run generated SQL script to reindex GIN indexes & monitor for progress and errors. It will take several hours to complete the task for the Main cluster.

        1. [ ] Press `Ctrl-b` then `0` to go to the first terminal.

        1. [ ] Run reindex SQL script in MAIN:

          ```sh
          nohup sudo gitlab-psql -c '\timing on' -bef /var/tmp/reindex_gin_concurrently.sql > /var/tmp/reindex_gin_concurrently.out 2>&1 &
          tail -f /var/tmp/reindex_gin_concurrently.out
          grep -i 'error:' /var/tmp/reindex_gin_concurrently.out
          ```

        1. [ ] In the tmux session, press `Ctrl-b` then `1` to go to the second terminal.

        1. [ ] Run reindex SQL script in CI:

          ```sh
          nohup sudo gitlab-psql -c '\timing on' -bef /var/tmp/reindex_ci_gin_concurrently.sql > /var/tmp/reindex_ci_gin_concurrently.out 2>&1 &
          tail -f /var/tmp/reindex_ci_gin_concurrently.out
          grep -i 'error:' /var/tmp/reindex_ci_gin_concurrently.out
          ```

1. [ ] 🐘 {+ DBRE +}: Enable the background migrations:

    1. [ ] Enable background migrations feature flags by typing the following into `#production`:

        - STAGING:
            1. [ ] `/chatops run feature set execute_batched_migrations_on_schedule true --staging`
            1. [ ] `/chatops run feature set execute_background_migrations true --staging`

        - PRODUCTION:
            1. [ ] `/chatops run feature set execute_batched_migrations_on_schedule true`
            1. [ ] `/chatops run feature set execute_background_migrations true`

    1. [ ] Inform the database team that the CR is completed and that the background migrations feature flags have been re-enabled. Post the following comment on the gitlab.com CR (<<%= gitlab_com_cr %>>):

        ```text
        Hi @gitlab-org/database-team,

        Please note that we have completed the work for this CR in the `<%= env %>` environment. Therefore we have re-enabled the `execute_batched_migrations_on_schedule` and `execute_background_migrations` feature flags.

        Could you please confirm that they have been re-enabled correctly?

        Thanks!
        ```

<%=
partial "patroni_os_upgrade/_amcheck_partial.md.erb", vars={
  reindex_concurrently: true,
  amcheck_main_host: production ? "patroni-main-2004-10-db" : "patroni-main-2004-06-db",
  amcheck_ci_host: "patroni-ci-2004-03-db",
  amcheck_suffix: "2004",
  amcheck_limit: 3885
}, 2
%>

1. [ ] __PRODUCTION ONLY__  🐘 {+ DBRE +}: Enable database reindexing:

    1. [ ] Enable the reindexing by typing the following in `#production`:

        ```text
        /chatops run feature set database_reindexing true
        ```

    1. [ ] Inform the database team that the CR is completed and that database reindexing has been re-enabled. Post the following comment on the gitlab.com CR (<<%= gitlab_com_cr %>>):

        ```text
        Hi @gitlab-org/database-team,

        Please note that we have completed the work for this CR. Therefore we have re-enabled the `database_reindexing` feature flag in `PRODUCTION`.

        Could you please confirm that it has been re-enabled correctly?

        Thanks!
        ```

</details>

<details>
  <summary>

### __Rollback__ (if required)

  </summary>

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post an update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message:

        ```text
        Due to an issue during the maintenance, we have initiated a rollback of the changes. We will send another update within the next 30 minutes.
        ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Ask the IMOC or the Head Honcho if this message should be sent to any slack rooms:
   - `#whats-happening-at-gitlab`
   - `#infrastructure-lounge` (cc `@sre-oncall`)
   - `#g_delivery` (cc `@release-managers`)
   - `#community-relations`

#### Health check

1. [ ] 🐺 {+ Coordinator +}: Ensure that there are no active critical alerts or open incidents:
    - STAGING:
        - <https://dashboards.gitlab.net/d/SOn6MeNmk/alerts?orgId=1&var-interval=1m&var-environment=gstg&var-alertname=All&var-alertstate=All&var-prometheus=prometheus-01-inf-gstg&var-prometheus_app=prometheus-app-01-inf-gstg>
    - PRODUCTION:
        - <https://dashboards.gitlab.net/d/SOn6MeNmk/alerts?orgId=1&var-interval=1m&var-environment=gprd&var-alertname=All&var-alertstate=All&var-prometheus=prometheus-01-inf-gprd&var-prometheus_app=prometheus-app-01-inf-gprd>
        - <https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=incident>

1. [ ] 🔪 {+ Playbook-Runner +}: Verify that the ansible inventory is up to date and reflects the real state from the cluster.

1. [ ] 🐘 {+ DBRE +} Check prometheus sanity check metrics for reads all going to the correct hosts
    - STAGING:
      1. [Index reads for MAIN tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22(patroni-0.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22(patroni-main-2004-.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries going to the `patroni-main-2004` cluster.
      1. [Sequential scans of MAIN tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22patroni-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22patroni-main-2004-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: should be 0 for the `patroni` cluster
      1. [Index reads for CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-2004.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=1&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: base load of 0.13/s for ci_pipelines, ci_build_needs, ci_pipeline_variables only due to prometheus exporter on `patroni-ci` cluster.
      1. [Sequential scans of CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-2004-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: should be 0 for the `patroni-ci` cluster

    - PRODUCTION:
      1. [Index reads for MAIN tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22(patroni-v12-.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2Cinstance%3D~%22(patroni-main-2004-.*)%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries going to the `patroni-main-2004` cluster.
      1. [Sequential scans of MAIN tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2C%20instance%3D~%22patroni-v12-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(projects%7Cnamespaces%7Cmembers)%22%2C%20instance%3D~%22patroni-main-2004-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries going to the `patroni-main-2004` cluster.
      1. [Index reads for CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2C%20instance%3D~%22patroni-ci-%5B0-9%5D%2B-db-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-2004-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries going to the `patroni-ci-2004` cluster. Base load of 0.13/s for ci_pipelines, ci_build_needs, ci_pipeline_variables only due to prometheus exporter on `patroni-ci` cluster.
      1. [Sequential scans of CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-%5B0-9%5D%2B-db-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D&g1.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-2004-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g1.tab=0&g1.stacked=0&g1.range_input=1h&g1.max_source_resolution=0s&g1.deduplicate=1&g1.partial_response=0&g1.store_matches=%5B%5D)
          - [ ] Expected result: all queries going to the `patroni-ci-2004` cluster.

#### OS Upgrades Rollback

Just like we triggered the upgrades in parallel, we will do the same when rolling back.

1. [ ] 🔪 {+ Playbook-Runner +}: Execute Ansible playbook to stop services:

   ```sh
   . ~/maintenance-mode.env
   # Step 2 == Stop zonal services (except sidekiq)
   # Step 3 == Stop regional/canary services
   ansible-playbook -e @variables.yml enable.yml -t step-2,step-3
   ```

1. [ ] 🐘 {+ DBRE +}: Observe sidekiq execution rates and queue lengths to ensure they are going down:

    - [ ] STAGING: [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-1h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main)
    - [ ] STAGING: [Kibana Sidekiq jobs without `json.meta.root_caller_id: Cronjob`](https://nonprod-log.gitlab.net/goto/fabd2290-d882-11ec-b771-57a829f2c394)
    - [ ] PRODUCTION: [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-1h&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main)
    - [ ] PRODUCTION: [Kibana Sidekiq jobs without `json.meta.root_caller_id: Cronjob`](https://log.gprd.gitlab.net/goto/149eb250-db3f-11ec-aade-19e9974a7229)

1. [ ] Wait 5 minutes to allow sidekiq queues to go down before continuing.

1. [ ] 🔪 {+ Playbook-Runner +}: Once we're ready to shutdown sidekiq, run the final step:

   ```sh
   # Step 4 == Stop sidekiq
   ansible-playbook -e @variables.yml enable.yml -t step-4
   ```

1. [ ] 🐘 {+ DBRE +}: Monitor what pgbouncer pool has connections:

    - STAGING: [Thanos](https://thanos.gitlab.net/graph?g0.expr=pgbouncer_pools_client_active_connections%7Benv%3D%22gstg%22%2C%20user%3D%22gitlab%22%7D&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
    - PRODUCTION: [Thanos](https://thanos.gitlab.net/graph?g0.expr=pgbouncer_pools_client_active_connections%7Benv%3D%22gprd%22%2C%20user%3D%22gitlab%22%7D&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

1. [ ] 🐘 {+ DBRE +}: Monitor the Primary Leader and Standby Leader PostgreSQL Log Files:
    1. [ ] Login to node 01 of each cluster:
        <%- if production %>
        - `ssh patroni-v12-01-db-gprd.c.gitlab-production.internal`
        - `ssh patroni-ci-01-db-gprd.c.gitlab-production.internal`
        - `ssh patroni-main-2004-01-db-gprd.c.gitlab-production.internal`
        - `ssh patroni-ci-2004-01-db-gprd.c.gitlab-production.internal`
        <%- else %>
        - `ssh patroni-01-db-gstg.c.gitlab-staging-1.internal`
        - `ssh patroni-main-2004-01-db-gstg.c.gitlab-staging-1.internal`
        - `ssh patroni-ci-01-db-gstg.c.gitlab-staging-1.internal`
        - `ssh patroni-ci-2004-01-db-gstg.c.gitlab-staging-1.internal`
        <%- end %>

    1. [ ] Get leader for each cluster:

        ```sh
        sudo gitlab-patronictl list
        ```

    1. [ ] Connect via SSH to the previously identified leaders.
    1. [ ] Tail Postgres Logs:

        ```sh
        sudo tail -f /var/log/gitlab/postgresql/postgresql.csv
        ```

1. [ ] 🐘 {+ DBRE +}: Execute a checkpoint on the Primary Leaders (MAIN AND CI) as identified earlier:

    ```sh
    sudo gitlab-psql -c "checkpoint;"
    ```

##### MAIN DB Rollback

Goal: __Set Patroni MAIN 16.04 cluster as Primary and Patroni MAIN 20.04 as Secondary__

1. [ ] 🔪 {+ Playbook-Runner +}: Press `Ctrl-b` then `up` to go to the first terminal.

1. [ ] 🔪 {+ Playbook-Runner +}: Execute playbook to rollback to Patroni 16.04 cluster:

      ```sh
      ENV=<%= env %>
      cd ~/src/db-migration/pg-os-upgrade
      ./bin/ansible-exec.sh -e ${ENV} -C main-rollback
      ```

  This process will take some time. Continue with the CI DB Rollback.

##### CI DB Rollback

Goal: __Set Patroni CI 16.04 cluster as Primary and Patroni CI 20.04 as Secondary__

1. [ ] 🔪 {+ Playbook-Runner +}: Press `Ctrl-b` then `down` to go to the second terminal.

1. [ ] 🔪 {+ Playbook-Runner +}: Execute playbook to rollback to Patroni 16.04 cluster:

  ```sh
  ENV=<%= env %>
  cd ~/src/db-migration/pg-os-upgrade
  ./bin/ansible-exec.sh -e ${ENV} -C ci-rollback
  ```

### Complete the rollback

1. [ ] 🔪 {+ Playbook-Runner +}: Restart services:

    ```sh
    . ~/maintenance-mode.env
    cd ~/src/ansible-migrations/maintenance-mode

    # Shutdown all services (including sidekiq)
    ansible-playbook -e @variables.yml enable.yml -t k8s

    # Start services
    ansible-playbook -e @variables.yml disable.yml -t start-services
    ```

1. [ ] 🏆 {+ Quality +} Confirm that our smoke tests are still passing:

<%= partial "patroni_os_upgrade/_trigger_smoke_tests_partial.md.erb", {}, 4 %>

1. [ ] 🔪 {+ Playbook-Runner +}: Open up access:

    ```sh
    ansible-playbook -e @variables.yml -t open-access disable.yml
    ```

1. [ ] 🐘 {+ DBRE +}: If the following MRs have been merged, then revert them, get them merged and wait until they get deployed to the Chef server by monitoring the `ops.gitlab.net` pipeline:
    1. [ ] STAGING: <https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/2143>
    1. [ ] PRODUCTION: <https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/2214>

1. [ ] 🔪 {+ Playbook-Runner +}: Re-enable Chef in all patroni MAIN AND CI instances:

    1. [ ] CI

        ```sh
        ENV=<%= env %>
        CLUSTER=ci
        cd ~/src/db-migration/pg-os-upgrade
        ./bin/ansible-exec.sh -e ${ENV} -C ${CLUSTER} -p enable_chef
        ```

    1. [ ] MAIN

        ```sh
        ENV=<%= env %>
        CLUSTER=main
        cd ~/src/db-migration/pg-os-upgrade
        ./bin/ansible-exec.sh -e ${ENV} -C ${CLUSTER} -p enable_chef
        ```

1. [ ] 🏆 {+ Quality +}: Perform [Verification](#verification) above.

1. [ ] 🏆 {+ Quality +}: (1 hour after __Start After-Blackout QA__) Check that the Smoke, and Full E2E suite has passed.

1. [ ] 🔪 {+ Playbook-Runner +}: On the console VM, do the following tasks to remove the annotation on each node that disabled scale down:

    - [ ] Remove annotation:

      ```sh
      for c in $(BYPASS_SAFETY_PROMPT=true kubectl config get-contexts -o=name); do
        BYPASS_SAFETY_PROMPT=true kubectl --context $c annotate node --all cluster-autoscaler.kubernetes.io/scale-down-disabled-
      done
      ```

    - [ ] Confirm no nodes have the annotation set:

      ```sh
      for c in $(BYPASS_SAFETY_PROMPT=true kubectl config get-contexts -o=name); do
        BYPASS_SAFETY_PROMPT=true kubectl --context $c get nodes -o=jsonpath='{.items[?(@.metadata.annotations.cluster-autoscaler\.kubernetes\.io\/scale-down-disabled=="true")].metadata.name}'
      done
      ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message:

        ```text
        GitLab.com rollback for the database layer is almost complete. The site is back up and running although we're continuing to verify that all systems are functioning correctly. Thank you for your patience.
        ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - [ ] Click "Finish Maintenance" and send the following:
       - Message:

          ```text
          GitLab.com's database rollback is complete now, and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.
          ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send the following message should be sent to slack rooms:

   ```text
   GitLab.com's database rollback is complete now, and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.
   ```

   - [ ] `#whats-happening-at-gitlab`
   - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
   - [ ] `#g_delivery` (cc `@release-managers`)

1. [ ]  🐘 {+ DBRE +}: Reindex GIN indexes using the CONCURRENTLY option for MAIN and CI clusters. This will take a very long time so we run it in a new tmux session.

    1. [ ] SSH to the console VM in `<%= env %>`

    1. [ ] Copy the generate GIN reindex SQL scripts to the Patroni leaders:

        ```sh
        cd ~/src

        rsync -av ./db-migration/pg-os-upgrade/playbooks/roles/patroni_os_upgrade/files/sqls/generate_reindex_*gin*.sql master.patroni.service.consul:/var/tmp/

        rsync -av ./db-migration/pg-os-upgrade/playbooks/roles/patroni_os_upgrade/files/sqls/generate_reindex_*gin*.sql master.patroni-ci.service.consul:/var/tmp/
        ```

    1. [ ] Start a new tmux session with two windows (main and CI):

        ```sh
        tmux new -s reindex_gin_concurrently \; rename-window 'main'\; new-window -n 'ci'\; previous-window\; attach
        ```

    1. [ ] You are now in terminal `0`. SSH to the Patroni leader in MAIN:

          ```sh
          ssh master.patroni.service.consul
          ```

    1. [ ] Switch to the second terminal (`Ctrl-b` then `1`) and SSH to the Patroni leader in CI:

          ```sh
          ssh master.patroni-ci.service.consul
          ```

    1. [ ] Generate SQL script to reindex GIN indexes using the CONCURRENTLY option. The generated SQL script will not include the GIN indexes that were reindexed during scheduled downtime for the upgrade.

        - [ ] Press `Ctrl-b` then `0` to go to the first terminal.

        - [ ] Run generate script in MAIN:

            ```sh
            sudo gitlab-psql -tAXf /var/tmp/generate_reindex_gin_concurrently.sql -o /var/tmp/reindex_gin_concurrently.sql
            ls -l /var/tmp/reindex_gin_concurrently.sql
            ```

        - [ ] Press `Ctrl-b` then `1` to go to the second terminal.

        - [ ] Run generate script in CI:

            ```sh
            sudo gitlab-psql -tAXf /var/tmp/generate_reindex_ci_gin_concurrently.sql -o /var/tmp/reindex_ci_gin_concurrently.sql
            ls -l /var/tmp/reindex_ci_gin_concurrently.sql
            ```

    1. [ ] Run generated SQL script to reindex GIN indexes & monitor for progress and errors. It will take several hours to complete the task for the Main cluster.

        1. [ ] Press `Ctrl-b` then `0` to go to the first terminal.

        1. [ ] Run reindex SQL script in MAIN:

          ```sh
          nohup sudo gitlab-psql -c '\timing on' -bef /var/tmp/reindex_gin_concurrently.sql > /var/tmp/reindex_gin_concurrently.out 2>&1 &
          tail -f /var/tmp/reindex_gin_concurrently.out
          grep -i 'error:' /var/tmp/reindex_gin_concurrently.out
          ```

        1. [ ] In the tmux session, press `Ctrl-b` then `1` to go to the second terminal.

        1. [ ] Run reindex SQL script in CI:

          ```sh
          nohup sudo gitlab-psql -c '\timing on' -bef /var/tmp/reindex_ci_gin_concurrently.sql > /var/tmp/reindex_ci_gin_concurrently.out 2>&1 &
          tail -f /var/tmp/reindex_ci_gin_concurrently.out
          grep -i 'error:' /var/tmp/reindex_ci_gin_concurrently.out
          ```

1. [ ] 🐘 {+ DBRE +}: Enable the background migrations after rollback:

    1. [ ] Enable background migrations feature flags by typing the following into `#production`:

        - STAGING:
            1. [ ] `/chatops run feature set execute_batched_migrations_on_schedule true --staging`
            1. [ ] `/chatops run feature set execute_background_migrations true --staging`

        - PRODUCTION:
            1. [ ] `/chatops run feature set execute_batched_migrations_on_schedule true`
            1. [ ] `/chatops run feature set execute_background_migrations true`

    1. [ ] Inform the database team that the CR has been rolled back and that background migrations have been re-enabled. Post the following comment on the gitlab.com CR (<<%= gitlab_com_cr %>>):

        ```text
        Hi @gitlab-org/database-team,

        Please note that we have completed the work for this CR in the `<%= env %>` environment. Therefore we have re-enabled the `execute_batched_migrations_on_schedule` and `execute_background_migrations` feature flags.

        Could you please confirm that they have been re-enabled correctly?

        Thanks!
        ```

<%= partial "patroni_os_upgrade/_amcheck_partial.md.erb", vars={ reindex_concurrently: true, amcheck_main_host: production ? "patroni-v12-10-db" : "patroni-06-db", amcheck_ci_host: "patroni-ci-03-db" } %>

1. [ ] __PRODUCTION ONLY__  🐘 {+ DBRE +}: Enable database reindexing after rollback:

    1. [ ] Enable the reindexing by typing the following in `#production`:

        ```text
        /chatops run feature set database_reindexing true
        ```

    1. [ ] Inform the database team that the CR has been rolled back and that database reindexing has been re-enabled in PROD. Post the following comment on the gitlab.com CR (<<%= gitlab_com_cr %>>):

        ```text
        Hi @gitlab-org/database-team,

        Please note that we have completed the work for this CR. Therefore we have re-enabled the `database_reindexing` feature flag in `PRODUCTION`.

        Could you please confirm that it has been re-enabled correctly?

        Thanks!
        ```

</details>

### Wrap Up

1. [ ] 🔪 {+ Playbook-Runner +}: `rm ~/maintenance-mode.env` on console machine
1. [ ] 🔪 {+ Playbook-Runner +}: `rm ~/gitlab-qa-<%= env %>.env` on QA VM

---

## Extra details

### In case the Playbook-Runner is disconnected

As most of the steps are executed in a tmux session owned by the
Playbook-Runner role we need a safety net in case this person loses their
internet connection or otherwise drops off half way through. Since other
SREs/DBREs also have root access on the console node where everything is
running they should be able to recover it in different ways. We tested the
following approach to recovering the tmux session, updating the ssh agent and
taking over as a new ansible user.

- `ssh host`
- Add your public SSH key to `/home/PREVIOUS_PLAYBOOK_USERNAME/.ssh/authorized_keys`
- `sudo chef-client-disable <%= gitlab_com_cr %>` so that we don't override the above
- `ssh -A PREVIOUS_PLAYBOOK_USERNAME@host`
- `echo $SSH_AUTH_SOCK`
- `tmux attach -t 0`
- `export SSH_AUTH_SOCK=<VALUE from previous SSH_AUTH_SOCK output>`
- `<ctrl-b> :`
- `set-environment -g 'SSH_AUTH_SOCK' <VALUE from previous SSH_AUTH_SOCK output>`
- `export ANSIBLE_REMOTE_USER=NEW_PLAYBOOK_USERNAME`
- `<ctrl-b> :`
- `set-environment -g 'ANSIBLE_REMOTE_USER' <your-user>`
