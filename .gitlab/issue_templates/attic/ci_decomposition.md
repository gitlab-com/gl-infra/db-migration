# CI Decomposition Rollout Team

| Role                               | Assigned To |
| ---------------------------------- | ----------- |
| 🐺 Coordinator                     |             |
| 🔪 Playbook-Runner                 |             |
| ☎ Comms-Handler                    |             |
| 🐘 Database-Wrangler               |             |
| 🏆 Quality                         |             |
| 👷 Sharding Team Backend Engineer  |             |
| 🎩 IMOC                            |             |
| 📣 CMOC                            |             |
| 🚑 EOC                             |             |
| 📐 Development                     |             |
| 💾 Database Maintainer             |             |
| 💾 Head Honcho                     |             |

# Immediately

Perform these steps when the issue is created.

- [ ] 🐺 {+ Coordinator +}: Fill out the names of the rollout team in the table above.
- [ ] 🐺 {+ Coordinator +}: Fill out dates/times and links in this issue:
  - Start Time: `__MAINTENANCE_START_TIME__` & End Time: `__MAINTENANCE_END_TIME__`
  - __PRODUCTION ONLY__ Blog Post: __BLOG_POST_URL__
  - __PRODUCTION ONLY__ End Time: __MAINTENANCE_END_TIME__

# Support Options

| Provider | Plan | Details | Create Ticket |
|----------|------|---------|---------------|
| __Google Cloud Platform__ | [Gold Support](https://cloud.google.com/support/?options=premium-support#options) | 24x7, email & phone, 1hr response on critical issues | [__Create GCP Support Ticket__](https://enterprise.google.com/supportcenter/managecases) |

# Entry points

| Entry point                | Before                                                               | Blocking mechanism                                             | Allowlist | QA needs                                                                    | Notes                                                                                               |
| -------------------------- | -------------------------------------------------------------------- | -------------------------------------------------------------- | --------- | --------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------- |
| Web traffic                | Available via gitlab.com (Cloudflare)                                | Cloudflare maintenance mode                                    | QA VM     | Required for QA                                                             |                                                                                                     |
| API traffic                | Available via gitlab.com (Cloudflare)                                | Cloudflare maintenance mode                                    | QA VM     | Required for QA. Also QA docker runners needs access to this                |                                                                                                     |
| Shared runners             | Available by connecting via internal LB, and gitlab.com (Cloudflare) | Cloudflare maintenance mode; gcloud FW rule managed by Ansible | None      | Not required for QA as QA sets up own Docker-based Runner on its VM         | If we require shared runners, then it is possible to only allow private shared runners              |
| Project, and group runners | Available by connecting via gitlab.com (Cloudflare)                  | Cloudflare maintenance mode                                    | None      | N/A                                                                         |                                                                                                     |
| KAS                        | Available via kas.gitlab.com                                         | gcloud Cloud Armor rule managed by Ansible                     | None      | Not required for QA                                                         |                                                                                                     |
| git-over-ssh               | Available via gitlab.com port 22, and altssh.gitlab.com              | gcloud FW rule managed by Ansible                              | None      | Not required for QA (2 git-over-ssh QA smoke tests will fail, but that's OK) | We have git-over-ssh, git-over-http QA tests. Also CI pipeline tests can be triggered by commit API |
| Container registry         | Available via registry.gitlab.com                                    | Cloudflare maintenance mode                                    | None      | Not required for QA                                                         |                                                                                                     |
| Pages                      | Available via *.gitlab.io, and various custom domains                | Unavailable because GitLab.com is down. There is a cache but it will expire in `gitlab_cache_expiry` minutes  | N/A       | N/A                                                                         |                                                                                                     |

# Database hosts

- [Staging replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gstg&var-prometheus=Global&var-prometheus_app=Global)
- [Production replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gprd&var-prometheus=Global&var-prometheus_app=Global)

# Accessing the rails and database consoles

## Staging

- rails: `ssh $USER-rails@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db replica: `ssh $USER-db@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db primary: `ssh $USER-db-primary@console-01-sv-gstg.c.gitlab-staging-1.internal`
- ci db replica: `ssh $USER-db-ci@console-01-sv-gstg.c.gitlab-staging-1.internal`
- ci db primary: `ssh $USER-db-ci-primary@console-01-sv-gstg.c.gitlab-staging-1.internal`

```sh
ssh postgres-03-db-gprd.c.gitlab-production.internal
sudo gitlab-psql
```

## Production

- rails: `ssh $USER-rails@console-01-sv-gprd.c.gitlab-production.internal`
- main db replica: `ssh $USER-db@console-01-sv-gprd.c.gitlab-production.internal`
- main db primary: `ssh $USER-db-primary@console-01-sv-gprd.c.gitlab-production.internal`
- ci db replica: `ssh $USER-db-ci@console-01-sv-gprd.c.gitlab-production.internal`
- ci db primary: `ssh $USER-db-ci-primary@console-01-sv-gprd.c.gitlab-production.internal`

```sh
ssh postgres-06-db-gstg.c.gitlab-staging-1.internal
sudo gitlab-psql
```

# Dashboards and debugging

These dashboards might be useful during the rollout:

## Staging

- [PostgreSQL replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gstg&var-prometheus=Global&var-prometheus_app=Global)
- [Triage overview](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s&var-env=gstg&var-environment=gstg&var-prometheus=prometheus-01-inf-gstg&var-prometheus_app=prometheus-app-01-inf-gstg&var-backend=All&var-type=All&var-stage=main)
- [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-24h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main)
- [Kibana Sidekiq jobs without `json.meta.root_caller_id: Cronjob`](https://nonprod-log.gitlab.net/goto/fabd2290-d882-11ec-b771-57a829f2c394)
- [Kibana Sidekiq jobs with `json.db_ci_count > 0`](https://nonprod-log.gitlab.net/goto/73d5f5e0-db3e-11ec-b771-57a829f2c394)
- [Sentry - includes application errors](https://sentry.gitlab.net/gitlab/staginggitlabcom/)
- [Logs (Kibana)](https://nonprod-log.gitlab.net/app/kibana)
- [Executed statements for `lock_writes.rake`](https://nonprod-log.gitlab.net/goto/f30e75d0-edfe-11ec-b771-57a829f2c394)

## Production

- [PostgreSQL replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1)
- [Triage overview](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s&var-env=gprd&var-environment=gprd&var-prometheus=prometheus-01-inf-gprd&var-prometheus_app=prometheus-app-01-inf-gprd&var-backend=All&var-type=All&var-stage=main)
- [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-24h&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main)
- [Kibana Sidekiq jobs without `json.meta.root_caller_id: Cronjob`](https://log.gprd.gitlab.net/goto/149eb250-db3f-11ec-aade-19e9974a7229)
- [Kibana Sidekiq jobs with `json.db_ci_count > 0`](https://log.gprd.gitlab.net/goto/3cacc160-db3f-11ec-a125-c377a8daf518)
- [Sentry - includes application errors](https://sentry.gitlab.net/gitlab/gitlabcom/)
  - Workhorse: <https://sentry.gitlab.net/gitlab/gitlab-workhorse-gitlabcom/>
  - Rails (backend): <https://sentry.gitlab.net/gitlab/gitlabcom/>
  - Rails (frontend): <https://sentry.gitlab.net/gitlab/gitlabcom-clientside/>
  - Gitaly (golang): <https://sentry.gitlab.net/gitlab/gitaly-production/>
  - Gitaly (ruby): <https://sentry.gitlab.net/gitlab/gitlabcom-gitaly-ruby/>
- [Logs (Kibana)](https://log.gprd.gitlab.net/app/kibana)
- [Executed statements for `lock_writes.rake`](https://log.gprd.gitlab.net/goto/74012c00-edff-11ec-8656-f5f2137823ba)

# Repos used during the rollout

The following Ansible playbooks are referenced throughout this issue:

- Stopping and starting components, maintenance page and traffic management: <https://gitlab.com/gitlab-com/gl-infra/ansible-migrations/-/tree/master/maintenance-mode>
- CI Promotion & Rollback: <https://gitlab.com/gitlab-com/gl-infra/db-migration/-/tree/master/ci-decomposition>

# T minus 3 days ({+ TODO: add date +})

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Ensure that the maintenance window is scheduled on status.io.
1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message: `In 3 days time, we will be undergoing some scheduled maintenance to our database layer so we expect the GitLab.com site to be unavailable for up to 2 hours starting from __MAINTENANCE_START_TIME__ UTC, 2022-07-02. We apologize in advance for any inconvenience this may cause.`
1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send the tweet link from `@gitlabstatus` to `#social_media_action` channel on Slack:
   - Message: `Hi team, please retweet and pin this from our status page on GitLab Twitter about the upcoming production change where GitLab.com will go down for up to 2 hours: {TWEET_LINK}`
   - [ ] `@gitlab` retweeted from `@gitlabstatus`
   - [ ] Tweet pinned on `@gitlab`
1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send on Slack `#whats-happening-at-gitlab`:
   - Message:
   ```
   :loudspeaker: *CI Decomposition scheduled for 2022-07-02 between 05:00 UTC and 9:00 UTC to split our database into Main and CI* :rocket:
   Taking place in 3 days time!

   :hammer_and_wrench: *What to expect?*
   GitLab.com will be unavailable for up to 2 hours starting from __MAINTENANCE_START_TIME__ UTC, 2022-07-02.

   Please read this [blog post](https://about.gitlab.com/blog/2022/06/02/splitting-database-into-main-and-ci/) for more information.
   A detailed timeline is available: [gitlab-org&7791](https://gitlab.com/groups/gitlab-org/-/epics/7791#proposed-timeline)
   You can read [daily updates](https://gitlab.com/groups/gitlab-org/-/epics/7791#last-async-update) as well.
   ```
1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Share message from `#whats-happening-at-gitlab` to the following channels:
   - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
   - [ ] `#g_delivery` (cc `@release-managers`)
   - [ ] `#community-relations` (Inform Marketing team)
   - [ ] `#support_gitlab-com` (Inform Support SaaS team)
      - [ ] Share with team Google Doc link to template for incoming tickets regarding the maintenance: https://docs.google.com/document/d/1zBXTpbQMbDtQHXiLX7VX3_-hLHRz0qTJNKLDXBseqtk/edit?usp=sharing
1. [ ] 👷 {+ Sharding Team Backend Engineer +}: [Create a C1 change request in the production repo](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/new?issuable_template=change_management) and link to the ops.gitlab.net issue created in the previous step. Example: <https://gitlab.com/gitlab-com/gl-infra/production/-/issues/4018>
    - [ ] Ensure the CR is reviewed by the 🚑 {+ EOC +}
1. [ ] 👷 {+ Sharding Team Backend Engineer +}: Ensure this issue has been created on <https://ops.gitlab.net/gitlab-com/gl-infra/db-migration>, since `gitlab.com` will be unavailable during the rollout!!!
1. [ ] 🐘 {+ Database-Wrangler +}: Create a merge request that may be needed in case of rollback and link it in the below rollback section

# T minus 2 days ({+ TODO: add date +})

1. [ ] 🔪 {+ Playbook-Runner +}: If the downtime window falls within the maintenance window of our GKE clusters, add maintenance exclusion windows for the duration of the downtime window (plus an hour on either side). For example: if downtime is scheduled to start at `2022-07-02 05:00 UTC`, add a maintenance window exclusion from `2022-07-02 04:00 UTC` until `2022-07-02 08:00 UTC`. This is to avoid any automated maintenance taking place on the GKE clusters while we're carrying out our work.

# T minus 1 day ({+ TODO: add date +})

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message: `Reminder: Tomorrow we will be undergoing scheduled maintenance to our database layer. We expect the GitLab.com site to be unavailable for up to 2 hours. Starting time: __MAINTENANCE_START_TIME__ UTC, 2022-07-02.`
1. [ ] PRODUCTION ONLY ☎ {+ Comms-Handler +}: Send message to `#social_media_action` to retweet from `@gitlabstatus` Twitter.
   - Message: `Hi team, please retweet this from our status page to GitLab Twitter about the scheduled maintenance that is taking place tomorrow: {TWEET_LINK}`
   - [ ] `@gitlab` retweeted from `@gitlabstatus`
1. [ ] __PRODUCTION ONLY__ 👷 {+ Sharding Team Backend Engineer +} Disable the `container_registry_migration_phase2_enabled` feature flag via chatops (`/chatops run feature set container_registry_migration_phase2_enabled false`).
    * [ ] Log this change in https://gitlab.com/gitlab-org/gitlab/-/issues/350543.
1. [ ] __PRODUCTION ONLY__ 👷 {+ Sharding Team Backend Engineer +} Remind the Fulfillment team about the downtime so they can make the necessary changes to customers.gitlab.com
   1. Ping in `#s_fulfillment` : `Team, as a reminder we are going ahead with downtime in GitLab.com tomorrow per <LINK>. Please consider this as a reminder for you to set maintenance mode in customers.gitlab.com as appropriate cc @Vitaly Slobodin`.
1. [ ] 👷 {+ Sharding Team Backend Engineer +}: Disable background migrations and inform `#g_database` team so they know to re-enable them after the process is all complete
   1. PRODUCTION:
      1. `/chatops run feature set execute_batched_migrations_on_schedule false`
      1. `/chatops run feature set execute_background_migrations false`
   1. STAGING:
      1. `/chatops run feature set execute_batched_migrations_on_schedule false --staging`
      1. `/chatops run feature set execute_background_migrations false --staging`
1. [ ] 🔪 {+ Playbook-Runner +}: Make sure that the console node is actually running the up to date Gitlab version
   1. From `sudo gitlab-rails console` run `Gitlab.revision`. It should match [STAGING](https://staging.gitlab.com/help) or [PRODUCTION](https://gitlab.com/help)

1. [ ] 🏆 {+ Quality +}: Prepare the QA machine:

    - [ ] Login

      ```sh
      ssh ci-decomposition-qa-01-sv-gstg.c.gitlab-staging-1.internal
      ```

      Even though it's in staging, the same VM is used for running tests against production.

    - [ ] Create `~/gitlab-qa-$ENV.env` on the QA VM by running the following locally on your own machine and pasting the output into the file:

      ```sh
      export QA_ENVS_API_KEY= # Key with owner access to ops.gitlab.net/gitlab-org/gitlab-qa/quality
      ENV=gprd # Set to either gstg or gprd
      cd db-migration # db-migration project
      cd ci-decomposition/bin
      ./get_qa_envs.rb $ENV
      ```

    - [ ] Confirm that our smoke tests are still passing by running [Trigger smoke tests](#trigger-smoke-tests)

# T minus 3 hours ({+ TODO: add date +})

1. [ ] ☎ {+ Comms-Handler +}: Create a broadcast message
    - Staging: <https://staging.gitlab.com/admin/broadcast_messages>
    - Production: <https://gitlab.com/admin/broadcast_messages>
    - Text: `We will soon be undergoing scheduled maintenance to our database layer. We expect GitLab.com to be unavailable for up to 2 hours starting from __MAINTENANCE_START_TIME__ UTC, 2022-07-02. Please note that any CI jobs that start before the maintenance window but complete during the window period will fail and may need to be started again.`
    - Start date: now
    - End date: now + 6 hours

1. [ ] PRODUCTION ONLY ☎ {+ Comms-Handler +}: Send on Slack `#whats-happening-at-gitlab`:
   - Message:

      ```text
      :loudspeaker: *CI Decomposition scheduled for 2022-07-02 between 05:00 UTC and 9:00 UTC to split our database into Main and CI* :rocket:
      This is taking place in 3 hours :hourglass_flowing_sand:

      :hammer_and_wrench: *What to expect?*
      GitLab.com will be unavailable for up to 2 hours starting from __MAINTENANCE_START_TIME__ UTC.

      You can follow our [issue](__ISSUE_LINK__)) on ops.gitlab.net.
      ```

1. [ ] PRODUCTION ONLY ☎ {+ Comms-Handler +}: Share message from `#whats-happening-at-gitlab` to the following channels:
   - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
   - [ ] `#g_delivery` (cc `@release-managers`)

1. [ ] 🔪 {+ Playbook-Runner +}: Get the console VM ready for action

    - [ ] SSH to the console VM (in `gprd` or `gstg`)
    - [ ] Start a tmux/screen session
    - [ ] Clone repos:

      ```sh
      rm -r src
      mkdir src
      cd src
      git clone git@gitlab.com:gitlab-com/gl-infra/ansible-migrations.git
      git clone git@gitlab.com:gitlab-com/gl-infra/db-migration.git
      ```

    - [ ] Ensure you have the pre-requisites installed:

      ```sh
      sudo apt install python3.8-venv
      python3 -m venv ~/venv
      source ~/venv/bin/activate
      pip install --upgrade pip
      cd ~/src/ansible-migrations/maintenance-mode
      pip install -r requirements.txt

      ENV=gprd # or gstg
      GCP_PROJECT=gitlab-production # or gitlab-staging-1
      ```

    - [ ] Ensure that Ansible can talk to hosts:

      ```sh
      cd ~/src/db-migration/ci-decomposition
      ansible -i inventory/${ENV}.yml all -m ping
      ```

      You shouldn't see any failed hosts!

    - [ ] Configure `gcloud`:

      ```sh
      gcloud auth login
      gcloud config set project ${GCP_PROJECT}
      gcloud container clusters list
      gcloud container clusters get-credentials ${ENV}-us-east1-b --region=us-east1-b
      gcloud container clusters get-credentials ${ENV}-us-east1-c --region=us-east1-c
      gcloud container clusters get-credentials ${ENV}-us-east1-d --region=us-east1-d
      gcloud container clusters get-credentials ${ENV}-gitlab-gke --region=us-east1

    - [ ] Test `kubectl` works:

      ```sh
      BYPASS_SAFETY_PROMPT="true" kubectl --context gke_${GCP_PROJECT}_us-east1_${ENV}-gitlab-gke -n gitlab get deployments
      ```

    - [ ] Create `~/maintenance-mode.env` with the following content

      ```sh
      export CLOUDFLARE_API_TOKEN=X # in 1Password
      export CLOUDFLARE_ZONE=gitlab.com # or staging.gitlab.com
      export ENVIRONMENT=gprd # or gstg
      export GCP_PROJECT=gitlab-production # or gitlab-staging-1
      ```

1. [ ] 🔪 {+ Playbook-Runner +}: On the console VM, do the following tasks to add an annotation to each node that disables scale down:

    - [ ] Add annotation:

      ```sh
      for c in $(BYPASS_SAFETY_PROMPT=true kubectl config get-contexts -o=name); do
        BYPASS_SAFETY_PROMPT=true kubectl --context $c annotate node --all cluster-autoscaler.kubernetes.io/scale-down-disabled=true
      done
      ```

    - [ ] Confirm annotation was added:

      ```sh
      for c in $(BYPASS_SAFETY_PROMPT=true kubectl config get-contexts -o=name); do
        BYPASS_SAFETY_PROMPT=true kubectl --context $c get nodes -o=jsonpath='{.items[?(@.metadata.annotations.cluster-autoscaler\.kubernetes\.io\/scale-down-disabled=="true")].metadata.name}' | tr ' ' '\n' | sort
      done
      ```

# T minus 1 hour ({+ TODO: add date +})

1. [ ] 🔪 {+ Playbook-Runner +}: Add the following silences at <https://alerts.gitlab.net>:
    - Start time: NOW + 1H
    - End time: NOW + 3H
    - Matchers:
      - __STAGING__
        - [ ] `env="gstg"`
        - [ ] `instance=~"https://staging.gitlab.com.*"`
      - __PRODUCTION__
        - [ ] `env="gprd"`
        - [ ] `instance=~"https://gitlab.com.*"`

1. [ ] 🔪 {+ Playbook-Runner +}: Run the following playbook to disable crons on the leader and standby leader:

    ```sh
    cd ~/src/db-migration/ci-decomposition
    export WRITE_MODE=true
    export CHEF_DISABLE_COMMENT="CI Decomposition Rollout - https://ops.gitlab.net/gitlab-com/gl-infra/db-migration/-/issues/31"
    ENV=gprd # gstg or gprd
    ansible-playbook -e @variables.yml -i inventory/$ENV.yml -t disable-db-crons playbooks/upgrade.yml
    ```

1. [ ] 🔪 {+ Playbook-Runner +}: Execute Ansible playbooks on the console VM to save current replica counts and HPA min/max limits:

   ```sh
   . ~/maintenance-mode.env

   cd ~/src/ansible-migrations/maintenance-mode

   K8S_BACKUP_DIR="$HOME/k8s-deployments-snapshot-$(date +%Y%m%d)" ansible-playbook -e @variables.yml enable.yml -t save-config
   K8S_BACKUP_DIR="$HOME/k8s-deployments-snapshot-$(date +%Y%m%d)" ansible-playbook -e @variables.yml disable.yml -t save-config
   ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message: `GitLab.com will undergo maintenance in 1 hour. Please note that any CI jobs that start before the maintenance window but complete during the window period (up to 2 hours) will fail and may need to be started again.`

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post to `#announcements` on Slack:
    - Message: `Maintenance on database layer starting in 1 hour. We expect the site to be unavailable for up to 2 hours. Starting at __MAINTENANCE_START_TIME__ UTC.`

1. [ ] __PRODUCTION ONLY__ ☁ 🔪 {+ Playbook-Runner +}: Create a maintenance window in PagerDuty for the [GitLab Production](https://gitlab.pagerduty.com/service-directory/PATDFCE), [SLO alerts gprd cny stage](https://gitlab.pagerduty.com/service-directory/P3DG414), [SLO Alerts gprd main stage](https://gitlab.pagerduty.com/service-directory/P7Q44DU) and [Dean Man's snitch](https://gitlab.pagerduty.com/service-directory/PFQXAWL) services. You can create a single maintenance window with the previous services selected.

1. [ ] 🐘 {+ Database-Wrangler +}: Check that all chef MRs are rebased and contain the proper changes.

## CI Decomposition Call

These steps will be run in a Zoom call. The 🐺 {+ Coordinator +} runs the call,
asking other roles to perform each step on the checklist at the appropriate
time.

Changes are made one at a time, and verified before moving onto the next step.
All the steps will be executed from a console VM and we should keep the session
shared (tmux, screen...).

Whoever is performing a change should share their screen and explain their
actions as they work through them. Everyone else should watch closely for
mistakes or errors! A few things to keep an especially sharp eye out for:

- Exposed credentials (except short-lived items like 2FA codes)
- Running commands against the wrong hosts
- Navigating to the wrong pages in web browsers (gstg vs. gprd, etc)

Remember that the call will be recorded. We will consider making it public after we confirmed that no [SAFE data](https://about.gitlab.com/handbook/legal/safe-framework/) was leaked.
If you see something happening that shouldn't be public, mention it.

### Roll call

- [ ] 🐺 {+ Coordinator +}: Mark the change request as `/label ~"change::in-progress"`
- [ ] 🐺 {+ Coordinator +}: Ensure everyone mentioned above is on the call
- [ ] 🐺 {+ Coordinator +}: Ensure the Zoom room host is on the call

### Notify Users of Maintenance Window

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - [ ] Click "Start Maintenance" and send the following:
       - Message: `GitLab.com is now shutting down for planned maintenance for database layer improvements for the next 2 hours. See you on the other side!`

### Health check

1. [ ] 🐺 {+ Coordinator +}: Ensure that there are no active critical alerts or open incidents:
    - Staging
        - <https://dashboards.gitlab.net/d/SOn6MeNmk/alerts?orgId=1&var-interval=1m&var-environment=gstg&var-alertname=All&var-alertstate=All&var-prometheus=prometheus-01-inf-gstg&var-prometheus_app=prometheus-app-01-inf-gstg>
    - Production
        - <https://dashboards.gitlab.net/d/SOn6MeNmk/alerts?orgId=1&var-interval=1m&var-environment=gprd&var-alertname=All&var-alertstate=All&var-prometheus=prometheus-01-inf-gprd&var-prometheus_app=prometheus-app-01-inf-gprd>
        - <https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=incident>

1. [ ] 🔪 {+ Playbook-Runner +}: Verify that the ansible inventory is up to date and reflects the real state from the cluster.

1. [ ] 👷 {+ Backend Engineer +} Check prometheus sanity check metrics for reads all going to the correct hosts
   1. [ ]  PRODUCTION [Index reads for CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-v12-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => expected base load of 0.13/s for ci_pipelines, ci_build_needs, ci_pipeline_variables only due to prometheus exporter
   1. [ ] PRODUCTION [Sequential scans of CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-v12-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => should be 0
   1. [ ] STAGING [Index reads for CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => expected base load of 0.13/s for ci_pipelines, ci_build_needs, ci_pipeline_variables only due to prometheus exporter
   1. [ ] STAGING [Sequential scans of CI tables](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))%20and%20on(instance)%20pg_replication_is_replica%3D%3D1&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => should be 0

# T minus zero (CI Decomposition day: {+ YYYY-MM-DD HH:mm UTC +})

We expect the maintenance window to last for up to 2 hours, starting from now.

## CI decomposition Phase 7 rollout

### Bring it all down (T plus 0 min)

Playbook source: <https://gitlab.com/gitlab-com/gl-infra/ansible-migrations/-/tree/master/maintenance-mode>

- [ ] 🔪 {+ Playbook-Runner +}: Execute Ansible playbook to set a maintenance page on Cloudflare and add GCP FW block rules:

   ```sh
   . ~/maintenance-mode.env

   cd ~/src/ansible-migrations/maintenance-mode

   # [Step 1] Put up maintenance page on Cloudflare + add GCP FW ingress blocking rules:
   ansible-playbook -e @variables.yml enable.yml -t step-1 -e "allowed_ips=34.73.152.174"  # QA VM
   ```

- [ ] 🔪 {+ Playbook-Runner +}: Validation:
  - __STAGING__
    - [ ] {+ BLOCKED +}: `ssh git@staging.gitlab.com`
    - [ ] {+ REDIRECT +}: In an incognito window, <https://staging.gitlab.com> should redirect to <https://about.staging.gitlab.com>
    - [ ] {+ 503 +}: In a window where you're logged in, <https://staging.gitlab.com> should redirect to a `503` maintenance page.
    - [ ] {+ 503 +}: <https://staging.gitlab.com/gsgl> should redirect to a `503` maintenance page.
    - [ ] {+ 200 +}: Run `curl -o /dev/null -s https://staging.gitlab.com/gsgl -w '%{http_code}\n'` from the QA machine (`ci-decomposition-qa-01-sv-gstg.c.gitlab-staging-1.internal`).
    - [ ] {+ 502 +}: Run `curl -o /dev/null -s --http1.1 https://kas.staging.gitlab.com/ -w '%{http_code}\n'` from your machine
  - __PRODUCTION__
    - [ ] {+ BLOCKED +}: `ssh git@gitlab.com`
    - [ ] {+ REDIRECT +}: In an incognito window, <https://gitlab.com> should redirect to <https://about.gitlab.com>
    - [ ] {+ 503 +}: In a window where you're logged in, <https://gitlab.com> should redirect to a `503` maintenance page.
    - [ ] {+ 503 +}: <https://gitlab.com/gsgl> should redirect to a `503` maintenance page.
    - [ ] {+ 200 +}: Run `curl -o /dev/null -s https://gitlab.com/gsgl -w '%{http_code}\n'` from the QA machine (`ci-decomposition-qa-01-sv-gstg.c.gitlab-staging-1.internal`).
    - [ ] {+ 502 +}: Run `curl -o /dev/null -s --http1.1 https://kas.gitlab.com/ -w '%{http_code}\n'` from your machine

- [ ] 🔪 {+ Playbook-Runner +}: Execute Ansible playbook to stop services:

   ```sh
   # [Step 2] Stop zonal services (except sidekiq)
   # [Step 3] Stop regional/canary services
   ansible-playbook -e @variables.yml enable.yml -t step-2,step-3
   ```

- [ ] 👷 {+ Sharding Team Backend Engineer+}: Observe sidekiq execution rates and queue lengths to ensure they are going down:

  - [ ] STAGING: [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-1h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main)
  - [ ] STAGING: [Kibana Sidekiq jobs without `json.meta.root_caller_id: Cronjob`](https://nonprod-log.gitlab.net/goto/fabd2290-d882-11ec-b771-57a829f2c394)
  - [ ] PRODUCTION: [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-1h&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main)
  - [ ] PRODUCTION: [Kibana Sidekiq jobs without `json.meta.root_caller_id: Cronjob`](https://log.gprd.gitlab.net/goto/149eb250-db3f-11ec-aade-19e9974a7229)

- [ ] Wait 5 minutes to allow sidekiq queues to go down before continuing.

- [ ] 🔪 {+ Playbook-Runner +}: Once we're ready to shutdown sidekiq, run the final step:

   ```sh
   # [Step 4] Stop sidekiq:
   ansible-playbook -e @variables.yml enable.yml -t step-4
   ```

- [ ] __STAGING ONLY__ 🔪 {+ Playbook-Runner +}: Pause the following snitches:
  - [ ] <https://deadmanssnitch.com/snitches/4d658f8add/pause>
  - [ ] <https://deadmanssnitch.com/snitches/404e513abf/pause>
  - [ ] <https://deadmanssnitch.com/snitches/ad599f4910/pause>

  This is done here __after__ shutting down all services to hopefully avoid snitches getting inadvertently unpaused and alerting the EOC.

- [ ] 🐘 {+ Database-Wrangler +}: Monitor what pgbouncer pool has connections:

  - STAGING: [Thanos](https://thanos.gitlab.net/graph?g0.expr=pgbouncer_pools_client_active_connections%7Benv%3D%22gstg%22%2C%20user%3D%22gitlab%22%7D&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
  - PRODUCTION: [Thanos](https://thanos.gitlab.net/graph?g0.expr=pgbouncer_pools_client_active_connections%7Benv%3D%22gprd%22%2C%20user%3D%22gitlab%22%7D&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

- [ ] 🐘 {+ Database-Wrangler +}: Monitor the Writer and Standby Leader PostgreSQL Log Files using:

   ```sh
   sudo tail -f /var/log/gitlab/postgresql/postgresql.csv
   ```

- [ ] 🐘 {+ Database-Wrangler +}: Execute a checkpoint on the primary database:

   ```sh
   sudo gitlab-psql -c "checkpoint;"
   ```

### CI DB promotion (T plus ~20 min)

Playbook source: <https://gitlab.com/gitlab-com/gl-infra/db-migration/-/tree/master/ci-decomposition>

- [ ] 🔪 {+ Playbook-Runner +}: Run Ansible playbook and stop just before promoting:

    ```sh
    cd ~/src/db-migration/ci-decomposition
    export WRITE_MODE=true
    export CHEF_DISABLE_COMMENT="CI Decomposition Rollout - https://ops.gitlab.net/gitlab-com/gl-infra/db-migration/-/issues/31"
    ENV=gprd # gstg or gprd
    ansible-playbook -e @variables.yml -i inventory/$ENV.yml -t do-until-promote playbooks/upgrade.yml
    ```

- [ ] 🐘 {+ Database-Wrangler +}: Ensure no writes are happening:

  - STAGING: [Thanos](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22gstg%22%2C%20type%3D%22patroni%22%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22gstg%22%2C%20type%3D%22patroni%22%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22gstg%22%2C%20type%3D%22patroni%22%7D%5B1m%5D))%20by%20(instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
  - PRODUCTION: [Thanos](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22gprd%22%2C%20type%3D%22patroni%22%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22gprd%22%2C%20type%3D%22patroni%22%7D%5B1m%5D))%20by%20(instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22gprd%22%2C%20type%3D%22patroni%22%7D%5B1m%5D))%20by%20(instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

- [ ] 🔪 {+ Playbook-Runner +}: Continue with the remaining steps to promote the CI DB:

    ```sh
    ansible-playbook -e @variables.yml -i inventory/$ENV.yml -t promote-onwards playbooks/upgrade.yml
    ```

### Bring services back up (T plus ~35 min)

- [ ] 🔪 {+ Playbook-Runner +}: Execute Ansible playbook to start all services:

  ```sh
  . ~/maintenance-mode.env

  cd ~/src/ansible-migrations/maintenance-mode
  ansible-playbook -e @variables.yml -t start-services disable.yml
  ```

### During-Blackout QA (T plus ~1 hour)

#### Trigger smoke tests

1. [ ] 🏆 {+ Quality +}: Run automated tests against the new decomposed application from `ci-decomposition-qa-01-sv-gstg.c.gitlab-staging-1.internal`:

    Most of the variables below can be retrieved from 1Password or from the CI/CD Variables page on ops.gitlab.net:

    - [Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/settings/ci_cd)
    - [Production](https://ops.gitlab.net/gitlab-org/quality/production/-/settings/ci_cd)

    ```sh
    # Terminal 1
    ENV=gprd                              # Set to either gstg or gprd
    TEST_INSTANCE_TYPE=Production         # Staging or Production

    . ~/gitlab-qa-$ENV.env

    sudo -E gitlab-qa Test::Instance::${TEST_INSTANCE_TYPE} -- qa/specs/features/browser_ui/4_verify/testing/endpoint_coverage_spec.rb
    ```

    ```sh
    # Terminal 2
    ENV=gprd                              # Set to either gstg or gprd
    TEST_INSTANCE_TYPE=Production         # Staging or Production

    . ~/gitlab-qa-$ENV.env

    QA_KNAPSACK_REPORT_NAME=cidecomposition CI_NODE_TOTAL=2 CI_NODE_INDEX=0 sudo -E gitlab-qa Test::Instance::${TEST_INSTANCE_TYPE} -- --tag smoke -f d -f json
    ```

    ```sh
    # Terminal 3
    ENV=gprd                              # Set to either gstg or gprd
    TEST_INSTANCE_TYPE=Production         # Staging or Production

    . ~/gitlab-qa-$ENV.env

    QA_KNAPSACK_REPORT_NAME=cidecomposition CI_NODE_TOTAL=2 CI_NODE_INDEX=1 sudo -E gitlab-qa Test::Instance::${TEST_INSTANCE_TYPE} -- --tag smoke -f d -f json
    ```

- In the event, you need to use the browser to debug the site during the Blackout QA period, you can use `ssh -D` to configure SOCKS proxy.

  - Run:

      ```sh
      ssh -vv -ND 4444 ci-decomposition-qa-01-sv-gstg.c.gitlab-staging-1.internal
      ```

  - Then configure your browser settings to use SOCKS Hosts as `127.0.0.1` and
    port as `4444`. This should allow you access to connect to the site via
    the QA VM.

#### Evaluation of QA results - __Decision Point__

NOTE: We can ignore failures in QA test that relate to pushing over SSH as this is blocked.

#### Metrics sanity check from QA results

1. [ ] 👷 {+ Sharding Team Backend Engineer +} Check prometheus sanity check metrics for reads all going to the correct hosts
   1. [ ] PRODUCTION [Index reads for CI tables on Main](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-v12-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => expected to drop to 0 (or base load of 0.13/s for ci_pipelines, ci_build_needs, ci_pipeline_variables only due to prometheus exporter) once we performed the failover (see <https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15708>)
   1. [ ] PRODUCTION [Sequential scans of CI tables on Main](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-v12-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => should have dropped to 0 once we performed the failover
   1. [ ] PRODUCTION [Index reads for CI tables on CI](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => expected to have started to get some queries once we performed the failover
   1. [ ] PRODUCTION [Sequential scans of CI tables on CI](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => expected to have started to get some queries once we performed the failover

   1. [ ] STAGING [Index reads for CI tables on Main](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => expected base load of 0.13/s for ci_pipelines, ci_build_needs, ci_pipeline_variables only due to prometheus exporter
   1. [ ] STAGING [Sequential scans of CI tables on Main](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => should have dropped to 0 once we performed the failover
   1. [ ] STAGING [Index reads for CI tables on CI](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_idx_tup_fetch%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => expected to have started to get some queries once we performed the failover
   1. [ ] STAGING [Sequential scans of CI tables on CI](https://thanos-query.ops.gitlab.net/graph?g0.expr=(sum(rate(pg_stat_user_tables_seq_tup_read%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cinstance%3D~%22patroni-ci-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance))&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => expected to have started to get some queries once we performed the failover

1. [ ] 👷 {+ Sharding Team Backend Engineer +} Check prometheus sanity check metrics for writes all going to the correct hosts
   1. [ ] PRODUCTION [Writes to CI tables outside of CI databases](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cfqdn!~%22.*ci.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cfqdn!~%22.*ci.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cfqdn!~%22.*ci.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => should have dropped to 0 once we performed the failover
   1. [ ] PRODUCTION [Writes to CI tables in CI](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cfqdn%3D~%22.*ci.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cfqdn%3D~%22.*ci.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22gprd%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cfqdn%3D~%22.*ci.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => expected to have started to get some queries once we performed the failover

   1. [ ] STAGING [Writes to CI tables outside of CI databases](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cfqdn%3D~%22patroni-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cfqdn%3D~%22patroni-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cfqdn%3D~%22patroni-0.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => should have dropped to 0 once we performed the failover
   1. [ ] STAGING [Writes to CI tables outside of CI databases](https://thanos-query.ops.gitlab.net/graph?g0.expr=sum(increase(pg_stat_user_tables_n_tup_ins%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cfqdn%3D~%22patroni-ci-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_del%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cfqdn%3D~%22patroni-ci-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance)%0A%2B%0Asum(increase(pg_stat_user_tables_n_tup_upd%7Benv%3D%22gstg%22%2C%20relname%3D~%22(ci_.*%7Cexternal_pull_requests%7Ctaggings%7Ctags)%22%2Cfqdn%3D~%22patroni-ci-.*%22%7D%5B1m%5D))%20by%20(relname%2C%20instance)%0Aand%20on(instance)%20pg_replication_is_replica%3D%3D0&g0.tab=0&g0.stacked=0&g0.range_input=1hd&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
      - => expected to have started to get some queries once we performed the failover

1. [ ] 👷 {+ Sharding Team Backend Engineer +} Check Sentry if there are errors that might indicate database problems
   1. [ ] PRODUCTION [Sentry](https://sentry.gitlab.net/gitlab/gitlabcom/)
   1. [ ] STAGING [Sentry](https://sentry.gitlab.net/gitlab/staginggitlabcom/)

#### Commitment

If QA has succeeded, then we can continue to "Complete the Upgrade". If some
QA has failed, the 🐺 {+ Coordinator +} must decide whether to continue with the
upgrade, or to [rollback](#rollback-if-required). A decision to continue
in these circumstances should be counter-signed by the 🎩 {+ Head Honcho +}.

The following are commitment criteria:

1. We expect two QA tests to fail related to SSH access (the SSH access is blocked).
1. If no errors were observed and we are beyond `T + 70 mins`: we continue moving forward without a rollback as the happy path is the fastest to execute.
1. If we observed errors and we are beyond `T + 70 mins`: we rollback as we anticipate the rollback to take at least 30 minutes
1. If we observed errors and we are before `T + 70 mins`: we investigate the failure for up to 10 minutes, and re-run QA.
1. If the re-run of QA failed: we rollback.

Goals:

1. The top priority is to maintain data integrity. Rolling back after the maintenance
   window has ended is very difficult, and will result in any changes made in the
   interim being lost.
1. Failures with an unknown cause should be investigated further. If we can't
   determine the root cause within the maintenance window, we should rollback.

### Complete the Upgrade (T plus ~2 hours)

1. [ ] 🔪 {+ Playbook-Runner +}: Execute Ansible playbook to remove maintenance page on Cloudflare and remove the temporary GCP FW block rules:

  ```sh
  . ~/maintenance-mode.env

  cd ~/src/ansible-migrations/maintenance-mode
  ansible-playbook -e @variables.yml -t open-access disable.yml
  ```

#### Communicate

1. [ ] 🐺 {+ Coordinator +}: Remove the broadcast message (if it's after the initial window, it has probably expired automatically)
1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message: `GitLab.com planned maintenance for the database layer is almost complete. The site is back up and running although we're continuing to verify that all systems are functioning correctly. Thank you for your patience.`
1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - [ ] Click "Finish Maintenance" and send the following:
       - Message: `GitLab.com's database layer maintenance upgrade is complete now, and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.`
1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: In the same thread from the earlier post, post the following message and click on the checkbox "Also send to X channel" so the threaded message would be published to the channel:
   - Message:
   ```
   :done: *GitLab.com database layer maintenance upgrade is complete now.* :celebrate:
   We’ll continue to monitor the platform to ensure all systems are functioning correctly.
   ```
      - [ ] `#whats-happening-at-gitlab`
      - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
      - [ ] `#g_delivery` (cc `@release-managers`)
1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send a message to `#social_media_action` to unpin maintenance tweet on `@gitlab` Twitter:
   - Message: `Hi team :wave:, the maintenance upgrade is complete now, you may unpin the maintenance tweet on GitLab Twitter.`

#### Verification

1. __Start After-Blackout QA__ This is the second half of the test plan.
    1. [ ] 🏆 {+ Quality +}: Trigger Smoke and Full E2E suite against the environment that was upgraded:
        1. [ ] [Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules): `Four hourly smoke tests`, and `Daily Full QA suite`
        1. [ ] [Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules): `Four hourly smoke tests`, and `Twice daily full run`
    1. [ ] 👷 {+ Sharding Team Backend Engineer +}: Go to [www-gitlab-com pipelines](https://gitlab.com/gitlab-com/www-gitlab-com/-/pipelines) and `Run pipeline` on `master`. Wait for the pipeline to succeed.

#### Tidying up

1. [ ] 🔪 {+ Playbook-Runner +}: Merge Chef CI and K8s changes for pgbouncers (point at new host) + patroni-ci (standby_cluster=null) + `database_tasks: true` + deploy node point at new host
   1. [ ] PRODUCTION: Chef: <https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/1931>
   1. [ ] PRODUCTION: K8s: <https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/1828>
   1. [ ] STAGING: Chef: <https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/1930>
   1. [ ] STAGING: K8s: <https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/1827>

1. [ ] 🔪 {+ Playbook-Runner +}: Ensure 100% that the changes merged from the previous step have been deployed to the Chef server before re-enabling Chef by confirming the linked `master` pipeline for ops.gitlab.net completed successfully.

1. [ ] 🔪 {+ Playbook-Runner +}: Re-enable Chef in all patroni + pgbouncers instances:

    ```sh
    cd ~/src/db-migration/ci-decomposition
    ansible-playbook -e @variables.yml -i inventory/$ENV.yml -t reenable-chef playbooks/common.yml
    ```

1. [ ] __STAGING ONLY__ 🔪 {+ Playbook-Runner +}: Unpause the following snitches:
    - [ ] <https://deadmanssnitch.com/snitches/4d658f8add/pause>
    - [ ] <https://deadmanssnitch.com/snitches/404e513abf/pause>
    - [ ] <https://deadmanssnitch.com/snitches/ad599f4910/pause>

1. [ ] __PRODUCTION ONLY__ 🔪 {+ Playbook-Runner +}: Remove scheduled maintenance for the _Dead Man's Snitch_ service in PagerDuty: <https://gitlab.pagerduty.com/service-directory/PFQXAWL>

1. [ ] 🔪 {+ Playbook-Runner +}: Remove silences added during this process from <https://alerts.gitlab.net>

1. [ ] 🐘 {+ Database-Wrangler +}:
    - Check that DR replicas have followed the timeline switch and do not lag behind (there's also an alert for this)
      - Log into the "master node" for DR replicas, and run `select * from pg_stat_replication`;
      - Check DR replicas DCS config `standby_cluster` to figure which host is the master node;

1. [ ] 🐘 {+ Database-Wrangler +}:
    - Initiate a full backup of `patroni-ci`
      - Run a [Wal-G backup](https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/patroni/postgresql-backups-wale-walg.md#daily-basebackup)
      - Run a manual GCS Snapshot (https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/patroni-ci/rebuild_ci_cluster_from_prod.md#take-a-snapshot-from-the-writer-node)

1. [ ] 🐺 {+ Coordinator +}: Check for any concerning alerts:
    - Staging: <https://alerts.gitlab.net> or #alerts-nonprod in Slack
    - Production: <https://alerts.gitlab.net> or #alerts in Slack

1. [ ] 🐺 {+ Coordinator +}: Mark the change request as `/label ~"change::complete"`

1. [ ] 🔪 {+ Playbook-Runner +}: `rm ~/maintenance-mode.env` on console machine
1. [ ] 🔪 {+ Playbook-Runner +}: `rm ~/gitlab-qa-$ENV.env` on QA VM

1. [ ] 🏆 {+ Quality +}: (1 hour after __Start After-Blackout QA__) Check that the Smoke, and Full E2E suite has passed.

1. [ ] 🏆 {+ Quality +}: [Trigger smoke tests](#trigger-smoke-tests) one more time now that Chef will have had time to run

1. [ ] 🔪 {+ Playbook-Runner +}: On the console VM, do the following tasks to remove the annotation on each node that disabled scale down:

    - [ ] Remove annotation:

      ```sh
      for c in $(BYPASS_SAFETY_PROMPT=true kubectl config get-contexts -o=name); do
        BYPASS_SAFETY_PROMPT=true kubectl --context $c annotate node --all cluster-autoscaler.kubernetes.io/scale-down-disabled-
      done
      ```

    - [ ] Confirm no nodes have the annotation set:

      ```sh
      for c in $(BYPASS_SAFETY_PROMPT=true kubectl config get-contexts -o=name); do
        BYPASS_SAFETY_PROMPT=true kubectl --context $c get nodes -o=jsonpath='{.items[?(@.metadata.annotations.cluster-autoscaler\.kubernetes\.io\/scale-down-disabled=="true")].metadata.name}'
      done
      ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Notify our customer over [Slack channel](https://gitlab.slack.com/archives/CDRLZUVHB) that decomposition finished, and request to validate GitLab.com.

1. [ ] __PRODUCTION ONLY__ 👷 {+ Sharding Team Backend Engineer +}: Confirm that [#database-lab](https://gitlab.slack.com/archives/CLJMDRD8C) works properly for CI database.

1. [ ] __PRODUCTION ONLY__ 👷 {+ Sharding Team Backend Engineer +}: Confirm that [Teleport access](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15783) works properly for CI database.

    ```bash
    # Test `console-ro`
    tsh login --proxy=teleport.gprd.gitlab.net --request-roles=database --request-reason="Testing newly created CI DB teleport on production https://ops.gitlab.net/gitlab-com/gl-infra/db-migration/-/issues/31"
    tsh db connect --db-user=console-ro --db-name=gitlabhq_production db-secondary-ci
    psql> select max(id) from ci_builds limit 1;
    ```

    ```bash
    # Test `console-rw`
    tsh db connect --db-user=console-rw --db-name=gitlabhq_production db-primary-ci
    psql> update ci_builds set id = 43299641 where id = 43299641;
    ```

## __ROLLBACK__ (if required)

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post an update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message: `Due to an issue during the maintenance, we have initiated a rollback of the changes. We will send another update within the next 30 minutes.`

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Ask the IMOC or the Head Honcho if this message should be sent to any slack rooms:
   - `#whats-happening-at-gitlab`
   - `#infrastructure-lounge` (cc `@sre-oncall`)
   - `#g_delivery` (cc `@release-managers`)
   - `#community-relations`

### Rollback Strategies

#### Rollback to main

Goal: __CI writes _and_ reads to the main cluster__

1. [ ] 🔪 {+ Playbook-Runner +}: Shift CI write traffic to main:

    If pgbouncers have already been updated to point to `patroni-ci`, then you need to run the following playbook:

    ```sh
    cd ~/src/db-migration/ci-decomposition
    ansible-playbook -e @variables.yml -i inventory/$ENV.yml -t write-rollback playbooks/rollback.yml
    ```

    You'll need to enter a number to increase the sequences by (check with _Database Wrangler_).

1. [ ] 🔪 {+ Playbook-Runner +}: Shift CI read traffic to main:

    ```sh
    cd ~/src/db-migration/ci-decomposition
    ansible-playbook -e @variables.yml -i inventory/$ENV.yml -t read-rollback-to-main playbooks/rollback.yml
    ```

#### Rollback to CI1

Goal: __CI writes to main and CI reads to the CI1 cluster__

1. [ ] 🔪 {+ Playbook-Runner +}: Shift CI write traffic to main:

    If pgbouncers have already been updated to point to `patroni-ci`, then you need to run the following playbook:

    ```sh
    cd ~/src/db-migration/ci-decomposition
    ansible-playbook -e @variables.yml -i inventory/$ENV.yml -t write-rollback playbooks/rollback.yml
    ```

    You'll need to enter a number to increase the sequences by (check with _Database Wrangler_).

1. [ ] 🔪 {+ Playbook-Runner +}: Shift CI read traffic to CI1:

    ```sh
    cd ~/src/db-migration/ci-decomposition
    ansible-playbook -e @variables.yml -i inventory/$ENV.yml -t read-rollback-to-ci1 playbooks/rollback.yml
    ```

### Complete the rollback

1. [ ] 🔪 {+ Playbook-Runner +}: Restart services:

    ```sh
    . ~/maintenance-mode.env
    cd ~/src/ansible-migrations/maintenance-mode

    # Shutdown all services (including sidekiq)
    ansible-playbook -e @variables.yml enable.yml -t k8s

    # Start services
    ansible-playbook -e @variables.yml disable.yml -t start-services
    ```

1. [ ] 🏆 {+ Quality +} Confirm that our smoke tests are still passing by running [Trigger smoke tests](#trigger-smoke-tests)

1. [ ] 🔪 {+ Playbook-Runner +}: Open up access:

    ```sh
    ansible-playbook -e @variables.yml -t open-access disable.yml
    ```

1. [ ] 🐘 {+ Database-Wrangler +}: Merge the merge request to make the rollback permanent. {+ TODO must be created in T-3 days +} Should be the same as <https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/1964> for the relevant environment and rollback cluster

1. [ ] 🔪 {+ Playbook-Runner +}: Ensure 100% that the changes merged from the previous step have been deployed to the Chef server before re-enabling Chef by confirming the linked `master` pipeline for ops.gitlab.net completed successfully.

1. [ ] 🔪 {+ Playbook-Runner +}: Re-enable Chef in all patroni + pgbouncers instances:

    ```sh
    cd ~/src/db-migration/ci-decomposition
    ansible-playbook -e @variables.yml -i inventory/$ENV.yml -t reenable-chef playbooks/common.yml
    ```

1. [ ] Perform [Verification](#verification) above.

1. [ ] 🏆 {+ Quality +}: (1 hour after __Start After-Blackout QA__) Check that the Smoke, and Full E2E suite has passed.

1. [ ] 🔪 {+ Playbook-Runner +}: Wait an hour for Chef to run on the Patroni nodes

1. [ ] 🔪 {+ Playbook-Runner +}: Remove manually created rollback files that are redundant now that Chef has run

    ```sh
    cd ~/src/db-migration/ci-decomposition
    ansible-playbook -e @variables.yml -i inventory/$ENV.yml -t cleanup-post-chef playbooks/rollback.yml
    ```

1. [ ] 🔪 {+ Playbook-Runner +}: On the console VM, do the following tasks to remove the annotation on each node that disabled scale down:

    - [ ] Remove annotation:

      ```sh
      for c in $(BYPASS_SAFETY_PROMPT=true kubectl config get-contexts -o=name); do
        BYPASS_SAFETY_PROMPT=true kubectl --context $c annotate node --all cluster-autoscaler.kubernetes.io/scale-down-disabled-
      done
      ```

    - [ ] Confirm no nodes have the annotation set:

      ```sh
      for c in $(BYPASS_SAFETY_PROMPT=true kubectl config get-contexts -o=name); do
        BYPASS_SAFETY_PROMPT=true kubectl --context $c get nodes -o=jsonpath='{.items[?(@.metadata.annotations.cluster-autoscaler\.kubernetes\.io\/scale-down-disabled=="true")].metadata.name}'
      done
      ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message: `GitLab.com rollback for the database layer is almost complete. The site is back up and running although we're continuing to verify that all systems are functioning correctly. Thank you for your patience.`

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - [ ] Click "Finish Maintenance" and send the following:
       - Message: `GitLab.com's database rollback is complete now, and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.`

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send the following message should be sent to slack rooms:

   ```text
   GitLab.com's database rollback is complete now, and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.
   ```

   - [ ] `#whats-happening-at-gitlab`
   - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
   - [ ] `#g_delivery` (cc `@release-managers`)

### Wrap Up

1. [ ] 🔪 {+ Playbook-Runner +}: `rm ~/maintenance-mode.env` on console machine
1. [ ] 🔪 {+ Playbook-Runner +}: `rm ~/gitlab-qa-$ENV.env` on QA VM

## Extra details

### In case the Playbook-Runner is disconnected

As most of the steps are executed in a tmux session owned by the
Playbook-Runner role we need a safety net in case this person loses their
internet connection or otherwise drops off half way through. Since other
SREs/DBREs also have root access on the console node where everything is
running they should be able to recover it in different ways. We tested the
following approach to recovering the tmux session, updating the ssh agent and
taking over as a new ansible user.

- `ssh host`
- Add your public SSH key to `/home/PREVIOUS_PLAYBOOK_USERNAME/.ssh/authorized_keys`
- `chef-client disable` so that we don't override the above
- `ssh -A PREVIOUS_PLAYBOOK_USERNAME@host`
- `echo $SSH_AUTH_SOCK`
- `tmux attach -t 0`
- `export SSH_AUTH_SOCK=<VALUE from previous SSH_AUTH_SOCK output>`
- `<ctrl-b> :`
- `set-environment -g 'SSH_AUTH_SOCK' <VALUE from previous SSH_AUTH_SOCK output>`
- `export ANSIBLE_REMOTE_USER=NEW_PLAYBOOK_USERNAME`
- `<ctrl-b> :`
- `set-environment -g 'ANSIBLE_REMOTE_USER' <your-user>`
