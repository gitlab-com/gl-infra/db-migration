<%- if false %>
<!--
This template has lots of ERB tags so it's not meant to be used verbatim to create an issue. To create
the markdown to copy into an issue, you run it like this (times are in UTC):

# imoc = https://gitlab.pagerduty.com/schedules-new - "Infra Escalation"
# cmoc = https://gitlab.pagerduty.com/schedules-new - "Incident Management - CMOC"
# eoc = https://gitlab.pagerduty.com/schedules-new - "SRE 8-hour"
# quality = https://gitlab.com/gitlab-org/quality/pipeline-triage#dri-weekly-rotation-schedule

# cluster_ver = The version of postgresql in each cluster (they should match!)
# src_cluster = The cluster we're decomposing *from*, i.e. main
# dst_cluster = The cluster that is being decomposed *to*, i.e. ci or sec

  erb \
    pcl_start_time="2024-10-25 23:00" \
    upgrade_start_time="2024-10-26 06:00" \
    switchover_start_time="2024-10-27 06:00" \
    pcl_end_time="2024-10-29 11:00" \
    production=1 \
    src_cluster=main \
    dst_cluster=ci \
    cluster_ver=v16 \
    -T- decomposition.md

For examples:

  erb \
    pcl_start_time="2024-10-25 23:00" \
    upgrade_start_time="2024-10-26 06:00" \
    switchover_start_time="2024-10-27 06:00" \
    pcl_end_time="2024-10-29 11:00" \
    production=1 \
    src_cluster=main \
    dst_cluster=sec \
    cluster_ver=v16 \
    video_call="TBD" \
    gitlab_com_cr="https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18593" \
    post_upgrade_mr="https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/5056" \
    post_upgrade_monitoring_mr="https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/9546" \
    post_switchover_mr="https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/5057" \
    post_switchover_monitoring_mr="https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/9297" \
    teleport_db_endpoint_mr="https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/6030" \
    console_config_endpoint_mr="https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18639" \
    destroy_old_cluster_mr="TODO" \
    -T- decomposition.md | pbcopy

NOTE:
  1. You'll need to have the activesupport gem installed: `gem install activesupport`
  2. To save the contents in your clipboard, you can pipe the output to `pbcopy` on OS/X or
     `xclip -selection clipboard` on Linux (assuming you have it installed).
-->
<%- end %>
<%
  unless defined?(production)
    raise "you must specify either 'production=0' (i.e., staging) or 'production=1'"
  end

  unless defined?(src_cluster)
    raise "you must specify src_cluster name (e.g., 'src_cluster=main' or 'src_cluster=ci')"
  end

  unless defined?(dst_cluster)
    raise "you must specify dst_cluster name (e.g., 'dst_cluster=registry' or 'dst_cluster=sec')"
  end

  unless defined?(cluster_ver)
    raise "you must specify 'cluster_ver', i.e. 'cluster_ver=v16'"
  end

  unless ["0", "1"].include?(production)
    raise "production must be either 0 or 1"
  end

  unless defined?(post_upgrade_mr)
    post_upgrade_mr = '{+ TODO +}'
  end

  unless defined?(post_upgrade_monitoring_mr)
    post_upgrade_monitoring_mr = '{+ TODO +}'
  end

  unless defined?(post_switchover_mr)
    post_switchover_mr = '{+ TODO +}'
  end

  unless defined?(post_switchover_monitoring_mr)
    post_switchover_monitoring_mr = '{+ TODO +}'
  end
  
  unless defined?(teleport_db_endpoint_mr)
    teleport_db_endpoint_mr = '{+ TODO +}'
  end

  unless defined?(console_config_endpoint_mr)
    console_config_endpoint_mr = '{+ TODO +}'
  end

  unless defined?(destroy_old_cluster_mr)
    destroy_old_cluster_mr = '{+ TODO +}'
  end

  unless defined?(gitlab_com_cr)
    gitlab_com_cr = '{+ TODO +}'
  end
  production = production.to_i == 0 ? false : true

  if src_cluster == 'main'
    src_cluster_short_prefix = ''
    src_cluster_suffix = ''
  else
    src_cluster_short_prefix = "#{src_cluster}-"
    src_cluster_suffix = "-#{src_cluster}"
  end

  if dst_cluster == 'main'
    dst_cluster_short_prefix = ''
    dst_cluster_suffix = ''
  else
    dst_cluster_short_prefix = "#{dst_cluster}-"
    dst_cluster_suffix = "-#{dst_cluster}"
  end

  src_cluster_prefix = "patroni-#{src_cluster}-#{cluster_ver}"
  dst_cluster_prefix = "patroni-#{dst_cluster}-#{cluster_ver}"
  src_cluster_host_prefix = "patroni-#{src_cluster}-#{cluster_ver}-"
  dst_cluster_host_prefix = "patroni-#{dst_cluster}-#{cluster_ver}-"

  if src_cluster == 'main'
    src_cluster_host_prefix = "patroni-#{src_cluster}-#{cluster_ver}-1"
  end
  if dst_cluster == 'main'
    dst_cluster_host_prefix = "patroni-#{dst_cluster}-#{cluster_ver}-"
  end

  dst_gcs_backup_location = "https://console.cloud.google.com/storage/browser/gitlab-gstg-postgres-backup/pitr-walg-#{dst_cluster}-#{cluster_ver}"
  dst_backup_host = "patroni-#{dst_cluster}-#{cluster_ver}-02"
  publication_count_reverse = 4
  block_ddl_ff = 1

  if production
    env = 'gprd'
    gcp_project = 'gitlab-production'
  else
    env = 'gstg'
    gcp_project = 'gitlab-staging-1'
  end

  src_prometheus_type = "patroni#{src_cluster_suffix}"
  src_consul_service_name = "patroni#{src_cluster_suffix}"
  src_consul_replica_endpoint = "#{src_cluster_short_prefix}db-replica"
  dst_prometheus_type = "patroni#{dst_cluster_suffix}"
  dst_consul_service_name = "patroni#{dst_cluster_suffix}"
  dst_consul_replica_endpoint = "#{dst_cluster_short_prefix}db-replica"

  src_prod_db_name = 'gitlabhq_production'
  dst_prod_db_name = 'gitlabhq_production'

  if src_cluster == 'registry'
    src_prod_db_name = 'gitlabhq_registry'
  end

  if dst_cluster == 'registry'
    dst_prod_db_name = 'gitlabhq_registry'
  end

  require 'time'
  require 'active_support'
  require 'active_support/time'
  
  pcl_start_ptime = Time.strptime(pcl_start_time + ' +0000', "%Y-%m-%d %H:%M %z")
  decomp_start_ptime = Time.strptime(upgrade_start_time + ' +0000', "%Y-%m-%d %H:%M %z")
  switch_start_ptime = Time.strptime(switchover_start_time + ' +0000', "%Y-%m-%d %H:%M %z")
  pcl_end_ptime = Time.strptime(pcl_end_time + ' +0000', "%Y-%m-%d %H:%M %z")

  pcl_start_time_str = pcl_start_time + ' UTC'
  decomp_start_time_str = upgrade_start_time + ' UTC'
  switch_start_time_str = switchover_start_time + ' UTC'
  pcl_end_time_str = pcl_end_time + ' UTC'
  
  window_in_hours = ((pcl_end_ptime - switch_start_ptime) / 3600).to_i

  @no_qa_cluster_list = ['registry','embedding']

  TBD_placeholder ||= "__[TBD - It is a placeholder for now. We will add step(s)/link(s)/description here if necessary]__"

  # This is the IP of the VM used to run "gitlab-qa" against staging/production
  qa_vm_ip = '104.196.188.184' #  Nik: do we really needed this?

  # Using define_method instead of def so we can access the variables in the outer scope
  define_method("role") do |name|
    unless binding.local_variable_defined?(name)
      return ""
    end

    binding.local_variable_get(name).split(",").map do |name|
      "@" + name
    end.join(" ")
  end

  # Similarly we use define_method here (instead of "def") so we can access the variables in the outer scope
  define_method("partial") do |file, vars={}, indent_spaces=0|
    erbvar = '_erbout' + rand(1000).to_s

    b = binding
    vars.each do |k, v|
      b.local_variable_set k.to_sym, v
    end

    ' ' * indent_spaces + ERB.new(File.read(file), nil, '-', erbvar).result(b).split("\n").join("\n" + ' ' * indent_spaces)
  end
-%>

# Database Decomposition <%= dst_cluster.upcase %> database in <%= env.upcase %>

Note: This CR will be copied to ops.gitlab.net, where it will be utilized in the event of an unexpected downtime for gitlab.com
Link to gitlab.com CR: <<%= gitlab_com_cr %>>

## Database Decomposition Rollout Team

| Role                              | Assigned To                        |
| --------------------------------- | ---------------------------------- |
| :wolf: Coordinator                | <%= role('coordinator') %>         |
| :knife: DB Playbook-Runner        | <%= role('db_playbook_runner') %>  |
| :telephone: Comms-Handler         | <%= role('comms_handler') %>       |
| :trophy: Quality                  | <%= role('quality') %>             |
| :ambulance: EOC                   | <%= role('eoc') %>                 |
| :fire_engine: IMOC                | <%= role('imoc') %>                |
| :mega: CMOC                       | Check CMOC escalation table bellow |
| :flashlight: Database Maintainers | <%= role('db_mantainer') %>        |
| :floppy_disk: Database Escalation | <%= role('db_escalation') %>       |
| :truck: Delivery Escalation       | <%= role('delivery_escalation') %> |
| :tophat: Head Honcho              | <%= role('head_honcho') %>         |

##### :mega: CMOC Escalation Table

Important: Just for when each window begins - else ping `@cmoc` on Slack

| Date and Step                             | Assigned To |
| ----------------------------------------- | ----------- |
| <%= pcl_start_time_str %> - PCL start     | {+ TBD +}   |
| <%= decomp_start_time_str %> - Upgrade      | {+ TBD +}   |
| <%= switch_start_time_str %> - Switchover | {+ TBD +}   |
| <%= pcl_end_time_str %> - PCL finish      | {+ TBD +}   |

## Collaboration

During the change window, the rollout team will collaborate using the following communications channels:

| App | Direct Link |
|---|---|
| Slack | [#g_database_operations](https://gitlab.enterprise.slack.com/archives/C02K0JTKAHJ) |
| Video Call | <%= video_call ||= "<LINK_TO_VIDEO_CALL>" %> |

# Immediately

Perform these steps when the issue is created.

- [ ] 🐺 {+ Coordinator +}: Fill out the names of the rollout team in the table above.

# Support Options

| Provider | Plan | Details | Create Ticket |
|----------|------|---------|---------------|
| __Google Cloud Platform__ | [Gold Support](https://cloud.google.com/support/?options=premium-support#options) | 24x7, email & phone, 1hr response on critical issues | [__Create GCP Support Ticket__](https://enterprise.google.com/supportcenter/managecases) |

# Entry points

| Entry point                | Before                                                               | Blocking mechanism                                             | Allowlist | QA needs                                                                    | Notes                                                                                               |
| -------------------------- | -------------------------------------------------------------------- | -------------------------------------------------------------- | --------- | --------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------- |
| Pages                      | Available via *.gitlab.io, and various custom domains                | Unavailable if GitLab.com goes down for a brief time. There is a cache but it will expire in `gitlab_cache_expiry` minutes  | N/A       | N/A                                                                         |
|                                                                                                     |

# Database hosts

- [Staging replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gstg&var-prometheus=Global&var-prometheus_app=Global)
- [Production replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gprd&var-prometheus=Global&var-prometheus_app=Global)

# Accessing the rails and database consoles

<%- if production %>
## Production
- rails: `ssh $USER-rails@console-01-sv-gprd.c.gitlab-production.internal`
- main db replica: `ssh $USER-db@console-01-sv-gprd.c.gitlab-production.internal`
- main db primary: `ssh $USER-db-primary@console-01-sv-gprd.c.gitlab-production.internal`
- ci db replica: `ssh $USER-db-ci@console-01-sv-gprd.c.gitlab-production.internal`
- ci db primary: `ssh $USER-db-ci-primary@console-01-sv-gprd.c.gitlab-production.internal`
- main db psql: `ssh -t patroni-main-v14-02-db-gprd.c.gitlab-production.internal sudo gitlab-psql`
- ci db psql: `ssh -t patroni-ci-v14-02-db-gprd.c.gitlab-production.internal sudo gitlab-psql`
- registry db psql: `ssh -t patroni-registry-v14-01-db-gprd.c.gitlab-production.internal sudo gitlab-psql`
<%- else %>
## Staging
- rails: `ssh $USER-rails@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db replica: `ssh $USER-db@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db primary: `ssh $USER-db-primary@console-01-sv-gstg.c.gitlab-staging-1.internal`
- ci db replica: `ssh $USER-db-ci@console-01-sv-gstg.c.gitlab-staging-1.internal`
- ci db primary: `ssh $USER-db-ci-primary@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db psql: `ssh -t patroni-main-v14-102-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`
- ci db psql: `ssh -t patroni-ci-v14-102-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`
- registry db psql: `ssh -t patroni-registry-v14-01-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`
<%- end %>

# Dashboards and debugging
These dashboards might be useful during the rollout:

<%- if production %>
## Production
- [PostgreSQL replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1)
- [Triage overview](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s&var-env=gprd&var-environment=gprd&var-prometheus=prometheus-01-inf-gprd&var-prometheus_app=prometheus-app-01-inf-gprd&var-backend=All&var-type=All&var-stage=main)
- [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-24h&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main)
- [Sentry - includes application errors](https://sentry.gitlab.net/gitlab/gitlabcom/)
  - Workhorse: <https://sentry.gitlab.net/gitlab/gitlab-workhorse-gitlabcom/>
  - Rails (backend): <https://sentry.gitlab.net/gitlab/gitlabcom/>
  - Rails (frontend): <https://sentry.gitlab.net/gitlab/gitlabcom-clientside/>
  - Gitaly (golang): <https://sentry.gitlab.net/gitlab/gitaly-production/>
  - Gitaly (ruby): <https://sentry.gitlab.net/gitlab/gitlabcom-gitaly-ruby/>
- [Logs (Kibana)](https://log.gprd.gitlab.net/app/kibana)
<%- else %>
## Staging
- [PostgreSQL replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gstg&var-prometheus=Global&var-prometheus_app=Global)
- [Triage overview](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s&var-env=gstg&var-environment=gstg&var-prometheus=prometheus-01-inf-gstg&var-prometheus_app=prometheus-app-01-inf-gstg&var-backend=All&var-type=All&var-stage=main)
- [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-24h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main)
- [Sentry - includes application errors](https://sentry.gitlab.net/gitlab/staginggitlabcom/)
- [Logs (Kibana)](https://nonprod-log.gitlab.net/app/kibana)
<%- end %>

Destination db: <%= dst_cluster %>
- [monitoring_pgbouncer_gitlab_user_conns](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pc6%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28pgbouncer_pools_client_active_connections%7Benv%3D%5C%22<%= env %>%5C%22,%20user%3D%5C%22gitlab%5C%22,%20type%3D~%5C%22<%= dst_prometheus_type %>%7C<%= dst_prometheus_type %>-<%= cluster_ver %>%2A%5C%22%7D%29%20by%20%28fqdn,database%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_chef_client_enabled](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28chef_client_enabled%7Benv%3D%5C%22<%= env %>%5C%22,type%3D~%5C%22<%= dst_prometheus_type %>%7C<%= dst_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%29%20by%20%28fqdn%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-15m%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_chef_client_last_run](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22min%28%28time%28%29-chef_client_last_run_timestamp_seconds%7Benv%3D%5C%22<%= env %>%5C%22,type%3D~%5C%22<%= dst_prometheus_type %>%7C<%= dst_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%29%2F60%29%20by%20%28fqdn%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_chef_client_error](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28chef_client_error%7Benv%3D%5C%22<%= env %>%5C%22,type%3D~%5C%22<%= dst_prometheus_type %>%7C<%= dst_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%29%20by%20%28fqdn%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_snapshot_last_run](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22gitlab_job_success_timestamp_seconds%7Bexported_job%3D%5C%22patroni-gcs-snapshot%5C%22,%20env%3D%5C%22<%= env %>%5C%22,%20type%3D%5C%22<%= dst_prometheus_type %>%5C%22%7D%20%3E%20gitlab_job_start_timestamp_seconds%7Bexported_job%3D%5C%22patroni-gcs-snapshot%5C%22,%20env%3D%5C%22<%= env %>%5C%22,%20type%3D%5C%22<%= dst_prometheus_type %>%5C%22%7D%20and%20on%20%28exported_job,%20env,%20type%29%20gitlab_job_failed%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-3h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_user_tables_writes](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22sum%28increase%28pg_stat_user_tables_n_tup_ins%7Benv%3D%5C%22<%= env %>%5C%22,datname%3D%5C%22<%= dst_prod_db_name %>%5C%22,type%3D~%5C%22<%= dst_prometheus_type %>%7C<%= dst_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%5B1m%5D%29%29%20by%20%28instance%29%5Cn%2B%5Cnsum%28increase%28pg_stat_user_tables_n_tup_del%7Benv%3D%5C%22<%= env %>%5C%22,datname%3D%5C%22<%= dst_prod_db_name %>%5C%22,type%3D~%5C%22<%= dst_prometheus_type %>%7C<%= dst_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%5B1m%5D%29%29%20by%20%28instance%29%5Cn%2B%5Cnsum%28increase%28pg_stat_user_tables_n_tup_upd%7Benv%3D%5C%22<%= env %>%5C%22,datname%3D%5C%22<%= dst_prod_db_name %>%5C%22,type%3D~%5C%22<%= dst_prometheus_type %>%7C<%= dst_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%5B1m%5D%29%29%20by%20%28instance%29%5Cnand%20on%28instance%29%20pg_replication_is_replica%3D%3D0%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_user_tables_reads](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22%28sum%28rate%28pg_stat_user_tables_seq_tup_read%7Benv%3D%5C%22<%= env %>%5C%22,datname%3D%5C%22<%= dst_prod_db_name %>%5C%22,type%3D~%5C%22<%= dst_prometheus_type %>%7C<%= dst_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%5B1m%5D%29%29%20by%20%28fqdn%29%29%20and%20on%28fqdn%29%20pg_replication_is_replica%3D%3D1%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_gitlab_maintenance_mode](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28gitlab_maintenance_mode%7Benv%3D%5C%22<%= env %>%5C%22%7D%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-15m%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)


Source db: <%= src_cluster %>
- [monitoring_pgbouncer_gitlab_user_conns](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pc6%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28pgbouncer_pools_client_active_connections%7Benv%3D%5C%22<%= env %>%5C%22,%20user%3D%5C%22gitlab%5C%22,%20type%3D~%5C%22<%= src_prometheus_type %>%7C<%= src_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%29%20by%20%28fqdn,database%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_chef_client_enabled](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28chef_client_enabled%7Benv%3D%5C%22<%= env %>%5C%22,type%3D~%5C%22<%= src_prometheus_type %>%7C<%= src_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%29%20by%20%28fqdn%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-15m%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_chef_client_last_run](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22min%28%28time%28%29-chef_client_last_run_timestamp_seconds%7Benv%3D%5C%22<%= env %>%5C%22,type%3D~%5C%22<%= src_prometheus_type %>%7C<%= src_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%29%2F60%29%20by%20%28fqdn%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_chef_client_error](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28chef_client_error%7Benv%3D%5C%22<%= env %>%5C%22,type%3D~%5C%22<%= src_prometheus_type %>%7C<%= src_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%29%20by%20%28fqdn%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_snapshot_last_run](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22gitlab_job_success_timestamp_seconds%7Bexported_job%3D%5C%22patroni-gcs-snapshot%5C%22,%20env%3D%5C%22<%= env %>%5C%22,%20type%3D%5C%22<%= src_prometheus_type %>%5C%22%7D%20%3E%20gitlab_job_start_timestamp_seconds%7Bexported_job%3D%5C%22patroni-gcs-snapshot%5C%22,%20env%3D%5C%22<%= env %>%5C%22,%20type%3D%5C%22<%= src_prometheus_type %>%5C%22%7D%20and%20on%20%28exported_job,%20env,%20type%29%20gitlab_job_failed%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-3h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_user_tables_writes](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22sum%28increase%28pg_stat_user_tables_n_tup_ins%7Benv%3D%5C%22<%= env %>%5C%22,datname%3D%5C%22<%= src_prod_db_name %>%5C%22,type%3D~%5C%22<%= src_prometheus_type %>%7C<%= src_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%5B1m%5D%29%29%20by%20%28instance%29%5Cn%2B%5Cnsum%28increase%28pg_stat_user_tables_n_tup_del%7Benv%3D%5C%22<%= env %>%5C%22,datname%3D%5C%22<%= src_prod_db_name %>%5C%22,type%3D~%5C%22<%= src_prometheus_type %>%7C<%= src_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%5B1m%5D%29%29%20by%20%28instance%29%5Cn%2B%5Cnsum%28increase%28pg_stat_user_tables_n_tup_upd%7Benv%3D%5C%22<%= env %>%5C%22,datname%3D%5C%22<%= src_prod_db_name %>%5C%22,type%3D~%5C%22<%= src_prometheus_type %>%7C<%= src_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%5B1m%5D%29%29%20by%20%28instance%29%5Cnand%20on%28instance%29%20pg_replication_is_replica%3D%3D0%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_user_tables_reads](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22%28sum%28rate%28pg_stat_user_tables_seq_tup_read%7Benv%3D%5C%22<%= env %>%5C%22,datname%3D%5C%22<%= src_prod_db_name %>%5C%22,type%3D~%5C%22<%= src_prometheus_type %>%7C<%= src_prometheus_type %>-<%= cluster_ver %>%5C%22%7D%5B1m%5D%29%29%20by%20%28fqdn%29%29%20and%20on%28fqdn%29%20pg_replication_is_replica%3D%3D1%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [monitoring_gitlab_maintenance_mode](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28gitlab_maintenance_mode%7Benv%3D%5C%22<%= env %>%5C%22%7D%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-15m%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)

# Repos used during the rollout
The following Ansible playbooks are referenced throughout this issue:
- Postgres Physical-to-Logical Replication, Decomposition, and Rollback: <https://gitlab.com/gitlab-com/gl-infra/db-migration/-/tree/master/pg-physical-to-logical>

---

# High level overview

This gives an high level overview on the procedure.

<details><summary>Decomposition Flowchart</summary>

```mermaid
flowchart TB
    subgraph Prepare new environment
    A[Create new cluster <%= dst_cluster %> as a carbon copy of <%= src_cluster %>] --> B
    B[Attach <%= dst_cluster %> as a standby-only-cluster to <%= src_cluster %> via physical replication] --> C
    end
    C[Make sure both clusters are in sync] --> D1
    subgraph Break Physical Replication: ansible-playbook physical-to-logical.yml
    D1[Disable Chef] --> D2
    D2[Perform clean shutdown of <%= dst_cluster %>] --> D3
    D3[On <%= src_cluster %>, create a replication slot and publication FOR ALL <%= src_cluster %> TABLES; remember its LSN] --> D4
    D4[Configure recovery_target_lsn on <%= dst_cluster %>] --> D5
    D5[Start <%= dst_cluster %>] --> D6
    D6[Let <%= dst_cluster %> reach the slot's LSN, still using physical replication] --> D7
    D7[Once slot's LSN is reached, promote <%= dst_cluster %> leader] --> D8
    D9[Create logical subscription with copy_data=false] --> D10
    D10[Let <%= dst_cluster %> catch up using logical replication] --> H
    end
    subgraph Redirect RO to <%= dst_cluster %>
    H[Redirect RO only to <%= dst_cluster %>] --> R
    R[Check if cluster is operational and metrics are normal] --"Normal"--> S
    R --"Abnormal"--> GR
    S[DBRE verify E2E tests run as expected with Quality help] --"Normal"--> T
    S --"Abnormal"-->GR
    end
    T[Switchover: Redirect RW traffict to <%= dst_cluster %>] --> U1
    subgraph Post Switchover Verification
    U1[Check if cluster is operational and metrics are normal]--"Normal"--> U2
    U1 --"Abnormal"--> LR
    U2[Enable Chef, run Chef-Client] --"Normal"--> U3
    U2 --"Abnormal"--> LR
    U3[Check if cluster is operational and metrics are normal] --"Normal"--> Success
    U3 --"Abnormal"--> LR
    Success[Success!]
    end
    subgraph GR[Gracefull Rollback - no dataloss]
    GR1[Start gracefull rollback]
    end
    subgraph LR[Fix forward]
    LR1[Fix all issues] -->LR2
    LR2[Return to last failed step]
    end
```

</details>

Playbook source: <https://gitlab.com/gitlab-com/gl-infra/db-migration/-/tree/master/pg-physical-to-logical>


# Prep Tasks

<details>
  <summary>

  - [ ] __SWITCHOVER minus 1 week (<%= 7.days.ago(switch_start_ptime).strftime('%Y-%m-%d %H:%M') %> UTC)__
  
  </summary>

<%- if production %>
  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on @gitlabstatus. Workflow: https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events
     
      - Message:
        ```text
        Next week, we will be undergoing scheduled maintenance to our <%= src_cluster.upcase %> and <%= dst_cluster.upcase %> database layers. The maintenance will start at <%= switch_start_time_str %> and should finish at <%= pcl_end_time_str %> (including performance regression observability period). GitLab.com will be available during the whole period as the maintenance should be seamless and transparent for the application. We apologize in advance for any inconvenience this may cause. See <<%= gitlab_com_cr %>>
        ```

  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery .
      - Message:
          ```text
          Hi @release-managers :waves:,
          
          We will be undergoing scheduled maintenance to our <%= src_cluster.upcase %> and <%= dst_cluster.upcase %> database layers in `PRODUCTION`. The operational lock and PCL will start at <%= pcl_start_time_str %> and should finish at <%= pcl_end_time_str %> (including performance regression observability period). We would like confirm that deployments that affect <%= src_cluster.upcase %> and <%= dst_cluster.upcase %> database clusters would need to be stopped during the window. All details can be found in the CR. Please be so kind and comment the acknowledgement on <%= gitlab_com_cr %>. :bow:
          ```        
<%- else %>
  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery . Kindly request for approval on this issue. 
      - Message:
          ```text
          Hi @release-managers :waves:,

          We will be undergoing scheduled maintenance to our <%= src_cluster.upcase %> and <%= dst_cluster.upcase %> database layers in `STAGING`. The operational lock will start at <%= pcl_start_time_str %> and should finish at <%= pcl_end_time_str %>  (including performance regression observability period). We would like confirm that deployments that affect <%= src_cluster.upcase %> and <%= dst_cluster.upcase %> database clusters would need to be stopped during the window. All details can be found in the CR. Please be so kind and comment the acknowledgement on <%= gitlab_com_cr %>. :bow:
          ```
<%- end %>

</details>

<details>
  <summary>

  - [ ] __SWITCHOVER minus 3 days (<%= 3.days.ago(switch_start_ptime).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

<%- if production %>
  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on @gitlabstatus. Workflow: https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events
     
      - Message:
        ```text
        In 3 days, we will be undergoing scheduled maintenance to our <%= src_cluster.upcase %> and <%= dst_cluster.upcase %> database layers. The maintenance will start at <%= switch_start_time_str %> and should finish at <%= pcl_end_time_str %> (including performance regression observability period). GitLab.com will be available during the whole period as the maintenance should be seamless and transparent for the application. We apologize in advance for any inconvenience this may cause. See <<%= gitlab_com_cr %>>
        ```

      - [ ] Check @gitlab retweeted from @gitlabstatus

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send on Slack `#whats-happening-at-gitlab`:

      - Message:

        ```text
        :loudspeaker: *Database decomposition for our <%= src_cluster.upcase %> and <%= dst_cluster.upcase %> database clusters in our SaaS platform* is scheduled to start at <%= switch_start_time_str %> and should finish at <%= pcl_end_time_str %> (including performance regression observability period) :rocket:
        Taking place in 3 days time! See <<%= gitlab_com_cr %>>

        :hammer_and_wrench: *What to expect?*
        GitLab.com will be available during the whole period as the maintenance should be seamless and transparent for the application. If you experience any issues potentially related to the decomposition in the next few days after the maintenance, please open an issue and reach the upgrade team at slack channel `#g_infra_database_reliability`
        ```

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Share message from `#whats-happening-at-gitlab` to the following channels:

      - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
      - [ ] `#g_delivery` (cc `@release-managers`)
      - [ ] `#support_gitlab-com` (Inform Support SaaS team)
          - [ ] Share with team a link to the change request regarding the maintenance
  <%- end %>

  1. [ ] 🏆 {+ Quality On-Call +}: Check that you have `Maintainer` or `Owner` permission in <https://ops.gitlab.net/gitlab-org/quality> to be able to trigger Smoke QA pipeline in schedules ([Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules), [Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules)). Reach out to Test Platform to get access if you don't have permission to trigger scheduled pipelines in the linked projects.

</details>

<details>
  <summary>

  - [ ] __PCL Start time (<%= pcl_start_time_str %>) - DECOMPOSITION minus <%= ((decomp_start_ptime - pcl_start_ptime) / 3600).to_i  %>  hours__

  </summary>

<%- if production %>
  <%- if block_ddl_ff %>
  1. [ ] 🔪 {+ Playbook-Runner +}: Ensure the CR is reviewed by the 🚑 {+ EOC +}

  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery the operational lock the `<%= src_cluster.upcase %>` and `<%= dst_cluster.upcase %>` database
      ```text
      Hi @release-managers :waves:,
      As scheduled we started the Deployment Hard PCL and enabled DDL block feature flat for the Decomposition in the <%= src_cluster.upcase %> and <%= dst_cluster.upcase %> databases in the <%= env.upcase %> environment, until <%= pcl_end_time_str %>.
      If there’s any incident with potential necessity to revert/apply db-migrations, please reach out @dbo members during the weekend as they are on-call and will evaluate if there will be impact in the upgrade or not.
      All details can be found in the CR - <%= gitlab_com_cr %>. :bow:
      ```

  1. [ ] ☎ {+ Comms-Handler +}: Inform the database at #g_database
      ```text
      Hi @gl-database,

      Please note that we started the operational block for the `<%= src_cluster.upcase %>` and `<%= dst_cluster.upcase %>` database decomposition, therefore we are blocking database model/structure modifications, by disabling the following tasks(`execute_batched_migrations_on_schedule` and `execute_background_migrations`, reindexing, async_foreign_key, async_index features and partition_manager_sync_partitions) in the <%= env.upcase %> environment.
      We will re-enable DDLs once the CR is finished and the rollback window is closed at <%= pcl_end_time_str %>.

      Thanks!
      ```

  1. [ ] 🔪 {+ Playbook-Runner +}: Disable the DDL-related feature flags:
      1. [ ] Disable feature flags by typing the following into `#production`:
          1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags true`
  <%- else %>
  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery the operational lock the in the `<%= src_cluster.upcase %>` and `<%= dst_cluster.upcase %>` databases
      ```text
      :siren-siren: :siren-siren: :siren-siren:
      Hi @release-managers :waves:,

      Due to a CR of PostgreSQL, please DO NOT merge any update versions on `<%= src_cluster.upcase %>` and `<%= dst_cluster.upcase %>` database MRs, in the `PRODUCTION` environment, until <%= pcl_end_time_str %>, as we are performing the database decomposition on this database.

      Regular auto-deployments can continue in the MAIN and CI databases.

      All details can be found in the CR. Please be so kind and comment the acknowledgement on <%= gitlab_com_cr %>. :bow:
      ```
  <%- end %>
<%- else %>
  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery 
  <%- if block_ddl_ff %>
      ```text
      Hi @release-managers :waves:, 

      We would like to communicate that deployments should be stopped in the STAGING environment, in the next hour, as we should start the database decomposition of the <%= src_cluster.upcase %> and <%= dst_cluster.upcase %> PostgreSQL clusters at <%= decomp_start_time_str %> - see <%= gitlab_com_cr %>. :bow:
      ```
  <%- else %>
      ```text
      Hi @release-managers :waves:, 

      We would like to communicate that in about 1 hour we will start the decomposition of the <%= src_cluster.upcase %> and <%= dst_cluster.upcase %> databases in the STAGING environment, therefore remind that no MRs should be merged to upgrade this database after <%= decomp_start_time_str %> - see <%= gitlab_com_cr %>. :bow:

      Please note that, auto-deployments and database migrations executions into MAIN and CI can continue and are not affected.
      ```
  <%- end %>
<%- end %>

  1. [ ] 🏆 {+ Quality On-Call +}: Confirm that QA tests are passing as a pre-upgrade sanity check
<%- unless @no_qa_cluster_list.include?(dst_cluster)  %>
      1. [ ] 🏆 {+ Quality On-Call +}: Confirm that smoke QA tests are passing on the current cluster by checking latest status for `Smoke` Type tests in <%- if production %>[Production](https://gitlab-qa-allure-reports.s3.amazonaws.com/production-sanity/master/index.html) and [Canary](https://gitlab-qa-allure-reports.s3.amazonaws.com/canary-sanity/master/index.html) <%- else %>[Staging](https://gitlab-qa-allure-reports.s3.amazonaws.com/staging-sanity/master/index.html) and [Staging Canary](https://gitlab-qa-allure-reports.s3.amazonaws.com/staging-canary-sanity/master/index.html) <%- end %> Allure reports listed in [QA pipelines](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/debugging-qa-test-failures/#qa-test-pipelines).
          - [ ] 🏆 {+ Quality On-Call +}: Trigger Smoke E2E suite against the environment that was decomposed: <%- if production %>[Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules): `Four hourly smoke tests`<%- else %>[Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules): `Four hourly smoke tests`<%- end %>. This has an estimated duration of 15 minutes.
          - [ ] 🏆 {+ Quality On-Call +}: If the smoke tests fail, we should re-run the failed job to see if it is reproducible. 
          - [ ] 🏆 {+ Quality On-Call +}: In parallel reach out to [on-call Test Platform DRI](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/oncall-rotation/#schedule) for the help with investigation. If there is no avialable on-call DRI, reach out to `#test-platform` and escalate with the [management team](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/#management-team).
<%- else  %>
      - [ ] 🔪 {+ Playbook-Runner +}: Perform manual test using Gitlab.com Web UI to validate <%- if cluster == "registry" %>Conteiner Registry<%- elsif cluster == "embedding" %>Gitlab Duo Chat (AI)<%- end %> usability. {+ TODO: +} Improve QA on this test.
<%- end %>

## Prepare the environment

  1. [ ] 🔪 {+ Playbook-Runner +}: Check that all needed Chef MRs are rebased and contain the proper changes.
      1. [ ] Post-Upgrade MR, to change pgbouncer configurations in <%= dst_cluster %>:
          - MR for <%= dst_cluster %>: <%= post_upgrade_mr %>
      1. [ ] Post-Upgrade monitoring MR, to configure Monitoring tags, in Mimir/Prometheus for use during switchover:
          - MR for <%= dst_cluster %>: <%= post_upgrade_monitoring_mr %>
      1. [ ] Post-Switchover MR to configure Consul
          - MR for <%= env %>-patroni-<%= dst_cluster %>: <%= post_switchover_mr %>
      1. [ ] Post-Switchover MR to configure Monitoring tags, in Mimir/Prometheus and update snapshot source
          - MR for <%= env %>-patroni-<%= dst_cluster %>: <%= post_switchover_monitoring_mr %>
      1. [ ] Post-Switchover MR to configure Teleport
          - MR for <%= env %>-patroni-<%= dst_cluster %>: <%= teleport_db_endpoint_mr %>
      1. [ ] Post-Switchover MR to configure Console
          - MR for <%= env %>-patroni-<%= dst_cluster %>: <%= console_config_endpoint_mr %>
 
  1. [ ] 🔪 {+ Playbook-Runner +}: Get the console VM ready for action
      - [ ] SSH to the console VM in `<%= env %>`
         - [ ] `ssh <% if production %>console-01-sv-gprd.c.gitlab-production.internal<% else %>console-01-sv-gstg.c.gitlab-staging-1.internal<% end %>`
      - [ ] Configure dbupgrade user
         - [ ] Disable screen sharing to reduce risk of exposing private key
         - [ ] Change to user dbupgrade `sudo su - dbupgrade`
         - [ ] Copy dbupgrade user's private key from 1Password to `~/.ssh/id_dbupgrade`
         - [ ] `chmod 600 ~/.ssh/id_dbupgrade`
         - [ ] Use key as default `ln -s /home/dbupgrade/.ssh/id_dbupgrade /home/dbupgrade/.ssh/id_rsa`
         - [ ] Repeat the same steps steps on target leader (it also has to have the private key)
         - [ ] Enable re-screen sharing if beneficial
      - [ ] Create an [access_token](https://gitlab.com/-/profile/personal_access_tokens) with at least `read_repository` for the next step
      - [ ] Clone repos:

        ```sh
        rm -rf ~/src \
          && mkdir ~/src \
          && cd ~/src \
          && git clone https://gitlab.com/gitlab-com/gl-infra/db-migration.git \
          && cd db-migration \
          && git checkout master
        ```

      - [ ] Ensure you have Ansible installed:

        ```sh
        python3 -m venv ansible
        source ansible/bin/activate
        python3 -m pip install --upgrade pip
        python3 -m pip install ansible
        python3 -m pip install jmespath
        ansible --version
        ```

      - [ ] Ensure that Ansible can talk to all the hosts in <%= env %>-<%= src_cluster %> and <%= env %>-<%= dst_cluster %>
          ```sh
          # TODO - make inventory name fully dynamic
          cd ~/src/db-migration/pg-logical-to-physical
          ansible -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" -i inventory/<%= env %>-sec-decomp.yml all -m ping
          ```

      - [ ] In advance, run pre-checks:
          ```sh
          # TODO - make inventory name fully dynamic
          cd ~/src/db-migration/pg-logical-to-physical
          ansible-playbook \
            -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" \
            -i inventory/<%= env %>-sec-decomp.yml physical_prechecks.yml 2>&1 \
          | ts | tee -a ansible_upgrade_pre_checks_<%= env %>_<%= dst_cluster %>_$(date +%Y%m%d).log
          ```

        You shouldn't see any failed hosts!


1. [ ] 🔪 {+ Playbook-Runner +}: Add the following silences at <https://alerts.gitlab.net> to silence alerts in <%= src_cluster %> and <%= dst_cluster %> nodes until 4 hours after the switchover time:
      - Start time: `<%= switch_start_ptime.strftime('%FT%T.000Z') %>`
      - Duration: `<%= ((4.hours.since(switch_start_ptime) - switch_start_ptime) / 3600).to_i %>h`
    - Matchers
        - <%= src_cluster %>
            - `env="<%= env %>"`
            - `fqdn=~"<%= src_cluster_prefix %>.*"`
        - <%= dst_cluster %>
            - `env="<%= env %>"`
            - `fqdn=~"<%= dst_cluster_prefix %>.*"`

</details>


  1. [ ] 🐺 {+ Coordinator +}: Get a green light from the 🚑 {+ EOC +}

## <%= dst_cluster.upcase %> Decomposition rollout

<details>
  <summary>

  - [ ] __Prepare Environment__
  
  </summary>

<%- unless production %>

  <%- if block_ddl_ff %>
  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery 
      ```text
      Hi @release-managers :waves:, 
      We would like to make sure that deployments have been stopped for our `<%= src_cluster.upcase %>` and `<%= dst_cluster.upcase %>` database in the `STAGING` environment, until <%= pcl_end_time_str %>. Be aware that we are deactivating certain feature flags during this time. All details can be found in the CR. Please be so kind and comment the acknowledgement on <%= gitlab_com_cr %>. :bow:
      ```

  1. [ ] ☎ {+ Comms-Handler +}: Inform the database team at #g_database
      ```text
      Hi @gl-database,

      Please note that we started the operational block for the `<%= dst_cluster.upcase %>` and `<%= dst_cluster.upcase %>` clusters for <%= dst_cluster.upcase %> Decomposition, therefore we are blocking database model/structure modifications, by disabling the following tasks (`execute_batched_migrations_on_schedule` and `execute_background_migrations`, reindexing, async_foreign_key, async_index features and partition_manager_sync_partitions) in the `STAGING` environment.

      We will re-enable DDLs once the CR is finished and the rollback window is closed at <%= pcl_end_time_str %>.

      Thanks!
      ```

  1. [ ] 🔪 {+ Playbook-Runner +}: Disable the DDL-related feature flags:
      1. [ ] Disable feature flags by typing the following into `#production`:
          1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags true --staging`
  
  <%- else %>
  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery 
      ```text
      :siren-siren: :siren-siren: :siren-siren:
      Hi @release-managers :waves:,
        
      Due to a CR of PostgreSQL, please DO NOT merge any update versions on `<%= cluster.upcase %>` database MRs, in the `STAGING` environment, until <%= pcl_end_time_str %> as we are performing the PostgreSQL engine update on this database. 

      All details can be found in the CR. Please be so kind and comment the acknowledgement on <%= gitlab_com_cr %>. :bow:
      ```

  <%- end %>
<%- end %>
</details>

<details>
  <summary>

  - [ ] __Prechecks__
  
  </summary>

<%- if block_ddl_ff %>
  1. [ ] 🐺 {+ Coordinator +}: Check if disallow_database_ddl_feature_flags is ENABLED:
  <%- if production %>
      - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags`
  <%- else %>
      - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags --staging`
  <%- end %>
<%- end %>

<%- if production %>
  1. [ ] 🔪 {+ Playbook-Runner +}: ADD the following silences at <https://alerts.gitlab.net> to silence `WALGBaseBackup` alerts in <%= src_cluster_prefix %> until the end of the maintenance:
      - Start time: `<%= decomp_start_ptime.strftime('%FT%T.000Z') %>`
      - Duration: `<%= (((pcl_end_ptime - decomp_start_ptime) / 3600).to_i + 48) %>h`
        - [ ] `env="gprd"`
        - [ ] `type="<%= env %>-<%= src_cluster_prefix %>"`
        - [ ] `alertname=~"WALGBaseBackupFailed|walgBaseBackupDelayed"`

  1. [ ] 🔪 {+ Playbook-Runner +}: ADD the following silences at <https://alerts.gitlab.net> to silence `WALGBaseBackup` alerts in <%= dst_cluster_prefix %> until the end of the maintenance:
      - Start time: `<%= decomp_start_ptime.strftime('%FT%T.000Z') %>`
      - Duration: `<%= (((pcl_end_ptime - decomp_start_ptime) / 3600).to_i + 2) %>h`
        - [ ] `env="gprd"`
        - [ ] `type="<%= env %>-<%= dst_cluster_prefix %>"`
        - [ ] `alertname=~"WALGBaseBackupFailed|walgBaseBackupDelayed"`
<%- end %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Monitor what pgbouncer pool has connections: [monitoring_pgbouncer_gitlab_user_conns][monitoring_pgbouncer_gitlab_user_conns]

  1. [ ] 🔪 {+ Playbook-Runner +}: Check if anyone except application is connected to source primary and interrupt them:
      1. [ ] Login to source primary
          ```sh
          ssh <%= src_cluster_host_prefix %>01-db-<%= env %>.c.<%= gcp_project %>.internal
          ```
      1. [ ] Check all connections that are not `gitlab`:
          ```sh
          gitlab-psql -c "
            select
              pid, client_addr, usename, application_name, backend_type,
              clock_timestamp() - backend_start as connected_ago,
              state,
              left(query, 200) as query
            from pg_stat_activity
            where
              pid <> pg_backend_pid()
              and not backend_type ~ '(walsender|logical replication|pg_wait_sampling)'
              and usename not in ('gitlab', 'gitlab-registry', 'pgbouncer', 'postgres_exporter', 'gitlab-consul')
              and application_name <> 'Patroni'
            "
          ```
      1. [ ] If there are sessions that potentially can perform any writes, spend up to 10 minutes to make an attempt to find the actors and ask them to stop.
      1. [ ] Finally, terminate all the remaining sessions that are not coming from application/infra components and potentially can cause writes:
          ```sh
          gitlab-psql -c "
            select pg_terminate_backend(pid)
            from pg_stat_activity
            where
              pid <> pg_backend_pid()
              and not backend_type ~ '(walsender|logical replication|pg_wait_sampling)'
              and usename not in ('gitlab', 'gitlab-registry', 'pgbouncer', 'postgres_exporter', 'gitlab-consul')
              and application_name <> 'Patroni'
            "
          ```

  1. [ ] 🔪 {+ Playbook-Runner +}: Run physical_prechecks playbook:

      ```sh
      # TODO - make inventory name fully dynamic
      cd ~/src/db-migration/pg-physical-to-logical
      ansible-playbook \
        -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" \
        -i inventory/<%= env %>-sec-decomp.yml physical_prechecks.yml 2>&1 \
      | ts | tee -a ansible_physical-to-logical_<%= env %>_<%= dst_cluster %>_$(date +%Y%m%d).log
      ```
  
  1. [ ] 🔪 {+ Playbook-Runner +}: Verify configuration of pgbouncer-sec and pgbouncer-sidekiq-sec

      ```sh
      knife ssh "role:<%= env %>*pgbouncer*<%= dst_cluster %>*" "sudo grep master.patroni /var/opt/gitlab/pgbouncer/databases.ini"
      # should return master.patroni.service.consul prior to switchover!
</details>

<details>
  <summary>

  - [ ] __Convert Physical Replication to Logical__

  </summary>

  1. [ ] 🔪 {+ Playbook-Runner +}: Run physical-to-logical playbook:

      ```sh
      # TODO - make inventory name fully dynamic
      cd ~/src/db-migration/pg-physical-to-logical
      ansible-playbook \
        -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" \
        -i inventory/<%= env %>-sec-decomp.yml physical_to_logical.yml 2>&1 \
      | ts | tee -a ansible_physical-to-logical_<%= env %>_<%= dst_cluster %>_$(date +%Y%m%d).log
      ```

</details>

<details>
  <summary>

  - [ ] __Read Only Traffic Switchover__

  </summary>



<details>
  <summary>

  - [ ] __Console Node Rollout__

  </summary>

1. [ ] 🔪 {+ Playbook-Runner +}: Switchover [gstg rails console](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/blob/master/roles/gstg-base-console-ro-node.json#L142) (teleport) chef connection configuration to new `patroni-sec-v16` DB. Writes will go through PGBouncer host to `main` and reads to `sec` replicas.
    - [ ] merge [chef-repo MR](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/5583)

1. [ ] 🔪 {+ Playbook-Runner +}: Simple checks if application sees a proper configuration. **Expected: sec load balancer and sec_replica for read connection**

    ```ruby
    [1] pry(main)> ApplicationRecord.load_balancer.name
    => :main
    [2] pry(main)> Gitlab::Database::SecApplicationRecord.load_balancer.name
    => :sec
    [3] pry(main)> ApplicationRecord.connection.pool.db_config.name
    => "main"
    [4] pry(main)> Gitlab::Database::SecApplicationRecord.connection.pool.db_config.name
    => "sec"
    [5] pry(main)> Gitlab::Database::SecApplicationRecord.load_balancer.read { |connection| connection.pool.db_config.name }
    => "sec_replica"
    [6]  Gitlab::Database::SecApplicationRecord.load_balancer.read_write { |connection| connection.pool.db_config.name }
    => "sec"
    ```

1. [ ] 🔪 {+ Playbook-Runner +}: Simple checks to see if application can still talk to sec_replica database. **Expected: db_config_name:sec_replica**

    ```ruby
    [10] pry(main)> ActiveRecord::Base.logger = Logger.new(STDOUT)
    [11] pry(main)> Gitlab::Database::SecApplicationRecord.load_balancer.read { |connection| connection.select_all("SELECT COUNT(*) FROM vulnerability_user_mentions") }
      (20.3ms)  SELECT COUNT(*) FROM vulnerability_user_mentions /*application:console,db_config_name:main_replica,line:/data/cache/bundle-2.7.4/ruby/2.7.0/gems/marginalia-1.10.0/lib/marginalia/comment.rb:25:in `block in construct_comment'*/
    => #<ActiveRecord::Result:0x00007fcfc79ccdb0 @column_types={}, @columns=["count"], @hash_rows=nil, @rows=[[1]]>
    ```

</details>


<details>
  <summary>

  - [ ] __Web Node Canary Rollout__

  </summary>

1. [ ] 🔪 {+ Playbook-Runner +}: Switchover [gstg web configuration](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/e1825d58ae2ae5a3892767a90edf18c1fe466b08/releases/gitlab/values/gstg-cny.yaml.gotmpl#L102) to new `pgbouncer-sec`
    - [ ] merge [k8s-workload MR](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/4154)
- [ ] Verify connectivity, monitor pgbouncer connections
- [ ] Observe `logs` and `prometheus` for errors 

</details>

<details>
  <summary>

  - [ ] __Observable Logs and Prometheus Metrics__

  </summary>

All logs will split `db_*_count` metrics into separate buckets describing each used connection:

1. [ ] 🔪 {+ Playbook-Runner +}: Ensure `json.db_sec_count : *` logs are present

- Primary connection usage by state - [`pg_stat_activity_count`](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22r2a%22:%7B%22datasource%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22,%22queries%22:%5B%7B%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D,%22editorMode%22:%22code%22,%22expr%22:%22sum%28pg_stat_activity_count%7Benv%3D%5C%22gstg%5C%22,datname%3D%5C%22gitlabhq_production%5C%22,%20tier%3D%5C%22db%5C%22,%20type%3D%5C%22patroni%5C%22,%20fqdn%3D%5C%22patroni-main-v16-01-db-gstg.c.gitlab-staging-1.internal%5C%22%7D%29%22,%22instant%22:true,%22legendFormat%22:%22__auto%22,%22range%22:true,%22refId%22:%22A%22%7D,%7B%22refId%22:%22B%22,%22expr%22:%22sum%20by%20%28state%29%20%28pg_stat_activity_count%7Benv%3D%5C%22gstg%5C%22,datname%3D%5C%22gitlabhq_production%5C%22,%20tier%3D%5C%22db%5C%22,%20type%3D%5C%22patroni%5C%22,%20fqdn%3D%5C%22patroni-main-v16-01-db-gstg.c.gitlab-staging-1.internal%5C%22%7D%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [`pgbouncer_stats_queries_pooled_total`](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22r2a%22:%7B%22datasource%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22,%22queries%22:%5B%7B%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D,%22editorMode%22:%22code%22,%22expr%22:%22sum%20by%20%28type%29%20%28rate%28pgbouncer_stats_queries_pooled_total%7Benv%3D%5C%22gstg%5C%22,%20type%3D~%5C%22pgbouncer.%2A%5C%22,database%3D~%5C%22gitlabhq_prod.%2A%5C%22%7D%5B1m%5D%29%29%22,%22instant%22:true,%22legendFormat%22:%22__auto%22,%22range%22:true,%22refId%22:%22A%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)

</details>

<details>
  <summary>

  - [ ] __Sidekiq Node Rollout__

  </summary>

1. [ ] 🔪 {+ Playbook-Runner +}: Switchover [gstg sidekiq configuration](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/e1825d58ae2ae5a3892767a90edf18c1fe466b08/releases/gitlab/values/gstg.yaml.gotmpl?page=2#L1152) to new `pgbouncer-sec`
    - [ ] merge [k8s-workload MR](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/4155)
- [ ] Verify connectivity, monitor pgbouncer connections
- [ ] Observe `logs` and `prometheus` for errors 

</details>


<details>
  <summary>

  - [ ] __Observable Logs and Prometheus Metrics__

  </summary>

All logs will split `db_*_count` metrics into separate buckets describing each used connection:

1. [ ] 🔪 {+ Playbook-Runner +}: Ensure `json.db_sec_count : *` logs are present

- Primary connection usage by state - [`pg_stat_activity_count`](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22r2a%22:%7B%22datasource%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22,%22queries%22:%5B%7B%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D,%22editorMode%22:%22code%22,%22expr%22:%22sum%28pg_stat_activity_count%7Benv%3D%5C%22gstg%5C%22,datname%3D%5C%22gitlabhq_production%5C%22,%20tier%3D%5C%22db%5C%22,%20type%3D%5C%22patroni%5C%22,%20fqdn%3D%5C%22patroni-main-v16-01-db-gstg.c.gitlab-staging-1.internal%5C%22%7D%29%22,%22instant%22:true,%22legendFormat%22:%22__auto%22,%22range%22:true,%22refId%22:%22A%22%7D,%7B%22refId%22:%22B%22,%22expr%22:%22sum%20by%20%28state%29%20%28pg_stat_activity_count%7Benv%3D%5C%22gstg%5C%22,datname%3D%5C%22gitlabhq_production%5C%22,%20tier%3D%5C%22db%5C%22,%20type%3D%5C%22patroni%5C%22,%20fqdn%3D%5C%22patroni-main-v16-01-db-gstg.c.gitlab-staging-1.internal%5C%22%7D%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [`pgbouncer_stats_queries_pooled_total`](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22r2a%22:%7B%22datasource%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22,%22queries%22:%5B%7B%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D,%22editorMode%22:%22code%22,%22expr%22:%22sum%20by%20%28type%29%20%28rate%28pgbouncer_stats_queries_pooled_total%7Benv%3D%5C%22gstg%5C%22,%20type%3D~%5C%22pgbouncer.%2A%5C%22,database%3D~%5C%22gitlabhq_prod.%2A%5C%22%7D%5B1m%5D%29%29%22,%22instant%22:true,%22legendFormat%22:%22__auto%22,%22range%22:true,%22refId%22:%22A%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)

</details>


<details>
  <summary>

  - [ ] __Web Node Rollout__

  </summary>

1. [ ] 🔪 {+ Playbook-Runner +}: Switchover [gstg web configuration](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/e1825d58ae2ae5a3892767a90edf18c1fe466b08/releases/gitlab/values/gstg.yaml.gotmpl?page=2#L1488) to new `pgbouncer-sec`
    - [ ] merge [k8s-workload MR](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/4156)
1. [ ] 🔪 {+ Playbook-Runner +}: Verify connectivity, monitor pgbouncer connections
1. [ ] 🔪 {+ Playbook-Runner +}: Observe `logs` and `prometheus` for errors
1. [ ] 🔪 {+ Playbook-Runner +}: Cleanup: Remove overrides in each configuration node and promote [chef database connection configuration to gstg-base](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/blob/f6da5f81d4cf41b5efba7d896b55d130fca738ab/roles/gstg-base.json#L293)) setting `sec` to new `patroni-sec-v16` DB. Writes will continue to go through PGBouncer host to `main` and reads to `sec` replicas.
    - [ ] Ensure `database_tasks: false` is set for `gstg-base-deploy-node.json` to prevent migrations from being ran for sec DB, to be unset in Phase 7 ([see note](https://gitlab.com/gitlab-org/gitlab/-/issues/509190#note_2278000723))

</details>


<details>
  <summary>

  - [ ] __Observable Logs and Prometheus Metrics__

  </summary>

#### 4.4.1 Observable logs

All logs will split `db_*_count` metrics into separate buckets describing each used connection:

1. [ ] 🔪 {+ Playbook-Runner +}: Ensure `json.db_sec_count : *` logs are present

#### 4.4.2. Observable prometheus metrics

- Primary connection usage by state - [`pg_stat_activity_count`](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22r2a%22:%7B%22datasource%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22,%22queries%22:%5B%7B%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D,%22editorMode%22:%22code%22,%22expr%22:%22sum%28pg_stat_activity_count%7Benv%3D%5C%22gstg%5C%22,datname%3D%5C%22gitlabhq_production%5C%22,%20tier%3D%5C%22db%5C%22,%20type%3D%5C%22patroni%5C%22,%20fqdn%3D%5C%22patroni-main-v16-01-db-gstg.c.gitlab-staging-1.internal%5C%22%7D%29%22,%22instant%22:true,%22legendFormat%22:%22__auto%22,%22range%22:true,%22refId%22:%22A%22%7D,%7B%22refId%22:%22B%22,%22expr%22:%22sum%20by%20%28state%29%20%28pg_stat_activity_count%7Benv%3D%5C%22gstg%5C%22,datname%3D%5C%22gitlabhq_production%5C%22,%20tier%3D%5C%22db%5C%22,%20type%3D%5C%22patroni%5C%22,%20fqdn%3D%5C%22patroni-main-v16-01-db-gstg.c.gitlab-staging-1.internal%5C%22%7D%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)
- [`pgbouncer_stats_queries_pooled_total`](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22r2a%22:%7B%22datasource%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22,%22queries%22:%5B%7B%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D,%22editorMode%22:%22code%22,%22expr%22:%22sum%20by%20%28type%29%20%28rate%28pgbouncer_stats_queries_pooled_total%7Benv%3D%5C%22gstg%5C%22,%20type%3D~%5C%22pgbouncer.%2A%5C%22,database%3D~%5C%22gitlabhq_prod.%2A%5C%22%7D%5B1m%5D%29%29%22,%22instant%22:true,%22legendFormat%22:%22__auto%22,%22range%22:true,%22refId%22:%22A%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)

</details>


<details>
  <summary>

  - [ ] __Verify Read Traffic to patroni-sec__

  </summary>

1. [ ] 🔪 {+ Playbook-Runner +}: [monitoring_pgbouncer_gitlab_user_conns](https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pc6%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28pgbouncer_pools_client_active_connections%7Benv%3D%5C%22<%= env %>%5C%22,%20user%3D%5C%22gitlab%5C%22,%20type%3D~%5C%22<%= dst_prometheus_type %>%7C<%= dst_prometheus_type %>-<%= cluster_ver %>%2A%5C%22%7D%29%20by%20%28fqdn,database%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1)

</details>

</details>


  #### Phase 7 – execute!

<details>
  <summary>

  - [ ] __Phase 7 - switchover__

  </summary>
  
  <%- if production %>
  1. [ ] 🔪 {+ Playbook-Runner +}: Schedule a job to enable `gitlab_maintenance_mode` into a node exporter, during the upgrade window:
      - [ ] SSH to a console VM in `<%= env %>` (eg. `ssh console-01-sv-<%= env %>.c.<%= gcp_project %>.internal` )
        - [ ] Schedule jobs:
          ```sh
          sudo su -
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 1\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom | at -t <%= switch_start_ptime.strftime('%Y%m%d%H%M') %>
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 0\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom | at -t <%= pcl_end_ptime.strftime('%Y%m%d%H%M') %>
          cat /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          atq
          ```
          
  1. [ ] __PRODUCTION ONLY__ ☁ 🔪 {+ Playbook-Runner +}: Create a maintenance window in [PagerDuty](https://gitlab.pagerduty.com/service-directory/maintenance-windows) with the following:

      - Which services are affected?
        - [GitLab Production](https://gitlab.pagerduty.com/service-directory/PATDFCE)
        - [SLO alerts gprd cny stage](https://gitlab.pagerduty.com/service-directory/P3DG414)
        - [SLO Alerts gprd main stage](https://gitlab.pagerduty.com/service-directory/P7Q44DU)
        - [Dead Man's snitch](https://gitlab.pagerduty.com/service-directory/PFQXAWL)

      - Why is this maintenance happening?

        ```text
        Decomposing <%= dst_cluster %> data from <%= src_cluster_prefix %> to <%= dst_cluster_prefix %> so silencing the pager.
        ```

      - Select `Start at a scheduled time`:

        - Timezone: `(UTC+00:00) UTC`
        - Start: `<%= switch_start_ptime.strftime('%m/%d/%Y | %I:%M %p') %>`
        - End: `<%= pcl_end_ptime.strftime('%m/%d/%Y | %I:%M %p') %>`
<%- end %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Run Ansible playbook for Database Decomposition for the <%= env %>-<%= dst_cluster %> cluster:  

      ```sh
      # TODO - make inventory name fully dynamic
      cd ~/src/db-migration/pg-physical-to-logical
      ansible-playbook \
        -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" \
        -i inventory/<%= env %>-sec-decomp.yml switchover.yml 2>&1 \
      | ts | tee -a ansible_upgrade_<%= env %>_<%= dst_cluster %>_$(date +%Y%m%d).log
      ```

  1. [ ] 🔪 {+ Playbook-Runner +}: Verify configuration of pgbouncer-sec and pgbouncer-sidekiq-sec

      ```sh
      knife ssh "role:<%= env %>*pgbouncer*<%= dst_cluster %>*" "sudo grep master.patroni /var/opt/gitlab/pgbouncer/databases.ini"
      # should return master.patroni-sec.service.consul after switchover!
      ```
</details>

<details>
  <summary>

  - [ ] __Persist pgbouncer configurations__

  </summary>

  1. [ ] 🔪 {+ Playbook-Runner +}: Merge the MR that reconfigures pgbouncer in Chef for <%= dst_cluster_prefix %>. First confirm there are no errors in merge pipeline. If the MR was merged, then revert it, and get it merged properly.
      1. [ ] MR for <%= dst_cluster_prefix %>: <%= post_upgrade_mr %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Ensure that the changes merged from the previous step have been deployed to the Chef server before re-enabling Chef by confirming the linked `master` pipeline for `ops.gitlab.net` completed successfully.

  1. [ ] 🔪 {+ Playbook-Runner +}: Run chef-client on one pgbouncer host and verify the configuration was not changed (changes require a reload to migrate traffic, so check nothing changed.  If needed, revert the MR and update to resolve)

      ```sh
      knife ssh "role:<%= env %>*pgbouncer*<%= dst_cluster %>*" "sudo grep master.patroni /var/opt/gitlab/pgbouncer/databases.ini"
      # should return master.patroni-sec.service.consul after switchover!
      ```

  1. [ ] 🔪 {+ Playbook-Runner +}: Check WRITES going to the TARGET cluster, `<%= dst_cluster_prefix %>`: [monitoring_user_tables_writes][monitoring_user_tables_writes]

  1. [ ] 🔪 {+ Playbook-Runner +}: Check READS going to the TARGET cluster, `<%= dst_cluster_prefix %>`: [monitoring_user_tables_reads][monitoring_user_tables_reads].

  1. [ ] 🔪 {+ Playbook-Runner +}: Confirm chef-client is ENABLED in all nodes [monitoring_chef_client_enabled][monitoring_chef_client_enabled]

  1. [ ] 🔪 {+ Playbook-Runner +}: Start cron.service on all <%= env %>-<%= dst_cluster %> nodes:
      ```sh
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo systemctl is-active cron.service"
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo systemctl start cron.service"
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo systemctl is-active cron.service"
      ```


</details>

#### Post Switchover QA Tests

<details>
  <summary>

  - [ ] Post Switchover QA Testing

  </summary>

<%- unless @no_qa_cluster_list.include?(dst_cluster)  %>
1. [ ] 🏆 {+ Quality On-Call +}: Trigger Smoke E2E suite against the environment that was upgraded:<%- if production %>[Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules): `Four hourly smoke tests`<%- else %>[Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules): `Four hourly smoke tests`<%- end %>
<%- else  %>
1. [ ] 🔪 {+ Playbook-Runner +}: Perform manual test using Gitlab.com Web UI to validate <%- if dst_cluster == "registry"%>Conteiner Registry<%- elsif dst_cluster == "embedding"%>Gitlab Duo Chat (AI)<%- end %> usability. {+ TODO +}{+ TODO: +} Improve QA on this test.
<%- end %>

#### Communicate

<details>
  <summary>

  - [ ] __Communication__

  </summary>

<%- if production %>
  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>

      - Message:
        ```text
        Gitlab.com <%= dst_cluster.upcase %> database decomposition was performed. We'll continue to monitor for any performance issues until the end of the maintenance window. Thank you for your patience. See <<%= gitlab_com_cr %>>
        ```

    - [ ] Check @gitlab retweeted from @gitlabstatus

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: In the same thread from the earlier post, post the following message and click on the checkbox "Also send to X channel" so the threaded message would be published to the channel:
   - Message:
      ```text
      :done: *GitLab.com database layer maintenance decomposition is complete now.* :celebrate:
      We’ll continue to monitor the platform to ensure all systems are functioning correctly.
      ```
      - [ ] `#whats-happening-at-gitlab`
      - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
<%- end %>

</details>

<details>
  <summary>

  - [ ] __Wrapping Up__

  </summary>

<%- unless @no_qa_cluster_list.include?(dst_cluster)  %>

1. __Start Post Switchover QA__
    1. [ ] 🏆 {+ Quality On-Call +}: Full E2E suite against the environment that was decomposed:<%- if production %>[Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules): `Four hourly smoke tests`, and `Twice daily full run`<%- else %>[Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules): `Four hourly smoke tests`, and `Daily Full QA suite`<%- end %>
        - It will take 1+ hour to run these tests, so you can continue with the `Wrapping up` of the upgrade and check the test result latter;
<%- end %>

<%- if production %>
1. [ ] __PRODUCTION ONLY__ 🔪 {+ Playbook-Runner +}: If the scheduled maintenance is still active in [PagerDuty](https://gitlab.pagerduty.com/service-directory/maintenance-windows), click on `Update` then `End Now`.
<%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: Remove silences of `fqdn=~"<%= src_cluster_prefix %>.*"` and `fqdn=~"<%= dst_cluster_prefix %>.*"` we created during this process from <https://alerts.gitlab.net> 

<%- if production %>
1. [ ] __PRODUCTION ONLY__ 🔪 {+ Playbook-Runner +}: Ping `@NikolayS` on the gitlab.com CR (<<%= gitlab_com_cr %>>) (and/or in [#database-lab](https://gitlab.slack.com/archives/CLJMDRD8C)) that the work is complete so he can update the DLE environment.

1. [ ] __PRODUCTION ONLY__ 🔪 {+ Playbook-Runner +}: Confirm that Teleport access works properly and we're hitting the right clusters:
    ```bash
    # Login to Teleport
    tsh login --add-keys-to-agent=no --proxy=production.teleport.gitlab.net --request-roles=database-ro-gprd --request-reason="Testing DB connectivity after <%= dst_cluster %> Decomposition <%= gitlab_com_cr %> "
    query="select setting from pg_settings where name='cluster_name'"

    # Check <%= src_cluster.upcase %>
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=<%= dst_prod_db_name %> db-<%= src_cluster %>-primary-gprd --add-keys-to-agent=no
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=<%= dst_prod_db_name %> db-<%= src_cluster %>-replica-gprd --add-keys-to-agent=no

    # Check <%= dst_cluster.upcase %>
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=<%= dst_prod_db_name %> db-<%= dst_cluster %>-primary-gprd --add-keys-to-agent=no
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=<%= dst_prod_db_name %> db-<%= dst_cluster %>-replica-gprd --add-keys-to-agent=no
    ```
<%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: Create the wal-g daily restore schedule for the **[<%= env %>] - [<%= dst_cluster %>]** cluster at https://ops.gitlab.net/gitlab-com/gl-infra/data-access/durability/gitlab-restore/postgres-gprd/-/pipeline_schedules
    1. [ ] Change the following variables:
        - `PSQL_VERSION = 16`
        - `BACKUP_PATH = ?` (? = use the "directory" from the new v16 GCS backup location at: <%= dst_gcs_backup_location %>)

<%- if production %>
1. [ ] 🐺 {+ Coordinator +}: Check if gitlab_maintenance_mode is DISABLED for <%= env %> [monitoring_gitlab_maintenance_mode][monitoring_gitlab_maintenance_mode]
    - If is not disabled ask 🔪 {+ Playbook-Runner +} to manually disable it by:
      - SSH to a console VM in `<%= env %>` (eg. `ssh console-01-sv-<%= env %>.c.<%= gcp_project %>.internal` )
        - Set `gitlab_maintenance_mode=0` on node exporter :
          ```sh
          sudo su -
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 0\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          cat /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          atq
          ```
<%- end %>

<%- unless @no_qa_cluster_list.include?(dst_cluster)  %>
1. [ ] 🏆 {+ Quality On-Call +}: (after an hour): Check that the Smoke, and Full E2E suite has passed. If there are failures, reach out to [on-call Test Platform DRI](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/oncall-rotation/#schedule) for the help with investigation. If there is no avialable on-call DRI, reach out to `#test-platform` and escalate with the [management team](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/#management-team).

    1. [ ] 🏆 {+ Quality On-Call +}: If the Smoke or Full E2E tests fail, Quality performs an initial triage of the failure. If Quality cannot determine failure is 'unrelated', team decides on [declaring an incident](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/debugging-qa-test-failures/#failure-needs-escalation) and following the incident process.
<%- end %>

</details>

# Close Rollback Window

<details>
  <summary>

  - [ ] __SWITCHOVER plus <%= window_in_hours %> hours - Close PCL (<%= pcl_end_time_str %>)__

  </summary>

1. [ ] 🔪 {+ Playbook-Runner +}: Run Ansible playbook to Stop the Reverse Logical Replication:
    ```sh
    # TODO - make inventory name fully dynamic
    cd ~/src/db-migration/pg-physical-to-logical
    ansible-playbook \
      -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" <%- if dst_cluster == 'registry' %>-e "pg_source_dbname=gitlabhq_registry"<%- end %>\
        -i inventory/<%= env %>-sec-decomp.yml \
      stop_reverse_replication.yml -e "pg_publication_count_reverse=<%= publication_count_reverse %>" 2>&1 \
    | ts | tee -a stop_reverse_replication_<%= env %>_<%= dst_cluster %>_$(date +%Y%m%d).log
    ```

1. [ ] 🔪 {+ Playbook-Runner +}: On the TARGET cluster <%= src_cluster_prefix %> Leader/Writer, drop subscription (if still existing) for logical replication:
    - [ ] Check if the subscription still exist:
      ```sh
      gitlab-psql \
          -Xc "select subname, subenabled, subconninfo, subslotname, subpublications from pg_subscription" 
      ```
1. [ ] 🔪 {+ Playbook-Runner +}: On the SOURCE cluster <%= dst_cluster_prefix %>  Leader/Writer, drop publication and logical_replication_slot for reverse replication:
    - [ ] Check if the publication and replication slots still exist:
      ```sh
      gitlab-psql \
        -Xc "select pubname from pg_publication" \
        -Xc "select slot_name, plugin, slot_type, active from pg_replication_slots"
      ```

<%- if block_ddl_ff %>
1. [ ] 🔪 {+ Playbook-Runner +}: Enable feature flags by typing the following into `#production`:
  <%- if production %>
    - PRODUCTION:
      1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags false`
  <%- else %>
    - STAGING:
      1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags false --staging`
  <%- end %>

    1. [ ] 🐺 {+ Coordinator +}: Check if the underlying DDL lock FF is DISABLED:
      <%- if production %>
        - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags` should return DISABLED
      <%- else %>
        - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags --staging` should return DISABLED
      <%- end %>

    1. [ ] ☎ {+ Comms-Handler +}: Inform the database team that the CR is completed at #g_database:
        ```text
        Hi @gl-database,

        We are reaching out to inform that we have completed the work for the <%= gitlab_com_cr %> CR. Therefore we have re-enabled the `execute_batched_migrations_on_schedule`, `execute_background_migrations`, reindexing, async_foreign_key,  sync_index and partition_manager_sync_partitions features and tasks in `<%= env %>` environment.

        Thanks!
        ```
<%- end %>

<%- if production %>
1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: End of maintenance from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - [ ] Click "Finish Maintenance" and send the following:
       - Message:
          ```text
          GitLab.com scheduled maintenace for the <%= src_cluster.upcase %> and <%= dst_cluster.upcase %> database layers is complete. We'll continue to monitor the platform to ensure all systems are functioning correctly. Thank you for your patience.
          ```
    - [ ] Check @gitlab retweeted from @gitlabstatus
<%- end %>

1. [ ] ☎ {+ Comms-Handler +}: Inform `@release-managers` at #g_delivery about the end of the operational lock 
    ```text
    Hi @release-managers :waves:,

    We are reaching out to inform that we have completed the work for the <%= gitlab_com_cr %> CR in our `<%= env %>` SaaS environment. We are closing the operational block for deployments in the `<%= src_cluster.upcase %>` and `<%= dst_cluster.upcase %>` database, so regular deployment operations can be fully resumed.
    ```

1. [ ] 🔪 {+ Playbook-Runner +}: Open a separate issue to create/rebuild the <%= dst_cluster.upcase %> DR Archive and Delayed replicas. It will be completed in the next couple of working days.

1. [ ] 🐺 {+ Coordinator +}: Mark the change request as `/label ~"change::complete"`

</details>

# Rollback

<details>
  <summary>

  - [ ] __Rollback (if required)__

  </summary>

<%- if production %>
1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post an update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message:

        ```text
        Due to an issue during the planned maintenance for the database layer, we have initiated a rollback of the <%= src_cluster.upcase %> and <%= dst_cluster.upcase %> database layers and some performance impact still might be expected. We will provide update once the rollback process is completed.
        ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Ask the IMOC or the Head Honcho if this message should be sent to any slack rooms:
   - `#whats-happening-at-gitlab`
   - `#infrastructure-lounge` (cc `@sre-oncall`)
   - `#g_delivery` (cc `@release-managers`)
<%- end %>

- There will be **no rollback after closing the rollback window**!

1. [ ] 🔪 {+ Playbook-Runner +}: Monitor what pgbouncer pool has connections [monitoring_pgbouncer_gitlab_user_conns][monitoring_pgbouncer_gitlab_user_conns]

##### ROLLBACK – execute!
Goal: __Set <%= env %>-<%= src_cluster %> cluster as Primary cluster__

1. [ ] 🔪 {+ Playbook-Runner +}: Execute `switchover_rollback.yml` playbook to rollback to <%= src_cluster.upcase %> cluster:

      ```sh
      # TODO - make inventory name fully dynamic
      cd ~/src/db-migration/pg-physical-to-logical
      ansible-playbook \
        -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" <%- if dst_cluster == 'registry' %>-e "pg_source_dbname=gitlabhq_registry"<%- end %>\
        -i inventory/<%= env %>-sec-decomp.yml \
        switchover_rollback.yml -e "force_mode=true" 2>&1 \
      | ts | tee -a ansible_switchover_rollback_<%= env %>_<%= dst_cluster %>_$(date +%Y%m%d).log
      ```

1. [ ] 🔪 {+ Playbook-Runner +}: Check WRITES going to the SOURCE cluster, `<%= src_cluster_prefix %>`: [monitoring_user_tables_writes][monitoring_user_tables_writes]

1. [ ] 🔪 {+ Playbook-Runner +}: Check READS going to the SOURCE cluster, `<%= src_cluster_prefix %>`: [monitoring_user_tables_reads][monitoring_user_tables_reads]. 

1. [ ] 🔪 {+ Playbook-Runner +}: On the TARGET cluster <%= src_cluster_prefix %> Leader/Writer, drop subscription (if still existing) for logical replication:
    - [ ] Check if the subscription still exist:
      ```sh
      gitlab-psql \
          -Xc "select subname, subenabled, subconninfo, subslotname, subpublications from pg_subscription" 
      ```
1. [ ] 🔪 {+ Playbook-Runner +}: On the SOURCE cluster <%= dst_cluster_prefix %>  Leader/Writer, drop publication and logical_replication_slot for reverse replication:
    - [ ] Check if the publication and replication slots still exist:
      ```sh
      gitlab-psql \
        -Xc "select pubname from pg_publication" \
        -Xc "select slot_name, plugin, slot_type, active from pg_replication_slots"
      ```

1. [ ] 🔪 {+ Playbook-Runner +}: Shutdown the TARGET <%= env %>-base-db-<%= dst_cluster_prefix %> cluster to avoid any risk of splitbrain:
      ``` 
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo systemctl stop patroni"
      ```

### Complete the rollback

<%- unless @no_qa_cluster_list.include?(dst_cluster)  %>
1. [ ] 🔪 {+ Quality On-Call +}: Confirm that our smoke tests are still passing (continue the rollback as this might take an hour...)
<%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: Revert all the applied MRs (the amount of MRs is variable depending on where the CR failed)

1. [ ] 🔪 {+ Playbook-Runner +}: Re-enable Chef in all <%= env %>-<%= src_cluster %> nodes:
    ```sh
    knife ssh "role:<%= env %>-base-db-<%= src_cluster_prefix %>" "sudo chef-client-enable"

1. [ ] 🔪 {+ Playbook-Runner +}: Confirm chef-client is ENABLED in all nodes [monitoring_chef_client_enabled][monitoring_chef_client_enabled]

1. [ ] 🔪 {+ Playbook-Runner +}: Run chef-client on Patroni Nodes:
      ```sh
      knife ssh "role:<%= env %>-base-db-<%= src_cluster_prefix %>" "sudo chef-client"
      ```

1. [ ] 🔪 {+ Playbook-Runner +}: Confirm no errors while running chef-client [monitoring_chef_client_error][monitoring_chef_client_error]

<%- if production %>
1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - [ ] Click "Finish Maintenance" and send the following:
       - Message:

          ```text
          GitLab.com rollback for the database layer is complete, and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.
          ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send the following message to slack rooms:

   ```text
   GitLab.com rollback for the database layer is complete and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.
   ```
   - [ ] `#whats-happening-at-gitlab`
   - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
   - [ ] `#g_delivery` (cc `@release-managers`)
<%- end %>


<%- if block_ddl_ff %>
1. [ ] 🔪 {+ Playbook-Runner +}: Enable feature flags by typing the following into `#production`:
  <%- if production %>
    - PRODUCTION:
      1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags false`
  <%- else %>
    - STAGING:
      1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags false --staging`
  <%- end %>
 
    1. [ ] ☎ {+ Comms-Handler +}: Inform the database team that the CR is completed at #g_database:
        ```text
        Hi @gl-database,

        We are reaching out to inform that we have aborted and rolled back the <%= gitlab_com_cr %> CR. Therefore we have re-enabled the `execute_batched_migrations_on_schedule`, `execute_background_migrations`, reindexing, async_foreign_key,  sync_index and partition_manager_sync_partitions features and tasks in `<%= env %>` environment.

        Thanks!
        ```
<%- end %>

1. [ ] ☎ {+ Comms-Handler +}: Inform `@release-managers` at #g_delivery about the end of the operational lock 
    ```text
    Hi @release-managers :waves:,

    We are reaching out to inform that we have aborted and rolled back the <%= gitlab_com_cr %> CR in our `<%= env %>` SaaS environment. We are closing the operational block for deployments in the `<%= src_cluster.upcase %>` and `<%= dst_cluster.upcase %>` databases, so regular deployment operations can be fully resumed.
    ```

1. [ ] 🔪 {+ Playbook-Runner +}: Check if the underlying DDL lock FF is DISABLED:
  <%- if production %>
    - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags` should return DISABLED
  <%- else %>
    - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags --staging` should return DISABLED
  <%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: On two nodes, console and target leader, remove the private keys temporarily placed in `~dbupgrade/.ssh`:
    ```shell
    rm ~dbupgrade/.ssh/id_rsa
    rm ~dbupgrade/.ssh/id_dbupgrade
    ```

<%- if production %>
1. [ ] 🔪 {+ Playbook-Runner +}: ADD the following silences at <https://alerts.gitlab.net> to silence `WALGBaseBackup` alerts in <%= dst_cluster_prefix %> for 2 weeks (14 days = 336 hours)
    - Start time: `<%= switch_start_ptime.strftime('%FT%T.000Z') %>`
    - Duration: `336h`
      - [ ] `env="gprd"`
      - [ ] `type="<%= env %>-<%= dst_cluster_prefix %>"`
      - [ ] `alertname=~"WALGBaseBackupFailed|walgBaseBackupDelayed"`
<%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: UPDATE the following silence at <https://alerts.gitlab.net> to silence alerts in V16 nodes for 2 weeks (14 days = 336 hours):
    - Start time: `<%= switch_start_ptime.strftime('%FT%T.000Z') %>`
    - Duration: `336h`
    - Matcher:
      <%- if production %>
      - __PRODUCTION__
        - [ ] `env="gprd"`
        - [ ] `fqdn=~"<%= dst_cluster_prefix %>.*"`
      <%- else %>
      - __STAGING__
        - [ ] `env="gstg"`
        - [ ] `fqdn=~"<%= dst_cluster_prefix %>.*"`
      <%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: DELETE the following silences at <https://alerts.gitlab.net> 
    - Matcher:
      <%- if production %>
      - __PRODUCTION__
        - [ ] `env="gprd"`
        - [ ] `fqdn=~"<%= src_cluster_prefix %>.*"`
      <%- else %>
      - __STAGING__
        - [ ] `env="gstg"`
        - [ ] `fqdn=~"<%= src_cluster_prefix %>.*"`
      <%- end %>

<%- if production %>
1. [ ] 🐺 {+ Coordinator +}: Check if gitlab_maintenance_mode is DISABLED for <%= env %> [monitoring_gitlab_maintenance_mode][monitoring_gitlab_maintenance_mode]
    - If is not disabled ask 🔪 {+ Playbook-Runner +} to manually disable it by:
      - SSH to a console VM in `<%= env %>` (eg. `ssh console-01-sv-<%= env %>.c.<%= gcp_project %>.internal` )
        - Set `gitlab_maintenance_mode=0` on node exporter :
          ```sh
          sudo su -
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 0\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          cat /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          atq
          ```
<%- end %>

1. [ ] 🐺 {+ Coordinator +}: Mark the <%= gitlab_com_cr %> change request as `/label ~"change::aborted"`


</details>

## Extra details
### In case the Playbook-Runner is disconnected
As most of the steps are executed in a tmux session owned by the
Playbook-Runner role we need a safety net in case this person loses their
internet connection or otherwise drops off half way through. Since other
SREs/DBREs also have root access on the console node where everything is
running they should be able to recover it in different ways. We tested the
following approach to recovering the tmux session, updating the ssh agent and
taking over as a new ansible user.

- `ssh host`
- Add your public SSH key to `/home/PREVIOUS_PLAYBOOK_USERNAME/.ssh/authorized_keys`
- `sudo chef-client-disable <%= gitlab_com_cr %>` so that we don't override the above
- `ssh -A PREVIOUS_PLAYBOOK_USERNAME@host`
- `echo $SSH_AUTH_SOCK`
- `tmux attach -t 0`
- `export SSH_AUTH_SOCK=<VALUE from previous SSH_AUTH_SOCK output>`
- `<ctrl-b> :`
- `set-environment -g 'SSH_AUTH_SOCK' <VALUE from previous SSH_AUTH_SOCK output>`
- `export ANSIBLE_REMOTE_USER=NEW_PLAYBOOK_USERNAME`
- `<ctrl-b> :`
- `set-environment -g 'ANSIBLE_REMOTE_USER' <your-user>`
