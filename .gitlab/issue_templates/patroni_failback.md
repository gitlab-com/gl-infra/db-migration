# Failback, discarding changes made to GCP

Since staging is multi-use and we want to run the failover multiple times, we
need these steps anyway.

In the event of discovering a problem doing the failover on GitLab.com "for real"
(i.e. before opening it up to the public), it will also be super-useful to have
this documented and tested.

The priority is to get the site working again as quickly as possible.

## Fail back to the repmgr database cluster
TBD
## Reconfigure the application to use the repmgr database cluster.
TBD
/label ~"Failover Execution"
