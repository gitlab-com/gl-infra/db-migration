<%- if false %>
<!--
This template has lots of ERB tags so it's not meant to be used verbatim to create an issue. To create
the markdown to copy into an issue, you run it like this (times are in UTC):

# imoc = https://gitlab.pagerduty.com/schedules-new - "Infra Escalation"
# cmoc = https://gitlab.pagerduty.com/schedules-new - "Incident Management - CMOC"
# eoc = https://gitlab.pagerduty.com/schedules-new - "SRE 8-hour"
# quality = https://gitlab.com/gitlab-org/quality/pipeline-triage#dri-weekly-rotation-schedule

  erb \
    pcl_start_time="2024-10-25 23:00" \
    upgrade_start_time="2024-10-26 06:00" \
    switchover_start_time="2024-10-27 06:00" \
    pcl_end_time="2024-10-29 11:00" \
    production=1 \
    cluster=ci \
    -T- pg_upgrade.md

For examples:

  erb \
    pcl_start_time="2024-10-25 23:00" \
    upgrade_start_time="2024-10-26 06:00" \
    switchover_start_time="2024-10-27 06:00" \
    pcl_end_time="2024-10-29 11:00" \
    production=1 \
    cluster=ci \
    video_call="TBD" \
    gitlab_com_cr="https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18593" \
    post_upgrade_mr="https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/5056" \
    post_upgrade_monitoring_mr="https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/9546" \
    post_switchover_mr="https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/merge_requests/5057" \
    post_switchover_monitoring_mr="https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/9297" \
    teleport_db_endpoint_mr="https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/6030" \
    console_config_endpoint_mr="https://gitlab.com/gitlab-com/gl-infra/production/-/issues/18639" \
    destroy_old_cluster_mr="TODO" \
    -T- pg_upgrade.md | pbcopy

NOTE:
  1. You'll need to have the activesupport gem installed: `gem install activesupport`
  2. To save the contents in your clipboard, you can pipe the output to `pbcopy` on OS/X or
     `xclip -selection clipboard` on Linux (assuming you have it installed).
-->
<%- end %>
<%
  unless defined?(production)
    raise "you must specify either 'production=0' (i.e., staging) or 'production=1'"
  end

  unless defined?(cluster)
    raise "you must specify cluster name (e.g., 'cluster=main' or 'cluster=registry')"
  end

  unless ["0", "1"].include?(production)
    raise "production must be either 0 or 1"
  end

  unless defined?(post_upgrade_mr)
    post_upgrade_mr = '{+ TODO +}'
  end

  unless defined?(post_upgrade_monitoring_mr)
    post_upgrade_monitoring_mr = '{+ TODO +}'
  end

  unless defined?(post_switchover_mr)
    post_switchover_mr = '{+ TODO +}'
  end

  unless defined?(post_switchover_monitoring_mr)
    post_switchover_monitoring_mr = '{+ TODO +}'
  end
  
  unless defined?(teleport_db_endpoint_mr)
    teleport_db_endpoint_mr = '{+ TODO +}'
  end

  unless defined?(console_config_endpoint_mr)
    console_config_endpoint_mr = '{+ TODO +}'
  end

  unless defined?(destroy_old_cluster_mr)
    destroy_old_cluster_mr = '{+ TODO +}'
  end

  unless defined?(gitlab_com_cr)
    gitlab_com_cr = '{+ TODO +}'
  end
  production = production.to_i == 0 ? false : true

  if production
    env = 'gprd'
    gcp_project = 'gitlab-production'
    case cluster
     when "main"
      src_cluster_prefix = 'patroni-main-v14'
      dst_cluster_prefix = 'patroni-main-v16'
      src_cluster_host_prefix = 'patroni-main-v14-'
      dst_cluster_host_prefix = 'patroni-main-v16-'
      dst_gcs_backup_location = 'https://console.cloud.google.com/storage/browser/gitlab-gprd-postgres-backup/pitr-walg-main-v16'
      dst_backup_host = 'patroni-main-v16-02'
      publication_count_reverse = 4
      block_ddl_ff = 1
    when "ci"
      src_cluster_prefix = 'patroni-ci-v14'
      dst_cluster_prefix = 'patroni-ci-v16'
      src_cluster_host_prefix = 'patroni-ci-v14-'
      dst_cluster_host_prefix = 'patroni-ci-v16-'
      dst_gcs_backup_location = 'https://console.cloud.google.com/storage/browser/gitlab-gprd-postgres-backup/pitr-walg-ci-v16'
      dst_backup_host = 'patroni-ci-v16-02'
      publication_count_reverse = 4
      block_ddl_ff = 1
    when "registry"
      src_cluster_prefix = 'patroni-registry-v14'
      dst_cluster_prefix = 'patroni-registry-v16'
      src_cluster_host_prefix = 'patroni-registry-v14-'
      dst_cluster_host_prefix = 'patroni-registry-v16-'
      dst_gcs_backup_location = 'https://console.cloud.google.com/storage/browser/gitlab-gprd-postgres-backup/pitr-walg-registry-v16'
      dst_backup_host = 'patroni-registry-v16-02'
      publication_count_reverse = 1
    end
  else
    env = 'gstg'
    gcp_project = 'gitlab-staging-1'
    case cluster
    when "main"
      src_cluster_prefix = 'patroni-main-v14'
      dst_cluster_prefix = 'patroni-main-v16'
      src_cluster_host_prefix = 'patroni-main-v14-1'
      dst_cluster_host_prefix = 'patroni-main-v16-'
      dst_gcs_backup_location = 'https://console.cloud.google.com/storage/browser/gitlab-gstg-postgres-backup/pitr-walg-main-v16'
      dst_backup_host = 'patroni-main-v16-02'
      publication_count_reverse = 4
      block_ddl_ff = 1
    when "ci"
      src_cluster_prefix = 'patroni-ci-v14'
      dst_cluster_prefix = 'patroni-ci-v16'
      src_cluster_host_prefix = 'patroni-ci-v14-1'
      dst_cluster_host_prefix = 'patroni-ci-v16-'
      dst_gcs_backup_location = 'https://console.cloud.google.com/storage/browser/gitlab-gstg-postgres-backup/pitr-walg-ci-v16'
      dst_backup_host = 'patroni-ci-v16-02'
      publication_count_reverse = 4
      block_ddl_ff = 1
    when "registry"
      src_cluster_prefix = 'patroni-registry-v14'
      dst_cluster_prefix = 'patroni-registry-v16'
      src_cluster_host_prefix = 'patroni-registry-v14-'
      dst_cluster_host_prefix = 'patroni-registry-v16-'
      dst_gcs_backup_location = 'https://console.cloud.google.com/storage/browser/gitlab-gstg-postgres-backup/pitr-walg-registry-v16'
      dst_backup_host = 'patroni-registry-v16-02'
      publication_count_reverse = 1
    end
  end

  case cluster
  when "main"
    prometheus_type = 'patroni'
    consul_service_name = 'patroni'
    consul_replica_endpoint = 'db-replica'
    prod_db_name = 'gitlabhq_production'
  when "ci"
    prometheus_type = 'patroni-ci'
    consul_service_name = 'patroni-ci'
    consul_replica_endpoint = 'ci-db-replica'
    prod_db_name = 'gitlabhq_production'
  when "registry"
    prometheus_type = 'patroni-registry'
    consul_service_name = 'patroni-registry'
    consul_replica_endpoint = 'registry-db-replica'
    prod_db_name = 'gitlabhq_registry'
  end

  require 'time'
  require 'active_support'
  require 'active_support/time'
  
  pcl_start_ptime = Time.strptime(pcl_start_time + ' +0000', "%Y-%m-%d %H:%M %z")
  upgd_start_ptime = Time.strptime(upgrade_start_time + ' +0000', "%Y-%m-%d %H:%M %z")
  switch_start_ptime = Time.strptime(switchover_start_time + ' +0000', "%Y-%m-%d %H:%M %z")
  pcl_end_ptime = Time.strptime(pcl_end_time + ' +0000', "%Y-%m-%d %H:%M %z")

  pcl_start_time_str = pcl_start_time + ' UTC'
  upgd_start_time_str = upgrade_start_time + ' UTC'
  switch_start_time_str = switchover_start_time + ' UTC'
  pcl_end_time_str = pcl_end_time + ' UTC'
  
  window_in_hours = ((pcl_end_ptime - switch_start_ptime) / 3600).to_i

  @no_qa_cluster_list = ['registry','embedding']

  TBD_placeholder ||= "__[TBD - It is a placeholder for now. We will add step(s)/link(s)/description here if necessary]__"

  # This is the IP of the VM used to run "gitlab-qa" against staging/production
  qa_vm_ip = '104.196.188.184' #  Nik: do we really needed this?

  # Using define_method instead of def so we can access the variables in the outer scope
  define_method("role") do |name|
    unless binding.local_variable_defined?(name)
      return ""
    end

    binding.local_variable_get(name).split(",").map do |name|
      "@" + name
    end.join(" ")
  end

  # Similarly we use define_method here (instead of "def") so we can access the variables in the outer scope
  define_method("partial") do |file, vars={}, indent_spaces=0|
    erbvar = '_erbout' + rand(1000).to_s

    b = binding
    vars.each do |k, v|
      b.local_variable_set k.to_sym, v
    end

    ' ' * indent_spaces + ERB.new(File.read(file), nil, '-', erbvar).result(b).split("\n").join("\n" + ' ' * indent_spaces)
  end
-%>

# Postgres Upgrade <%= cluster.upcase %> database in <%= env.upcase %>

Link to gitlab.com CR: <<%= gitlab_com_cr %>>

## Postgres Upgrade Rollout Team

| Role                              | Assigned To                        |
| --------------------------------- | ---------------------------------- |
| :wolf: Coordinator                | <%= role('coordinator') %>         |
| :knife: Playbook-Runner           | <%= role('playbook_runner') %>     |
| :telephone: Comms-Handler         | <%= role('comms_handler') %>       |
| :trophy: Quality                  | <%= role('quality') %>             |
| :ambulance: EOC                   | <%= role('eoc') %>                 |
| :fire_engine: IMOC                | <%= role('imoc') %>                |
| :mega: CMOC                       | Check CMOC escalation table bellow |
| :flashlight: Database Maintainers | <%= role('db_mantainer') %>        |
| :floppy_disk: Database Escalation | <%= role('db_escalation') %>       |
| :truck: Delivery Escalation       | <%= role('delivery_escalation') %> |
| :tophat: Head Honcho              | <%= role('head_honcho') %>         |

##### :mega: CMOC Escalation Table

Important: Just for when each window begins - else ping `@cmoc` on Slack

| Date and Step                             | Assigned To |
| ----------------------------------------- | ----------- |
| <%= pcl_start_time_str %> - PCL start     | {+ TBD +}   |
| <%= upgd_start_time_str %> - Upgrade      | {+ TBD +}   |
| <%= switch_start_time_str %> - Switchover | {+ TBD +}   |
| <%= pcl_end_time_str %> - PCL finish      | {+ TBD +}   |

## Collaboration

During the change window, the rollout team will collaborate using the following communications channels:

| App | Direct Link |
|---|---|
| Slack | [#g_infra_database_reliability](https://gitlab.slack.com/archives/C02K0JTKAHJ) |
| Video Call | <%= video_call ||= "<LINK_TO_VIDEO_CALL>" %> |

# Immediately

Perform these steps when the issue is created.

- [ ] 🐺 {+ Coordinator +}: Fill out the names of the rollout team in the table above.

# Support Options

| Provider | Plan | Details | Create Ticket |
|----------|------|---------|---------------|
| __Google Cloud Platform__ | [Gold Support](https://cloud.google.com/support/?options=premium-support#options) | 24x7, email & phone, 1hr response on critical issues | [__Create GCP Support Ticket__](https://enterprise.google.com/supportcenter/managecases) |

# Entry points

| Entry point                | Before                                                               | Blocking mechanism                                             | Allowlist | QA needs                                                                    | Notes                                                                                               |
| -------------------------- | -------------------------------------------------------------------- | -------------------------------------------------------------- | --------- | --------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------- |
| Pages                      | Available via *.gitlab.io, and various custom domains                | Unavailable if GitLab.com goes down for a brief time. There is a cache but it will expire in `gitlab_cache_expiry` minutes  | N/A       | N/A                                                                         |
|                                                                                                     |

# Database hosts

- [Staging replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gstg&var-prometheus=Global&var-prometheus_app=Global)
- [Production replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gprd&var-prometheus=Global&var-prometheus_app=Global)

# Accessing the rails and database consoles

<%- if production %>
## Production
- rails: `ssh $USER-rails@console-01-sv-gprd.c.gitlab-production.internal`
- main db replica: `ssh $USER-db@console-01-sv-gprd.c.gitlab-production.internal`
- main db primary: `ssh $USER-db-primary@console-01-sv-gprd.c.gitlab-production.internal`
- ci db replica: `ssh $USER-db-ci@console-01-sv-gprd.c.gitlab-production.internal`
- ci db primary: `ssh $USER-db-ci-primary@console-01-sv-gprd.c.gitlab-production.internal`
- main db psql: `ssh -t patroni-main-v14-02-db-gprd.c.gitlab-production.internal sudo gitlab-psql`
- ci db psql: `ssh -t patroni-ci-v14-02-db-gprd.c.gitlab-production.internal sudo gitlab-psql`
- registry db psql: `ssh -t patroni-registry-v14-01-db-gprd.c.gitlab-production.internal sudo gitlab-psql`
<%- else %>
## Staging
- rails: `ssh $USER-rails@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db replica: `ssh $USER-db@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db primary: `ssh $USER-db-primary@console-01-sv-gstg.c.gitlab-staging-1.internal`
- ci db replica: `ssh $USER-db-ci@console-01-sv-gstg.c.gitlab-staging-1.internal`
- ci db primary: `ssh $USER-db-ci-primary@console-01-sv-gstg.c.gitlab-staging-1.internal`
- main db psql: `ssh -t patroni-main-v14-102-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`
- ci db psql: `ssh -t patroni-ci-v14-102-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`
- registry db psql: `ssh -t patroni-registry-v14-01-db-gstg.c.gitlab-staging-1.internal sudo gitlab-psql`
<%- end %>

# Dashboards and debugging
These dashboards might be useful during the rollout:

<%- if production %>
## Production
- [PostgreSQL replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1)
- [Triage overview](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s&var-env=gprd&var-environment=gprd&var-prometheus=prometheus-01-inf-gprd&var-prometheus_app=prometheus-app-01-inf-gprd&var-backend=All&var-type=All&var-stage=main)
- [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-24h&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main)
- [Sentry - includes application errors](https://sentry.gitlab.net/gitlab/gitlabcom/)
  - Workhorse: <https://sentry.gitlab.net/gitlab/gitlab-workhorse-gitlabcom/>
  - Rails (backend): <https://sentry.gitlab.net/gitlab/gitlabcom/>
  - Rails (frontend): <https://sentry.gitlab.net/gitlab/gitlabcom-clientside/>
  - Gitaly (golang): <https://sentry.gitlab.net/gitlab/gitaly-production/>
  - Gitaly (ruby): <https://sentry.gitlab.net/gitlab/gitlabcom-gitaly-ruby/>
- [Logs (Kibana)](https://log.gprd.gitlab.net/app/kibana)
<%- else %>
## Staging
- [PostgreSQL replication overview](https://dashboards.gitlab.net/d/000000244/postgresql-replication-overview?orgId=1&var-environment=gstg&var-prometheus=Global&var-prometheus_app=Global)
- [Triage overview](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s&var-env=gstg&var-environment=gstg&var-prometheus=prometheus-01-inf-gstg&var-prometheus_app=prometheus-app-01-inf-gstg&var-backend=All&var-type=All&var-stage=main)
- [Sidekiq overview](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1&from=now-24h&to=now&var-PROMETHEUS_DS=Global&var-environment=gstg&var-stage=main)
- [Sentry - includes application errors](https://sentry.gitlab.net/gitlab/staginggitlabcom/)
- [Logs (Kibana)](https://nonprod-log.gitlab.net/app/kibana)
<%- end %>

[monitoring_pgbouncer_gitlab_user_conns]: https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pc6%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28pgbouncer_pools_client_active_connections%7Benv%3D%5C%22<%= env %>%5C%22,%20user%3D%5C%22gitlab%5C%22,%20type%3D~%5C%22<%= prometheus_type %>%7C<%= prometheus_type %>-v16%5C%22%7D%29%20by%20%28fqdn,database%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1

[monitoring_chef_client_enabled]: https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28chef_client_enabled%7Benv%3D%5C%22<%= env %>%5C%22,type%3D~%5C%22<%= prometheus_type %>%7C<%= prometheus_type %>-v16%5C%22%7D%29%20by%20%28fqdn%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-15m%22,%22to%22:%22now%22%7D%7D%7D&orgId=1

[monitoring_chef_client_last_run]: https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22min%28%28time%28%29-chef_client_last_run_timestamp_seconds%7Benv%3D%5C%22<%= env %>%5C%22,type%3D~%5C%22<%= prometheus_type %>%7C<%= prometheus_type %>-v16%5C%22%7D%29%2F60%29%20by%20%28fqdn%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1

[monitoring_chef_client_error]: https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28chef_client_error%7Benv%3D%5C%22<%= env %>%5C%22,type%3D~%5C%22<%= prometheus_type %>%7C<%= prometheus_type %>-v16%5C%22%7D%29%20by%20%28fqdn%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1

[monitoring_snapshot_last_run]: https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22gitlab_job_success_timestamp_seconds%7Bexported_job%3D%5C%22patroni-gcs-snapshot%5C%22,%20env%3D%5C%22<%= env %>%5C%22,%20type%3D%5C%22<%= prometheus_type %>%5C%22%7D%20%3E%20gitlab_job_start_timestamp_seconds%7Bexported_job%3D%5C%22patroni-gcs-snapshot%5C%22,%20env%3D%5C%22<%= env %>%5C%22,%20type%3D%5C%22<%= prometheus_type %>%5C%22%7D%20and%20on%20%28exported_job,%20env,%20type%29%20gitlab_job_failed%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-3h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1

[monitoring_user_tables_writes]: https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22sum%28increase%28pg_stat_user_tables_n_tup_ins%7Benv%3D%5C%22<%= env %>%5C%22,datname%3D%5C%22<%= prod_db_name %>%5C%22,type%3D~%5C%22<%= prometheus_type %>%7C<%= prometheus_type %>-v16%5C%22%7D%5B1m%5D%29%29%20by%20%28instance%29%5Cn%2B%5Cnsum%28increase%28pg_stat_user_tables_n_tup_del%7Benv%3D%5C%22<%= env %>%5C%22,datname%3D%5C%22<%= prod_db_name %>%5C%22,type%3D~%5C%22<%= prometheus_type %>%7C<%= prometheus_type %>-v16%5C%22%7D%5B1m%5D%29%29%20by%20%28instance%29%5Cn%2B%5Cnsum%28increase%28pg_stat_user_tables_n_tup_upd%7Benv%3D%5C%22<%= env %>%5C%22,datname%3D%5C%22<%= prod_db_name %>%5C%22,type%3D~%5C%22<%= prometheus_type %>%7C<%= prometheus_type %>-v16%5C%22%7D%5B1m%5D%29%29%20by%20%28instance%29%5Cnand%20on%28instance%29%20pg_replication_is_replica%3D%3D0%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1

[monitoring_user_tables_reads]: https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22%28sum%28rate%28pg_stat_user_tables_seq_tup_read%7Benv%3D%5C%22<%= env %>%5C%22,datname%3D%5C%22<%= prod_db_name %>%5C%22,type%3D~%5C%22<%= prometheus_type %>%7C<%= prometheus_type %>-v16%5C%22%7D%5B1m%5D%29%29%20by%20%28fqdn%29%29%20and%20on%28fqdn%29%20pg_replication_is_replica%3D%3D1%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D%7D&orgId=1

[monitoring_gitlab_maintenance_mode]: https://dashboards.gitlab.net/explore?schemaVersion=1&panes=%7B%22pum%22:%7B%22datasource%22:%22mimir-gitlab-<%= env %>%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22max%28gitlab_maintenance_mode%7Benv%3D%5C%22<%= env %>%5C%22%7D%29%22,%22range%22:true,%22instant%22:true,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mimir-gitlab-<%= env %>%22%7D,%22editorMode%22:%22code%22,%22legendFormat%22:%22__auto%22%7D%5D,%22range%22:%7B%22from%22:%22now-15m%22,%22to%22:%22now%22%7D%7D%7D&orgId=1

# Repos used during the rollout
The following Ansible playbooks are referenced throughout this issue:
- Postgres Upgrade, Switchover & Rollback: <https://gitlab.com/gitlab-com/gl-infra/db-migration/-/tree/master/pg-upgrade-logical>

---

# High level overview

This gives an high level overview on the procedure.

<details><summary>Upgrade Flowchart</summary>

```mermaid
flowchart TB
    subgraph Prepare new environment
    A[Create new cluster $TARGET as a carbon copy of the one to upgrade, $SOURCE] --> B
    B[Attach $TARGET as a standby-only-cluster to $SOURCE via physical replication] --> C
    end
    C[Make sure both clusters are in sync] --> D1
    subgraph Upgrade: ansible-playbook upgrade.yml
    D1[Disable Chef] --> D2
    D2[Perform clean shutdown of $TARGET] --> D3
    D3[On $SOURCE, create a replication slot and publication FOR ALL TABLES; remember its LSN] --> D4
    D4[Configure recovery_target_lsn on $TARGET] --> D5
    D5[Start $TARGET] --> D6
    D6[Let $TARGET reach the slot's LSN, still using physical replication] --> D7
    D7[Once slot's LSN is reached, promote $TARGET leader] --> D8
    D8[Upgrade $TARGET to new version using pg_upgrade and rsync] --> D9
    D9[Create logical subscription with copy_data=false] --> D10
    D10[Let $TARGET catch up using logical replication] --> H
    end
    subgraph Prepare switchover
    H[Ensure that all replication lags are close to zero] --> I
    I[Merge Chef MRs so $TARGET uses roles for new PostgreSQL version] --> K
    K[Enable Chef, run Chef-Client] --> L
    L[Make sure Chef finished sucessfully and cluster is still operational] --> M
    M[Disable Chef again] --> N
    end
    N[Metrics and confirmation checks are as expected] --> O
    subgraph Switchover: ansible-playbook switchover.yml
    O[Redirect RO traffic to $TARGET standbys in addition to $SOURCE] --> P
    P[Check if cluster is operational and metrics are normal] --"Normal"--> Q
    P --"Abnormal"-->GR
    Q[Redirect RO only to $TARGET] --> R
    R[Check if cluster is operational and metrics are normal] --"Normal"--> S
    R --"Abnormal"--> GR
    S[DBRE verify E2E tests run as expected with Quality help] --"Normal"--> T
    S --"Abnormal"-->GR
    end
    T[Switchover: Redirect RW traffict to $TARGET] --> U1
    subgraph Post Switchover Verification
    U1[Check if cluster is operational and metrics are normal]--"Normal"--> U2
    U1 --"Abnormal"--> LR
    U2[Enable Chef, run Chef-Client] --"Normal"--> U3
    U2 --"Abnormal"--> LR
    U3[Check if cluster is operational and metrics are normal] --"Normal"--> Success
    U3 --"Abnormal"--> LR
    Success[Success!]
    end
    subgraph GR[Gracefull Rollback - no dataloss]
    GR1[Start gracefull rollback]
    end
    subgraph LR[Fix forward]
    LR1[Fix all issues] -->LR2
    LR2[Return to last failed step]
    end
```

</details>

Playbook source: <https://gitlab.com/gitlab-com/gl-infra/db-migration/-/tree/master/pg-upgrade-logical>

Sketches of the upgrade.yml actions can be found here: [upgrade.yml.pdf](https://gitlab.com/gitlab-com/gl-infra/db-migration/uploads/5d99fd4e9887e69180ed433c2f091cc3/upgrade.yml.pdf)

# Prep Tasks

<details>
  <summary>

  - [ ] __SWITCHOVER minus 1 week (<%= 7.days.ago(switch_start_ptime).strftime('%Y-%m-%d %H:%M') %> UTC)__
  
  </summary>

<%- if production %>
  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on @gitlabstatus. Workflow: https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events
     
      - Message:
        ```text
        Next week, we will be undergoing scheduled maintenance to our <%= cluster.upcase %> database layer. The maintenance will start at <%= switch_start_time_str %> and should finish at <%= pcl_end_time_str %> (including performance regression observability period). GitLab.com will be available during the whole period as the maintenance should be seamless and transparent for the application. We apologize in advance for any inconvenience this may cause. See <<%= gitlab_com_cr %>>
        ```

  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery .
      - Message:
          ```text
          Hi @release-managers :waves:,
          
          We will be undergoing scheduled maintenance to our <%= cluster.upcase %> database layer in `PRODUCTION`. The operational lock and PCL will start at <%= pcl_start_time_str %> and should finish at <%= pcl_end_time_str %> (including performance regression observability period). We would like confirm that deployments that affect <%= cluster.upcase %> database cluster would need to be stopped during the window. All details can be found in the CR. Please be so kind and comment the acknowledgement on <%= gitlab_com_cr %>. :bow:
          ```        
<%- else %>
  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery . Kindly request for approval on this issue. 
      - Message:
          ```text
          Hi @release-managers :waves:,

          We will be undergoing scheduled maintenance to our <%= cluster.upcase %> database layer in `STAGING`. The operational lock will start at <%= pcl_start_time_str %> and should finish at <%= pcl_end_time_str %>  (including performance regression observability period). We would like confirm that deployments that affect <%= cluster.upcase %> database cluster would need to be stopped during the window. All details can be found in the CR. Please be so kind and comment the acknowledgement on <%= gitlab_com_cr %>. :bow:
          ```
<%- end %>

</details>

<details>
  <summary>

  - [ ] __SWITCHOVER minus 3 days (<%= 3.days.ago(switch_start_ptime).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

<%- if production %>
  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on @gitlabstatus. Workflow: https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events
     
      - Message:
        ```text
        In 3 days, we will be undergoing scheduled maintenance to our <%= cluster.upcase %> database layer. The maintenance will start at <%= switch_start_time_str %> and should finish at <%= pcl_end_time_str %> (including performance regression observability period). GitLab.com will be available during the whole period as the maintenance should be seamless and transparent for the application. We apologize in advance for any inconvenience this may cause. See <<%= gitlab_com_cr %>>
        ```

      - [ ] Check @gitlab retweeted from @gitlabstatus

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send on Slack `#whats-happening-at-gitlab`:

      - Message:

        ```text
        :loudspeaker: *Postgres upgrade for our <%= cluster.upcase %> database clusters in our SaaS platform* is scheduled to start at <%= switch_start_time_str %> and should finish at <%= pcl_end_time_str %> (including performance regression observability period) :rocket:
        Taking place in 3 days time! See <<%= gitlab_com_cr %>>

        :hammer_and_wrench: *What to expect?*
        GitLab.com will be available during the whole period as the maintenance should be seamless and transparent for the application. If you experience any issues potentially related to the upgrade in the next few days after the upgrade, please open an issue and reach the upgrade team at slack channel `#g_infra_database_reliability`
        ```

  1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Share message from `#whats-happening-at-gitlab` to the following channels:

      - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
      - [ ] `#g_delivery` (cc `@release-managers`)
      - [ ] `#support_gitlab-com` (Inform Support SaaS team)
          - [ ] Share with team a link to the change request regarding the maintenance
  <%- end %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Check that you have `Maintainer` or `Owner` permission in <https://ops.gitlab.net/gitlab-org/quality> to be able to trigger Smoke QA pipeline in schedules ([Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules), [Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules)). Reach out to Test Platform to get access if you don't have permission to trigger scheduled pipelines in the linked projects.

</details>


<details>
  <summary>

  - [ ] __PCL Start time (<%= pcl_start_time_str %>) - UPGRADE minus <%= ((upgd_start_ptime - pcl_start_ptime) / 3600).to_i  %>  hours__

  </summary>

<%- if production %>
  <%- if block_ddl_ff %>
  1. [ ] 🔪 {+ Playbook-Runner +}: Ensure the CR is reviewed by the 🚑 {+ EOC +}

  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery the operational lock the `<%= cluster.upcase %>` database
      ```text
      Hi @release-managers :waves:,
      As scheduled we started the Deployment Hard PCL and enabled DDL block feature flat for the Upgrade in the <%= cluster.upcase %> database in the <%= env.upcase %> environment, until <%= pcl_end_time_str %>.
      If there’s any incident with potential necessity to revert/apply db-migrations, please reach out @dbre members during the weekend as they are on-call and will evaluate if there will be impact in the upgrade or not.
      All details can be found in the CR - <%= gitlab_com_cr %>. :bow:
      ```

  1. [ ] ☎ {+ Comms-Handler +}: Inform the database at #g_database
      ```text
      Hi @gl-database,

      Please note that we started the operational block for the `<%= cluster.upcase %>` cluster Postgres upgrade, therefore we are blocking database model/structure modifications, by disabling the following tasks(`execute_batched_migrations_on_schedule` and `execute_background_migrations`, reindexing, async_foreign_key, async_index features and partition_manager_sync_partitions) in the <%= env.upcase %> environment.
      We will re-enable DDLs once the CR is finished and the rollback window is closed at <%= pcl_end_time_str %>.

      Thanks!
      ```

  1. [ ] 🔪 {+ Playbook-Runner +}: Disable the DDL-related feature flags:
      1. [ ] Disable feature flags by typing the following into `#production`:
          1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags true`
  <%- else %>
  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery the operational lock the in the `<%= cluster.upcase %>` database
      ```text
      :siren-siren: :siren-siren: :siren-siren:
      Hi @release-managers :waves:,

      Due to a CR of PostgreSQL, please DO NOT merge any update versions on `<%= cluster.upcase %>` database MRs, in the `PRODUCTION` environment, until <%= pcl_end_time_str %>, as we are performing the PostgreSQL engine update on this database.

      Regular auto-deployments can continue in the MAIN and CI databases.

      All details can be found in the CR. Please be so kind and comment the acknowledgement on <%= gitlab_com_cr %>. :bow:
      ```
  <%- end %>
<%- else %>
  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery 
  <%- if block_ddl_ff %>
      ```text
      Hi @release-managers :waves:, 

      We would like to communicate that deployments should be stopped in the STAGING environment, in the next hour, as we should start the database engine upgrade of the <%= cluster.upcase %> PostgreSQL cluster at <%= upgd_start_time_str %> - see <%= gitlab_com_cr %>. :bow:
      ```
  <%- else %>
      ```text
      Hi @release-managers :waves:, 

      We would like to communicate that in about 1 hour we will start the upgrade of the <%= cluster.upcase %> database in the STAGING environment, therefore remind that no MRs should be merged to upgrade this database after <%= upgd_start_time_str %> - see <%= gitlab_com_cr %>. :bow:

      Please note that, auto-deployments and database migrations executions into MAIN and CI can continue and are not affected.
      ```
  <%- end %>
<%- end %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Confirm that QA tests are passing as a pre-upgrade sanity check
<%- unless @no_qa_cluster_list.include?(cluster)  %>
      1. [ ] 🔪 {+ Playbook-Runner +}: Confirm that smoke QA tests are passing on the current cluster by checking latest status for `Smoke` Type tests in <%- if production %>[Production](https://gitlab-qa-allure-reports.s3.amazonaws.com/production-sanity/master/index.html) and [Canary](https://gitlab-qa-allure-reports.s3.amazonaws.com/canary-sanity/master/index.html) <%- else %>[Staging](https://gitlab-qa-allure-reports.s3.amazonaws.com/staging-sanity/master/index.html) and [Staging Canary](https://gitlab-qa-allure-reports.s3.amazonaws.com/staging-canary-sanity/master/index.html) <%- end %> Allure reports listed in [QA pipelines](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/debugging-qa-test-failures/#qa-test-pipelines).
          - [ ] 🔪 {+ Playbook-Runner +}: Trigger Smoke E2E suite against the environment that was upgraded:<%- if production %>[Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules): `Four hourly smoke tests`<%- else %>[Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules): `Four hourly smoke tests`<%- end %>. This has an estimated duration of 15 minutes.
          - [ ] 🔪 {+ Playbook-Runner +}: If the smoke tests fail, we should re-run the failed job to see if it is reproducible. 
          - [ ] 🔪 {+ Playbook-Runner +}: In parallel reach out to [on-call Test Platform DRI](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/oncall-rotation/#schedule) for the help with investigation. If there is no avialable on-call DRI, reach out to `#test-platform` and escalate with the [management team](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/#management-team).
<%- else  %>
      - [ ] 🔪 {+ Playbook-Runner +}: Perform manual test using Gitlab.com Web UI to validate <%- if cluster == "registry" %>Conteiner Registry<%- elsif cluster == "embedding" %>Gitlab Duo Chat (AI)<%- end %> usability. {+ TODO: +} Improve QA on this test.
<%- end %>

## Prepare the environment

  1. [ ] 🔪 {+ Playbook-Runner +}: Check that all needed Chef MRs are rebased and contain the proper changes.
      1. [ ] Post-Upgrade MR, to change v16 Chef Role to use v16 directories, binaries and file locations:
          - MR for <%= dst_cluster_prefix %>: <%= post_upgrade_mr %>
      1. [ ] Post-Upgrade monitoring MR, to configure Monitoring tags, in Mimir/Prometheus for use during switchover:
          - MR for <%= dst_cluster_prefix %>: <%= post_upgrade_monitoring_mr %>
      1. [ ] Post-Switchover MR to configure Consul
          - MR for <%= env %>-patroni-<%= cluster %>: <%= post_switchover_mr %>
      1. [ ] Post-Switchover MR to configure Monitoring tags, in Mimir/Prometheus and update snapshot source
          - MR for <%= env %>-patroni-<%= cluster %>: <%= post_switchover_monitoring_mr %>
      1. [ ] Post-Switchover MR to configure Teleport
          - MR for <%= env %>-patroni-<%= cluster %>: <%= teleport_db_endpoint_mr %>
      1. [ ] Post-Switchover MR to configure Console
          - MR for <%= env %>-patroni-<%= cluster %>: <%= console_config_endpoint_mr %>
 
  1. [ ] 🔪 {+ Playbook-Runner +}: Get the console VM ready for action
      - [ ] SSH to the console VM in `<%= env %>`
      - [ ] Configure dbupgrade user
         - [ ] Disable screen sharing to reduce risk of exposing private key
         - [ ] Change to user dbupgrade `sudo su - dbupgrade`
         - [ ] Copy dbupgrade user's private key from 1Password to `~/.ssh/id_dbupgrade`
         - [ ] `chmod 600 ~/.ssh/id_dbupgrade`
         - [ ] Use key as default `ln -s /home/dbupgrade/.ssh/id_dbupgrade /home/dbupgrade/.ssh/id_rsa`
         - [ ] Repeat the same steps steps on target leader (it also has to have the private key)
         - [ ] Enable re-screen sharing if benficial
      - [ ] Create an [access_token](https://gitlab.com/-/profile/personal_access_tokens) with at least `read_repository` for the next step
      - [ ] Clone repos:

        ```sh
        rm -rf ~/src \
          && mkdir ~/src \
          && cd ~/src \
          && git clone https://gitlab.com/gitlab-com/gl-infra/db-migration.git \
          && cd db-migration \
          && git checkout master
        ```

      - [ ] Ensure you have Ansible installed:

        ```sh
        python3 -m venv ansible
        source ansible/bin/activate
        python3 -m pip install --upgrade pip
        python3 -m pip install ansible
        python3 -m pip install jmespath
        ansible --version
        ```

      - [ ] Ensure that Ansible can talk to all the hosts in <%= env %>-<%= cluster %>
          ```sh
          cd ~/src/db-migration/pg-upgrade-logical
          ansible -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" -i inventory/<%= env %>-<%= cluster %>.yml all -m ping
          ```

      - [ ] In advance, run pre-checks, and upgrade-check, pre-install packages to ensure that everything is ready for future upgrade:
          ```sh
          cd ~/src/db-migration/pg-upgrade-logical
          ansible-playbook \
            -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" \
            -i inventory/<%= env %>-<%= cluster %>.yml \
            upgrade.yml -e "pg_old_version=14 pg_new_version=16<%- if cluster == 'registry' %> pg_source_dbname=gitlabhq_registry<%- end %>" \
            --tags "pre-checks, packages, upgrade-check" 2>&1 \
          | ts | tee -a ansible_upgrade_pre_checks_<%= env %>_<%= cluster %>_$(date +%Y%m%d).log
          ```

        You shouldn't see any failed hosts!

      - [ ] You might have to cleanup the Destination GCS  Backup location to avoid conflicts in `wal-g` (**IMPORTANT: perform this action pairing with another DBRE/SRE** to make sure that the you are deleting the right location)

          - GCS Backup Location: <%= dst_gcs_backup_location %>

1. [ ] 🔪 {+ Playbook-Runner +}: Add the following silences at <https://alerts.gitlab.net> to silence alerts in V16 nodes until 4 hours after the switchover time:
      - Start time: `<%= switch_start_ptime.strftime('%FT%T.000Z') %>`
      - Duration: `<%= ((4.hours.since(switch_start_ptime) - switch_start_ptime) / 3600).to_i %>h`
    - Matchers
        - `env="<%= env %>"`
        - `fqdn=~"<%= dst_cluster_prefix %>.*"`

</details>

<%- if production %>
<details>
  <summary>

  - [ ] __SWITCHOVER minus 24 hours (<%= 24.hours.ago(switch_start_ptime).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  ## Communicate maintenance for tomorrow

  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>

      - Message:
        ```text
        We will be undergoing scheduled switchover of our <%= cluster.upcase %> database to PostgreSQL v16 tomorrow. There may be a performance impact until <%= pcl_end_time_str %>. See <<%= gitlab_com_cr %>>
        ```

      - [ ] Check @gitlab retweeted from @gitlabstatus

</details>

# UPGRADE (<%= upgd_start_time_str %>) - Switchover minus <%= ((switch_start_ptime - upgd_start_ptime) / 3600).to_i  %>  hours

<%- end %>

  1. [ ] 🐺 {+ Coordinator +}: Get a green light from the 🚑 {+ EOC +}

## Postgres Upgrade rollout

<%- unless production %>

  <%- if block_ddl_ff %>
  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery 
      ```text
      Hi @release-managers :waves:, 
      We would like to make sure that deployments have been stopped for our `<%= cluster.upcase %>` database in the `STAGING` environment, until <%= pcl_end_time_str %>. Be aware that we are deactivating certain feature flags during this time. All details can be found in the CR. Please be so kind and comment the acknowledgement on <%= gitlab_com_cr %>. :bow:
      ```

  1. [ ] ☎ {+ Comms-Handler +}: Inform the database team at #g_database
      ```text
      Hi @gl-database,

      Please note that we started the operational block for the `<%= cluster.upcase %>` cluster Postgres upgrade, therefore we are blocking database model/structure modifications, by disabling the following tasks (`execute_batched_migrations_on_schedule` and `execute_background_migrations`, reindexing, async_foreign_key, async_index features and partition_manager_sync_partitions) in the `STAGING` environment.

      We will re-enable DDLs once the CR is finished and the rollback window is closed at <%= pcl_end_time_str %>.

      Thanks!
      ```

  1. [ ] 🔪 {+ Playbook-Runner +}: Disable the DDL-related feature flags:
      1. [ ] Disable feature flags by typing the following into `#production`:
          1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags true --staging`
  
  <%- else %>
  1. [ ] ☎ {+ Comms-Handler +}: Coordinate with `@release-managers` at #g_delivery 
      ```text
      :siren-siren: :siren-siren: :siren-siren:
      Hi @release-managers :waves:,
        
      Due to a CR of PostgreSQL, please DO NOT merge any update versions on `<%= cluster.upcase %>` database MRs, in the `STAGING` environment, until <%= pcl_end_time_str %> as we are performing the PostgreSQL engine update on this database. 

      All details can be found in the CR. Please be so kind and comment the acknowledgement on <%= gitlab_com_cr %>. :bow:
      ```

  <%- end %>
<%- end %>


### Pre Postgres upgrade checks

<%- if block_ddl_ff %>
  1. [ ] 🐺 {+ Coordinator +}: Check if disallow_database_ddl_feature_flags is ENABLED:
  <%- if production %>
      - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags`
  <%- else %>
      - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags --staging`
  <%- end %>
<%- end %>

<%- if production %>
  1. [ ] 🔪 {+ Playbook-Runner +}: ADD the following silences at <https://alerts.gitlab.net> to silence `WALGBaseBackup` alerts in <%= src_cluster_prefix %> until the end of the maintenance:
      - Start time: `<%= upgd_start_ptime.strftime('%FT%T.000Z') %>`
      - Duration: `<%= (((pcl_end_ptime - upgd_start_ptime) / 3600).to_i + 48) %>h`
        - [ ] `env="gprd"`
        - [ ] `type="<%= env %>-<%= src_cluster_prefix %>"`
        - [ ] `alertname=~"WALGBaseBackupFailed|walgBaseBackupDelayed"`

  1. [ ] 🔪 {+ Playbook-Runner +}: ADD the following silences at <https://alerts.gitlab.net> to silence `WALGBaseBackup` alerts in <%= dst_cluster_prefix %> until the end of the maintenance:
      - Start time: `<%= upgd_start_ptime.strftime('%FT%T.000Z') %>`
      - Duration: `<%= (((pcl_end_ptime - upgd_start_ptime) / 3600).to_i + 2) %>h`
        - [ ] `env="gprd"`
        - [ ] `type="<%= env %>-<%= dst_cluster_prefix %>"`
        - [ ] `alertname=~"WALGBaseBackupFailed|walgBaseBackupDelayed"`
<%- end %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Monitor what pgbouncer pool has connections: [monitoring_pgbouncer_gitlab_user_conns][monitoring_pgbouncer_gitlab_user_conns]

  1. [ ] 🔪 {+ Playbook-Runner +}: Check if anyone except application is connected to source primary and interrupt them:
      1. [ ] Login to source primary
          ```sh
          ssh <%= src_cluster_host_prefix %>01-db-<%= env %>.c.<%= gcp_project %>.internal
          ```
      1. [ ] Check all connections that are not `gitlab`:
          ```sh
          gitlab-psql -c "
            select
              pid, client_addr, usename, application_name, backend_type,
              clock_timestamp() - backend_start as connected_ago,
              state,
              left(query, 200) as query
            from pg_stat_activity
            where
              pid <> pg_backend_pid()
              and not backend_type ~ '(walsender|logical replication|pg_wait_sampling)'
              and usename not in ('gitlab', 'gitlab-registry', 'pgbouncer', 'postgres_exporter', 'gitlab-consul')
              and application_name <> 'Patroni'
            "
          ```
      1. [ ] If there are sessions that potentially can perform any writes, spend up to 10 minutes to make an attempt to find the actors and ask them to stop.
      1. [ ] Finally, terminate all the remaining sessions that are not coming from application/infra components and potentially can cause writes:
          ```sh
          gitlab-psql -c "
            select pg_terminate_backend(pid)
            from pg_stat_activity
            where
              pid <> pg_backend_pid()
              and not backend_type ~ '(walsender|logical replication|pg_wait_sampling)'
              and usename not in ('gitlab', 'gitlab-registry', 'pgbouncer', 'postgres_exporter', 'gitlab-consul')
              and application_name <> 'Patroni'
            "
          ```

  ### Postgres Upgrade

  #### UPGRADE – execute!
  
  1. [ ] 🔪 {+ Playbook-Runner +}: Cleanup the Destination GCS  Backup location to avoid conflicts in `wal-g` (**IMPORTANT: perform this action pairing with another DBRE/SRE** to make sure that the you are deleting the right location)

      - GCS Backup Location: <%= dst_gcs_backup_location %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Run Ansible playbook for Upgrading the <%= env %>-<%= cluster %> cluster:  

      ```sh
      cd ~/src/db-migration/pg-upgrade-logical
      ansible-playbook \
        -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" \
        -i inventory/<%= env %>-<%= cluster %>.yml \
        upgrade.yml -e "pg_old_version=14 pg_new_version=16<%- if cluster == 'registry' %> pg_source_dbname=gitlabhq_registry<%- end %>" 2>&1 \
      | ts | tee -a ansible_upgrade_<%= env %>_<%= cluster %>_$(date +%Y%m%d).log
      ```

  ### Post Postgres upgrades verification
  You can execute the following steps as soon as their respective upgrades in the previous step have finished executing.

  1. [ ] 🔪 {+ Playbook-Runner +}: Check logical replication lag, and wait to get in sync: [PG16 Upgrade Dashboard](https://dashboards.gitlab.net/d/postgresql-pg-upgrade/postgresql3a-postgres-upgrade-using-logical?orgId=1&var-PROMETHEUS_DS=mimir-gitlab-<%= env %>>g&var-environment=<%= env %>&var-cluster=<%= cluster %>&var-source_version=14&var-destination_version=16&from=now-1h&to=now&refresh=5m)

  1. [ ] 🔪 {+ Playbook-Runner +}: Merge the MR that update PostgreSQL dirs and binaries references in Chef for <%= dst_cluster_prefix %>. First confirm there are no errors in merge pipeline. If the MR was merged, then revert it, and get it merged.
      1. [ ] MR for <%= dst_cluster_prefix %>: <%= post_upgrade_mr %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Merge the MR that updates monitoring tags for <%= dst_cluster_prefix %>.
      1. [ ] MR for <%= dst_cluster_prefix %>: <%= post_upgrade_monitoring_mr %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Ensure that the changes merged from the previous step have been deployed to the Chef server before re-enabling Chef by confirming the linked `master` pipeline for `ops.gitlab.net` completed successfully.

  1. [ ] 🔪 {+ Playbook-Runner +}: Re-enable Chef in all <%= env %>-<%= cluster %> nodes:
        ```sh
        knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo chef-client-enable"
        ```

  1. [ ] 🔪 {+ Playbook-Runner +}: Confirm chef-client is ENABLED in all nodes [monitoring_chef_client_enabled][monitoring_chef_client_enabled]

  1. [ ] 🔪 {+ Playbook-Runner +}: Run chef-client on Patroni Nodes:
        ```sh
        knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo chef-client"
        ```
        Confirm that chef-client ran on all nodes [monitoring_chef_client_last_run][monitoring_chef_client_last_run]
      - [ ] 🔪 {+ Playbook-Runner +}: Confirm:
        - [ ] No errors while running chef-client [monitoring_chef_client_error][monitoring_chef_client_error]
            ```sh
            knife ssh "roles:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo /usr/lib/postgresql/16/bin/postgres --version"
            ```
            - Output should show `postgres (PostgreSQL) 16.x (Ubuntu 16.x-x.pgdg20.04+1)` for all nodes

        - [ ] Consul service endpoint `<%= consul_replica_endpoint %>-v16.service.consul.` points to v16 nodes, and consul service endpoint `<%= consul_replica_endpoint %>.service.consul.` is pointing to v14 replica nodes.
            ```sh
            dig @127.0.0.1 -p 8600 <%= consul_replica_endpoint %>.service.consul. SRV +short

            dig @127.0.0.1 -p 8600 master.<%= consul_service_name %>.service.consul. SRV +short
            ```

  1. [ ] 🔪 {+ Playbook-Runner +}: Stop chef on both old and new clusters, on all nodes, before we execute switchover
      ```sh
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %> OR role:<%= env %>-base-db-<%= src_cluster_prefix %>" "sudo chef-client-disable <%= gitlab_com_cr %>"
      ```

  1. [ ] 🔪 {+ Playbook-Runner +}: Confirm chef-client is DISABLED [monitoring_chef_client_enabled][monitoring_chef_client_enabled]

  1. [ ] 🔪 {+ Playbook-Runner +}: A KNOWN ISSUE (__TODO__ to improve) – at this point, it is very likely that logical replication is broken because `.pgpass` on **Target Leader** has only 1 line again (chef removed the 2nd line, that is needed to connect to the source leader). Get it back manually – copying the existing line, replacing `localhost` in the beginning with `*`.

  1. [ ] 🔪 {+ Playbook-Runner +}: Restart target patroni cluster nodes if `gitlab-patronictl list` command show `Pending restart` is required. A successful cluster restart will display `Success: restart on member` for each cluster memebers and a subsequent `gitlab-patronictl list` command will not show `Pending restart` required
        <%- if production %>
        ```sh
        sudo gitlab-patronictl list
        sudo gitlab-patronictl restart gprd-patroni-<%= cluster %>-v16 --force
        sudo gitlab-patronictl list
        ```
        <%- else %>
        ```sh
        sudo gitlab-patronictl list
        sudo gitlab-patronictl restart gstg-patroni-<%= cluster %>-v16 --force
        sudo gitlab-patronictl list
        ```    
        <%- end %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Check logical replication lag, and wait to get in sync: [PG16 Upgrade Dashboard](https://dashboards.gitlab.net/d/postgresql-pg-upgrade/postgresql3a-postgres-upgrade-using-logical?orgId=1&var-PROMETHEUS_DS=mimir-gitlab-<%= env %>>g&var-environment=<%= env %>&var-cluster=<%= cluster %>&var-source_version=14&var-destination_version=16&from=now-1h&to=now&refresh=5m)

  1. [ ] 🔪 {+ Playbook-Runner +}: Trigger GCS snapshot on the new v16 Patroni **<%= cluster.upcase %>** cluster:
      - [ ] SSH to `<%= "#{dst_backup_host}-db-#{env}.c.#{gcp_project}.internal" %>`
        - [ ] Run a [manual GCS Snapshot](https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/patroni-ci/rebuild_ci_cluster_from_prod.md#take-a-snapshot-from-the-writer-node)
            ```sh
            sudo su - gitlab-psql
            tmux new -s GCSSnapshot
            /usr/local/bin/gcs-snapshot.sh
            ```

  ### Start data corruption check - pg_amcheck

  1. [ ] 🔪 {+ Playbook-Runner +}: On the v16 **Replica nodes only**, run `pg_amcheck`. Run tmux and as a nohup command):
      - [ ] On all <%= dst_cluster_prefix %> **Replicas (NOT in the Leader)**:
          ```shell
          sudo su - gitlab-psql
          tmux a -t pg_amcheck || tmux new -s pg_amcheck
          export PGOPTIONS="-c statement_timeout=60min"
          export JOBS=8
          cd /tmp; nohup time /usr/lib/postgresql/16/bin/pg_amcheck -p 5432 -h localhost -U gitlab-superuser -d <%= prod_db_name %> -j $JOBS  --verbose -P 2>&1 | tee -a /var/tmp/pg_amcheck.$(date "+%F-%H-%M").log &
          tail -f /var/tmp/pg_amcheck.$(date "+%F-%H-%M").log
          ```
          - [ ] Monitor logical replication lagging, if it seems that the logical replication is throttling, kill pg_amcheck and start it again with a smaller value for `-j`;

          - [ ] **IMPORTANT: make sure you are not running pg_amcheck in the v16 Writer/Primary node**, as this will cause logical replication lag in the target and spikes of rollbacks and errors

          - [ ] **IMPORTANT: if there are any wal-g backup processes running in ANY v16 replica you can terminate/kill it**, as this will cause logical replication lag and very likely will make pg_amcheck to get aborted due to conflict with recovery;

<%- if production %>
</details>

<details>
  <summary>

  - [ ] __UPGRADE + 3 hours onward (<%= 3.hours.since(upgd_start_ptime).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

## Data Corruption and Lagging Check

  1. [ ] 🔪 {+ Playbook-Runner +}: On the v16 Replica nodes, review the pg_amcheck log files created in the previous steps to find out any data corruption errors and check amcheck progress:
      ```shell
      egrep 'ERROR:|DETAIL:|LOCATION:' /var/tmp/pg_amcheck.*.log
      cat /var/tmp/pg_amcheck.*.log | grep relations | tail -1
      ```

  1. [ ] 🔪 {+ Playbook-Runner +}: Monitor logical replication lagging, if it seems that the logical replication is throttling, kill pg_amcheck

<%- end %>

### Pre-Switchover Setup

  1. [ ] 🔪 {+ Playbook-Runner +}: Monitor logical replication lagging, if it seems that the logical replication is throttling, kill pg_amcheck

  1. [ ] 🔪 {+ Playbook-Runner +}: Add the following silences at <https://alerts.gitlab.net> to silence alerts in V14 nodes for 2 weeks after the PCL finishes:
      - Start time: `<%= switch_start_ptime.strftime('%FT%T.000Z') %>`
      - Duration: `<%= ((2.weeks.since(pcl_end_ptime) - switch_start_ptime) / 3600).to_i %>h`
      - Matchers
          - `env="<%= env %>"`
          - `fqdn=~"<%= src_cluster_prefix %>.*"`

  1. [ ] 🔪 {+ Playbook-Runner +}: Add the following silences at <https://alerts.gitlab.net> to silence sidekiq alerts for 6 hours during the switchover window:
      - Start time: `<%= switch_start_ptime.strftime('%FT%T.000Z') %>`
      - Duration: `<%= ((6.hour.since(switch_start_ptime) - switch_start_ptime) / 3600).to_i %>h`
      - Matchers
          - `env="<%= env %>"`
          - `alertname="SidekiqServiceSidekiqExecutionErrorSLOViolationSingleShard"`
          - `component="sidekiq_execution"`

  <%- if production %>
  1. [ ] 🔪 {+ Playbook-Runner +}: Schedule a job to enable `gitlab_maintenance_mode` into a node exporter, during the upgrade window:
      - [ ] SSH to a console VM in `<%= env %>` (eg. `ssh console-01-sv-<%= env %>.c.<%= gcp_project %>.internal` )
        - [ ] Schedule jobs:
          ```sh
          sudo su -
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 1\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom | at -t <%= switch_start_ptime.strftime('%Y%m%d%H%M') %>
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 0\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom | at -t <%= pcl_end_ptime.strftime('%Y%m%d%H%M') %>
          cat /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          atq
          ```
  <%- end %>

  <%- if production %>
  1. [ ] __PRODUCTION ONLY__ ☁ 🔪 {+ Playbook-Runner +}: Create a maintenance window in [PagerDuty](https://gitlab.pagerduty.com/service-directory/maintenance-windows) with the following:

      - Which services are affected?
        - [GitLab Production](https://gitlab.pagerduty.com/service-directory/PATDFCE)
        - [SLO alerts gprd cny stage](https://gitlab.pagerduty.com/service-directory/P3DG414)
        - [SLO Alerts gprd main stage](https://gitlab.pagerduty.com/service-directory/P7Q44DU)
        - [Dean Man's snitch](https://gitlab.pagerduty.com/service-directory/PFQXAWL)

      - Why is this maintenance happening?

        ```text
        Performing Postgres cluster upgrades so silencing the pager.
        ```

      - Select `Start at a scheduled time`:

        - Timezone: `(UTC+00:00) UTC`
        - Start: `<%= switch_start_ptime.strftime('%m/%d/%Y | %I:%M %p') %>`
        - End: `<%= pcl_end_ptime.strftime('%m/%d/%Y | %I:%M %p') %>`
<%- end %>

  1. [ ] 🔪 {+ Playbook-Runner +}: Ensure that we have a successful GCS snapshot from the Source database: [monitoring_snapshot_last_run][monitoring_snapshot_last_run]

<%- if production %>
</details>

<details>
  <summary>

  - [ ] __SWITCHOVER minus 1 hour (<%= 1.hour.ago(switch_start_ptime).strftime('%Y-%m-%d %H:%M') %> UTC)__

  </summary>

  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>

      - Message:
        ```text
        We will be undergoing scheduled switchover of our <%= cluster.upcase %> database to PostgreSQL v16 in 1 hour. There may be a performance impact until <%= pcl_end_time_str %>. See <<%= gitlab_com_cr %>>
        ```

      - [ ] Check @gitlab retweeted from @gitlabstatus

</details>
<%- end %>

## Postgres SWITCHOVER Window (<%= switch_start_time_str %>) 

<%- if production %>
These steps will be run in a video call. The 🐺 {+ Coordinator +} runs the call,
asking other roles to perform each step on the checklist at the appropriate
time.

Changes are made one at a time, and verified before moving onto the next step.
All the steps will be executed from a console VM and we should keep the session
shared (tmux, screen...).

Whoever is performing a change should share their screen and explain their
actions as they work through them. Everyone else should watch closely for
mistakes or errors! A few things to keep an especially sharp eye out for:

- Exposed credentials (except short-lived items like 2FA codes)
- Running commands against the wrong hosts
- Navigating to the wrong pages in web browsers (gstg vs. gprd, etc)

Remember that the call will be recorded. We will consider making it public after we confirmed that no [SAFE data](https://about.gitlab.com/handbook/legal/safe-framework/) was leaked.
If you see something happening that shouldn't be public, mention it.

### Roll call
- [ ] 🐺 {+ Coordinator +}: Mark the change request as `/label ~"change::in-progress"`
- [ ] 🐺 {+ Coordinator +}: Ensure everyone mentioned above is on the call
- [ ] 🐺 {+ Coordinator +}: Ensure the video call room host is on the call
- [ ] 🐺 {+ Coordinator +}: Get a green light from the 🚑 {+ EOC +}

- [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
  - Message:
    ```text
    Gitlab.com scheduled maintenance of our <%= cluster.upcase %> database layer have started. GitLab.com should be available during the whole period. See <<%= gitlab_com_cr %>>
    ```
  - [ ] Check @gitlab retweeted from @gitlabstatus
<%- end %>

### Data Corruption Checks

1. [ ] 🔪 {+ Playbook-Runner +}: On the **v16 Replica Nodes**, review the pg_amcheck log files created in the previous steps to find out any data corruption errors and to get the last status of the progress:
    ```shell
    egrep 'ERROR:|DETAIL:|LOCATION:' /var/tmp/pg_amcheck.*.log
    cat /var/tmp/pg_amcheck.*.log | grep relations | tail -1
    ```

1. [ ] 🐺 {+ Coordinator +}: If there are **any errors that indicate possible data corruption, then abort the Maintenance** and proceed with the partial rollback of the steps already performed;

1. [ ] 🔪 {+ Playbook-Runner +}: On the **v16 Replica Nodes**, kill `pg_amcheck` processes:
    ```shell
    sudo killall pg_amcheck
    ps -ef | grep pg_amcheck
    ```

1. [ ] 🔪 {+ Playbook-Runner +}: On the **v16 Replica Nodes**, terminate any existing backend processes:
    ```
    sudo gitlab-psql -c "
    select pg_terminate_backend(pid)
    from pg_stat_activity
    where
      pid <> pg_backend_pid()
      and not backend_type ~ '(walsender|logical replication|pg_wait_sampling)'
      and usename not in ('gitlab', 'gitlab-registry', 'pgbouncer', 'postgres_exporter', 'gitlab-consul')
      and application_name <> 'Patroni'
    "
    ```

1. [ ] 🐺 {+ Coordinator +}: [optional] Double check that no `pg_amcheck` processes nor queries are running on the **v16 Replica Nodes**.
    ```shell
    ps -ef | grep pg_amcheck

    sudo gitlab-psql -c "
    select pid, usename, application_name, client_addr, substr(query,1,120) as "query"
    from pg_stat_activity
    where
      pid <> pg_backend_pid()
      and not backend_type ~ '(walsender|logical replication|pg_wait_sampling)'
      and usename not in ('gitlab', 'gitlab-registry', 'pgbouncer', 'postgres_exporter', 'gitlab-consul')
      and application_name <> 'Patroni'
    "
    ```

### Pre-maintenance Health Checks

<%- if production %>
1. [ ] 🐺 {+ Coordinator +}: Check if gitlab_maintenance_mode is ENABLED for <%= env %> [monitoring_gitlab_maintenance_mode][monitoring_gitlab_maintenance_mode]
    - If is not enabled ask 🔪 {+ Playbook-Runner +} to manually enable it by:
      - SSH to a console VM in `<%= env %>` (eg. `ssh console-01-sv-<%= env %>.c.<%= gcp_project %>.internal` )
        - Set `gitlab_maintenance_mode=1` on node exporter :
          ```sh
          sudo su -
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 1\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          cat /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          atq
          ```
<%- end %>

1. [ ] 🐺 {+ Coordinator +}: Check if disallow_database_ddl_feature_flags is ENABLED:
  <%- if production %>
    - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags`
  <%- else %>
    - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags --staging`
  <%- end %>

1. [ ] 🐺 {+ Coordinator +}: Ensure that there are no active critical alerts (s1,s2) or open incidents:
    <%- if production %>
    - PRODUCTION:
        - <https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=incident>
        - <https://alerts.gitlab.net/#/alerts?silenced=false&inhibited=false&active=true&filter=%7Benv%3D%22gprd%22%2C%20severity%3D~%22s1%7Cs2%22%7D>
        - Check #production for recent alerts
        - In case of doubt confirm with EOC - @sre-oncall  
    <%- else %>
    - STAGING:
        - <https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=incident>
        - <https://alerts.gitlab.net/#/alerts?silenced=false&inhibited=false&active=true&filter=%7Benv%3D%22gstg%22%2C%20severity%3D~%22s1%7Cs2%22%7D>
        - Check #production for recent alerts
        - In case of doubt confirm with EOC - @sre-oncall  
    <%- end %>

<%- if production %>
1. [ ] 🐺 {+ Coordinator +}: Check Slerts if there are Alerts that might indicate database problems: [Production Alertmanager](https://alerts.gitlab.net/#/alerts?silenced=false&inhibited=false&active=true&filter=%7Benv%3D%22gprd%22%2C%20severity%3D~%22s1%7Cs2%22%7D)
<%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: Check WRITES going to the SOURCE cluster, `<%= src_cluster_prefix %>`: [monitoring_user_tables_writes][monitoring_user_tables_writes]

1. [ ] 🔪 {+ Playbook-Runner +}: Check READS going to the SOURCE cluster, `<%= src_cluster_prefix %>`: [monitoring_user_tables_reads][monitoring_user_tables_reads]

# T ZERO

We expect the maintenance window to last for up to <%= window_in_hours %> hours, starting from now.

### Pre Switchover checks
1. [ ] 🔪 {+ Playbook-Runner +}: Monitor what pgbouncer pool has connections: [monitoring_pgbouncer_gitlab_user_conns][monitoring_pgbouncer_gitlab_user_conns]

1. [ ] 🔪 {+ Playbook-Runner +}: Confirm chef-client is DISABLED [monitoring_chef_client_enabled][monitoring_chef_client_enabled]

### Evaluation of QA/Validations results - Commitment
If QA/Validations has succeeded, then we can continue to "Complete the Upgrade and Switchover to v16". If some
QA/Validations has failed, the 🐺 {+ Coordinator +} must decide whether to continue with the
upgrade, or to [rollback](#rollback-if-required). A decision to continue
in these circumstances should be counter-signed by the 🎩 {+ Head Honcho +}.

The following are commitment criteria:

Goals:
1. The top priority is to maintain data integrity. Rolling back after the maintenance
   window has ended is very difficult, and will result in any changes made in the
   interim being lost.
1. Failures with an unknown cause should be investigated further. If we can't
   determine the root cause within the maintenance window, we should rollback.


### Postgres Upgrade - Switchover

#### SWITCHOVER Replicas

1. [ ] 🔪 {+ Playbook-Runner +}: Run Ansible playbook to Switchover the <%= env %>-<%= cluster %> cluster Replicas:
    ```sh
    cd ~/src/db-migration/pg-upgrade-logical
    ansible-playbook \
      -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" <%- if cluster == 'registry' %>-e "pg_source_dbname=gitlabhq_registry"<%- end %>\
      -i inventory/<%= env %>-<%= cluster %>.yml \
      switchover_replica.yml 2>&1 \
    | ts | tee -a ansible_switchover_replica_<%= env %>_<%= cluster %>_$(date +%Y%m%d).log
    ```

      - [ ] 🔪 {+ Playbook-Runner +}: After 1st YES, The sevice `<%= consul_replica_endpoint %>.service.consul.` should be pointing to both V14 and V16 replica nodes
          ```sh
          dig @127.0.0.1 -p 8600 <%= consul_replica_endpoint %>.service.consul. SRV +short

          dig @127.0.0.1 -p 8600 master.<%= consul_service_name %>.service.consul. SRV +short
          ```
      - [ ] 🔪 {+ Playbook-Runner +}: Check Read-Only activity metrics for 15 minutes [PG16 Upgrade Dashboard](https://dashboards.gitlab.net/d/postgresql-pg-upgrade/postgresql3a-postgres-upgrade-using-logical?orgId=1&var-PROMETHEUS_DS=mimir-gitlab-<%= env %>>g&var-environment=<%= env %>&var-cluster=<%= cluster %>&var-source_version=14&var-destination_version=16&from=now-1h&to=now&refresh=5m)
        - [ ] Compare the volume of `standbys TPS (commits)` between Target vs Source (it should split workload 50/50 between old and new replicas)
        - [ ] Compare volume of `rollback TPS – ERRORS` 
      - [ ] 🐺 {+ Coordinator +}: Wait for the end of the hourly Write TPS spike to finish (around 18m past the hour)
      - [ ] 🔪 {+ Playbook-Runner +}: Second "y": **stop R/O traffic to old replicas**
      - [ ] 🔪 {+ Playbook-Runner +}: After 2nd YES, The sevice `<%= consul_replica_endpoint %>.service.consul.` should be pointing only to the new V16 replica nodes
          ```sh
          dig @127.0.0.1 -p 8600 <%= consul_replica_endpoint %>.service.consul. SRV +short

          dig @127.0.0.1 -p 8600 master.<%= consul_service_name %>.service.consul. SRV +short
          ```

      - [ ] 🔪 {+ Playbook-Runner +}: Check the metrics for as long as we observe connections to the `SOURCE` standbys, minimum time 15 minutes.  (this is not blocking the E2E tests).
<%- unless @no_qa_cluster_list.include?(cluster)  %>
      - [ ] 🔪 {+ Playbook-Runner +}: Trigger Smoke E2E suite against the environment that was upgraded:<%- if production %>[Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules): `Four hourly smoke tests`<%- else %>[Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules): `Four hourly smoke tests`<%- end %>. This has an estimated duration of 15 minutes.
      - [ ] 🔪 {+ Playbook-Runner +}: If the smoke tests fail, DBRE should re-run the failed job to see if it is reproducible. In parallel DBRE should reach out to [on-call Test Platform DRI](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/oncall-rotation/#schedule) for the help with investigation. If there is no avialable on-call DRI, reach out to `#test-platform` and escalate with the [management team](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/#management-team).
      - [ ] 🏆 {+ Quality On-Call +}: If the smoke tests fail, Quality performs an initial triage of the failure. If Quality cannot determine failure is 'unrelated' within that period - stop and reschedule the whole procedure.
<%- else  %>
      - [ ] 🔪 {+ Playbook-Runner +}: Perform manual test using Gitlab.com Web UI to validate <%- if cluster == "registry" %>Conteiner Registry<%- elsif cluster == "embedding" %>Gitlab Duo Chat (AI)<%- end %> usability. {+ TODO: +} Improve QA on this test.
<%- end %>


#### SWITCHOVER Leader/Writer
1. [ ] 🐺 {+ Coordinator +}: After this step there's a risk of **no return**. Proceed wisely!
    - [ ] Get agreement of peers and concent of 🎩 {+ Head Honcho +} to proceed
1. [ ] 🐺 {+ Coordinator +}: Wait for the end of the hourly Write TPS spike to finish (around 18m past the hour)
    - Note: Only proceed with R/W traffic primary switchover **when logical replication lag < 500 MiB**

1. [ ] 🔪 {+ Playbook-Runner +}: Run Ansible playbook to Switchover the <%= env %>-<%= cluster %> cluster Replicas:
    ```sh
    cd ~/src/db-migration/pg-upgrade-logical
    ansible-playbook \
      -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" <%- if cluster == 'registry' %>-e "pg_source_dbname=gitlabhq_registry"<%- end %>\
      -i inventory/<%= env %>-<%= cluster %>.yml \
      switchover_leader.yml -e "enable_reverse_logical_replication=true" -e "pg_publication_count_reverse=<%= publication_count_reverse %>" 2>&1 \
    | ts | tee -a ansible_switchover_leader_<%= env %>_<%= cluster %>_$(date +%Y%m%d).log
    ```

    - [ ] 🔪 {+ Playbook-Runner +}: The master service for `master.<%= consul_service_name %>.service.consul.` should be pointing only to the new V16 Leader/Writer node.
        ```sh
        dig @127.0.0.1 -p 8600 master.<%= consul_service_name %>.service.consul. SRV +shorthort
        ```
    - [ ] 🔪 {+ Playbook-Runner +}: If the playbook fails, repeat in the "forced" mode:
      ```sh
      cd ~/src/db-migration/pg-upgrade-logical
      ansible-playbook \
        -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" <%- if cluster == 'registry' %>-e "pg_source_dbname=gitlabhq_registry"<%- end %>\
        -i inventory/<%= env %>-<%= cluster %>.yml \
        switchover_leader.yml -e "enable_reverse_logical_replication=true" -e "pg_publication_count_reverse=<%= publication_count_reverse %>" -e "force_mode=true" 2>&1 \
      | ts | tee -a ansible_switchover_leader_<%= env %>_<%= cluster %>_$(date +%Y%m%d)_FORCE_MODE.log
      ```

#### Post Switchover QA Tests

<%- unless @no_qa_cluster_list.include?(cluster)  %>
1. [ ] 🔪 {+ Playbook-Runner +}: Trigger Smoke E2E suite against the environment that was upgraded:<%- if production %>[Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules): `Four hourly smoke tests`<%- else %>[Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules): `Four hourly smoke tests`<%- end %>
<%- else  %>
1. [ ] 🔪 {+ Playbook-Runner +}: Perform manual test using Gitlab.com Web UI to validate <%- if cluster == "registry"%>Conteiner Registry<%- elsif cluster == "embedding"%>Gitlab Duo Chat (AI)<%- end %> usability. {+ TODO +}{+ TODO: +} Improve QA on this test.
<%- end %>

#### Post Switchover persist new configuration

1. [ ] 🔪 {+ Playbook-Runner +}: Merge the MR that updates <%= cluster.upcase %> Consul and Monitoring, teleport DB endpoint, and console config endpoint:
    1. [ ] MR for <%= env %>-patroni-<%= cluster %> Consul: <%= post_switchover_mr %>
    1. [ ] MR for <%= env %>-patroni-<%= cluster %> Monitoring: <%= post_switchover_monitoring_mr %>
    1. [ ] MR for <%= cluster.upcase %> teleport DB endpoint: <%= teleport_db_endpoint_mr %>
<%- unless cluster == 'registry' %>
    1. [ ] MR for <%= cluster.upcase %> console config endpoint: <%= console_config_endpoint_mr %>
<%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: Ensure that the changes merged from the previous step have been deployed to the Chef server before re-enabling Chef by confirming the linked `master` pipeline for `ops.gitlab.net` completed successfully.

1. [ ] 🔪 {+ Playbook-Runner +}: Re-enable Chef in all <%= env %>-<%= cluster %> nodes:
    ```sh
    knife ssh "role:<%= env %>-base-db-<%= src_cluster_prefix %> OR role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo chef-client-enable"
    ```

1. [ ] 🔪 {+ Playbook-Runner +}: Confirm Chef is ENABLED in all nodes [monitoring_chef_client_enabled][monitoring_chef_client_enabled]

1. [ ] 🔪 {+ Playbook-Runner +}: Run chef-client on Patroni Nodes:
      ```sh
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %> OR role:<%= env %>-base-db-<%= src_cluster_prefix %>" "sudo chef-client"
      ```
    - [ ] 🔪 {+ Playbook-Runner +}: Confirm:
      - [ ] No errors while running chef-client [monitoring_chef_client_error][monitoring_chef_client_error]
          ```sh
          knife ssh "roles:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo /usr/lib/postgresql/16/bin/postgres --version"
          ```
      - [ ] The sevice `<%= consul_replica_endpoint %>.service.consul.` should be pointing only to the new V16 replica nodes, and the master service for `master.<%= consul_service_name %>.service.consul.` should be pointing only to the new V16 Leader/Writer node.
          ```sh
          dig @127.0.0.1 -p 8600 <%= consul_replica_endpoint %>.service.consul. SRV +short

          dig @127.0.0.1 -p 8600 master.<%= consul_service_name %>.service.consul. SRV +short
          ```

1. [ ] 🔪 {+ Playbook-Runner +}: Check WRITES going to the TARGET cluster, `<%= dst_cluster_prefix %>`: [monitoring_user_tables_writes][monitoring_user_tables_writes]

1. [ ] 🔪 {+ Playbook-Runner +}: Check READS going to the TARGET cluster, `<%= dst_cluster_prefix %>`: [monitoring_user_tables_reads][monitoring_user_tables_reads].

1. [ ] 🔪 {+ Playbook-Runner +}: Start cron.service on all <%= env %>-<%= cluster %> nodes:
      ```sh
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo systemctl is-active cron.service"
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo systemctl start cron.service"
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo systemctl is-active cron.service"
      ```

#### Communicate

<%- if production %>
  1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>

      - Message:
        ```text
        Gitlab.com <%= cluster.upcase %> database upgrade was performed. We'll continue to monitor for any performance issues until the end of the maintenance window. Thank you for your patience. See <<%= gitlab_com_cr %>>
        ```

    - [ ] Check @gitlab retweeted from @gitlabstatus

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: In the same thread from the earlier post, post the following message and click on the checkbox "Also send to X channel" so the threaded message would be published to the channel:
   - Message:
      ```text
      :done: *GitLab.com database layer maintenance upgrade is complete now.* :celebrate:
      We’ll continue to monitor the platform to ensure all systems are functioning correctly.
      ```
      - [ ] `#whats-happening-at-gitlab`
      - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
<%- end %>

<details>
  <summary>

### Complete the Switchover

  </summary>

<%- unless @no_qa_cluster_list.include?(cluster)  %>
#### Verification

1. __Start Post Switchover to v16 QA__
    1. [ ] 🔪 {+ Playbook-Runner +}: Full E2E suite against the environment that was upgraded:<%- if production %>[Production](https://ops.gitlab.net/gitlab-org/quality/production/-/pipeline_schedules): `Four hourly smoke tests`, and `Twice daily full run`<%- else %>[Staging](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules): `Four hourly smoke tests`, and `Daily Full QA suite`<%- end %>
        - It will take 1+ hour to run these tests, so you can continue with the `Wrapping up` of the upgrade and check the test result latter;
<%- end %>

#### Wrapping up
<%- if production %>
1. [ ] __PRODUCTION ONLY__ 🔪 {+ Playbook-Runner +}: If the scheduled maintenance is still active in [PagerDuty](https://gitlab.pagerduty.com/service-directory/maintenance-windows), click on `Update` then `End Now`.
<%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: Remove silences of `fqdn=~"<%= dst_cluster_prefix %>.*"` we created during this process from <https://alerts.gitlab.net> (**Important: don't remove the silence of SOURCE nodes**)

<%- if production %>
1. [ ] 🔪 {+ Playbook-Runner +}: ADD the following silences at <https://alerts.gitlab.net> to silence `WALGBaseBackup` alerts in <%= src_cluster_prefix %> for 2 weeks (14 days = 336 hours)
    - Start time: `<%= switch_start_ptime.strftime('%FT%T.000Z') %>`
    - Duration: `336h`
      - [ ] `env="gprd"`
      - [ ] `type="<%= env %>-<%= src_cluster_prefix %>"`
      - [ ] `alertname=~"WALGBaseBackupFailed|walgBaseBackupDelayed"`
<%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: Initiate a full backup (using `wal-g`) and trigger GCS snapshot on the new v16 Patroni **<%= cluster.upcase %>** cluster:
    - [ ] SSH to `<%= "#{dst_backup_host}-db-#{env}.c.#{gcp_project}.internal" %>`
        - [ ] Run a [Wal-G backup](https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/patroni/postgresql-backups-wale-walg.md#daily-basebackup):
          ```sh
          sudo su - gitlab-psql
          tmux new -s PGBasebackup
          nohup /opt/wal-g/bin/backup.sh >> /var/log/wal-g/wal-g_backup_push.log 2>&1 &
          ```
    - [ ] Open another SSH session to `<%= "#{dst_backup_host}-db-#{env}.c.#{gcp_project}.internal" %>`
        - [ ] Run a [manual GCS Snapshot](https://ops.gitlab.net/gitlab-com/runbooks/-/blob/master/docs/patroni-ci/rebuild_ci_cluster_from_prod.md#take-a-snapshot-from-the-writer-node)
          ```sh
          sudo su - gitlab-psql
          tmux new -s GCSSnapshot
          /usr/local/bin/gcs-snapshot.sh
          ```

<%- if production %>
1. [ ] __PRODUCTION ONLY__ 🔪 {+ Playbook-Runner +}: Ping `@NikolayS` on the gitlab.com CR (<<%= gitlab_com_cr %>>) (and/or in [#database-lab](https://gitlab.slack.com/archives/CLJMDRD8C)) that the work is complete so he can update the DLE environment.

1. [ ] __PRODUCTION ONLY__ 🔪 {+ Playbook-Runner +}: Confirm that Teleport access works properly and we're hitting the right clusters:
    ```bash
    # Login to Teleport
    tsh login --add-keys-to-agent=no --proxy=production.teleport.gitlab.net --request-roles=database-ro-gprd --request-reason="Testing DB connectivity after PG16 Upgrade - <%= gitlab_com_cr %> "
    query="select setting from pg_settings where name='cluster_name'"

    # Check MAIN
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=<%= prod_db_name %> db-main-primary-gprd --add-keys-to-agent=no
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=<%= prod_db_name %> db-main-replica-gprd --add-keys-to-agent=no

    # Check CI
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=<%= prod_db_name %> db-ci-primary-gprd --add-keys-to-agent=no
    echo "${query}" | tsh db connect --db-user=console-ro --db-name=<%= prod_db_name %> db-ci-replica-gprd --add-keys-to-agent=no
    ```
    You should see `v16` as part of the cluster name.
<%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: Update the wal-g daily restore schedule for the **[<%= env %>] - [<%= cluster %>]** cluster at https://ops.gitlab.net/gitlab-com/gl-infra/gitlab-restore/postgres-gprd/-/pipeline_schedules
    1. [ ] Change the following variables:
        - `PSQL_VERSION = 16`
        - `BACKUP_PATH = ?` (? = use the "directory" from the new v16 GCS backup location at: <%= dst_gcs_backup_location %>)

<%- if production %>
1. [ ] 🐺 {+ Coordinator +}: Check if gitlab_maintenance_mode is DISABLED for <%= env %> [monitoring_gitlab_maintenance_mode][monitoring_gitlab_maintenance_mode]
    - If is not disabled ask 🔪 {+ Playbook-Runner +} to manually disable it by:
      - SSH to a console VM in `<%= env %>` (eg. `ssh console-01-sv-<%= env %>.c.<%= gcp_project %>.internal` )
        - Set `gitlab_maintenance_mode=0` on node exporter :
          ```sh
          sudo su -
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 0\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          cat /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          atq
          ```
<%- end %>

<%- unless @no_qa_cluster_list.include?(cluster)  %>
1. [ ] 🔪 {+ Playbook-Runner +}: (after an hour): Check that the Smoke, and Full E2E suite has passed. If there are failures, reach out to [on-call Test Platform DRI](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/oncall-rotation/#schedule) for the help with investigation. If there is no avialable on-call DRI, reach out to `#test-platform` and escalate with the [management team](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/#management-team).

    1. [ ] 🏆 {+ Quality On-Call +}: If the Smoke or Full E2E tests fail, Quality performs an initial triage of the failure. If Quality cannot determine failure is 'unrelated', team decides on [declaring an incident](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/debugging-qa-test-failures/#failure-needs-escalation) and following the incident process.
<%- end %>

</details>

# Close Rollback Window

<details>
  <summary>

  - [ ] __SWITCHOVER plus <%= window_in_hours %> hours - Close PCL (<%= pcl_end_time_str %>)__

  </summary>

1. [ ] 🔪 {+ Playbook-Runner +}: Run Ansible playbook to Stop the Reverse Logical Replication:
    ```sh
    cd ~/src/db-migration/pg-upgrade-logical
    ansible-playbook \
      -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" <%- if cluster == 'registry' %>-e "pg_source_dbname=gitlabhq_registry"<%- end %>\
        -i inventory/<%= env %>-<%= cluster %>.yml \
      stop_reverse_replication.yml -e "pg_publication_count_reverse=<%= publication_count_reverse %>" 2>&1 \
    | ts | tee -a stop_reverse_replication_<%= env %>_<%= cluster %>_$(date +%Y%m%d).log
    ```

1. [ ] 🔪 {+ Playbook-Runner +}: On the TARGET cluster <%= src_cluster_prefix %> Leader/Writer, drop subscription (if still existing) for logical replication:
    - [ ] Check if the subscription still exist:
      ```sh
      gitlab-psql \
          -Xc "select subname, subenabled, subconninfo, subslotname, subpublications from pg_subscription" 
      ```
1. [ ] 🔪 {+ Playbook-Runner +}: On the SOURCE cluster <%= dst_cluster_prefix %>  Leader/Writer, drop publication and logical_replication_slot for reverse replication:
    - [ ] Check if the publication and replication slots still exist:
      ```sh
      gitlab-psql \
        -Xc "select pubname from pg_publication" \
        -Xc "select slot_name, plugin, slot_type, active from pg_replication_slots"
      ```

<%- if block_ddl_ff %>
1. [ ] 🔪 {+ Playbook-Runner +}: Enable feature flags by typing the following into `#production`:
  <%- if production %>
    - PRODUCTION:
      1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags false`
  <%- else %>
    - STAGING:
      1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags false --staging`
  <%- end %>

    1. [ ] 🐺 {+ Coordinator +}: Check if the underlying DDL lock FF is DISABLED:
      <%- if production %>
        - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags` should return DISABLED
      <%- else %>
        - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags --staging` should return DISABLED
      <%- end %>

    1. [ ] ☎ {+ Comms-Handler +}: Inform the database team that the CR is completed at #g_database:
        ```text
        Hi @gl-database,

        We are reaching out to inform that we have completed the work for the <%= gitlab_com_cr %> CR. Therefore we have re-enabled the `execute_batched_migrations_on_schedule`, `execute_background_migrations`, reindexing, async_foreign_key,  sync_index and partition_manager_sync_partitions features and tasks in `<%= env %>` environment.

        Thanks!
        ```
<%- end %>

<%- if production %>
1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: End of maintenance from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - [ ] Click "Finish Maintenance" and send the following:
       - Message:
          ```text
          GitLab.com scheduled maintenace for the <%= cluster.upcase %> database layer is complete. We'll continue to monitor the platform to ensure all systems are functioning correctly. Thank you for your patience.
          ```
    - [ ] Check @gitlab retweeted from @gitlabstatus
<%- end %>

1. [ ] ☎ {+ Comms-Handler +}: Inform `@release-managers` at #g_delivery about the end of the operational lock 
    ```text
    Hi @release-managers :waves:,

    We are reaching out to inform that we have completed the work for the <%= gitlab_com_cr %> CR in our `<%= env %>` SaaS environment. We are closing the operational block for deployments in the `<%= cluster.upcase %>` database, so regular deployment operations can be fully resumed.
    ```

1. [ ] 🔪 {+ Playbook-Runner +}: Shutdown the SOURCE <%= env %>-base-db-<%= src_cluster_prefix %> cluster to avoid any risk of splitbrain:
      ``` 
      knife ssh "role:<%= env %>-base-db-<%= src_cluster_prefix %>" "sudo systemctl stop patroni"
      ```
1. [ ] 🔪 {+ Playbook-Runner +}: Create and Review Plan and Apply the MR to destroy the old <%= src_cluster_prefix %> cluster in Terraform: <%= destroy_old_cluster_mr %>

1. [ ] 🔪 {+ Playbook-Runner +}: Add a silence for GCPScheduledSnapshotsDelayed for <%= src_cluster_prefix %> cluster so that no one will be paged for removed hosts
    - [ ] Duration: `7d`
    - [ ] `env="gprd"`
    - [ ] `disk_name=~"<%= src_cluster_prefix %>.*"`
    - [ ] `alertname="GCPScheduledSnapshotsDelayed"`

1. [ ] 🔪 {+ Playbook-Runner +}: Remove walg-basebackup group for <%= src_cluster_prefix %> cluster so that no one will be paged for removed hosts
    - [ ] Forward port from pushgateway `ssh -L 9091:10.221.19.2:9091 lb-bastion.gprd.gitlab.com`
    - [ ] Go to localhost:9091 in your brower
    - [ ] Locate the group with the labels job=walg-basebackup type=<%= env %>-<%= dst_cluster_prefix %>
    - [ ] Press Delete Group for that group

1. [ ] 🔪 {+ Playbook-Runner +}: Open a separate issue to rebuild each cluster's DR Archive and Delayed replicas. It will be completed in the next couple of working days.

1. [ ] 🐺 {+ Coordinator +}: Mark the change request as `/label ~"change::complete"`

</details>

# Rollback

<details>
  <summary>

### __Rollback__ (if required)

  </summary>

<%- if production %>
1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post an update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - Message:

        ```text
        Due to an issue during the planned maintenance for the database layer, we have initiated a rollback of the <%= cluster.upcase %> database layer and some performance impact still might be expected. We will provide update once the rollback process is completed.
        ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Ask the IMOC or the Head Honcho if this message should be sent to any slack rooms:
   - `#whats-happening-at-gitlab`
   - `#infrastructure-lounge` (cc `@sre-oncall`)
   - `#g_delivery` (cc `@release-managers`)
<%- end %>

#### Rollback Postgres Upgrade 

- There will be **no rollback after closing the rollback window**!

1. [ ] 🔪 {+ Playbook-Runner +}: Monitor what pgbouncer pool has connections [monitoring_pgbouncer_gitlab_user_conns][monitoring_pgbouncer_gitlab_user_conns]

##### ROLLBACK – execute!
Goal: __Set <%= env %>-<%= cluster %> *v14* cluster as Primary cluster__

1. [ ] 🔪 {+ Playbook-Runner +}: Execute `switchover_rollback.yml` playbook to rollback to v14 cluster:

      ```sh
      cd ~/src/db-migration/pg-upgrade-logical
      ansible-playbook \
        -e "ansible_ssh_private_key_file=/home/dbupgrade/.ssh/id_dbupgrade" <%- if cluster == 'registry' %>-e "pg_source_dbname=gitlabhq_registry"<%- end %>\
        -i inventory/<%= env %>-<%= cluster %>.yml \
        switchover_rollback.yml -e "force_mode=true" 2>&1 \
      | ts | tee -a ansible_switchover_rollback_<%= env %>_<%= cluster %>_$(date +%Y%m%d).log
      ```

1. [ ] 🔪 {+ Playbook-Runner +}: Check WRITES going to the SOURCE cluster, `<%= src_cluster_prefix %>`: [monitoring_user_tables_writes][monitoring_user_tables_writes]

1. [ ] 🔪 {+ Playbook-Runner +}: Check READS going to the SOURCE cluster, `<%= src_cluster_prefix %>`: [monitoring_user_tables_reads][monitoring_user_tables_reads]. 

1. [ ] 🔪 {+ Playbook-Runner +}: On the TARGET cluster <%= src_cluster_prefix %> Leader/Writer, drop subscription (if still existing) for logical replication:
    - [ ] Check if the subscription still exist:
      ```sh
      gitlab-psql \
          -Xc "select subname, subenabled, subconninfo, subslotname, subpublications from pg_subscription" 
      ```
1. [ ] 🔪 {+ Playbook-Runner +}: On the SOURCE cluster <%= dst_cluster_prefix %>  Leader/Writer, drop publication and logical_replication_slot for reverse replication:
    - [ ] Check if the publication and replication slots still exist:
      ```sh
      gitlab-psql \
        -Xc "select pubname from pg_publication" \
        -Xc "select slot_name, plugin, slot_type, active from pg_replication_slots"
      ```

1. [ ] 🔪 {+ Playbook-Runner +}: Shutdown the TARGET <%= env %>-base-db-<%= dst_cluster_prefix %> cluster to avoid any risk of splitbrain:
      ``` 
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo systemctl stop patroni"
      ```

### Complete the rollback

<%- unless @no_qa_cluster_list.include?(cluster)  %>
1. [ ] 🔪 {+ Playbook-Runner +}: Confirm that our smoke tests are still passing (continue the rollback as this might take an hour...)
<%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: Revert all the applied MRs (the amount of MRs is variable depending on where the CR failed)

1. [ ] 🔪 {+ Playbook-Runner +}: Re-enable Chef in all <%= env %>-<%= cluster %> nodes:
    ```sh
    knife ssh "role:<%= env %>-base-db-<%= src_cluster_prefix %> OR role:<%= env %>-base-db-<%= dst_cluster_prefix %>" "sudo chef-client-enable"

1. [ ] 🔪 {+ Playbook-Runner +}: Confirm chef-client is ENABLED in all nodes [monitoring_chef_client_enabled][monitoring_chef_client_enabled]

1. [ ] 🔪 {+ Playbook-Runner +}: Run chef-client on Patroni Nodes:
      ```sh
      knife ssh "role:<%= env %>-base-db-<%= dst_cluster_prefix %> OR role:<%= env %>-base-db-<%= src_cluster_prefix %>" "sudo chef-client"
      ```

1. [ ] 🔪 {+ Playbook-Runner +}: Confirm no errors while running chef-client [monitoring_chef_client_error][monitoring_chef_client_error]

<%- if production %>
1. [ ] __PRODUCTION ONLY__ 📣 {+ CMOC +}: Post update from Status.io maintenance site, publish on `@gitlabstatus`. Workflow: <https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#sending-updates-about-maintenance-events>
    - [ ] Click "Finish Maintenance" and send the following:
       - Message:

          ```text
          GitLab.com rollback for the database layer is complete, and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.
          ```

1. [ ] __PRODUCTION ONLY__ ☎ {+ Comms-Handler +}: Send the following message to slack rooms:

   ```text
   GitLab.com rollback for the database layer is complete and we're back up and running. We'll be monitoring the platform to ensure all systems are functioning correctly. Thank you for your patience.
   ```
   - [ ] `#whats-happening-at-gitlab`
   - [ ] `#infrastructure-lounge` (cc `@sre-oncall`)
   - [ ] `#g_delivery` (cc `@release-managers`)
<%- end %>


<%- if block_ddl_ff %>
1. [ ] 🔪 {+ Playbook-Runner +}: Enable feature flags by typing the following into `#production`:
  <%- if production %>
    - PRODUCTION:
      1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags false`
  <%- else %>
    - STAGING:
      1. [ ] `/chatops run feature set disallow_database_ddl_feature_flags false --staging`
  <%- end %>
 
    1. [ ] ☎ {+ Comms-Handler +}: Inform the database team that the CR is completed at #g_database:
        ```text
        Hi @gl-database,

        We are reaching out to inform that we have aborted and rolled back the <%= gitlab_com_cr %> CR. Therefore we have re-enabled the `execute_batched_migrations_on_schedule`, `execute_background_migrations`, reindexing, async_foreign_key,  sync_index and partition_manager_sync_partitions features and tasks in `<%= env %>` environment.

        Thanks!
        ```
<%- end %>

1. [ ] ☎ {+ Comms-Handler +}: Inform `@release-managers` at #g_delivery about the end of the operational lock 
    ```text
    Hi @release-managers :waves:,

    We are reaching out to inform that we have aborted and rolled back the <%= gitlab_com_cr %> CR in our `<%= env %>` SaaS environment. We are closing the operational block for deployments in the `<%= cluster.upcase %>` database, so regular deployment operations can be fully resumed.
    ```

1. [ ] 🔪 {+ Playbook-Runner +}: Check if the underlying DDL lock FF is DISABLED:
  <%- if production %>
    - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags` should return DISABLED
  <%- else %>
    - [ ] On slack `/chatops run feature get disallow_database_ddl_feature_flags --staging` should return DISABLED
  <%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: On two nodes, console and target leader, remove the private keys temporarily placed in `~dbupgrade/.ssh`:
    ```shell
    rm ~dbupgrade/.ssh/id_rsa
    rm ~dbupgrade/.ssh/id_dbupgrade
    ```

<%- if production %>
1. [ ] 🔪 {+ Playbook-Runner +}: ADD the following silences at <https://alerts.gitlab.net> to silence `WALGBaseBackup` alerts in <%= dst_cluster_prefix %> for 2 weeks (14 days = 336 hours)
    - Start time: `<%= switch_start_ptime.strftime('%FT%T.000Z') %>`
    - Duration: `336h`
      - [ ] `env="gprd"`
      - [ ] `type="<%= env %>-<%= dst_cluster_prefix %>"`
      - [ ] `alertname=~"WALGBaseBackupFailed|walgBaseBackupDelayed"`
<%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: UPDATE the following silence at <https://alerts.gitlab.net> to silence alerts in V16 nodes for 2 weeks (14 days = 336 hours):
    - Start time: `<%= switch_start_ptime.strftime('%FT%T.000Z') %>`
    - Duration: `336h`
    - Matcher:
      <%- if production %>
      - __PRODUCTION__
        - [ ] `env="gprd"`
        - [ ] `fqdn=~"<%= dst_cluster_prefix %>.*"`
      <%- else %>
      - __STAGING__
        - [ ] `env="gstg"`
        - [ ] `fqdn=~"<%= dst_cluster_prefix %>.*"`
      <%- end %>

1. [ ] 🔪 {+ Playbook-Runner +}: DELETE the following silences at <https://alerts.gitlab.net> 
    - Matcher:
      <%- if production %>
      - __PRODUCTION__
        - [ ] `env="gprd"`
        - [ ] `fqdn=~"<%= src_cluster_prefix %>.*"`
      <%- else %>
      - __STAGING__
        - [ ] `env="gstg"`
        - [ ] `fqdn=~"<%= src_cluster_prefix %>.*"`
      <%- end %>

<%- if production %>
1. [ ] 🐺 {+ Coordinator +}: Check if gitlab_maintenance_mode is DISABLED for <%= env %> [monitoring_gitlab_maintenance_mode][monitoring_gitlab_maintenance_mode]
    - If is not disabled ask 🔪 {+ Playbook-Runner +} to manually disable it by:
      - SSH to a console VM in `<%= env %>` (eg. `ssh console-01-sv-<%= env %>.c.<%= gcp_project %>.internal` )
        - Set `gitlab_maintenance_mode=0` on node exporter :
          ```sh
          sudo su -
          echo -e "# HELP gitlab_maintenance_mode record maintenance window\n# TYPE gitlab_maintenance_mode untyped\ngitlab_maintenance_mode 0\n" > /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          cat /opt/prometheus/node_exporter/metrics/gitlab_maintenance_mode.prom
          atq
          ```
<%- end %>

1. [ ] 🐺 {+ Coordinator +}: Mark the <%= gitlab_com_cr %> change request as `/label ~"change::aborted"`


</details>

## Extra details
### In case the Playbook-Runner is disconnected
As most of the steps are executed in a tmux session owned by the
Playbook-Runner role we need a safety net in case this person loses their
internet connection or otherwise drops off half way through. Since other
SREs/DBREs also have root access on the console node where everything is
running they should be able to recover it in different ways. We tested the
following approach to recovering the tmux session, updating the ssh agent and
taking over as a new ansible user.

- `ssh host`
- Add your public SSH key to `/home/PREVIOUS_PLAYBOOK_USERNAME/.ssh/authorized_keys`
- `sudo chef-client-disable <%= gitlab_com_cr %>` so that we don't override the above
- `ssh -A PREVIOUS_PLAYBOOK_USERNAME@host`
- `echo $SSH_AUTH_SOCK`
- `tmux attach -t 0`
- `export SSH_AUTH_SOCK=<VALUE from previous SSH_AUTH_SOCK output>`
- `<ctrl-b> :`
- `set-environment -g 'SSH_AUTH_SOCK' <VALUE from previous SSH_AUTH_SOCK output>`
- `export ANSIBLE_REMOTE_USER=NEW_PLAYBOOK_USERNAME`
- `<ctrl-b> :`
- `set-environment -g 'ANSIBLE_REMOTE_USER' <your-user>`
