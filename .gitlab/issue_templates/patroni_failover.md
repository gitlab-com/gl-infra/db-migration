# Failover Team

| Role                                                                   | Assigned To                |
| -----------------------------------------------------------------------|----------------------------|
| 🐺 Coordinator                                                         | __TEAM_COORDINATOR__       |
| 🔪 Chef-Runner                                                         | __TEAM_CHEF_RUNNER__       |
| ☎ Comms-Handler                                                       | __TEAM_COMMS_HANDLER__     |
| 🐘 Database-Wrangler                                                   | __TEAM_DATABASE_WRANGLER__ |
| ☁ Cloud-conductor                                                     | __TEAM_CLOUD_CONDUCTOR__   |
| 🏆 Quality                                                             | __TEAM_QUALITY__           |
| ↩ Fail-back Handler (_Staging Only_)                                  | __TEAM_FAILBACK_HANDLER__  |
| 🎩 Head Honcho (_Production Only_)                                     | __TEAM_HEAD_HONCHO__       |

(try to ensure that 🔪, ☁ and ↩ are always the same person for any given run)


# Immediately

Perform these steps when the issue is created.

- [ ] 🐺 {+ Coordinator +}: Fill out the names of the failover team in the table above.
- [ ] 🐺 {+ Coordinator +}: Fill out dates/times and links in this issue:
    - Start Time: `__MAINTENANCE_START_TIME__` & End Time: `__MAINTENANCE_END_TIME__`
    - Google Working Doc: __GOOGLE_DOC_URL__ (for PRODUCTION, create a new doc and make it writable for GitLabbers, and readable for the world)
    - **PRODUCTION ONLY** Blog Post: __BLOG_POST_URL__
    - **PRODUCTION ONLY** End Time: __MAINTENANCE_END_TIME__


# Support Options

| Provider | Plan | Details | Create Ticket |
|----------|------|---------|---------------|
| **Google Cloud Platform** | [Gold Support](https://cloud.google.com/support/?options=premium-support#options) | 24x7, email & phone, 1hr response on critical issues | [**Create GCP Support Ticket**](https://enterprise.google.com/supportcenter/managecases) |
| **Ongres** |   | 24x7, email & phone, 1hr response on critical issues | +12074050942 |


# Database hosts

* [Staging replication overview](https://dashboards.gitlab.net/d/000000249/postgresql-replication-overview&var-environment=gprd)
* [Production replication overview](https://dashboards.gitlab.net/d/000000249/postgresql-replication-overview?orgId=1&var-environment=gstg)

# Running the failover

The failover steps are automated in a Ruby script. Its usage is like this: `ruby ./bin/migrate.rb {failover|failback} {gstg|gprd} [step]`.

The script is expected to run on a node inside the private network of the environment we're failing over on.

In the document below, we have the commands the script is going to execute in plain text, **You're not supposed copy-n-paste
these commands as they are for reference purposes only, use the script at all times**. Before each command below is a step
name in parenthesis, pass this name to the script to skip directly to this step.

Run these commands to get up and running for executing the script:

Staging:

```
your-workstation  $ eval `ssh-agent -s`
your-workstation  $ ssh-add ~/.ssh/id_rsa # Replace with the path to the key you use to login into the infrastructure
your-workstation  $ scp -r /path/to/chef-repo/.chef deploy-01-sv-gstg.c.gitlab-staging-1.internal:~
your-workstation  $ ssh -A deploy-01-sv-gstg.c.gitlab-staging-1.internal
deploy-01-sv-gstg $ git clone git@ops.gitlab.net:gitlab-cookbooks/chef-repo.git
deploy-01-sv-gstg $ git clone git@gitlab.com:gitlab-com/gl-infra/db-migration.git
deploy-01-sv-gstg $ cd db-migration
deploy-01-sv-gstg $ tmux
deploy-01-sv-gstg $ ruby ./bin/migrate.rb failover gstg
```

Production:

```
your-workstation  $ eval `ssh-agent -s`
your-workstation  $ ssh-add ~/.ssh/id_rsa # Replace with the path to the key you use to login into the infrastructure
your-workstation  $ scp -r /path/to/chef-repo/.chef deploy-01-sv-gprd.c.gitlab-production.internal:~
your-workstation  $ ssh -A deploy-01-sv-gprd.c.gitlab-production.internal
deploy-01-sv-gprd $ git clone git@ops.gitlab.net:gitlab-cookbooks/chef-repo.git
deploy-01-sv-gprd $ git clone git@gitlab.com:gitlab-com/gl-infra/db-migration.git
deploy-01-sv-gprd $ cd db-migration
deploy-01-sv-gprd $ tmux
deploy-01-sv-gprd $ ruby ./bin/migrate.rb failover gprd
```
# Monitoring

- [ ] To monitor network blocking:
    * Staging: `watch -n 5 bin/hostinfo staging.gitlab.com fe-{01..03}-lb-gstg.c.gitlab-staging-1.internal altssh.staging.gitlab.com fe-altssh-{01..02}-lb-gstg.c.gitlab-staging-1.internal`
    * Production: `watch -n 5 bin/hostinfo gitlab.com fe-{01..16}-lb-gprd.c.gitlab-production.internal altssh.gitlab.com fe-altssh-{01..02}-lb-gprd.c.gitlab-production.internal`

# Accessing the rails and database consoles

## Staging

* rails: `ssh $USER-rails@console-01-sv-gstg.c.gitlab-staging-1.internal`
* db replica: `ssh $USER-db@console-01-sv-gstg.c.gitlab-staging-1.internal`
* db primary: `ssh $USER-db-primary@console-01-sv-gstg.c.gitlab-staging-1.internal`

```
ssh postgres-03-db-gprd.c.gitlab-production.internal
sudo gitlab-psql
```

## Production

* rails: `ssh $USER-rails@console-01-sv-gprd.c.gitlab-production.internal`
* db replica: `ssh $USER-db@console-01-sv-gprd.c.gitlab-production.internal`
* db primary: `ssh $USER-db-primary@console-01-sv-gprd.c.gitlab-production.internal`

```
ssh postgres-06-db-gstg.c.gitlab-staging-1.internal
sudo gitlab-psql
```

# Dashboards and debugging

* These dashboards might be useful during the failover:
    * Staging:
        * [Staging replication overview](https://dashboards.gitlab.net/d/000000249/postgresql-replication-overview&var-environment=gprd)
        * [Triage overview](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?refresh=30s&orgId=1&var-environment=gstg)
        * [DB failover dashboard](https://dashboards.gitlab.net/dashboards/f/bmK5DRLik/postgresql_patroni)
    * Production:
        * [Production replication overview](https://dashboards.gitlab.net/d/000000249/postgresql-replication-overview?orgId=1&var-environment=gstg)
        * [Triage overview](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?refresh=30s&orgId=1&var-environment=gprd)
        * [DB failover dashboard TBD]()

* Sentry includes application errors. 
    * Staging: https://sentry.gitlap.com/gitlab/staginggitlabcom/
    * Production:
        * Workhorse: https://sentry.gitlap.com/gitlab/gitlab-workhorse-gitlabcom/
        * Rails (backend): https://sentry.gitlap.com/gitlab/gitlabcom/
        * Rails (frontend): https://sentry.gitlap.com/gitlab/gitlabcom-clientside/
        * Gitaly (golang): https://sentry.gitlap.com/gitlab/gitaly-production/
        * Gitaly (ruby): https://sentry.gitlap.com/gitlab/gitlabcom-gitaly-ruby/
* The logs can be used to inspect any area of the stack in more detail
    * https://log.gitlab.net/


# ** PRODUCTION ONLY** T minus 3 days (Date TBD)

1. [ ] ☎ {+ Comms-Handler +}: announce in #general slack and on team call date of database failover.
1. [ ] ☎ {+ Comms-Handler +}: inform the Marketing and Support team about the upcoming Database failover.
1. [ ] Ensure this issue has been created on https://ops.gitlab.net/gitlab-com/gl-infra/db-migration, since `gitlab.com` will be unavailable during the real failover!!!
1. [ ] ☎ {+ Comms-Handler +}: Tweet about the failover from `@gitlab` and `@gitlabstatus`
    -  Tweet: `In a week we will shortly be undergoing some scheduled maintenance to our database services. We expect the duration to be less than 30 minutes. `
1. [ ] ☎ {+ Comms-Handler +}: Ensure that the maintenance window is scheduled on status.io.

# T minus 1 day (Date TBD)

1. [ ] 🐘 {+ Database-Wrangler +}: Compare DB parameters between the Postgres and the Patroni clusters
    * On the current master and current leader run: `gitlab-psql -c 'SHOW ALL;' | awk -F '|' 'NR>2 { gsub(/[ \t]+$/, "", $1); gsub(/[ \t]+$/, "", $2); print $1, $2 }' > patroni_or_postgres.conf`
    * Copy both outputs locally (e.g. using `scp`) and run `diff -u patroni.conf postgres.conf`
    * The changes should be minimal; like version differences, file locations, ..., but not essential parameters like shared_bufferes, ...
1. [ ] 🐘 {+ Database-Wrangler +}: Make sure the Patroni cluster does not have a pending restart
    * Restart them if they do. On any patroni node run: `gitlab-patronictl restart pg-ha-cluster --force`
1. [ ] **PRODUCTION ONLY** ☎ {+ Comms-Handler +}: Tweet from `@gitlab`.
    -  Tweet: `Tomorrow we will shortly be undergoing some scheduled maintenance to our database services. We expect the duration to be less than 30 minutes.`
1. [ ] **PRODUCTION ONLY** ☎ {+ Comms-Handler +}: Retweet `@gitlab` tweet from `@gitlabstatus` with further details
    -  Tweet: `In a week we will shortly be undergoing some scheduled maintenance to our database services. We expect the duration to be less than 30 minutes. Starting time : 12:00 UTC.`

# T minus 1 hour (__FAILOVER_DATE__) [📁](bin/scripts/02_failover/050_t-1h)

**STAGING FAILOVER TESTING ONLY**: to speed up testing, this step can be done less than 1 hour before failover

GitLab runners attempting to post artifacts back to GitLab.com during the
maintenance window will fail and the artifacts may be lost. To avoid this as
much as possible, we'll stop any new runner jobs from being picked up, starting
an hour before the scheduled maintenance window.

1. [ ] **PRODUCTION ONLY** ☎ {+ Comms-Handler +}: Tweet from `@gitlabstatus`
    -  Tweet: `As part of upcoming GitLab.com maintenance work, Please note that any CI jobs that start before the maintenance window but complete during the window period will fail and may need to be started again. __MAINTENANCE_END_TIME__ UTC. GitLab.com will undergo maintenance in 1 hour. Working doc: __GOOGLE_DOC_URL__`
1. [ ] ☎ {+ Comms-Handler +}: Post to #announcements on Slack:
    -  "Maintenance on database cluster, downtime 30 min. Starting at 12:00 UTC."
1. [ ] **PRODUCTION ONLY** ☁ {+ Cloud-conductor +}: Create a maintenance window in PagerDuty for [GitLab Production service](https://gitlab.pagerduty.com/services/PATDFCE) for 30 minutes starting in an hour from now.
1. [ ] **PRODUCTION ONLY** ☁ {+ Cloud-conductor +}: [Create an alert silence](https://alerts.gprd.gitlab.net/#/silences/new) for 30 minutes starting in an hour from now with the following matcher(s):
    - `environment`: `gprd`

- [ ] ☎ {+ Comms-Handler +}: Create a broadcast message
    * Staging: https://staging.gitlab.com/admin/broadcast_messages
    * Production: https://gitlab.com/admin/broadcast_messages
    * Text: We will shortly be undergoing some scheduled maintenance to our database services. We expect the duration to be less than 30 minutes. Please note that any CI jobs that start before the maintenance window but complete during the window period will fail and may need to be started again. LINK_TO_ISSUE_ETC
    * Start date: now
    * End date: now + 30 minutes

1. [ ] 🐘 {+ Database-Wrangler +}: Check that all MRs are rebased and contain the proper changes
    * Staging: https://ops.gitlab.net/gitlab-cookbooks/chef-repo/merge_requests?label_name%5B%5D=Patroni+Migration+-+Staging
    * Production: https://ops.gitlab.net/gitlab-cookbooks/chef-repo/merge_requests?label_name%5B%5D=Patroni+Migration+-+Production
1. [ ] 🐘 {+ Database-Wrangler +}: (`compare_db_params`) Compare DB parameters between the Postgres and the Patroni clusters
    * On the current master and current leader run: `gitlab-psql -c 'SHOW ALL;' | awk -F '|' 'NR>2 { gsub(/[ \t]+$/, "", $1); gsub(/[ \t]+$/, "", $2); print $1, $2 }' > patroni_or_postgres.conf`
    * Copy both outputs locally (e.g. using `scp`) and run `diff -u patroni.conf postgres.conf`
    * The changes should be minimal; like version differences, file locations, ..., but not essential parameters like shared_bufferes, ...
1. [ ] 🐘 {+ Database-Wrangler +}: (`restart_patroni_cluster_if_needed`) Make sure the Patroni cluster does not have a pending restart
    * Restart them if they do. On any patroni node run: `gitlab-patronictl restart pg-ha-cluster --force`
1. [ ] 🐘 {+ Database-Wrangler +}: (`verify_services_are_active`)
    * `knife ssh roles:$env-base-db-patroni "sudo systemctl status patroni"`
    * `knife ssh roles:$env-base-db-patroni "sudo systemctl status pgbouncer"`
    * `knife ssh roles:$env-base-db-patroni "sudo -u gitlab-psql /usr/lib/postgresql/9.6/bin/pg_ctl status -D /var/opt/gitlab/postgresql/data 2>/dev/null | grep 'server is running'"`


## Failover Call

These steps will be run in a Zoom call. The 🐺 {+ Coordinator +} runs the call,
asking other roles to perform each step on the checklist at the appropriate
time.

Changes are made one at a time, and verified before moving onto the next step.
Whoever is performing a change should share their screen and explain their
actions as they work through them. Everyone else should watch closely for
mistakes or errors! A few things to keep an especially sharp eye out for:

* Exposed credentials (except short-lived items like 2FA codes)
* Running commands against the wrong hosts
* Navigating to the wrong pages in web browsers (gstg vs. gprd, etc)

Remember that the intention is for the call to be broadcast live on the day. If
you see something happening that shouldn't be public, mention it.


### Roll call

- [ ] 🐺 {+ Coordinator +}: Ensure everyone mentioned above is on the call
- [ ] 🐺 {+ Coordinator +}: Ensure the Zoom room host is on the call

### Notify Users of Maintenance Window

1. [ ] **PRODUCTION ONLY** ☎ {+ Comms-Handler +}: Tweet from `@gitlabstatus`
    -  `GitLab.com will soon shutdown for planned maintenance for migration to Patroni. See you on the other side!`
1. [ ] **PRODUCTION ONLY** ☎ {+ Comms-Handler +}: Update maintenance status on status.io
    -  https://app.status.io/dashboard/5b36dc6502d06804c08349f7/maintenance/5b50be1e6e3499540bd86e65/edit
    - `GitLab.com  planned maintenance for migration to Patroni is starting. See you on the other side!`


### Health check

1. [ ] 🐺 {+ Coordinator +}: Ensure that there are no active critical alerts or open incidents:
    * Staging
        * https://dashboards.gitlab.net/d/SOn6MeNmk/alerts?orgId=1&var-interval=1m&var-environment=gstg&var-alertname=All&var-alertstate=All&var-prometheus=prometheus-01-inf-gstg&var-prometheus_app=prometheus-app-01-inf-gstg
    * Production
        * https://dashboards.gitlab.net/d/SOn6MeNmk/alerts?orgId=1&var-interval=1m&var-environment=gprd&var-alertname=All&var-alertstate=All&var-prometheus=prometheus-01-inf-gprd&var-prometheus_app=prometheus-app-01-inf-gprd
        * https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=incident

# T minus zero (failover day) (__FAILOVER_DATE__)

We expect the maintenance window to last for up to 30 minutes, starting from now.

## Failover Procedure

### Prevent updates to the primary

#### Block non-essential network access to the primary

1. [ ] 🔪 {+ Chef-Runner +}: (`block_access_to_gitlab_com`) Update HAProxy config to allow Geo and VPN traffic over HTTPS and drop everything else
    * Staging
        * Apply this MR: https://ops.gitlab.net/gitlab-cookbooks/chef-repo/merge_requests/282
        * Run `knife ssh -p 2222 roles:gstg-base-lb 'sudo chef-client'`
    * Production:
        * Apply this MR: TBD (was: https://dev.gitlab.org/cookbooks/chef-repo/merge_requests/2254)
        * Run `knife ssh -p 2222 roles:gprd-base-lb 'sudo chef-client'`
1. [ ] 🔪 {+ Chef-Runner +}: (`restart_haproxy_t_minus_zero_hour`) Restart HAProxy on all LBs to terminate any on-going connections
    * This terminates ongoing SSH, HTTP and HTTPS connections, including AltSSH
    * Staging: `knife ssh -p 2222 roles:gstg-base-lb 'sudo systemctl restart haproxy'`
    * Production: `knife ssh -p 2222 roles:gprd-base-lb 'sudo systemctl restart haproxy'`

Running CI jobs will no longer be able to push updates. Jobs that complete now may be lost.

### Initiate the database failover

1. [ ] 🔪 {+ Chef-Runner +}: (`stop_all_clients`) Stop Unicorn, Sidekiq and Mailroom
    * Staging:
        * `knife ssh roles:gstg-base' sudo gitlab-ctl stop unicorn'`
        * `knife ssh roles:gstg-base-be 'sudo gitlab-ctl stop sidekiq-cluster'`
        * `knife ssh roles:gstg-base-be 'sudo gitlab-ctl stop mailroom'`
    * Production:
        * `knife ssh roles:gprd-base' sudo gitlab-ctl stop unicorn'`
        * `knife ssh roles:gprd-base-be 'sudo gitlab-ctl stop sidekiq-cluster'`
        * `knife ssh roles:gprd-base-be 'sudo gitlab-ctl stop mailroom'`
1. [ ] 🔪 {+ Chef-Runner +}: (`stop_consul_on_postgres_cluster`) Stop consul across the Postgres cluster to avoid needless failover when we stop the master
    * Staging: `knife ssh roles:gstg-base-db-postgres 'sudo gitlab-ctl stop consul'`
    * Production: `knife ssh roles:gprd-base-db-postgres 'sudo gitlab-ctl stop consul'`
1. [ ] 🔪 {+ Chef-Runner +}: (`stop_repmgrd_on_postgres_cluster`) Stop repmgrd across the Postgres cluster to avoid needless failover when we stop the master
    * Staging: `knife ssh roles:gstg-base-db-postgres 'sudo gitlab-ctl stop repmgrd'`
    * Production: `knife ssh roles:gprd-base-db-postgres 'sudo gitlab-ctl stop repmgrd'`
1. [ ] 🔪 {+ Chef-Runner +}: (`stop_pgbouncer_on_postgres_cluster`) Stop pgbouncer across the Postgres cluster to terminate idle connection on the master
    * Staging: `knife ssh roles:gstg-base-db-postgres 'sudo gitlab-ctl stop pgbouncer'`
    * Production: `knife ssh roles:gprd-base-db-postgres 'sudo gitlab-ctl stop pgbouncer'`
1. [ ] 🐘 {+ Database-Wrangler +}: (`check_connections_on_master`) On the current master, Check for any existing connections
    * `sudo gitlab-psql; SELECT COUNT(*) FROM pg_stat_activity WHERE state !='idle'; SELECT state, COUNT(*) FROM pg_stat_activity GROUP BY state; SELECT * FROM pg_stat_activity WHERE state !='idle';`
1. [ ] 🐘 {+ Database-Wrangler +}: (`create_tombstone_object`) Create a DB object to be marked as tombstone
    * `sudo gitlab-psql -c "CREATE DATABASE patroni_pre_detach;"`
    * `sudo gitlab-psql -c "CHECKPOINT;"`
1. [ ] 🐘 {+ Database-Wrangler +}: (`check_tombstone_object_on_patroni_1`) On the current leader, check for for the presence of the tombstone object
    * `sudo gitlab-psql -c '\l' | grep patroni_pre_detach`
1. [ ] 🐘 {+ Database-Wrangler +}: (`switch_xlog_on_master`) If the tombstone object is not present, on the current master run `sudo gitlab-psql -c 'SELECT * FROM pg_switch_xlog()'`, repeat the step above
1. [ ] 🐘 {+ Database-Wrangler +}: (`verify_wal_position`) Execute the bellow query on the master, for verifying that there are no phantom writes, a zero means we're good to go:

```sql
SELECT
 pg_xlog_location_diff(pg_current_xlog_location(),sent_location) as local_queue_diff
FROM pg_stat_replication       sr
   JOIN pg_replication_slots   rs
   ON (sr.pid = rs.active_pid)
WHERE application_name like 'patroni%';
```
1. [ ] 🐘 {+ Database-Wrangler +}: (`stop_postgres_cluster`) Stop the Postgres cluster
    * Staging: `knife ssh roles:gstg-base-db-postgres 'sudo gitlab-ctl stop postgresql'`
    * Production: `knife ssh roles:gprd-base-db-postgres 'sudo gitlab-ctl stop postgresql'`
1. [ ] 🔪 {+ Chef-Runner +}: (`remove_standby_config`) Remove `standby_cluster` config from Patroni Chef role
    * Staging: https://ops.gitlab.net/gitlab-cookbooks/chef-repo/merge_requests/280
    * Production: https://ops.gitlab.net/gitlab-cookbooks/chef-repo/merge_requests/380
1. [ ] 🔪 {+ Chef-Runner +}: (`remove_standby_config`) Run chef-client across the Patroni cluster
    * Staging: `knife ssh roles:gstg-base-db-patroni 'sudo chef-client'`
    * Production: `knife ssh roles:gprd-base-db-patroni 'sudo chef-client'`
1. [ ] ☁ {+ Cloud-conductor +}: (`verify_leader_is_detached`) Verify that the current leader is no longer a standby leader and is accepting read/write connections
    * `gitlab-psql -c 'SELECT * FROM pg_is_in_recovery()' | grep f`
    * Check the logs at `/var/log/gitlab/postgresql/postgres.log`
1. [ ] 🔪 {+ Chef-Runner +}: (`verify_leader_is_marked_healthy`) Verify that Patroni ILB is marking the current leader as healthy from Patroni dashboard
1. [ ] 🔪 {+ Chef-Runner +}: (`analyze_database`) Run `ANALYZE` on the Patroni leader
    * `sudo nohup gitlab-psql -c 'ANALYZE VERBOSE;' &> /tmp/analyze_database.log &`
1. [ ] 🔪 {+ Chef-Runner +}: (`change_clients_db_config`) Change clients' DB host and DB load balancing hosts to point to Patroni ILB and the Patroni secondaries, respectively
    * Staging: https://ops.gitlab.net/gitlab-cookbooks/chef-repo/merge_requests/281
    * Production: https://ops.gitlab.net/gitlab-cookbooks/chef-repo/merge_requests/381
1. [ ] 🔪 {+ Chef-Runner +}: (`change_clients_db_config`) Apply changes across the fleet
    * Staging: `knife ssh roles:gstg-base 'sudo chef-client'`
    * Production: `knife ssh roles:gprd-base 'sudo chef-client'`
1. [ ] 🔪 {+ Chef-Runner +}: (`start_all_clients`) Restart the clients
    * Staging:
        * `knife ssh roles:gstg-base' sudo gitlab-ctl start unicorn'`
        * `knife ssh roles:gstg-base-be 'sudo gitlab-ctl start sidekiq-cluster'`
        * `knife ssh roles:gstg-base-be 'sudo gitlab-ctl start mailroom'`
    * Production:
        * `knife ssh roles:gprd-base' sudo gitlab-ctl start unicorn'`
        * `knife ssh roles:gprd-base-be 'sudo gitlab-ctl start sidekiq-cluster'`
        * `knife ssh roles:gprd-base-be 'sudo gitlab-ctl start mailroom'`

### Post failover healthchecks

1. [ ] 🐺 {+ Coordinator +}: Check for any alerts that might have been raised and investigate them
    * Staging: https://alerts.gstg.gitlab.net or #alerts-gstg in Slack
    * Production: https://alerts.gprd.gitlab.net or #alerts in Slack
1. [ ] 🐘 {+ Database-Wrangler +}: Archiving and backups
    * (`check_wale_archiving`) Check that WAL archiving works by monitoring progress of `ps aux | grep "postgres: pg-ha-cluster: archiver process"`
    * (`take_fresh_base_backup`) Take a fresh basebackup and make sure the process succeeds (tmux): `sudo -u gitlab-psql PATH=/usr/lib/postgresql/9.6/bin:$PATH PGPORT=5432 PGHOST=localhost PGUSER=gitlab-superuser nohup /usr/bin/envdir /etc/wal-e.d/env /opt/wal-e/bin/wal-e backup-push /var/opt/gitlab/postgresql/data &> /tmp/wal-e_backup_push.log &`


## During-Blackout QA

#### Verification with gitlab-qa

1. Run existing automated tests against the new environment
2. Run our new performance test automation
3. Run load tests using https://gitlab.com/gitlab-com/large-staging-collider

#### Evaluation of QA results - **Decision Point**


#### Commitment

If QA has succeeded, then we can continue to "Complete the Migration". If some
QA has failed, the 🐺 {+ Coordinator +} must decide whether to continue with the
failover, or to abort, failing back to REPMGR. A decision to continue in these
circumstances should be counter-signed by the 🎩 {+ Head Honcho +}.

The top priority is to maintain data integrity. Failing back after the blackout
window has ended is very difficult, and will result in any changes made in the
interim being lost.

Failures with an unknown cause should be investigated further. If we can't
determine the root cause within the blackout window, we should fail back.

We should abort for failures caused by missing data unless all the following apply:

If the number of failures seems high (double digits?), strongly consider failing
back even if they each seem trivial - the causes of each failure may interact in
unexpected ways.


## Complete the Migration (T plus 30 min )


#### Reconfiguration

1. [ ] 🐘 {+ Database-Wrangler +}:
    * Trigger a restore to check the backup is fine: https://gitlab.com/gitlab-restore/postgres-gprd/pipelines
    * Check that DR replicas have followed the timeline switch and do not lag behind (there's also an alert for this)
1. [ ] 🔪 {+ Chef-Runner +}: (`allow_access_to_gitlab_com`) Make the service accessible to the outside world
    * Staging: Revert https://ops.gitlab.net/gitlab-cookbooks/chef-repo/merge_requests/282
    * Production: Revert https://ops.gitlab.net/gitlab-cookbooks/chef-repo/merge_requests/382
1. [ ] 🔪 {+ Chef-Runner +}: (`restart_haproxy_t_plus_1_hour`) Restart HAProxy
    * Staging: `knife ssh -p 2222 roles:gstg-base-lb 'sudo systemctl restart haproxy'`
    * Production: `knife ssh -p 2222 roles:gprd-base-lb 'sudo systemctl restart haproxy'`

#### Communicate

1. [ ] 🐺 {+ Coordinator +}: Remove the broadcast message (if it's after the initial window, it has probably expired automatically)
1. [ ] **PRODUCTION ONLY** ☎ {+ Comms-Handler +}: Update maintenance status on status.io
    -  https://app.status.io/dashboard/5b36dc6502d06804c08349f7/maintenance/5b50be1e6e3499540bd86e65/edit
    - `GitLab.com  planned maintenance for migration to Patroni is almost complete. GitLab.com is available although we're continuing to verify that all systems are functioning correctly. ``
1. [ ] **PRODUCTION ONLY** ☎ {+ Comms-Handler +}: Tweet from `@gitlabstatus`
    -  `GitLab.com's migration to Patroni is almost complete. Site is back up, although we're continuing to verify that all systems are functioning correctly.`

#### Verification

1. **Start After-Blackout QA** This is the second half of the test plan.
    1. [ ] 🏆 {+ Quality +}: Ensure all "after the blackout" QA automated tests have succeeded
    1. [ ] 🏆 {+ Quality +}: Ensure all "after the blackout" QA manual tests have succeeded


## **PRODUCTION ONLY** Post migration

1. [ ] 🐺 {+ Coordinator +}: Close the failback issue - it isn't needed

/label ~"Failover Execution"