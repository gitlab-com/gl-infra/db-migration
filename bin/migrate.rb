require 'shellwords'

# Usage: ruby ./bin/failover.rb {gstg|gprd} [step_to_start_at]

# This is script is doesn't represent its author(s) or the company.
# Given enough time it could've looked better, but for now it works.

# Assumptions:
# * chef-repo is at ~/chef-repo or set CHEF_REPO_PATH envvar
# * knife is installed and runs properly inside the repo

CHEF_REPO_PATH = ENV['CHEF_REPO_PATH'] || File.expand_path('~/chef-repo')

$direction = ARGV.shift
$environment = ARGV.shift
$starting_step = ARGV.shift.to_s

raise "Direction has to be either 'failover' or 'failback', got '#{$direction}'" unless %w(failover failback).include?($direction)
raise "Environment has to be either 'gstg' or 'gprd', got '#{$environment}'" unless %w(gstg gprd).include?($environment)

class Colorizer
  CLEAR  = "\e[0m"
  GREEN  = "\e[32m"
  WHITE  = "\e[37m"
  YELLOW = "\e[33m"

  # Both #set_color and #lookup_color are copied from thor gem,
  # slightly modified, though
  def set_color(string, *colors)
    if colors.compact.empty?
      string
    elsif colors.all? { |color| color.is_a?(Symbol) }
      ansi_colors = colors.map { |color| lookup_color(color) }
      "#{ansi_colors.join}#{string}#{CLEAR}"
    end
  end

  private

  def lookup_color(color)
    return color unless color.is_a?(Symbol)

    self.class.const_get(color.to_s.upcase)
  end
end

$colorizer = Colorizer.new

PATRONI_NODES = {
  'gstg' => [
    'patroni-01-db-gstg.c.gitlab-staging-1.internal',
    'patroni-02-db-gstg.c.gitlab-staging-1.internal',
    'patroni-03-db-gstg.c.gitlab-staging-1.internal',
    'patroni-04-db-gstg.c.gitlab-staging-1.internal',
    'patroni-05-db-gstg.c.gitlab-staging-1.internal',
    'patroni-06-db-gstg.c.gitlab-staging-1.internal',
  ],
  'gprd' => [
    'patroni-01-db-gprd.c.gitlab-production.internal',
    'patroni-02-db-gprd.c.gitlab-production.internal',
    'patroni-03-db-gprd.c.gitlab-production.internal',
    'patroni-04-db-gprd.c.gitlab-production.internal',
    'patroni-05-db-gprd.c.gitlab-production.internal',
    'patroni-06-db-gprd.c.gitlab-production.internal',
  ]
}

POSTGRES_NODES = {
  'gstg' => [
    'postgres-01-db-gstg.c.gitlab-staging-1.internal',
    'postgres-02-db-gstg.c.gitlab-staging-1.internal',
    'postgres-03-db-gstg.c.gitlab-staging-1.internal',
    'postgres-04-db-gstg.c.gitlab-staging-1.internal',
    'postgres-05-db-gstg.c.gitlab-staging-1.internal',
    'postgres-06-db-gstg.c.gitlab-staging-1.internal',
  ],
  'gprd' => [
    'postgres-01-db-gprd.c.gitlab-production.internal',
    'postgres-02-db-gprd.c.gitlab-production.internal',
    'postgres-03-db-gprd.c.gitlab-production.internal',
    'postgres-04-db-gprd.c.gitlab-production.internal',
    'postgres-05-db-gprd.c.gitlab-production.internal',
    'postgres-06-db-gprd.c.gitlab-production.internal',
  ]
}

FAILOVER_STEPS = [
  'compare_db_params',
  'restart_patroni_cluster_if_needed',
  'verify_services_are_active',
  #'expire_jobs_after_1_hour',
  #'snapshot_dbs_t_minus_1_hour',
  'block_access_to_gitlab_com',
  'restart_haproxy_t_minus_zero_hour',
  #'snapshot_dbs_t_minus_0_hour',
  'stop_all_clients',
  'stop_consul_on_postgres_cluster',
  'stop_repmgrd_on_postgres_cluster',
  'stop_pgbouncer_on_postgres_cluster',
  'check_connections_on_master',
  'create_tombstone_object',
  'check_tombstone_object_on_patroni_1',
  # 'switch_xlog_on_master',
  # 'check_tombstone_object_on_patroni_2',
  'verify_wal_position',
  'stop_postgres_cluster',
  'remove_standby_config',
  'verify_leader_is_detached',
  'verify_leader_is_marked_healthy',
  'analyze_database',
  'change_clients_db_config',
  'start_all_clients',
  'check_wale_archiving',
  'take_fresh_base_backup',
  #'expire_jobs_after_3_hour',
  'allow_access_to_gitlab_com',
  'restart_haproxy_t_plus_1_hour'
]

FAILBACK_STEPS = [
  'start_postgres_cluster',
  'start_pgbouncer_on_postgres_cluster',
  'start_repmgrd_on_postgres_cluster',
  'start_consul_on_postgres_cluster',
  'revert_clients_db_config',
]

## Helper methods
def run_cmd(cmd, allow_failure: false)
  puts $colorizer.set_color("===> #{cmd}", :green)
  output = `#{cmd}`
  exit_status = $?.exitstatus.to_i

  puts output
  raise "#{cmd} failed to execute. Exit status = #{exit_status}" if exit_status != 0 && !allow_failure

  output
end

def input(msg)
  print $colorizer.set_color(msg + " ", :white)
  gets.strip
end

def bannered_text(text)
  puts '-' * 75
  puts text
  puts '-' * 75
end

def yes_no_input(question, exit_if_no: true)
  answer = input(question + " [y,N]")
  return true if answer == 'y'

  if exit_if_no
    exit
  else
    false
  end
end

def repeatable_step
  loop do
    yield

    return unless yes_no_input('Do you want to repeat this step?', exit_if_no: false)
  end
end

def print_list(list)
  list.each_with_index do |item, index|
    puts "[#{index + 1}] #{item}"
  end
end

def in_chef_repo
  output = nil

  Dir.chdir(CHEF_REPO_PATH) do
    output = yield
  end

  output
end

def roles_to_apply_from_branch(branch_name)
  roles_to_apply = []

  in_chef_repo do
    run_cmd("git checkout #{branch_name}")

    merge_base = run_cmd("git merge-base HEAD master").strip
    run_cmd("git diff #{merge_base}..HEAD")

    roles_to_apply = run_cmd("git diff --numstat #{merge_base}..HEAD | cut -f3").split
  end

  roles_to_apply
end

def apply_roles_from_branch(branch_name)
  in_chef_repo do
    run_cmd("git checkout #{branch_name}")

    roles_to_apply = roles_to_apply_from_branch(branch_name)
    apply_roles(roles_to_apply)
  end
end

def apply_roles(roles_to_apply)
  print_list(roles_to_apply)

  yes_no_input("The roles above will be applied, proceed?")
  run_cmd("knife role from file #{roles_to_apply.join(' ')}")

  roles_query = roles_to_apply.map { |role| "roles:#{role.gsub(%r{^roles/(.*?).json}, '\1')}" }.join(' OR ').shellescape
  chef_cmd = 'sudo chef-client'.shellescape

  run_cmd("knife ssh -p2222 #{roles_query} #{chef_cmd}", allow_failure: true)
  run_cmd("knife ssh #{roles_query} #{chef_cmd}", allow_failure: true) # Sue me!
end

def select_patroni_leader
  return $patroni_leader if $patroni_leader

  chef_cmd = 'gitlab-patronictl list 2>/dev/null | grep Leader | cut -d\| -f3'.shellescape
  output = in_chef_repo { run_cmd("knife ssh roles:#{$environment}-base-db-patroni #{chef_cmd} | head -1") }

  $patroni_leader = output.split.last
end

def select_postgres_host
  return $postgres_master if $postgres_master

  chef_cmd = 'sudo gitlab-psql -c "select * from pg_is_in_recovery();"'.shellescape
  output = in_chef_repo { run_cmd("knife ssh roles:#{$environment}-base-db-postgres #{chef_cmd} | grep f") }

  $postgres_master = output.split.first
end

## Steps
def step_compare_db_params
  patroni_host = select_patroni_leader
  postgres_host = select_postgres_host

  cmd = "sudo gitlab-psql -c 'SHOW ALL;' | awk -F '|' 'NR>2 { gsub(/[ \\t]+$/, \"\", $1); gsub(/[ \\t]+$/, \"\", $2); print $1, $2 }'".shellescape
  {postgres: postgres_host, patroni: patroni_host}.each do |cluster, host|
    run_cmd(%Q(ssh #{host} #{cmd} > #{cluster}.conf))
  end

  run_cmd('diff -u patroni.conf postgres.conf', allow_failure: true)
end

def step_restart_patroni_cluster_if_needed
  list_cmd = "gitlab-patronictl list".shellescape

  run_cmd(%Q(ssh #{PATRONI_NODES[$environment][0]} #{list_cmd}))

  print_list(PATRONI_NODES[$environment])
  indexes = input("Patroni hostname(s) to be restarted (comma separated):")
  if indexes.empty?
    puts "No restart selection!"
    return
  end

  patroni_memebers = indexes.split(',').map { |index| PATRONI_NODES[$environment][index.to_i] }.join(' ')
  restart_cmd = "gitlab-patronictl restart --force pg-ha-cluster #{patroni_memebers}".shellescape
  run_cmd(%Q(ssh #{PATRONI_NODES[$environment][0]} #{cmd}))
end

def step_verify_services_are_active
  %w(patroni pgbouncer).each do |service|
    bannered_text(service)
    chef_cmd = "sudo PAGER=cat systemctl status #{service} | grep 'active (running)'".shellescape
    in_chef_repo { run_cmd("knife ssh roles:#{$environment}-base-db-patroni #{chef_cmd}") }
  end

  bannered_text('PostgreSQL')
  chef_cmd = "sudo -u gitlab-psql /usr/lib/postgresql/9.6/bin/pg_ctl status -D /var/opt/gitlab/postgresql/data 2>/dev/null | grep 'server is running'".shellescape
  in_chef_repo { run_cmd("knife ssh roles:#{$environment}-base-db-patroni #{chef_cmd}") }
end

def step_expire_jobs_after_1_hour
  ruby_cmd = 'Ci::Runner.instance_type.where("id NOT IN (?)", Ci::Runner.instance_type.joins(:taggings).joins(:tags).where("tags.name = ?", "gitlab-org").pluck(:id)).update_all(maximum_timeout: 3600)'.shellescape
  run_cmd("sudo gitlab-rails r #{ruby_cmd}")
end

def step_block_ci_jobs
  apply_roles_from_branch("#{$environment}-disable-ci-requests")
end

def step_snapshot_dbs_t_minus_1_hour
  puts "Please run `./bin/snapshot-dbs #{$environment}` from the db-migrate locally"
end
alias step_snapshot_dbs_t_minus_0_hour step_snapshot_dbs_t_minus_1_hour

def step_block_access_to_gitlab_com
  apply_roles_from_branch("#{$environment}-deny-inbound-traffic")
end

def step_restart_haproxy_t_minus_zero_hour
  chef_cmd = 'sudo systemctl restart haproxy'.shellescape
  in_chef_repo { run_cmd("knife ssh -p 2222 roles:#{$environment}-base-lb #{chef_cmd}") }
end
alias step_restart_haproxy_t_plus_1_hour step_restart_haproxy_t_minus_zero_hour

def step_stop_all_clients
  in_chef_repo do
    unicorn_cmd = "sudo gitlab-ctl stop unicorn".shellescape
    unicorn_query = "roles:#{$environment}-base AND omnibus-gitlab_gitlab_rb_unicorn_enable:true".shellescape
    run_cmd("knife ssh #{unicorn_query} #{unicorn_cmd}")

    sidekiq_cmd = "sudo gitlab-ctl stop sidekiq-cluster".shellescape
    run_cmd("knife ssh roles:#{$environment}-base-be #{sidekiq_cmd}")

    mailroom_cmd = "sudo gitlab-ctl stop mailroom".shellescape
    run_cmd("knife ssh roles:#{$environment}-base-be #{mailroom_cmd}")
  end
end

def step_check_connections_on_master
  repeatable_step do
    postgres_master = select_postgres_host

    bannered_text("Number of connection that are not idle:")
    psql_cmd = "sudo gitlab-psql -c \"SELECT COUNT(*) FROM pg_stat_activity WHERE state !='idle';\"".shellescape
    run_cmd("ssh #{postgres_master} #{psql_cmd}")

    bannered_text("Connection that are not idle:")
    psql_cmd = "sudo gitlab-psql -xc \"SELECT * FROM pg_stat_activity WHERE state !='idle';\"".shellescape
    run_cmd("ssh #{postgres_master} #{psql_cmd}")

    bannered_text("Number of connection grouped by state")
    psql_cmd = "sudo gitlab-psql -c \"SELECT state, COUNT(*) FROM pg_stat_activity GROUP BY state;\"".shellescape
    run_cmd("ssh #{postgres_master} #{psql_cmd}")
  end
end

def step_create_tombstone_object
  postgres_master = select_postgres_host

  [
    'CREATE DATABASE patroni_pre_detach;',
    'CHECKPOINT;'
  ].each do |query|
    psql_cmd = "sudo gitlab-psql -c '#{query}'".shellescape
    run_cmd("ssh #{postgres_master} #{psql_cmd}")
  end
end

def step_check_tombstone_object_on_patroni_1
  patroni_leader = select_patroni_leader

  psql_cmd = "sudo gitlab-psql -c '\\l'".shellescape
  run_cmd("ssh #{patroni_leader} #{psql_cmd} | grep patroni_pre_detach")
end
alias step_check_tombstone_object_on_patroni_2 step_check_tombstone_object_on_patroni_1

def step_switch_xlog_on_master
  postgres_master = select_postgres_host

  psql_cmd = "sudo gitlab-psql -x -c 'SELECT * FROM pg_switch_xlog()'".shellescape
  run_cmd("ssh #{postgres_master} #{psql_cmd}")
end

def step_verify_wal_position
  repeatable_step do
    postgres_master = select_postgres_host

    psql_query = <<~CMD
SELECT
 pg_xlog_location_diff(pg_current_xlog_location(),sent_location) as local_queue_diff
FROM pg_stat_replication       sr
   JOIN pg_replication_slots   rs
   ON (sr.pid = rs.active_pid)
WHERE application_name like 'patroni%';
    CMD

    psql_cmd = "sudo gitlab-psql -At -c \"#{psql_query}\"".shellescape
    output = run_cmd("ssh #{postgres_master} #{psql_cmd}")
    if output.strip == '0'
      bannered_text("Local diff is zero, you are good to go!")
    else
      bannered_text("Local diff IS NOT zero, you ARE NOT good to go!")
    end
  end
end

def step_stop_consul_on_postgres_cluster
  chef_cmd = 'sudo gitlab-ctl stop consul'.shellescape
  in_chef_repo { run_cmd("knife ssh roles:#{$environment}-base-db-postgres #{chef_cmd}") }
end

def step_stop_repmgrd_on_postgres_cluster
  chef_cmd = 'sudo gitlab-ctl stop repmgrd'.shellescape
  in_chef_repo { run_cmd("knife ssh roles:#{$environment}-base-db-postgres #{chef_cmd}") }
end

def step_stop_pgbouncer_on_postgres_cluster
  chef_cmd = 'sudo gitlab-ctl stop pgbouncer'.shellescape
  in_chef_repo { run_cmd("knife ssh roles:#{$environment}-base-db-postgres #{chef_cmd}") }
end

def step_stop_postgres_cluster
  chef_cmd = 'sudo gitlab-ctl stop postgresql'.shellescape
  in_chef_repo { run_cmd("knife ssh roles:#{$environment}-base-db-postgres #{chef_cmd}") }
end

def step_remove_standby_config
  apply_roles_from_branch("#{$environment}-remove-patroni-standby")
  input('NOTE: Merge the MR containing this change into master, DO IT NOW!')
end

def step_verify_leader_is_detached
  patroni_leader = select_patroni_leader

  psql_cmd = "sudo gitlab-psql -c 'SELECT * FROM pg_is_in_recovery()'".shellescape
  run_cmd("ssh #{patroni_leader} #{psql_cmd} | grep f")
end

# TODO: Improve using API?
def step_verify_leader_is_marked_healthy
  yes_no_input("Go to the GCP dashboard and check that the ILB has ONE healthy node, I'll wait ...")
end

def step_analyze_database
  patroni_leader = select_patroni_leader

  analyze_cmd = "echo 'SET statement_timeout=0; ANALYZE VERBOSE;' | sudo nohup gitlab-psql &> /tmp/analyze_database.log &".shellescape
  run_cmd("ssh #{patroni_leader} #{analyze_cmd}")
end

def step_change_clients_db_config
  apply_roles_from_branch("#{$environment}-use-patroni-hosts-in-clients")
  input('NOTE: Merge the MR containing this change into master, DO IT NOW!')
end

def step_start_all_clients
  in_chef_repo do
    unicorn_cmd = "sudo gitlab-ctl start unicorn".shellescape
    unicorn_query = "roles:#{$environment}-base AND omnibus-gitlab_gitlab_rb_unicorn_enable:true".shellescape
    run_cmd("knife ssh #{unicorn_query} #{unicorn_cmd}")

    sidekiq_cmd = "sudo gitlab-ctl start sidekiq-cluster".shellescape
    run_cmd("knife ssh roles:#{$environment}-base-be #{sidekiq_cmd}")

    mailroom_cmd = "sudo gitlab-ctl start mailroom".shellescape
    run_cmd("knife ssh roles:#{$environment}-base-be #{mailroom_cmd}")
  end
end

def step_check_wale_archiving
  patroni_leader = select_patroni_leader

  check_cmd = "ps aux | grep 'postgres: pg-ha-cluster: archiver process' | grep -v grep".shellescape
  run_cmd("ssh #{patroni_leader} #{check_cmd}")
end

def step_take_fresh_base_backup
  patroni_leader = select_patroni_leader

  backup_cmd = "cd /tmp; sudo -u gitlab-psql PATH=/usr/lib/postgresql/9.6/bin:$PATH PGPORT=5432 PGHOST=localhost PGUSER=gitlab-superuser nohup /usr/bin/envdir /etc/wal-e.d/env /opt/wal-e/bin/wal-e backup-push /var/opt/gitlab/postgresql/data &> /tmp/wal-e_backup_push.log &".shellescape
  run_cmd("ssh #{patroni_leader} #{backup_cmd}")
end

def step_expire_jobs_after_3_hour
  ruby_cmd = 'Ci::Runner.instance_type.where("id NOT IN (?)", Ci::Runner.instance_type.joins(:taggings).joins(:tags).where("tags.name = ?", "gitlab-org").pluck(:id)).update_all(maximum_timeout: 10800)'.shellescape
  run_cmd("sudo gitlab-rails r #{ruby_cmd}")
end

def step_allow_access_to_gitlab_com
  branch_name = "#{$environment}-deny-inbound-traffic"

  roles_to_apply = roles_to_apply_from_branch(branch_name)

  in_chef_repo do
    run_cmd("git checkout master")

    apply_roles(roles_to_apply)
  end
end

def step_start_postgres_cluster
  chef_cmd = 'sudo gitlab-ctl start postgresql'.shellescape
  in_chef_repo { run_cmd("knife ssh roles:#{$environment}-base-db-postgres #{chef_cmd}") }
end

def step_start_consul_on_postgres_cluster
  chef_cmd = 'sudo gitlab-ctl start consul'.shellescape
  in_chef_repo { run_cmd("knife ssh roles:#{$environment}-base-db-postgres #{chef_cmd}") }
end

def step_start_repmgrd_on_postgres_cluster
  chef_cmd = 'sudo gitlab-ctl start repmgrd'.shellescape
  in_chef_repo { run_cmd("knife ssh roles:#{$environment}-base-db-postgres #{chef_cmd}") }
end

def step_start_pgbouncer_on_postgres_cluster
  chef_cmd = 'sudo gitlab-ctl start pgbouncer'.shellescape
  in_chef_repo { run_cmd("knife ssh roles:#{$environment}-base-db-postgres #{chef_cmd}") }
end

def step_revert_clients_db_config
  branch_name = "#{$environment}-use-patroni-hosts-in-clients"

  roles_to_apply = roles_to_apply_from_branch(branch_name)

  in_chef_repo do
    run_cmd("git checkout master")

    apply_roles(roles_to_apply)
  end
end

# -----------------------------------------------------------------------------------------------------
steps = $direction == 'failover' ? FAILOVER_STEPS : FAILBACK_STEPS
starting_step_index = $starting_step.empty? ? 0 : steps.index($starting_step)

steps[starting_step_index..-1].each do |step_name|
  begin
    yes_no_input("Do you want to start step #{step_name}?")

    method("step_#{step_name}").call
    puts $colorizer.set_color(('=' * 30) + " End of step #{step_name} " + ('=' * 30), :yellow)
    puts
  rescue => e
    puts e.inspect
    retry if yes_no_input('Do you want to retry this step?')
  end
end
