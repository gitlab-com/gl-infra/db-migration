#!/bin/bash

########################################################################################################################################################
## This script fetches WAL files from a GCS stoage location into a local restore dir using wal-g, it will fetch the newest WAL_COUNT archived WAL files
########################################################################################################################################################

WALG_ENV_DIR="/etc/wal-g.d/env-gprd"
GCS_CRED_FILE="/etc/wal-g.d/gcs-gprd.json"
GCS_WAL_LOC="gs://gitlab-gprd-postgres-backup/pitr-walg-main-v14/wal_005"
RESTORE_DIR="/var/opt/gitlab/wal_restore"
WAL_LIST_DL_FILE="wal_list.download"
WAL_COUNT="150"

gcloud auth login --cred-file=${GCS_CRED_FILE}
gsutil ls -l $GCS_WAL_LOC/* | sort -k3 | tail -n ${WAL_COUNT} | awk -F"/" '{print $6}' | awk -F"." '{print $1}' > ${RESTORE_DIR}/${WAL_LIST_DL_FILE}

## Is better to have the list of files in a file, as the "gsutil ls" is quite slow to execute to build the list. Makes easier to iterate over WAL_LIST_DL_FILE if there's any error after building the list.

if [ $(wc -l < ${RESTORE_DIR}/${WAL_LIST_DL_FILE}) -gt 0 ]
then
  for WAL_FILE in $(cat ${RESTORE_DIR}/${WAL_LIST_DL_FILE})
  do
    /usr/bin/envdir $WALG_ENV_DIR /opt/wal-g/bin/wal-g wal-fetch $WAL_FILE $RESTORE_DIR/$WAL_FILE
  done
fi