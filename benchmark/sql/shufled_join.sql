CREATE OR REPLACE FUNCTION public.shufled_join(text[], text[], OUT val1 text, OUT val2 text)
 RETURNS SETOF record
 LANGUAGE sql
AS $function$
with v1 as
 (
 select *, rank() over (order by random())
  from unnest($1::text[])
  order by random()
) ,

v2 as (select *, rank() over (order by random()) from unnest($2::text[]))

select v1.unnest, v2.unnest from v1 join v2 using(rank)


$function$
;




CREATE OR REPLACE FUNCTION public.shufled_join(text[], text[], text[], text[],text[],text[],text[], 
OUT val1 text, OUT val2 text, OUT val3 text, OUT val4 text, OUT val5 text, OUT val6 text, out val7 text)
 RETURNS SETOF record
 LANGUAGE sql
AS $function$
with v1 as
 (
 select *, rank() over (order by random())
  from unnest($1::text[])
  order by random()
) ,

v2 as (select *, rank() over (order by random()) from unnest($2::text[])),
v3 as (select *, rank() over (order by random()) from unnest($3::text[])),
v4 as (select *, rank() over (order by random()) from unnest($4::text[])),
v5 as (select *, rank() over (order by random()) from unnest($5::text[])),
v6 as (select *, rank() over (order by random()) from unnest($6::text[])),
v7 as (select *, rank() over (order by random()) from unnest($7::text[]))

select v1.unnest, v2.unnest,v3.unnest,v4.unnest, v5.unnest, v6.unnest, v7.unnest 
from v1 join v2 using(rank) join v3 using(rank) join v4 using(rank)
join v5 using(rank) join v6 using(rank) join v7 using(rank)

$function$
;

CREATE OR REPLACE FUNCTION public.shufled_join(text[], text[], text[], 
OUT val1 text, OUT val2 text, OUT val3 text)
 RETURNS SETOF record
 LANGUAGE sql
AS $function$
with v1 as
 (
 select *, rank() over (order by random())
  from unnest($1::text[])
  order by random()
) ,

v2 as (select *, rank() over (order by random()) from unnest($2::text[])),
v3 as (select *, rank() over (order by random()) from unnest($3::text[]))

select v1.unnest, v2.unnest,v3.unnest
from v1 join v2 using(rank) join v3 using(rank)
$function$
;

CREATE OR REPLACE FUNCTION public.shufled_join(text[], text[], text[], text[],
OUT val1 text, OUT val2 text, OUT val3 text, OUT val4 text)
 RETURNS SETOF record
 LANGUAGE sql
AS $function$
with v1 as
 (
 select *, rank() over (order by random())
  from unnest($1::text[])
  order by random()
) ,

v2 as (select *, rank() over (order by random()) from unnest($2::text[])),
v3 as (select *, rank() over (order by random()) from unnest($3::text[])),
v4 as (select *, rank() over (order by random()) from unnest($4::text[]))

select v1.unnest, v2.unnest,v3.unnest, v4.unnest
from v1 join v2 using(rank) join v3 using(rank)
join v4 using(rank)
$function$
;

CREATE OR REPLACE FUNCTION public.shufled_join(text[], text[], text[], text[], text[],
OUT val1 text, OUT val2 text, OUT val3 text, OUT val4 text, OUT val5 text)
 RETURNS SETOF record
 LANGUAGE sql
AS $function$
with v1 as
 (
 select *, rank() over (order by random())
  from unnest($1::text[])
  order by random()
) ,

v2 as (select *, rank() over (order by random()) from unnest($2::text[])),
v3 as (select *, rank() over (order by random()) from unnest($3::text[])),
v4 as (select *, rank() over (order by random()) from unnest($4::text[])),
v5 as (select *, rank() over (order by random()) from unnest($5::text[]))

select v1.unnest, v2.unnest,v3.unnest, v4.unnest, v5.unnest
from v1 join v2 using(rank) join v3 using(rank)
join v4 using(rank)
join v5 using (rank)
$function$
;