CREATE OR REPLACE FUNCTION random_int(int)
returns bigint
as
$$
SELECT floor(random() * $1 + 1)::int;
$$ language sql