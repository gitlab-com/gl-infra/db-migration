CREATE OR REPLACE FUNCTION public.weigthted_selection(ids anyarray, percents anyarray)
 RETURNS anyelement
 LANGUAGE sql
AS $function$
with myvalues(id, percent) as (select * from unnest($1,$2) ),
CTE AS (
    SELECT random() * (SELECT SUM(percent) FROM myvalues) R
)
SELECT id
FROM (
    SELECT id, SUM(percent) OVER (ORDER BY id) S, R
    FROM myvalues CROSS JOIN CTE
) Q
WHERE S >= R
ORDER BY id
limit 1;
$function$
;          

CREATE OR REPLACE FUNCTION public.weigthted_selection(ids boolean[], percents anyarray)
 RETURNS boolean
 LANGUAGE sql
AS $function$
with myvalues(id, percent) as (select * from unnest($1,$2) ),
CTE AS (
    SELECT random() * (SELECT SUM(percent) FROM myvalues) R
)
SELECT id
FROM (
    SELECT id, SUM(percent) OVER (ORDER BY id) S, R
    FROM myvalues CROSS JOIN CTE
) Q
WHERE S >= R
ORDER BY id
limit 1;
$function$
;

CREATE OR REPLACE FUNCTION public.weigthted_selection(ids text[], percents anyarray)
 RETURNS text
 LANGUAGE sql
AS $function$
with myvalues(id, percent) as (select * from unnest($1,$2) ),
CTE AS (
    SELECT random() * (SELECT SUM(percent) FROM myvalues) R
)
SELECT id
FROM (
    SELECT id, SUM(percent) OVER (ORDER BY id) S, R
    FROM myvalues CROSS JOIN CTE
) Q
WHERE S >= R
ORDER BY id
limit 1;
$function$