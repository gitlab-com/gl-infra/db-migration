CREATE OR REPLACE FUNCTION public.randomize(text[])
RETURNS SETOF text
LANGUAGE sql
AS $function$
select * from unnest($1::text[]) 
order by random()
$function$
;