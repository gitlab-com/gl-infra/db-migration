
create database wkload_jmeter;

\c wkload_jmeter;

--Create pgss_sampling and pgsa_sampling tables:

drop table if exists pgss_sampling_writer; 
create table pgss_sampling_writer (date_captured timestamptz, queryid bigint, query text, calls bigint, total_time double precision);
drop table if exists pgsa_sampling_writer; 
create table pgsa_sampling_writer (date_captured timestamptz, query text);

drop table if exists pgss_sampling_reader; 
create table pgss_sampling_reader (date_captured timestamptz, queryid bigint, query text, calls bigint, total_time double precision);
drop table if exists pgsa_sampling_reader; 
create table pgsa_sampling_reader (date_captured timestamptz, query text);



--Import csv:

truncate table pgss_sampling_writer;
\copy pgss_sampling_writer from '/tmp/wkload-jmeter-temp/pgss_sampling-patroni-main-2004-04-db-gprd.csv' csv delimiter ',';

truncate table pgsa_sampling_writer;
\copy pgsa_sampling_writer from '/tmp/wkload-jmeter-temp/pgsa_sampling-patroni-main-2004-04-db-gprd.csv' csv delimiter ',';

truncate table pgss_sampling_reader;
\copy pgss_sampling_reader from '/tmp/wkload-jmeter-temp/pgss_sampling-patroni-main-2004-01-db-gprd.csv' csv delimiter ',';

truncate table pgsa_sampling_reader;
\copy pgsa_sampling_reader from '/tmp/wkload-jmeter-temp/pgsa_sampling-patroni-main-2004-01-db-gprd.csv' csv delimiter ',';



--Create pgss_fingerprint, pgsa_fingerprint tables

drop table if exists pgss_fingerprint_writer;
create table pgss_fingerprint_writer (queryid text, query text, fingerprint text);

drop table if exists pgsa_fingerprint_writer; 
create table pgsa_fingerprint_writer (query text, fingerprint text);

drop table if exists pgss_fingerprint_reader;
create table pgss_fingerprint_reader (queryid text, query text, fingerprint text);

drop table if exists pgsa_fingerprint_reader; 
create table pgsa_fingerprint_reader (query text, fingerprint text);

create index ix_pgss_fingerprint_reader_1 on pgss_fingerprint_reader(fingerprint);
create index ix_pgsa_fingerprint_reader_1 on pgsa_fingerprint_reader(fingerprint);
create index ix_pgss_sampling_reader_1 on pgss_sampling_reader(CAST(queryid AS bigint));
create index ix_pgss_fingerprint_reader_2 on pgss_fingerprint_reader(queryid);

analyze verbose pgss_sampling_reader;

create index ix_pgss_fingerprint_writer_1 on pgss_fingerprint_writer(fingerprint);
create index ix_pgsa_fingerprint_writer_1 on pgsa_fingerprint_writer(fingerprint);
create index ix_pgss_sampling_writer_1 on pgss_sampling_writer(CAST(queryid AS bigint));
create index ix_pgss_fingerprint_writer_2 on pgss_fingerprint_writer(queryid);

analyze verbose pgss_sampling_writer;