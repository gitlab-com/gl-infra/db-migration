#!/bin/bash 

hostname=$(hostname)

sudo /usr/local/bin/gitlab-psql -XAtc "
  copy (
  with ss_data as (select * from pg_stat_statements)
    select
      date_trunc('minute', clock_timestamp()) as date_captured,
      queryid,
      regexp_replace(query, E'[ \\t\\n\\r]+', ' ', 'g') as query,
      calls,
      total_time
    from (select * from ss_data order by total_time desc limit 100) a
    union
    select
      date_trunc('minute', clock_timestamp()) as date_captured,
      queryid,
      regexp_replace(query, E'[ \\t\\n\\r]+', ' ', 'g') as query,
      calls,
      total_time
    from (select * from ss_data) a
    ) to stdout csv delimiter ',' \
" 2>>/tmp/pgss_sampling-$hostname.error.log >>/tmp/pgss_sampling-$hostname.csv

exit $?