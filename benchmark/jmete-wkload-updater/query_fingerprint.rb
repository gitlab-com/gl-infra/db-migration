#!/usr/bin/env ruby

## This script parses the SQL statements from the "sample" tables to populate the "fingerprint" tables for both, PGSS (pg_stat_statements) amd PGSA (pg_stat_activity)

require 'pg_query'
require 'pg'
require 'logger'

log = Logger.new(STDOUT)
log.level = Logger::INFO

p_instance_role = 'writer'
p_dbhost = 'localhost'
p_dbport = '5432'
p_dbname = 'wkload_jmeter'
p_dbuser = 'gitlab-superuser'

p_pgss_sample_percentage = 0.8
p_sort_column = 'total_time' ##We can order PGSS by 'total_time' or 'calls' columns

con = PG.connect(:host => p_dbhost, :dbname => p_dbname, :user => p_dbuser, :port => p_dbport)

v_pgss_sample_tb = 'pgss_sampling_' + p_instance_role
v_pgss_fgprnt_tb = 'pgss_fingerprint_' + p_instance_role

v_pgsa_sample_tb = 'pgsa_sampling_' + p_instance_role
v_pgsa_fgprnt_tb = 'pgsa_fingerprint_' + p_instance_role

## We truncate the fingerprint tables every time the script is executed to avoid duplicated entries
begin
  con.exec "truncate table #{v_pgss_fgprnt_tb}"
  con.exec "truncate table #{v_pgsa_fgprnt_tb}"
rescue StandardError => e
  log.error "ERROR message: %s" % [e.message]
  log.error "ERROR DETAILS: %s" % [e.backtrace.inspect]
end

# to filter the top SQL percentage of calls or total_time we need to get the whole sum of the sorting column
rs = con.exec "SELECT sum(#{p_sort_column}) as sum_#{p_sort_column} from #{v_pgss_sample_tb}"
v_pgss_sort_total = rs[0]["sum_#{p_sort_column}"].to_i

### Process PGSS sampling
v_sum_sorting = 0
v_pgss_count = 0
v_pgss_errors = 0
rs = con.exec "SELECT queryid, query, sum(#{p_sort_column}) as query_sort_column from #{v_pgss_sample_tb} group by queryid, query order by sum(#{p_sort_column}) desc"
rs.each do |row|
  begin
    v_sum_sorting += row["query_sort_column"].to_i
    break if v_sum_sorting > (v_pgss_sort_total * p_pgss_sample_percentage) #Filter to get just a top SQL percentage over the total time/call in PGSS 
    v_query = row["query"]
    v_fingerprint = PgQuery.fingerprint(v_query) ### Parse fingerprint of normalized query from PGSS
    log.debug "PGSS PARSED fingerprint %s => %s" % [row['queryid'],v_fingerprint]
    con.exec "insert into #{v_pgss_fgprnt_tb} values ('%s', '%s', '%s')" % [row['queryid'], v_query.gsub("'","''"), v_fingerprint] ### Insert fingerpting, queryid and query into the pgss_fingerprint_[read|write] table
    log.debug "#{v_pgss_fgprnt_tb} INSERTED %s => %s" % [row['queryid'],v_fingerprint]
    v_pgss_count += 1
  rescue StandardError => e
    log.error "ERROR during query processing PGSS, id %s query: %s" % [row['queryid'],v_query]
    log.error "ERROR message: %s" % [e.message]
    log.error "ERROR DETAILS: %s" % [e.backtrace.inspect]
    v_pgss_errors += 1
  end
end

### Process PGSA sampling
v_pgsa_count = 0
v_pgsa_errors = 0
rs = con.exec "select query from #{v_pgsa_sample_tb}"
rs.each do |row|
  begin
    v_query = row["query"]
    v_fingerprint = PgQuery.fingerprint(v_query)  ### Parse fingerprint of denormalized query from PGSA
    log.debug "PGSA PARSED fingerprint %s => %s" % [row['queryid'],v_fingerprint]
    con.exec "insert into #{v_pgsa_fgprnt_tb} values ('%s','%s')" % [v_query.gsub("'","''"), v_fingerprint] ### Insert fingerpting and query into the pgsa_fingerprint_[read|write] table
    log.debug "#{v_pgsa_fgprnt_tb} INSERTED %s" % [v_fingerprint]
    v_pgsa_count += 1 
  rescue StandardError => e
    log.error "ERROR during query processing PGSA, query: %s" % [v_query]
    log.error "ERROR message: %s" % [e.message]
    log.error "ERROR DETAILS: %s" % [e.backtrace.inspect]
    v_pgsa_errors += 1
  end
end

begin
  log.info "Analysing #{v_pgss_fgprnt_tb} and #{v_pgsa_fgprnt_tb} tables after load..."
  con.exec "analyze verbose #{v_pgss_fgprnt_tb}"
  con.exec "analyze verbose #{v_pgsa_fgprnt_tb}"
rescue StandardError => e
  log.error "ERROR message: %s" % [e.message]
  log.error "ERROR DETAILS: %s" % [e.backtrace.inspect]
end

log.info "TOTAL queries processed - PGSS: %i - PGSA: %i" % [v_pgss_count,v_pgsa_count]
log.info "TOTAL errors found - PGSS: %i - PGSA: %i" % [v_pgss_errors,v_pgsa_errors]