#!/usr/bin/env ruby

## This script parses the SQL statements from the "sample" tables to populate the "fingerprint" tables for both, PGSS (pg_stat_statements) amd PGSA (pg_stat_activity)

require 'pg_query'
require 'pg'
require 'csv'
require 'logger'

log = Logger.new(STDOUT)
log.level = ENV['DEBUG'].nil? ? Logger::INFO : Logger::DEBUG

p_instance_role = 'reader'
p_dbhost = 'localhost'
p_dbport = '5432'
p_dbname = 'wkload_jmeter'
p_dbuser = 'gitlab-superuser'
p_directory = './plans' 

@con = PG.connect(:host => p_dbhost, :dbname => p_dbname, :user => p_dbuser, :port => p_dbport)

v_pgss_fgprnt_tb = 'pgss_fingerprint_' + p_instance_role
v_pgsa_fgprnt_tb = 'pgsa_fingerprint_' + p_instance_role

## Use prepared statement for PGSA query as it will be used multiple times
@con.prepare("query_pgsa_fingerprint","select query from #{v_pgsa_fgprnt_tb} where fingerprint = $1")

v_pgss_count = 0
v_pgss_errors = 0
v_pgss_skipped = 0
rs = @con.exec("SELECT queryid, fingerprint, query from #{v_pgss_fgprnt_tb}")
rs.each do |row|
  begin
    v_pgss_query = row["query"]
    v_pgss_queryid = row["queryid"]
    v_fingerprint = row["fingerprint"]

    rs_pgsa = @con.exec_prepared("query_pgsa_fingerprint", [v_fingerprint])

    if rs_pgsa.count < 1 
      log.info "PARSING SKIP: (%s, %s) no matching PGSA statements" % [v_fingerprint,v_pgss_queryid]
      v_pgss_skipped += 1
      next
    end  
    v_pgss_count += 1

    v_pgss_query_arg_count = v_pgss_query.scan(/\$\d+/).size ## count number of $ arguments in the PGSS normalized statement

    v_argfilename = "queryid_#{v_pgss_queryid}_args.csv"
    log.info("PARSING START: PGSS -- QUERYID #{v_pgss_queryid} -- FILE = #{v_argfilename}")
    f_csv_arg_file = CSV.open(v_argfilename,"w") ## Write argument list into CSV arguments file named "queryid_#{v_pgss_queryid}_args.csv"

    v_pgsa_count = 0
    v_pgsa_errors = 0
    rs_pgsa.each do |row_pgsa|
      begin
        v_pgsa_count += 1
        v_pgsa_query = row_pgsa["query"]

        ## Tokenise v_pgsa_query with PgQuery.scan() to filter/select tokens who name ends with `CONST` (eg. SCONST and ICONST), for each token `t` get the t.start and t.end positions, then use t.start and t.end positions in the query string to get the values
        v_query_arguments = []
        v_query_arguments = PgQuery.scan(v_pgsa_query).select { |r| r.is_a?(PgQuery::ScanResult) }.flat_map { |sr| sr.tokens.select { |t| t.token.to_s.end_with?('CONST') || t.token.to_s.casecmp?('NULL_P') || t.token.to_s.casecmp?('TRUE_P') || t.token.to_s.casecmp?('FALSE_P') }.flat_map { |t| v_pgsa_query[t.start..(t.end-1)] } }

        if v_query_arguments.size == v_pgss_query_arg_count ## Check if arguments parsed from PGSA query match the number of $? arguments in PGSS
          f_csv_arg_file << v_query_arguments           
        else
          log.warn("PGSA arguments count #{v_query_arguments.size.to_s} don't match PGSS argument count #{v_pgss_query_arg_count.to_s}")
          log.debug("parsing query #{v_pgsa_query}")  
          log.debug("query arguments #{v_query_arguments.to_s}")
          v_pgsa_errors += 1
        end
      rescue StandardError => e
        log.error "Error during query processing PGSA, fingerprint %s query: %s" % [v_fingerprint,v_pgsa_query]
        log.error "Error message: %s" % [e.message]
        log.error "DETAILS: %s" % [e.backtrace.inspect]
        v_pgsa_errors += 1
      end
    end

    if v_pgsa_errors > 0
      v_pgss_errors += 1
      log.warn(%Q[Please debug "PGSA arguments count don't match" errors found #{v_pgsa_errors} for query: #{v_pgss_query}])
    end
    log.info "PARSING END: query (%s, %s) arguments processed: %i - errors found: %i" % [v_pgss_queryid,v_fingerprint,v_pgsa_count,v_pgsa_errors]
  rescue StandardError => e
    log.error("Error during query processing PGSS, id %s query: %s" % [v_pgss_queryid,v_pgss_query])
    log.error("Error message: %s" % [e.message])
    log.error("DETAILS: %s" % [e.backtrace.inspect])
  end
end

log.info "SUMMARY (DB Role: %s) -- PGSS statements: %i, PGSS skipped: %i, PGSS errored: %i" % [p_instance_role,v_pgss_count,v_pgss_skipped,v_pgss_errors]