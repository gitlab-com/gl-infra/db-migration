#!/bin/bash 

# Usage:
echo "Usage ./sample_pgsa.sh [period] [file-sufix]"

#Sample data out of pg_stat_activity for PERIOD every 1 second (default to 1 hour)
#
# Only capture statements that:
#   - query size is less than 4096 characters
#   - executed by the gitlab user in the gitlabhq_production database
#   - are in execution for less than 1 second

PERIOD=3600

if [[ $1 -gt 0 ]]
then
  PERIOD=$1
fi

FILE_SUFFIX=$(hostname)

if [[ $2 ]]
then
  FILE_SUFFIX+="-$2"
fi

CSV_FILE="/tmp/pgsa_sampling-$FILE_SUFFIX.csv"
ERR_LOG_FILE="/tmp/pgsa_sampling-$FILE_SUFFIX.error.log"


echo "Executing PGSA sampling for a period of $PERIOD seconds; output to $CSV_FILE"

for i in $(seq 1 $PERIOD)
do
  PGOPTIONS="-c statement_timeout=3s" \
  sudo /usr/local/bin/gitlab-psql -XAtc "
    copy(
    select clock_timestamp() as date_captured,
    regexp_replace(query, E'[ \\t\\n\\r]+', ' ', 'g') as query
    from pg_stat_activity
    where
      clock_timestamp() - query_start < '1 seconds'::interval
      and query <> ''
      and query <> ';'
      and length(query) < 4096
      and backend_type = 'client backend'
      and usename = 'gitlab'
      and datname = 'gitlabhq_production'
    ) to stdout csv delimiter ','" \
  2>>$ERR_LOG_FILE >>$CSV_FILE
  sleep 1
done

echo "Errors can be found at $ERR_LOG_FILE"

exit $?