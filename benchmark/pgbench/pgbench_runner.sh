#!/bin/bash

# 1. Reset PostgreSQL statistics before running tests
# 2. Execute pgbench tests with different concurrency levels (numbers of clients from 10 to 450)
# 3. Collect PostgreSQL artifacts

ARTIFACTS_DIR="$PWD"/ARTIFACTS
# create artifacts directory (if not exist)
mkdir -p "${ARTIFACTS_DIR}"

SOURCE_HOST=localhost
SOURCE_PORT=5432
SOURCE_USER=gitlab-superuser
SOURCE_DB=test
#SOURCE_SSH_HOST="$SOURCE_HOST" # used to collect some artifacts

#export PGPASSWORD="${SOURCE_USER_PASSWORD}"

function reset_pg_stat() {
  echo "Resetting Postgres statistics..."
  psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -tAXc "select pg_stat_reset(), pg_stat_reset_shared('archiver'), pg_stat_reset_shared('bgwriter')" &>/dev/null
  psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -tAXc "select pg_stat_statements_reset()" &>/dev/null
  psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -tAXc "select pg_stat_kcache_reset()" &>/dev/null
  psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -tAXc "select pg_wait_sampling_reset_profile()" &>/dev/null
}
function reset_pg_log() {
  LOG_DIR=$(psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -tAXc "show log_directory")
  LOG_FILE=$(ls -t $LOG_DIR | grep .log | head -n 1)
  LOG_FILE_CSV=$(ls -t $LOG_DIR | grep .csv | head -n 1)
  echo "Resetting Postgres log..."
  # clear current log file
  if [[ -n "$LOG_FILE" ]]; then echo '' > $LOG_DIR/$LOG_FILE &>/dev/null; fi
  if [[ -n "$LOG_FILE_CSV" ]]; then echo '' > $LOG_DIR/$LOG_FILE_CSV &>/dev/null; fi
  # delete other old log files
  find $LOG_DIR -type f ! -name $LOG_FILE ! -name $LOG_FILE_CSV -delete &>/dev/null
}
function collect_artifacts() {
  run_time=$(date +%Y-%m-%d-%H%M)
  mkdir -p "${ARTIFACTS_DIR}"/"$run_time"

  echo "Collect artifacts..."
  # pg_stat_*** views
  for pg_stat_view in $(psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -tAXc "select relname from pg_catalog.pg_class where relkind = 'view' and relname like 'pg_stat%'"); do
    psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -Xc "copy (select * from $pg_stat_view) to stdout with csv header delimiter ',';" > "$ARTIFACTS_DIR"/"$run_time"/"$pg_stat_view".csv
  done
  # pg_stat_kcache()
    psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -Xc "copy (select * from pg_stat_kcache()) to stdout with csv header delimiter ',';" > "$ARTIFACTS_DIR"/"$run_time"/pg_stat_kcache.csv
  # pg_wait_sampling_profile
  psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -Xc "copy (
    select event_type as wait_type, event as wait_event, sum(count) as of_events from pg_wait_sampling_profile
    group by event_type, event order by of_events desc
    ) to stdout with csv header delimiter ',';" > "$ARTIFACTS_DIR"/"$run_time"/pg_wait_sampling_profile.csv
  # pg_wait_sampling_profile_query
  psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -Xc "copy (
    select p.event_type as wait_type, p.event as wait_event, sum(p.count) as of_events, s.queryid, s.query
    from pg_wait_sampling_profile p join pg_stat_statements s on p.queryid = s.queryid
    group by s.queryid, s.query, p.event_type, p.event order by of_events desc
    ) to stdout with csv header delimiter ',';" > "$ARTIFACTS_DIR"/"$run_time"/pg_wait_sampling_profile_query.csv
  # pg_settings
  psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -Xc "copy (select * from pg_settings order by name) to stdout with csv header delimiter ',';" > "$ARTIFACTS_DIR"/"$run_time"/pg_settings_all.csv
  psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -Xc "select name, setting as current_setting, unit, boot_val as default, context from pg_settings where source <> 'default';" > "$ARTIFACTS_DIR"/"$run_time"/pg_settings_non_default.txt
  # pg_conf
  PG_CONF_FILE=$(psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -tAXc "show config_file")
  scp "$SOURCE_SSH_HOST":"$PG_CONF_FILE" "$ARTIFACTS_DIR"/"$run_time"/ &>/dev/null
  # pg_log
  for log_file in $(ls "$LOG_DIR"); do
    cat $LOG_DIR/$log_file | gzip -c > "$ARTIFACTS_DIR"/"$run_time"/$log_file.gz
  done
}

###################################################
  psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -Xc "select name, setting as current_setting, unit, boot_val as default, context from pg_settings where source <> 'default';" > "$ARTIFACTS_DIR"/"$run_time"/pg_settings_non_default.txt
  # pg_conf
  PG_CONF_FILE=$(psql -h "$SOURCE_HOST" -p "$SOURCE_PORT" -d "$SOURCE_DB" -U "$SOURCE_USER" -tAXc "show config_file")
  scp "$SOURCE_SSH_HOST":"$PG_CONF_FILE" "$ARTIFACTS_DIR"/"$run_time"/ &>/dev/null
  # pg_log
  for log_file in $(ls "$LOG_DIR"); do
    cat $LOG_DIR/$log_file | gzip -c > "$ARTIFACTS_DIR"/"$run_time"/$log_file.gz
  done
}

###################################################

# execute functions reset_pg_stat(), reset_pg_log()
reset_pg_stat
reset_pg_log

# execute tests
for jobs in 10 20 40 80 160 320 450; do
  # execute functions reset_pg_stat(), reset_pg_log()
  reset_pg_stat
  reset_pg_log
  pgbench -h "$SOURCE_HOST" -p "$SOURCE_PORT" -U "$SOURCE_USER" --client=$jobs --jobs=$jobs -P 10 -T 600 -n "$SOURCE_DB" 2>&1 \
    | ts \
    | tee -a "${ARTIFACTS_DIR}/pgbench_result-$(hostname -s).log"
  collect_artifacts
  sleep 900
done
# execute function collect_artifacts()
collect_artifacts

# archive a ARTIFACTS directory
tar -zcvf ARTIFACTS.tar.gz ARTIFACTS &>/dev/null

echo "Test is completed"
exit
