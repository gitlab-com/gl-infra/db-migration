#! /bin/bash

## Benchmarking GitLab database
## Usage: run-bench.sh [-h | --host] <database host> [-d | --database] <database> [-U --user] <database_user> [-p --port] <port>

# Transform long options to short ones
for arg in "$@"; do
  shift
  case "$arg" in
    "--host") set -- "$@" "-h" ;;
    "--database") set -- "$@" "-d" ;;
    "--user")   set -- "$@" "-U" ;;
    "--port")   set -- "$@" "-p" ;;
    "--env")   set -- "$@" "-e" ;;
    "--count")   set -- "$@" "-c" ;;
    "--plan")   set -- "$@" "-t" ;;
    "--results")   set -- "$@" "-r" ;;
    "--jobs") set -- "$@" "-j" ;;
    "--duration") set -- "$@" "-T" ;;

    *)        set -- "$@" "$arg"
  esac
done

OPTIND=1

# Some defaults
DATAENV=stg   ## data folder with values for each query
COUNT=100
PLAN=postgres-benchmark.jmx
RESULTS=bench-results.csv
PORT=5432
JOBS=1
DURATION=60

while getopts ":h:d:U:p:e:c:r:t:j:T:" opt
do
  case "$opt" in
    "h") HOST=$OPTARG ;;
    "d") DATABASE=${OPTARG} ;;
    "U") USER=${OPTARG} ;;
    "p") PORT=${OPTARG} ;;
    "e") DATAENV=${OPTARG} ;;
    "c") COUNT=${OPTARG} ;;
    "t") PLAN=${OPTARG} ;;
    "r") RESULTS=${OPTARG} ;;
    "j") JOBS=${OPTARG};;
    "T") DURATION=${OPTARG};;

  esac
done
shift $((OPTIND-1))

source .env


export DATAENVIRONMENT=$DATAENV
export DATABASEURL=jdbc:postgresql://${HOST}:${PORT}/${DATABASE}?autoReconnect=true
export DATABASEUSER=$USER
export HEAP="-Xms4096m -Xmx16384m"

../jmeter-5.5/bin/jmeter -n -t plans/${PLAN} -JLoopCount=${COUNT} -JNumJobs=${JOBS} -JTestDuration=${DURATION} -l results/${RESULTS}
exit $?