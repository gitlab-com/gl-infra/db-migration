### GitLab database benchmarking

This file explains how to install, configure, execute and evaluate PostgreSQL database using [jmeter](http://jmeter.apache.org/)

## Prerequisites

- java 8+

```sudo apt install openjdk-8-jre-headless```


## Download the repository
The repo contains the plan for test execution, and ships the last version of jmeter.

```sh
git clone https://gitlab.com/gitlab-com/gl-infra/db-migration.git
```

## Define Postgres password
```bash
cd ./benchmark
cp bin/.env.example bin/.env
vi bin/.env # edit the file and define Postgres password
```

## Execute the test
There a script for bench execution:
```sh
cd $REPOPATH/benchmark
./bin/run-bench.sh -h <database host> -c <count> -U <user> -p <port> -r <result-file> -j <parallel-jobs> -T <benchmark-duration> -t <plan>
```


## Get the results
Results are located by default at `results/bench-results.csv` (or whatever you have specify with the `-r` flag)
Currently, the header of the result file contains the following colunms:
- timestamp
- eplapsed (in millliseconds)
- label
- Response code (200, 404, etc)
- Response message 
- threadName
- dataType (e.g. text)
- success (boolean)
- failureMessage (if any)

And others. This file can be analyzed using other jmeter plugins, like the [Aggregate Report](https://jmeter.apache.org/usermanual/component_reference.html#Aggregate_Report)



