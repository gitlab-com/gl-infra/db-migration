---
- name: "Switchover the traffic to the new Patroni leader"
  hosts: 'patroni_cluster:pgbouncer:pgbouncer_sidekiq'
  gather_facts: true
  become: true
  become_user: "{{ os_pg_user }}"
  any_errors_fatal: true
  vars:
    non_interactive: false  # if 'true', no confirmation will be requested before performing the Switchover.
  tasks:
    - name: "[Prepare] Stop Chef client to prevent unexpected changes during the switchover"
      become: true
      become_user: root
      command: chef-client-disable "Switchover the traffic to the new Patroni leader"
      when:
        - inventory_hostname in groups['patroni_cluster']
      tags: always

    - name: '[Prepare] Get Patroni Cluster Leader Node'
      uri:
        url: http://{{ inventory_hostname }}:{{ patroni_restapi_port }}/leader
        status_code: 200
      register: patroni_leader_result
      changed_when: false
      failed_when: false
      when:
        - inventory_hostname in groups['patroni_cluster']
      tags: always

    - name: '[Prepare] Set the variable: patroni_cluster_leader: true'
      set_fact:
        patroni_cluster_leader: true
      when:
        - inventory_hostname in groups['patroni_cluster']
        - patroni_leader_result.status == 200
      tags: always

    - name: '[Prepare] Set the variable: patroni_cluster_leader: false'
      set_fact:
        patroni_cluster_leader: false
      when:
        - inventory_hostname in groups['patroni_cluster']
        - patroni_leader_result.status != 200
      tags: always

    - name: "[Prepare] Print Patroni Cluster Leader before the switchover"
      debug:
        msg: "Patroni Cluster Leader: {{ inventory_hostname }}"
      when:
        - inventory_hostname in groups['patroni_cluster']
        - patroni_cluster_leader
      tags: always

    - name: "[Prepare] Get Patroni Cluster info"
      uri:
        url: "http://{{ inventory_hostname }}:{{ patroni_restapi_port }}/cluster"
        method: GET
        return_content: true
        headers:
          Content-Type: "application/json"
      register: patroni_cluster_info
      changed_when: false
      when:
        - inventory_hostname in groups['patroni_cluster']
        - patroni_cluster_leader
      tags: always

    - name: "[Prepare] Get Patroni Cluster Replica Node (without 'nofailover: true' tag)"
      shell: >
        set -o pipefail;
        curl -s http://{{ inventory_hostname }}:{{ patroni_restapi_port }}/cluster
        | jq -r '[.members[]
        | select(.role == "replica" and (.tags.nofailover // false) != true) | .name]
        | map("\(. | @sh)")
        | join(",")'
      args:
        executable: /bin/bash
      register: active_replicas
      changed_when: false
      when:
        - inventory_hostname in groups['patroni_cluster']
        - patroni_cluster_leader
      tags: always

    # I'm going to keep the following Debug message to print active replicas without 'nofailover: true' tags
    - name: "Debug - Print active replicas without 'nofailover: true' tags"
      debug:
        msg: "Active replicas without 'nofailover: true' tags: {{ active_replicas.stdout }}"
      when:
        - inventory_hostname in groups['patroni_cluster']
        - patroni_cluster_leader
      tags: always

    # Perform Pre-Checks before switching traffic
    ###################################################################
    #
    - name: '[Pre-Check] Add host to group "patroni_primary" (in-memory inventory)'
      add_host:
        name: "{{ item }}"
        groups: patroni_primary
      when: hostvars[item]['patroni_cluster_leader']
      loop: "{{ groups['patroni_cluster'] }}"
      changed_when: false
      tags: always

    # I've commented out the following Block. I'd like to keep it in the playbook. If I need it, I'll use it but please do not remove it for now.
    # - block:
    #     - name: '[Pre-Check]  Set the variable: allowed_max_replication_lag_bytes'
    #       set_fact:
    #         allowed_max_replication_lag_bytes: "{{ force_mode_max_replication_lag_bytes if force_mode | bool else max_replication_lag_bytes }}"
    #       when:
    #         - inventory_hostname in groups['patroni_cluster']
    #       tags: always

    #     - name: "[Pre-Check] Make sure there is no high streaming replication lag (more than {{ allowed_max_replication_lag_bytes | int | human_readable }})"
    #       command: >-
    #         gitlab-psql -tAXc "select pg_wal_lsn_diff(pg_current_wal_lsn(), replay_lsn) max_lag_bytes
    #         from pg_stat_replication
    #         order by max_lag_bytes
    #         desc limit 1"
    #       register: max_streaming_rep_lag_bytes
    #       changed_when: false
    #       failed_when: false
    #       until: max_streaming_rep_lag_bytes.stdout|int < allowed_max_replication_lag_bytes|int
    #       retries: 90  # 3 minute
    #       delay: 2

    #     - block:  # Stop if the replication lag is high
    #         - name: "Print streaming replication lag"
    #           debug:
    #             msg: "Current streaming replication lag:
    #               {{ max_streaming_rep_lag_bytes.stdout | int | human_readable }}"

    #         - name: "Pre-Check error. Please try again later"
    #           fail:
    #             msg: High logical replication lag, please try again later.
    #       when:
    #         - max_streaming_rep_lag_bytes.stdout is defined
    #         - max_streaming_rep_lag_bytes.stdout|int >= allowed_max_replication_lag_bytes|int
    #   when:
    #     - inventory_hostname in groups['patroni_cluster']
    #     - patroni_cluster_leader
    #   tags:
    #     - leader

    - name: '[Pre-Check] Make sure there are no long-running transactions (more than {{ max_transaction_sec }} seconds)'
      command: >-
        gitlab-psql -tAXc "select pid, usename, client_addr, clock_timestamp() - xact_start as xact_age,
          state, wait_event_type ||':'|| wait_event as wait_events,
          left(regexp_replace(query, E'[ \\t\\n\\r]+', ' ', 'g'),100) as query
          from pg_stat_activity
          where clock_timestamp() - xact_start > '{{ max_transaction_sec }} seconds'::interval
          and backend_type = 'client backend' and pid <> pg_backend_pid()
          order by xact_age desc limit 10"
      register: pg_long_transactions
      changed_when: false
      failed_when: false
      until: pg_long_transactions.stdout | length < 1
      retries: 150  # max wait time: 5 minutes
      delay: 2
      when:
        - inventory_hostname in groups['patroni_cluster']
        - patroni_cluster_leader
        - not force_mode | bool  # skip if force_mode is true
      tags:
        - leader

    # Stop if long-running transactions are detected (if force_mode: false)
    - block:
        - name: "Print long-running (>{{ max_transaction_sec }}s) transactions"
          debug:
            msg: "{{ pg_long_transactions.stdout_lines }}"

        - name: "Pre-Check error. Please try again later"
          fail:
            msg: long-running transactions detected (more than {{ max_transaction_sec }} seconds), please try again later.
      when:
        - inventory_hostname in groups['patroni_cluster']
        - patroni_cluster_leader
        - pg_long_transactions.stdout is defined
        - pg_long_transactions.stdout | length > 0
      tags:
        - leader

    # Switchover Patroni Leader traffic
    # Request confirmation (if 'non_interactive' is 'false')
    - block:
        - name: "[Prepare] Patroni switchover - Perform patroni switchover to send traffic to the new patroni leader node?"
          pause:
            prompt: "Are you sure you want to perform patroni switchover? [y/N]"
          delegate_to: "{{ groups['patroni_cluster'][0] }}"
          register: prompt_result

        - name: "[Prepare] Set the variable: ask_result_leader"
          set_fact:
            ask_result_leader: "{{ hostvars[groups['patroni_cluster'][0]]['prompt_result']['user_input'] }}"
      when:
        - not non_interactive | bool
      tags:
        - leader
        - consul-config

    - name: "[Pre-Check] Execute CHECKPOINT on Patroni Cluster just before the patroni switchover (duration: ~1-3 min)"
      command: gitlab-psql -tAXc "CHECKPOINT"
      async: 3600  # run the command asynchronously
      poll: 0
      register: checkpoint_result
      when:
        - inventory_hostname in groups['patroni_cluster']

    - name: "[Pre-Check] Wait for the CHECKPOINT to complete"
      async_status:
        jid: "{{ checkpoint_result.ansible_job_id }}"
      register: checkpoint_job_result
      until: checkpoint_job_result.finished
      retries: 360
      delay: 10
      when:
        - inventory_hostname in groups['patroni_cluster']

      # We will wait here until we find a right opprtunity (streaming replication lag to 0 bytes) before we even perform PAUSE on all the PGBouncers.
      # It ensures our patroni cluster's and streaming replication are healthy.
      # With this check in-place, we ar more likely to get streaming replication lag to zero after we PAUSE PgBouncers.
    - block:
        - name: "[Pre-Switchover] Wait until the streaming replication lag is 0 bytes"
          command: >-
            gitlab-psql -tAXc "select pg_wal_lsn_diff(pg_current_wal_lsn(), replay_lsn) min_lag_bytes
            from pg_stat_replication
            where application_name in ({{ active_replicas.stdout }})
            order by min_lag_bytes
            asc limit 1"
          register: min_streaming_rep_lag_bytes
          until: min_streaming_rep_lag_bytes.stdout|int == 0
          retries: 300  # max wait time: 10 minutes
          delay: 2
          changed_when: false
          ignore_errors: true  # show the error and continue the playbook execution
          when:
            - inventory_hostname in groups['patroni_cluster']
            - patroni_cluster_leader
            - (ask_result_leader is defined and ask_result_leader == "y" or non_interactive | bool)
            - not force_mode | bool  # skip if force_mode is true
          tags:
            - leader

        - block:  # Stop if the replication lag is high
            - name: "[Prepare] Print streaming replication lag"
              debug:
                msg: "Current streaming replication lag:
                  {{ min_streaming_rep_lag_bytes.stdout | int | human_readable }}"

            - name: "[Prepare] Pre-Check error. Please try again later"
              fail:
                msg: High logical replication lag, please try again later.
          when:
            - min_streaming_rep_lag_bytes.stdout is defined
            - min_streaming_rep_lag_bytes.stdout|int > 0|int
      when:
        - inventory_hostname in groups['patroni_cluster']
        - patroni_cluster_leader
      tags:
        - leader

    # Perform PAUSE in parallel on all pgbouncers servers (if force_mode: false)
    #
    # This script performs the following actions:
    # 1. Waits for active database queries on the pg_server to complete.
    # 2. If there are no active queries, sends a PAUSE command to each pgbouncer server in the pgb_servers list (in parallel to all servers).
    # 3. If all pgbouncer servers are successfully paused, the script exits.
    # 4. If active queries do not complete within 30 seconds, the script forcibly terminates slow active queries using pg_slow_active_terminate_query.
    # 5. If after that it is still not possible to pause the pgbouncer servers within 60 seconds from the start of the script, the script exits with an error.
    #
    # The script uses the pause_results array to track the results of executing the PAUSE command on each pgbouncer server.
    # The timeout 2s command is used to set a timeout for the execution of the pgb-console command.
    # If the execution of the pgb-console command does not finish within 2 seconds, the timeout command will interrupt the execution of pgb-console and execute the pgb_resume_query command to remove the pause and ensure atomicity.
    #
    # Finally, the script checks whether all servers have been successfully paused by comparing the number of successful PAUSE executions to the total number of pgbouncer servers.
    - name: "[Switchover] PAUSE PgBouncer pools"
      become: true
      become_user: "{{ lookup('env', 'USER') }}"
      shell: |
        pgb_servers="{{ (groups['pgbouncer'] + groups['pgbouncer_sidekiq']) | join('\n') }}"
        pgb_count=$(echo -e "$pgb_servers" | wc -l)
        pg_server="{{ groups['patroni_primary'][0] }}"
        pg_slow_active_count_query="select count(*) from pg_stat_activity where pid <> pg_backend_pid() and backend_type = 'client backend' and state <> 'idle' and query_start < clock_timestamp() - interval '{{ pg_slow_active_query_treshold_to_analyse }} ms'"
        pg_slow_active_terminate_query="select pg_terminate_backend(pid) from pg_stat_activity where pid <> pg_backend_pid() and backend_type = 'client backend' and state <> 'idle' and query_start < clock_timestamp() - interval '{{ pg_slow_active_query_treshold_to_terminate }} ms'"

        start_time=$(date +%s)
        while true; do
          current_time=$(date +%s)
          # initialize pgb_paused_count to 0 (we assume that all pgbouncers are not paused)
          pgb_paused_count=0

          # wait for the active queries to complete on pg_server
          pg_slow_active_count=$(ssh -o StrictHostKeyChecking=no "$pg_server" "gitlab-psql -tAXc \"$pg_slow_active_count_query\"")
          echo "$(date): pg_slow_active_count: $pg_slow_active_count"

          if [[ "$pg_slow_active_count" == 0 ]]; then
            # pause pgbouncer on all pgb_servers. We send via ssh to all pgbouncers in parallel and collect results from all (maximum wait time 2 seconds)
            IFS=$'\n' pause_results=($(echo -e "$pgb_servers" | xargs -I {} -P "$pgb_count" -n 1 ssh -o StrictHostKeyChecking=no {} "timeout {{ pgbouncer_pool_pause_timeout }}s sudo pgb-console -c \"PAUSE\" 2>&1 || true"))
            echo "${pause_results[*]}"
            # analyze the pause_results array to count the number of paused pgbouncers
            pgb_paused_count=$(echo "${pause_results[*]}" | grep -o -e "PAUSE" -e "already suspended/paused" | wc -l)
            echo "$(date): pgb_count: $pgb_count, pgb_paused: $pgb_paused_count"
          fi

          # make sure that the pause is performed on all pgbouncer servers, to ensure atomicity
          if [[ "$pgb_paused_count" -eq "$pgb_count" ]]; then
            break # pause is performed on all pgb_servers, exit from the loop
          elif [[ "$pgb_paused_count" -gt 0 && "$pgb_paused_count" -ne "$pgb_count" ]]; then
            # pause is not performed on all pgb_servers, perform resume (we do not use timeout because we mast to resume all pgbouncers)
            IFS=$'\n' resume_results=($(echo -e "$pgb_servers" | xargs -I {} -P "$pgb_count" -n 1 ssh -o StrictHostKeyChecking=no {} "sudo pgb-console -c \"RESUME\" 2>&1 || true"))
            echo "${resume_results[*]}"
          fi

          # after 30 seconds of waiting, terminate active sessions and try pausing again
          if (( current_time - start_time >= {{ pgbouncer_pool_pause_terminate_after }} )); then
            pg_terminate_result=$(ssh -o StrictHostKeyChecking=no "$pg_server" "gitlab-psql -tAXc \"$pg_slow_active_terminate_query\"")
            echo "$(date): terminate active queries"
          fi

          # if it was not possible to pause for 60 seconds, exit with an error
          if (( current_time - start_time >= {{ pgbouncer_pool_pause_give_up_after }} )); then
            echo "$(date): it was not possible to pause (exit by timeout)"
            exit 1
          fi
        done > /tmp/pgbouncer_pool_pause_{{ ansible_date_time.date }}.log
      args:
        executable: /bin/bash
      register: pause_pgbouncer_pool_result
      delegate_to: localhost
      run_once: true
      when:
        - (ask_result_leader is defined and ask_result_leader == "y" or non_interactive | bool)
        - pgbouncer_pool_pause | bool
        - not force_mode | bool  # skip if force_mode is true
      tags:
        - leader

    # # Attention!
    # # During the pause, it is necessary to do as few tasks as possible,
    # # since the time during which the pause depends on it, and therefore the latency of queries increases.

    # (if force_mode: true) Terminate active sessions
    # To ensure that there are no writing transactions that can make changes to the database
    # Note: this task is not necessary if the pool is paused
    - name: "[Switchover] Terminate active sessions on the patroni cluster leader"
      command: >-
        gitlab-psql -tAXc "select pg_terminate_backend(pid)
          from pg_stat_activity
          where pid <> pg_backend_pid()
          and backend_type = 'client backend'
          and state <> 'idle'"
      ignore_errors: true
      when:
        - inventory_hostname in groups['patroni_cluster']
        - patroni_cluster_leader
        - (ask_result_leader is defined and ask_result_leader == "y" or non_interactive | bool)
        - force_mode | bool
      tags:
        - leader

    # To ensure we will have ZERO DATA LOSS during the Patroni Leader Switchover, we will wait here to until we have a streaming replication lag to zero bytes.
    # We do not want to wait here for to long too as all our PgBouncders are already PAUSED at this stage. For now, I've set to wait here for 60 Seconds but we can fine-tune it, as necessary.
    # If we do not get streaming replication lag to zero bytes, we will NOT PERFORM the SWTICHOVER. We will troubleshoot the issue and try again.
    - block:
        - name: "[Switchover] Wait until the streaming replication lag is 0 bytes"
          command: >-
            gitlab-psql -tAXc "select pg_wal_lsn_diff(pg_current_wal_lsn(), replay_lsn) min_lag_bytes
            from pg_stat_replication
            where application_name in ({{ active_replicas.stdout }})
            order by min_lag_bytes
            asc limit 1"
          register: min_streaming_rep_lag_bytes
          until: min_streaming_rep_lag_bytes.stdout|int == 0
          retries: 60      # Wait for maximum 60 seconds
          delay: 1
          changed_when: false
          ignore_errors: true  # ignore the errors to RESUME anyway.
          when:
            - inventory_hostname in groups['patroni_cluster']
            - patroni_cluster_leader
            - (ask_result_leader is defined and ask_result_leader == "y" or non_interactive | bool)
            - not force_mode | bool  # skip if force_mode is true
          tags:
            - leader

        - name: "[Switchover] Perform Patroni Switchover"
          uri:
            url: http://{{ inventory_hostname }}:{{ patroni_restapi_port }}/switchover
            method: POST
            body: '{"leader":"{{ inventory_hostname }}"}'
            body_format: json
          register: patroni_switchover_result
          until: patroni_switchover_result.status == 200
          retries: 30  # max wait time is 30 seconds
          delay: 1
          ignore_errors: true  # ignore the errors to RESUME anyway.
          when:
            - inventory_hostname in groups['patroni_cluster']
            - patroni_cluster_leader
            - (ask_result_leader is defined and ask_result_leader == "y" or non_interactive | bool)
            - min_streaming_rep_lag_bytes.stdout|int == 0
            - not switchover_to_hostname is defined
          tags:
            - patroni-switchover
            - leader

        # - name: "[Switchover] Print Patroni Switchover results"
        #   debug:
        #     msg: "Print Patroni Switchover results = {{ patroni_switchover_result }}"
        #   ignore_errors: true  # ignore the errors to RESUME anyway.
        #   delegate_to: localhost
        #   run_once: true
        #   # when:
        #   #   - inventory_hostname in groups['patroni_cluster']
        #   #   - patroni_cluster_leader
        #   #   - switchover_to_hostname is defined
        #   tags: always

        - name: "[Switchover] Print Patroni Switchover command before the switchover provided command-line options to Ansible playbooks using the -e flag followed by the option name and value is defined for switchover_to_hostname variable"
          debug:
            msg: "Patroni Switchover Command: gitlab-patronictl switchover --master {{ inventory_hostname }} --candidate  {{ switchover_to_hostname }} --scheduled now --force"
          ignore_errors: true  # ignore the errors to RESUME anyway.
          when:
            - inventory_hostname in groups['patroni_cluster']
            - patroni_cluster_leader
            - switchover_to_hostname is defined
          tags: always

        - name: "[Switchover] Remove trailing .internal from {{switchover_to_hostname}}"
          set_fact:
            switchover_to_hostname_trimmed: "{{ switchover_to_hostname | regex_replace('\\.internal$', '') }}"
          delegate_to: localhost
          run_once: true
          when:
            - switchover_to_hostname is defined

        - name: "[Switchover] Wait until the streaming replication lag is 0 bytes for patroni node {{ switchover_to_hostname_trimmed }}"
          command: >-
            gitlab-psql -tAXc "select pg_wal_lsn_diff(pg_current_wal_lsn(), replay_lsn) min_lag_bytes
            from pg_stat_replication
            where application_name = '{{ switchover_to_hostname_trimmed }}'
            "
          register: min_streaming_rep_lag_bytes
          until: min_streaming_rep_lag_bytes.stdout|int == 0
          retries: 60      # Wait for maximum 60 seconds
          delay: 1
          changed_when: false
          ignore_errors: true  # ignore the errors to RESUME anyway.
          when:
            - inventory_hostname in groups['patroni_cluster']
            - patroni_cluster_leader
            - (ask_result_leader is defined and ask_result_leader == "y" or non_interactive | bool)
            - not force_mode | bool  # skip if force_mode is true
            - switchover_to_hostname is defined
          tags:
            - leader

        - name: "[Switchover] Perform Patroni Switchover"
          shell: "gitlab-patronictl switchover --master {{ inventory_hostname }} --candidate {{ switchover_to_hostname }} --scheduled now --force"
          become: true
          become_user: root
          ignore_errors: true  # ignore the errors to RESUME anyway.
          when:
            - inventory_hostname in groups['patroni_cluster']
            - patroni_cluster_leader
            - (ask_result_leader is defined and ask_result_leader == "y" or non_interactive | bool)
            - min_streaming_rep_lag_bytes.stdout|int == 0
            - switchover_to_hostname is defined
          tags:
            - patroni-switchover
            - leader

        # - name: "[Switchover] Print Patroni Switchover results"
        #   debug:
        #     msg: "Print Patroni Switchover results = {{ patroni_switchover_result }}"
        #   ignore_errors: true  # ignore the errors to RESUME anyway.
        #   delegate_to: localhost
        #   run_once: true
        #   when:
        #     - switchover_to_hostname is defined
        #   tags: always

        - name: "[Switchover] **PLEASE NOTE** - We have NOT PERFORMED SWITCHOVER because streaming replication lag did not reach our goal of the 0 bytes. Please tray gain."
          debug:
            msg: "Please try again. We have NOT PERFORMED SWITCHOVER because because the current streaming replication lag:
              {{ min_streaming_rep_lag_bytes.stdout | int | human_readable }}"
          when:
            - min_streaming_rep_lag_bytes.stdout is defined
            - min_streaming_rep_lag_bytes.stdout|int > 0

    # Perform RESUME in parallel on all pgbouncers servers (if force_mode: false)
    - name: "[Switchover] RESUME PgBouncer pools"
      become: true
      become_user: "{{ lookup('env', 'USER') }}"
      shell: |
        pgb_servers="{{ (groups['pgbouncer'] + groups['pgbouncer_sidekiq']) | join('\n') }}"
        pgb_count=$(echo -e "$pgb_servers" | wc -l)
        echo -e "$pgb_servers" | xargs -I {} -P "$pgb_count" -n 1 ssh -o StrictHostKeyChecking=no {} "sudo pgb-console -c \"RESUME\" 2>&1 || true"
      args:
        executable: /bin/bash
      delegate_to: localhost
      run_once: true
      when:
        - (ask_result_leader is defined and ask_result_leader == "y" or non_interactive | bool)
        - pgbouncer_pool_pause | bool
        - not force_mode | bool  # skip if force_mode is true
      tags:
        - leader

    # Perform tasks after switchover
    ####################################################################

    - name: "[Post-Switchover] Enable Chef client"
      become: true
      become_user: root
      shell: chef-client-enable
      when:
        - inventory_hostname in groups['patroni_cluster']
      tags: always

    - name: "[Post-Switchover] Wait until Patroni is healthy and reprint the Patroni cluster status"
      uri:
        url: http://{{ inventory_hostname }}:{{ patroni_restapi_port }}/health
        status_code: 200
      register: patroni_health_result
      until: patroni_health_result.status == 200
      retries: 30      # 30 seconds
      delay: 1
      changed_when: false
      failed_when: false
      when:
        - inventory_hostname in groups['patroni_cluster']

    - name: "[Post-Switchover] Make sure all the replicas including the former patroni leader is ready"
      command: >-
        gitlab-psql -tAXc "select 1"
      register: query_result
      until: query_result.rc == 0  # Wait until query succeeds (return code 0)
      retries: 60  # 60 seconds
      delay: 1
      changed_when: false
      ignore_errors: true  # show the error and continue the playbook execution
      when: inventory_hostname in groups['patroni_cluster']
      tags: always

    - name: "[Post-Switchover] Get the Patroni cluster state"
      run_once: true
      command: gitlab-patronictl list
      register: patronictl_result
      changed_when: false
      when: inventory_hostname in groups['patroni_cluster']
      tags: always

    - name: "[Post-Switchover] List the Patroni cluster members. **NOTE for DBREs** - FYI. The former patroni leader will most likely show status as `stopped` and Lag in MB as `unknown`. PLEASE WAIT for a few minutes and check it again to confirm the Status is `running` "
      run_once: true
      debug:
        msg: "{{ patronictl_result.stdout_lines }}"
      when:
        - patronictl_result.stdout_lines is defined
        - inventory_hostname in groups['patroni_cluster']
      tags: always

    - name: "[Post-Switchover] Switchover completed"
      run_once: true
      debug:
        msg: "The switchover has been completed."
      when: inventory_hostname in groups['patroni_cluster']

...
