# The DBRE Toolkit


DBRE Toolkit is a comprehensive set of Ansible playbooks, tools, scripts, and resources designed to streamline and simplify various database and cluster maintenance tasks. It is an indispensable resource for DBREs, providing them with the necessary tools to efficiently manage and maintain patroni clusters and database systems.


# List of playbooks:

## 1 - Switchover Patroni Leader

- `switchover_patroni_leader.yml` 
  - This playbook is designed to perform Patroni Leader Switchover. It is designed to switchover site traffic to a new patroni leader with **Zero-Downtime** using PgBouncer's PAUSE/RESUME option.
  - With the default value of `force_mode: false`, the Patroni Leader Switchover playbook will perform the swtichover with **Zero-Data-Loss** too. After PAUSEing the PgBouncers, the playbook will wait until the streaming replication lag becomes zero bytes on at least one of the patroni switchover candidate replicas not having `nofailover: true` patroni tag. Once replication lag becomes zero bytes, it will perform the switchover and perform RESUME on the PgBouncers.


## Requirements

1. Make sure your ssh-agent has your identity loaded (`ssh-add -L`), since we use agent forwarding by default (configured in `ansible.cfg`).

## Before performing Patroni Leader Switchover 
- Create inventory file for your environment

## How to run

### 1. Perform Patroni Switchover (Switchover traffic from the current Patroni Leader to a Patroni Replica):
- Add inventory file for your environment - e.g. `db-benchmarking-mock.yml`

e.g: `./db-migration/dbre-toolkit/db-benchmarking-mock.yml`

<details><summary>Click here to expand...</summary><p>

```yaml
all:
  vars:
    cluster_environment: db-benchmarking
    cluster_suffix: main
  children:
    patroni_cluster:
      hosts:
        patroni-mock-v14-01-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        patroni-mock-v14-02-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        patroni-mock-v14-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        patroni-mock-v14-04-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
    pgbouncer:
      hosts:
        pgbouncer-main-01-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        pgbouncer-main-02-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        pgbouncer-main-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
    pgbouncer_sidekiq:
      hosts:
```

In the example above, hosts in groups are indicated: 

- `patroni_cluster` - Patroni Cluster hosts. It is a list of hosts of the patroni cluster. The Switchover playbook will Switchover Patroni Leader to one of the patroni replicas.
- `pgbouncer`, `pgbouncer_sidekiq` - The dedicated PgBouncer hosts that proxy traffic to Primary (using the consul service name, e.q. `master.patroni.service.consul`). More details [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/pgbouncer/patroni-consul-postgres-pgbouncer-interactions.md#path-from-rails-app-to-primary-db).

</p></details>


- Run `switchover_patroni_leader.yml`  Ansible playbook

`db-benchmarking-mock` cluster:


```
cd ./db-migration/dbre-toolkit
ansible-playbook -i inventory/db-benchmarking-mock.yml   switchover_patroni_leader.yml 2>&1 | ts | tee -a ansible_switchover_patroni_leader_db-benchmarking-mock_$(date +%Y%m%d).log
```

## 2 - Switchover Patroni Leader to a Specific Replica

- `switchover_patroni_leader_specific_replica.yml` 
  - This playbook is designed to perform Patroni Leader Switchover to a Specific Replica. It is designed to switchover site traffic to a specific patroni leader with **Zero-Downtime** using PgBouncer's PAUSE/RESUME option.
  - With the default value of `force_mode: false`, the Patroni Leader Switchover playbook will perform the swtichover with **Zero-Data-Loss** too. After PAUSEing the PgBouncers, the playbook will wait until the streaming replication lag becomes zero bytes for the to Specific Replica mentioned on the command-line. Once replication lag becomes zero bytes, it will perform the switchover and perform RESUME on the PgBouncers.


## Requirements

1. Make sure your ssh-agent has your identity loaded (`ssh-add -L`), since we use agent forwarding by default (configured in `ansible.cfg`).

## Before performing Patroni Leader Switchover 
- Create inventory file for your environment

## How to run

### 1. Perform Patroni Switchover (Switchover traffic from the current Patroni Leader to a Patroni Replica):
- Add inventory file for your environment - e.g. `db-benchmarking-mock.yml`

e.g: `./db-migration/dbre-toolkit/db-benchmarking-mock.yml`

<details><summary>Click here to expand...</summary><p>

```yaml
all:
  vars:
    cluster_environment: db-benchmarking
    cluster_suffix: main
  children:
    patroni_cluster:
      hosts:
        patroni-mock-v14-01-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        patroni-mock-v14-02-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        patroni-mock-v14-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        patroni-mock-v14-04-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
    pgbouncer:
      hosts:
        pgbouncer-main-01-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        pgbouncer-main-02-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
        pgbouncer-main-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal:
    pgbouncer_sidekiq:
      hosts:
```

In the example above, hosts in groups are indicated: 

- `patroni_cluster` - Patroni Cluster hosts. It is a list of hosts of the patroni cluster. The Switchover playbook will Switchover Patroni Leader to one of the patroni replicas.
- `pgbouncer`, `pgbouncer_sidekiq` - The dedicated PgBouncer hosts that proxy traffic to Primary (using the consul service name, e.q. `master.patroni.service.consul`). More details [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/pgbouncer/patroni-consul-postgres-pgbouncer-interactions.md#path-from-rails-app-to-primary-db).

</p></details>


- Run `switchover_patroni_leader_specific_replica.yml`  Ansible playbook

`db-benchmarking-mock` cluster:


```
cd ./db-migration/dbre-toolkit
ansible-playbook -i inventory/db-benchmarking-mock.yml   switchover_patroni_leader_specific_replica.yml -e "switchover_to_hostname=patroni-mock-v14-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal" 2>&1 | ts | tee -a ansible_switchover_patroni_leader_db-benchmarking-mock_$(date +%Y%m%d).log
```
